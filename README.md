# IoTSystem

## IoT4Home Instructions

- To run our system follow these steps:
1. You will need docker installed on your system, if you have it position yourself in the setup folder and just run docker-compose up it will create a bucket with all necessary services
2. Now open influxDB in your browser `http://localhost:8086` and set up your bucket, it is recommended to name your default bucket iot, make sure to remember to write down the influx-db token, organization details, and credentials you provided, you will need them later
3. Now finish setting up influx-db by creating additional buckets required for downsampling those are `energy_consumption_downsampled_1h`, `energy_consumption_downsampled_1m`, `heartbeat_downsampled`, `readings_downsampled_1h`, `readings_downsampled_1m` with retention policies of `30days`, `1day`, `30days`, `30days`, `1day` respectively. When this is done, go to the tasks tab and import tasks from setup>tasks folder
4. Now set up telegraf based on your influx-db credentials open setup>telegraf.conf and imput them in required fields. Stop your container for a second and docker-compose up it one more time for changes to take effect
5. Now one more thing needs to be done for preparation and you will be able to start our whole system. Go to both the back and websockets folder where our two Spring servers lie and in resources>application.properties change information based on your needs, providing your SendGrid token and informations from steps 1-4. If you have done everything correctly up to this point you are ready to rock-n-roll
6. Start the main back-end application by positioning yourself inside the back folder and running mvn spring-boot:run, do the same just for the websocket server
7. Now go to the front folder and run `npm i` to install the required node-modules, then you will be able to run `npm run build` to build our front-end for production and when the build is done run `npm run start` to start the server for or NextJS application. Nginx will handle everything else for you, serving static content and routing requests to the appropriate servers
8. Now it is time to start our simulators, for this you will need Python 3.9 installed on your system as well as go 1.21.4. We recommend creating venv for Python simulators and installing necessary dependencies from requirement.txt. Once you have everything set you can run any Python simulator by running the command `python path_to_file.py {device_id} {property_id}`, where `device_id` and `property_id` are optional and if not provided simulator will enter initial pairing mode. Go to `localhost:3000/devices/add` to complete the setup process. Golang simulators are run with the command `go run path_to_folder_of_simulator {device_id} {property_id}` same for this simulator if command line arguments are not provided simulator will enter initial paring mode.
9. Script for database initialization can be found in simulators>LDA>test_scenarios.py run it with `python test_scenarios.py` and change it for your needs.
10. You are all set! Continue reading our instructions to learn the basics of using our IoT infrastructure.

## Technologies & infrastructure
- Backend: Java/Spring
- Frontend: React/NextJs
- Simulators: Python/Golang
- Docker

## Team
- Tamara Ilić SV45/2020
- Aleksandar Mishutkin SV81/2022
- Uroš Poček SV57/2020

## Docker Compose
- To run do `docker-compose up` in the `setup` directory.
- Go to `http://localhost:8086` influxdb and register typing `username=admin`, `password=adminadmin`, `organization=iotream2`, `bucket=iot`. On the next screen, you will get an API token for influxdb that you need to pass in `telegraf.conf` token = "add_token_here" & also past it in application.properties of both spring boots back and websocket.

`influx auth create -d mydb -o myorg -p 'read,write'`

- Rerun the docker container
- Telegraf topics are defined in `telegraf.conf` (currently topics = [ "measurements" ])
- MQTT `username = "devuser"` and `password = "changeme"` need to be set up for simulation to run

## Clean up
- To clear DB: stop container `iotsystem`, remove the container, and then do not forget to remove volumes: `iotsystem_dev-rdb-data` and `iotsystem_inflexdb_data`

## Sensors API

### Init
- Device/Sensor simulator should generate `uuid` (which will be the simulator `DEVICE_CODE`) and print it to the console, so the user of our system can copy and paste it on the front-end (first step in adding a new device) `http://localhost:3000/devices/add`.
- Then the simulator needs to publish a <mark>retained</mark> message (with an arbitrary payload) to a topic called by that `uuid` (ex. Published message "New device" to topic: d7d4f6c5-8a35-4670-beb7-c44cdfd42abb)
- Finally, the simulator needs to subscribe to the topic `server-init/"+DEVICE_CODE` to get information from the server when the user finishes setting up his device on the front end. The simulator will receive messages in JSON format
```json
{
    "deviceId" : 123,
    "propertyId": 123
}
```
- Simulator then needs to activate (start working) and subscribe to 2 topics `server-command/DEVICE_ID` and `server-status/PROPERTY_ID` and to unsubscribe from `server-init/"+DEVICE_CODE`.
- On the `server-command` topic, the simulator will get predefined commands for actions to execute (ex. turn on/off, start...) and you need to handle different actions
- On the `server-status` topic, there will always be <mark>retained</mark> message of current system status that any already set device should read and parse and get its configurations

### Server Status
- After the device subscribes to the topic `server-status/PROPERTY_ID` it will receive json that represents the current status with all devices for a specific apartment and its location (if needed for API usage or something else). For more details and up to date version of this json look back at apartments/DTO/ApartmentInfoDTO
```json
{
   "id":1,
   "apartmentDevices":{
      "ambientConditionsList":{},
      "airConditioningList":{},
      "washingMachineList":{},
      "lampList":{},
      "vehicleGateList":{},
      "sprinklerSystemList":{},
      "solarSystemList":{
         "1":{
            "numberOfPanels":5,
            "panelSize":1,
            "panelEfficiency":0.2,
            "powerUsage":0
         },
      },
      "batteryList":{
         "20":{
            "batteryCapacity":3,
            "level":0.0
         }
      },
      "chargerList":{}
   },
   "location":{
      "latitude":35.6897,
      "longitude":139.6922
   }
}

```

### Power Consumption
- Every device that has `POWER_SUPPLY_TYPE = INTERNAL_POWER` should occasionally (ex. every 60 seconds) send their power usage as a string on the topic `devices-consume/PROPERTY_ID`, devices that have set with `POWER_SUPPLY_TYPE = EXTERNAL_POWER` shouldn't send anything. (ex. publish(client, f"%s/%d", ENERGY_CONSUMPTION_TOPIC, PROPERTY_ID, str(POWER_USAGE_IN_KWH), retained=false))

### Command
- To execute some action on the device send JSON in the following format 
```json
{
    "command": "string",
    "userId": 123
}
```
to topic `server-command/+DEVICE_ID` and handle it in your simulator with a switch case based on a specific command. Then you need to persist this in InfluxDB by publishing a message (ex. command,device_id=%d,device_type=%s,user_id=%d command=\"%s\"") to topic `measurements`
- Command for on/off is a string "turn_on"/"turn_off"

### Readings
- If your device has readings (is simulating some sensor with values) you need to persist them to influxDB by publishing message every n (ex. 5) seconds to topic `measurements` in format (ex. "readings,device_id=%d,device_type=%s temperature=%.2f,humidity=%.2f", DEVICE_ID, DEVICE_TYPE, temp, humid), try to simulate sensor as accurate as possible by using external APIs to get real-world values for specific measurements. This sould be done only if device is active (set to "ON" state)

### Heartbeat
- Every device simulator if active(set to "ON" state) should send heartbeat every n (ex. 1) seconds to influxDB by publishing a message to topic `measurements` in format (ex. "heartbeat,device_id=%d,device_type=%s status=\"ok\"", DEVICE_ID, DEVICE_TYPE)