package com.iotsystem.teamtwo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iotsystem.teamtwo.apartments.DTO.ApartmentInfoDTO;
import com.iotsystem.teamtwo.apartments.services.IApartmentService;
import com.iotsystem.teamtwo.apartments.services.IStatusService;
import com.iotsystem.teamtwo.devices.DTOs.MessageDTO;
import com.iotsystem.teamtwo.devices.Services.DeviceActivityService;
import com.iotsystem.teamtwo.devices.Services.DeviceService;
import com.iotsystem.teamtwo.devices.Services.IDeviceActivityService;
import com.iotsystem.teamtwo.devices.Services.IDeviceService;
import com.iotsystem.teamtwo.models.Apartment;
import org.eclipse.paho.mqttv5.client.*;
import org.eclipse.paho.mqttv5.client.persist.MemoryPersistence;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.eclipse.paho.mqttv5.common.packet.MqttProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.messaging.simp.SimpMessagingTemplate;


import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.UUID;

@Configuration
public class MqttConfiguration{
    private final String broker;
    private final String username;
    private final String password;
    private final String uniqueClientIdentifier;

    public MqttConfiguration(Environment env) {
        this.broker = String.format("tcp://%s:%s", env.getProperty("mqtt.host"),
                env.getProperty("mqtt.port"));
        this.username = env.getProperty("mqtt.username");
        this.password = env.getProperty("mqtt.password");
        this.uniqueClientIdentifier = UUID.randomUUID().toString();
    }

    @Bean
    public IMqttClient mqttClient(ObjectMapper objectMapper, SimpMessagingTemplate simpMessagingTemplate, IStatusService statusService) throws Exception {
        MqttClient client = new MqttClient(this.broker, this.uniqueClientIdentifier, new MemoryPersistence());
        client.setCallback(new DeviceCallback(client,  objectMapper,  simpMessagingTemplate, statusService));
        MqttConnectionOptions options = new MqttConnectionOptions();
        options.setCleanStart(false);
        options.setAutomaticReconnect(true);
        options.setUserName(this.username);
        options.setPassword(Objects.requireNonNull(this.password).getBytes());
        client.connect(options);
        return client;
    }

    public static class DeviceCallback implements MqttCallback {
        private ObjectMapper objectMapper;
        private SimpMessagingTemplate simpMessagingTemplate;
        private IStatusService statusService;
        private IMqttClient mqttClient;
        private static final Logger LOGGER = LoggerFactory.getLogger(DeviceCallback.class);

        public DeviceCallback() {
        }

        public DeviceCallback(IMqttClient mqttClient,ObjectMapper objectMapper, SimpMessagingTemplate simpMessagingTemplate, IStatusService statusService) {
            this.objectMapper = objectMapper;
            this.simpMessagingTemplate = simpMessagingTemplate;
            this.mqttClient = mqttClient;
            this.statusService = statusService;
        }

        @Override
        public void disconnected(MqttDisconnectResponse mqttDisconnectResponse) {
            LOGGER.warn("disconnected: {}", mqttDisconnectResponse.getReasonString());
        }

        @Override
        public void mqttErrorOccurred(MqttException e) {
            LOGGER.error("error: {}", e.getMessage());
        }

        @Override
        public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
            LOGGER.warn("topic: {}, message: {}", topic, mqttMessage);
            if(topic.startsWith("device-setup")){
                this.simpMessagingTemplate.convertAndSend(
                        String.format("/new-device/%s", topic),
                        this.objectMapper.writeValueAsString(new MessageDTO("Connected"))
                );
            }else if(topic.startsWith("battery-level/")){
                this.statusService.updateBatteryLevel(Long.parseLong(topic.split("/")[1]),Float.parseFloat(mqttMessage.toString()));
            }

            LOGGER.info(String.format("Connected with %s",topic));
        }

        @Override
        public void deliveryComplete(IMqttToken iMqttToken) {
            LOGGER.warn("delivery complete, message id: {}", iMqttToken.getMessageId());
        }

        @Override
        public void connectComplete(boolean b, String s) {
            LOGGER.warn("connect complete, status: {} {}", b, s);
            List<ApartmentInfoDTO> apartmentInfoDTOS = statusService.getApartments();

            for(ApartmentInfoDTO apartmentInfoDTO : apartmentInfoDTOS){
                try {
                    byte[] messageContent = objectMapper.writeValueAsString(apartmentInfoDTO).getBytes();
                    MqttMessage message = new MqttMessage(messageContent);
                    message.setRetained(true);
                    this.mqttClient.publish("server-status/" + apartmentInfoDTO.getId().toString(),message);

                } catch (MqttException | JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            }

            List<Long> ids = statusService.getBatteries();
            for(Long id : ids){
                try {
                    mqttClient.subscribe("battery-level/" + id.toString(),0);
                } catch (MqttException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        @Override
        public void authPacketArrived(int i, MqttProperties mqttProperties) {
            LOGGER.warn("auth packet arrived , status: {} {}", i, mqttProperties.getAuthenticationMethod());
        }
    }
}
