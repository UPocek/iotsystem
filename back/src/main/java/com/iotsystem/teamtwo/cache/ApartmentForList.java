package com.iotsystem.teamtwo.cache;

import com.iotsystem.teamtwo.models.Apartment;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;

@RedisHash(timeToLive = 900)
public class ApartmentForList implements Serializable {
    @Id
    private Long apartmentId;

    private String apartmentName;

    public ApartmentForList() {
    }

    public ApartmentForList(Apartment apartment) {
        this.apartmentId = apartment.getId();
        this.apartmentName = apartment.getName();
    }

    public ApartmentForList(Long apartmentId, String apartmentName) {
        this.apartmentId = apartmentId;
        this.apartmentName = apartmentName;
    }

    public Long getApartmentId() {
        return apartmentId;
    }

    public void setApartmentId(Long apartmentId) {
        this.apartmentId = apartmentId;
    }

    public String getApartmentName() {
        return apartmentName;
    }

    public void setApartmentName(String apartmentName) {
        this.apartmentName = apartmentName;
    }
}
