package com.iotsystem.teamtwo.cache;

import com.iotsystem.teamtwo.apartments.DTO.ApartmentDTO;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;
import java.util.List;

@RedisHash(timeToLive = 300)
public class UserApartments implements Serializable {
    @Id
    private Long userId;

    private List<ApartmentDTO> apartments;

    public UserApartments(Long userId, List<ApartmentDTO> apartments) {
        this.userId = userId;
        this.apartments = apartments;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<ApartmentDTO> getApartments() {
        return apartments;
    }

    public void setApartments(List<ApartmentDTO> apartments) {
        this.apartments = apartments;
    }
}
