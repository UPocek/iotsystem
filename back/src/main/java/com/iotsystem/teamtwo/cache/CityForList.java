package com.iotsystem.teamtwo.cache;

import com.iotsystem.teamtwo.models.Apartment;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;

@RedisHash(timeToLive = 900)
public class CityForList implements Serializable {
    @Id
    private Long cityId;

    private String cityName;

    public CityForList() {
    }

    public CityForList(Apartment apartment) {
        this.cityId = Long.valueOf(apartment.getCity().getId());
        this.cityName = apartment.getCity().getCityAscii();
    }

    public CityForList(Long cityId, String cityName) {
        this.cityId = cityId;
        this.cityName = cityName;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
