package com.iotsystem.teamtwo.apartments.repositories;

import com.iotsystem.teamtwo.cache.ApartmentForList;
import org.springframework.data.repository.CrudRepository;

public interface IAllApartmentsListCache extends CrudRepository<ApartmentForList, Long> {

}
