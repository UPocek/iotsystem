package com.iotsystem.teamtwo.apartments.services;

import com.iotsystem.teamtwo.apartments.DTO.ApartmentInfoDTO;
import com.iotsystem.teamtwo.models.Apartment;

import java.util.List;

public interface IStatusService {
    List<ApartmentInfoDTO> getApartments();
    List<Long> getBatteries();
    void updateBatteryLevel(Long batteryId, Float newLevel);
}
