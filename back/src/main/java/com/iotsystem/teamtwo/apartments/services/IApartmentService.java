package com.iotsystem.teamtwo.apartments.services;

import com.iotsystem.teamtwo.apartments.DTO.AdminReportDTO;
import com.iotsystem.teamtwo.apartments.DTO.ApartmentDTO;
import com.iotsystem.teamtwo.apartments.DTO.CreateApartmentDTO;
import com.iotsystem.teamtwo.models.Apartment;
import com.iotsystem.teamtwo.user.DTOs.RegularUserDTO;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

public interface IApartmentService {
    Apartment add(CreateApartmentDTO apartmentDTO, Principal owner);

    List<ApartmentDTO> listAsDTOs(Principal user);

    List<AdminReportDTO> getAllApartmentsForAdmin(Principal user);

    List<AdminReportDTO> getAllCitiesForAdmin(Principal user);

    List<ApartmentDTO> getSharedApartments(Principal user);

    List<Apartment> listConfirmationNeeded(Principal admin);

    List<ApartmentDTO> listConfirmationNeededAsDTOs(Principal admin);

    void confirm(Long id) throws Exception;

    void reject(Long id, String reason) throws Exception;

    RegularUserDTO getOwner(Long apartmentId);

    Optional<Apartment> findById(Long id);

    ApartmentDTO getConfirmationNeeded(Long id, Principal user);

    RegularUserDTO giveApartmentAccessTo(Long apartmentId, String toUsername, Principal owner);

    void removeApartmentAccessTo(Long apartmentId, String toUsername, Principal owner);
}
