package com.iotsystem.teamtwo.apartments.services;

import com.iotsystem.teamtwo.apartments.DTO.ApartmentInfoDTO;
import com.iotsystem.teamtwo.apartments.repositories.IApartmentRepository;
import com.iotsystem.teamtwo.devices.Repositories.IBatteryRepository;
import com.iotsystem.teamtwo.models.Apartment;
import com.iotsystem.teamtwo.models.Battery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.FetchType;
import java.util.List;
import java.util.Optional;

@Service
public class StatusService implements IStatusService {

    @Autowired
    private IApartmentRepository apartmentRepository;
    @Autowired
    private IBatteryRepository batteryRepository;

    @Transactional
    @Override
    public List<ApartmentInfoDTO> getApartments() {
        List<Apartment> apartments = apartmentRepository.findAll();
        return apartments.stream().map(ApartmentInfoDTO::new).toList();
    }

    @Override
    public List<Long> getBatteries() {
        List<Battery> batteries = batteryRepository.findAll();
        return batteries.stream().map(Battery::getId).toList();
    }

    @Override
    public void updateBatteryLevel(Long batteryId, Float newLevel) {
        Battery battery = batteryRepository.findById(batteryId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Battery does not exist."));
        battery.setLevel(newLevel);
        batteryRepository.save(battery);
    }
}
