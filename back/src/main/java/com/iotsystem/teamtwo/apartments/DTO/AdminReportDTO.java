package com.iotsystem.teamtwo.apartments.DTO;

import com.iotsystem.teamtwo.cache.ApartmentForList;
import com.iotsystem.teamtwo.cache.CityForList;

public class AdminReportDTO {
    private String name;
    private Long id;

    public AdminReportDTO() {
    }

    public AdminReportDTO(ApartmentForList apartment) {
        this.name = apartment.getApartmentName();
        this.id = apartment.getApartmentId();
    }

    public AdminReportDTO(CityForList city) {
        this.name = city.getCityName();
        this.id = city.getCityId();
    }

    public AdminReportDTO(String name, Long id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
