package com.iotsystem.teamtwo.apartments.DTO;

import com.iotsystem.teamtwo.devices.DTOs.*;
import com.iotsystem.teamtwo.geospatial.DTO.LocationDTO;
import com.iotsystem.teamtwo.models.Apartment;

public class ApartmentInfoDTO {
    private Long id;
    private ApartmentDevicesDTO apartmentDevices;
    private LocationDTO location;
    private String city;

    public ApartmentInfoDTO() {
    }

    public ApartmentInfoDTO(Long id,ApartmentDevicesDTO apartmentDevices, LocationDTO location, String city) {
        this.id = id;
        this.apartmentDevices = apartmentDevices;
        this.location = location;
        this.city = city;
    }

    public ApartmentInfoDTO(Apartment apartment) {
        this.id = apartment.getId();

        this.apartmentDevices = new ApartmentDevicesDTO();
        apartment.getAmbientConditionsList().forEach(condition -> this.apartmentDevices.getAmbientConditionsList().put(condition.getId(), new AmbientConditionsDTO(condition)));
        apartment.getAirConditioningList().forEach(condition -> this.apartmentDevices.getAirConditioningList().put(condition.getId(), new AirConditioningDTO(condition)));
        apartment.getWashingMachineList().forEach(condition -> this.apartmentDevices.getWashingMachineList().put(condition.getId(), new WashingMachineDTO(condition)));
        apartment.getLampList().forEach(condition -> this.apartmentDevices.getLampList().put(condition.getId(), new LampDTO(condition)));
        apartment.getSprinklerSystemList().forEach(condition -> this.apartmentDevices.getSprinklerSystemList().put(condition.getId(), new SprinklerSystemDTO(condition)));
        apartment.getVehicleGateList().forEach(condition -> this.apartmentDevices.getVehicleGateList().put(condition.getId(), new VehicleGateDTO(condition)));
        apartment.getSolarSystemList().forEach(condition -> this.apartmentDevices.getSolarSystemList().put(condition.getId(), new SolarSystemDTO(condition)));
        apartment.getChargerList().forEach(condition -> this.apartmentDevices.getChargerList().put(condition.getId(), new ChargerDTO(condition)));
        apartment.getBatteryList().forEach(condition -> this.apartmentDevices.getBatteryList().put(condition.getId(), new BatteryDTO(condition)));

        this.location = new LocationDTO(apartment.getLocation());
        this.city = apartment.getCity().getCityAscii();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ApartmentDevicesDTO getApartmentDevices() {
        return apartmentDevices;
    }

    public void setApartmentDevices(ApartmentDevicesDTO apartmentDevices) {
        this.apartmentDevices = apartmentDevices;
    }

    public LocationDTO getLocation() {
        return location;
    }

    public void setLocation(LocationDTO location) {
        this.location = location;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
