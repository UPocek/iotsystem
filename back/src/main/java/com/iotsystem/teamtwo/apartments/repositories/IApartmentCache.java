package com.iotsystem.teamtwo.apartments.repositories;

import com.iotsystem.teamtwo.cache.UserApartments;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IApartmentCache extends CrudRepository<UserApartments, Long> {
}
