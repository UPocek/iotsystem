package com.iotsystem.teamtwo.apartments.controllers;

import com.iotsystem.teamtwo.apartments.DTO.AdminEnergyReportDTO;
import com.iotsystem.teamtwo.apartments.DTO.AdminReportDTO;
import com.iotsystem.teamtwo.apartments.DTO.ApartmentDTO;
import com.iotsystem.teamtwo.apartments.DTO.CreateApartmentDTO;
import com.iotsystem.teamtwo.apartments.services.IApartmentService;
import com.iotsystem.teamtwo.models.Apartment;
import com.iotsystem.teamtwo.security.annotations.CheckHasAccess;
import com.iotsystem.teamtwo.user.DTOs.RegularUserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/apartment")
public class ApartmentController {
    @Autowired
    private IApartmentService apartmentService;

    @PostMapping
    public ResponseEntity<Long> add(@RequestBody CreateApartmentDTO apartmentDTO, Principal user) {
        Apartment apartment = apartmentService.add(apartmentDTO, user);
        return new ResponseEntity<>(apartment.getId(), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<ApartmentDTO>> list(Principal user) {
        return new ResponseEntity<>(
                apartmentService.listAsDTOs(user),
                HttpStatus.OK
        );
    }

    @GetMapping("shared")
    public ResponseEntity<List<ApartmentDTO>> getSharedApartments(Principal user) {
        return new ResponseEntity<>(
                apartmentService.getSharedApartments(user),
                HttpStatus.OK
        );
    }

    @GetMapping("/energyReportData")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<AdminEnergyReportDTO> getAllEnergyReportData(Principal user) {
        return new ResponseEntity<>(
                new AdminEnergyReportDTO(apartmentService.getAllApartmentsForAdmin(user), apartmentService.getAllCitiesForAdmin(user)),
                HttpStatus.OK
        );
    }

    @GetMapping("/confirmation_needed/")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<List<ApartmentDTO>> listToConfirm(Principal user) {
        return new ResponseEntity<>(
                apartmentService.listConfirmationNeededAsDTOs(user),
                HttpStatus.OK
        );
    }

    @GetMapping("/confirmation_needed/{id}/")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<ApartmentDTO> getToConfirm(@PathVariable Long id, Principal user) {
        return new ResponseEntity<>(
                apartmentService.getConfirmationNeeded(id, user),
                HttpStatus.OK
        );
    }

    @GetMapping("/getOwner/{apartmentId}")
    @PreAuthorize("hasAuthority('ROLE_USER') || hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<RegularUserDTO> getOwner(@PathVariable Long apartmentId) {
        return new ResponseEntity<>(
                apartmentService.getOwner(apartmentId),
                HttpStatus.OK
        );
    }

    @PostMapping("/confirmation_needed/{id}/confirm")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<Void> confirm(@PathVariable Long id) throws Exception {
        apartmentService.confirm(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping("/confirmation_needed/{id}/reject")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<Void> reject(@RequestBody String reason, @PathVariable Long id) throws Exception {
        apartmentService.reject(id, reason);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping("/giveAccess/{apartmentId}/{toUsername}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Apartment")
    public ResponseEntity<RegularUserDTO> giveAccessToApartment(@PathVariable Long apartmentId, @PathVariable String toUsername, Principal owner) {
        return new ResponseEntity<>(
                apartmentService.giveApartmentAccessTo(apartmentId, toUsername, owner),
                HttpStatus.OK
        );
    }

    @PutMapping("/removeAccess/{apartmentId}/{toUsername}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Apartment")
    public ResponseEntity<Void> removeAccessToApartment(@PathVariable Long apartmentId, @PathVariable String toUsername, Principal owner) {
        apartmentService.removeApartmentAccessTo(apartmentId, toUsername, owner);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
