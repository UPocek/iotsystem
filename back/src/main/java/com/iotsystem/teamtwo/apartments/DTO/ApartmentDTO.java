package com.iotsystem.teamtwo.apartments.DTO;

import com.iotsystem.teamtwo.geospatial.DTO.CityDTO;
import com.iotsystem.teamtwo.geospatial.DTO.LocationDTO;
import com.iotsystem.teamtwo.models.Apartment;
import com.iotsystem.teamtwo.user.DTOs.RegularUserDTO;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

public class ApartmentDTO implements Serializable {
    private Long id;
    private String name;
    private String address;
    private CityDTO city;
    private LocationDTO location;
    private Integer squareFootage;
    private Integer numberOfFloors;
    private Integer numberOfRooms;
    private String status;
    private RegularUserDTO owner;
    private List<RegularUserDTO> usersWithAccess;

    public ApartmentDTO() {
    }

    public ApartmentDTO(Long id, String name, String address, CityDTO city, LocationDTO location, Integer squareFootage, Integer numberOfFloors, Integer numberOfRooms, String imageUrl, String status, RegularUserDTO owner) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.city = city;
        this.location = location;
        this.squareFootage = squareFootage;
        this.numberOfFloors = numberOfFloors;
        this.numberOfRooms = numberOfRooms;
        this.status = status;
        this.owner = owner;
    }

    public ApartmentDTO(Apartment apartment) {
        this.id = apartment.getId();
        this.name = apartment.getName();
        this.address = apartment.getAddress();
        this.city = new CityDTO(apartment.getCity());
        this.location = new LocationDTO(apartment.getLocation());
        this.squareFootage = apartment.getSquareFootage();
        this.numberOfFloors = apartment.getNumberOfFloors();
        this.numberOfRooms = apartment.getNumberOfRooms();
        this.status = apartment.getStatus().toString();
        this.owner = new RegularUserDTO(apartment.getOwner());
        this.usersWithAccess = apartment.getUsersWithAccess().stream().map(RegularUserDTO::new).toList();
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public CityDTO getCity() {
        return city;
    }

    public LocationDTO getLocation() {
        return location;
    }

    public Integer getSquareFootage() {
        return squareFootage;
    }

    public Integer getNumberOfFloors() {
        return numberOfFloors;
    }

    public Integer getNumberOfRooms() {
        return numberOfRooms;
    }

    public String getStatus() {
        return status;
    }

    public RegularUserDTO getOwner() {
        return owner;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCity(CityDTO city) {
        this.city = city;
    }

    public void setLocation(LocationDTO location) {
        this.location = location;
    }

    public void setSquareFootage(Integer squareFootage) {
        this.squareFootage = squareFootage;
    }

    public void setNumberOfFloors(Integer numberOfFloors) {
        this.numberOfFloors = numberOfFloors;
    }

    public void setNumberOfRooms(Integer numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setOwner(RegularUserDTO owner) {
        this.owner = owner;
    }

    public List<RegularUserDTO> getUsersWithAccess() {
        return usersWithAccess;
    }

    public void setUsersWithAccess(List<RegularUserDTO> usersWithAccess) {
        this.usersWithAccess = usersWithAccess;
    }
}
