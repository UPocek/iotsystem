package com.iotsystem.teamtwo.apartments.repositories;

import com.iotsystem.teamtwo.apartments.enums.ApartmentStatus;
import com.iotsystem.teamtwo.models.Apartment;
import com.iotsystem.teamtwo.models.RegularUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IApartmentRepository extends JpaRepository<Apartment, Long> {
    List<Apartment> findAllByOwner(RegularUser regularUser);

    List<Apartment> findByStatus(ApartmentStatus needConfirmation);
    Apartment findByStatusAndId(ApartmentStatus needConfirmation, Long id);
}
