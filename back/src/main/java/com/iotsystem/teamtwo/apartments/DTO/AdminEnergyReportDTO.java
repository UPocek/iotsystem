package com.iotsystem.teamtwo.apartments.DTO;

import com.iotsystem.teamtwo.admin.DTOs.AdminDTO;

import java.util.List;

public class AdminEnergyReportDTO {
    private List<AdminReportDTO> apartments;
    private List<AdminReportDTO> cities;

    public AdminEnergyReportDTO() {
    }

    public AdminEnergyReportDTO(List<AdminReportDTO> apartments, List<AdminReportDTO> cities) {
        this.apartments = apartments;
        this.cities = cities;
    }

    public List<AdminReportDTO> getApartments() {
        return apartments;
    }

    public void setApartments(List<AdminReportDTO> apartments) {
        this.apartments = apartments;
    }

    public List<AdminReportDTO> getCities() {
        return cities;
    }

    public void setCities(List<AdminReportDTO> cities) {
        this.cities = cities;
    }
}
