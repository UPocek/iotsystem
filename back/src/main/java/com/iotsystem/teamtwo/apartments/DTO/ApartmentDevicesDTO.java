package com.iotsystem.teamtwo.apartments.DTO;

import com.iotsystem.teamtwo.devices.DTOs.*;

import java.util.HashMap;
import java.util.List;

public class ApartmentDevicesDTO {

    private HashMap<Long,AmbientConditionsDTO> ambientConditionsList;
    private HashMap<Long,AirConditioningDTO> airConditioningList;
    private HashMap<Long,WashingMachineDTO> washingMachineList;
    private HashMap<Long,LampDTO> lampList;
    private HashMap<Long,VehicleGateDTO> vehicleGateList;
    private HashMap<Long,SprinklerSystemDTO> sprinklerSystemList;
    private HashMap<Long,SolarSystemDTO> solarSystemList;
    private HashMap<Long,BatteryDTO> batteryList;
    private HashMap<Long,ChargerDTO> chargerList;

    public ApartmentDevicesDTO() {
        this.ambientConditionsList = new HashMap<>();
        this.airConditioningList = new HashMap<>();
        this.washingMachineList = new HashMap<>();
        this.lampList = new HashMap<>();
        this.vehicleGateList = new HashMap<>();
        this.sprinklerSystemList = new HashMap<>();
        this.solarSystemList = new HashMap<>();
        this.batteryList = new HashMap<>();
        this.chargerList = new HashMap<>();
    }

    public ApartmentDevicesDTO(HashMap<Long, AmbientConditionsDTO> ambientConditionsList, HashMap<Long, AirConditioningDTO> airConditioningList, HashMap<Long, WashingMachineDTO> washingMachineList, HashMap<Long, LampDTO> lampList, HashMap<Long, VehicleGateDTO> vehicleGateList, HashMap<Long, SprinklerSystemDTO> sprinklerSystemList, HashMap<Long, SolarSystemDTO> solarSystemList, HashMap<Long, BatteryDTO> batteryList, HashMap<Long, ChargerDTO> chargerList) {
        this.ambientConditionsList = ambientConditionsList;
        this.airConditioningList = airConditioningList;
        this.washingMachineList = washingMachineList;
        this.lampList = lampList;
        this.vehicleGateList = vehicleGateList;
        this.sprinklerSystemList = sprinklerSystemList;
        this.solarSystemList = solarSystemList;
        this.batteryList = batteryList;
        this.chargerList = chargerList;
    }

    public HashMap<Long, AmbientConditionsDTO> getAmbientConditionsList() {
        return ambientConditionsList;
    }

    public void setAmbientConditionsList(HashMap<Long, AmbientConditionsDTO> ambientConditionsList) {
        this.ambientConditionsList = ambientConditionsList;
    }

    public HashMap<Long, AirConditioningDTO> getAirConditioningList() {
        return airConditioningList;
    }

    public void setAirConditioningList(HashMap<Long, AirConditioningDTO> airConditioningList) {
        this.airConditioningList = airConditioningList;
    }

    public HashMap<Long, WashingMachineDTO> getWashingMachineList() {
        return washingMachineList;
    }

    public void setWashingMachineList(HashMap<Long, WashingMachineDTO> washingMachineList) {
        this.washingMachineList = washingMachineList;
    }

    public HashMap<Long, LampDTO> getLampList() {
        return lampList;
    }

    public void setLampList(HashMap<Long, LampDTO> lampList) {
        this.lampList = lampList;
    }

    public HashMap<Long, VehicleGateDTO> getVehicleGateList() {
        return vehicleGateList;
    }

    public void setVehicleGateList(HashMap<Long, VehicleGateDTO> vehicleGateList) {
        this.vehicleGateList = vehicleGateList;
    }

    public HashMap<Long, SprinklerSystemDTO> getSprinklerSystemList() {
        return sprinklerSystemList;
    }

    public void setSprinklerSystemList(HashMap<Long, SprinklerSystemDTO> sprinklerSystemList) {
        this.sprinklerSystemList = sprinklerSystemList;
    }

    public HashMap<Long, SolarSystemDTO> getSolarSystemList() {
        return solarSystemList;
    }

    public void setSolarSystemList(HashMap<Long, SolarSystemDTO> solarSystemList) {
        this.solarSystemList = solarSystemList;
    }

    public HashMap<Long, BatteryDTO> getBatteryList() {
        return batteryList;
    }

    public void setBatteryList(HashMap<Long, BatteryDTO> batteryList) {
        this.batteryList = batteryList;
    }

    public HashMap<Long, ChargerDTO> getChargerList() {
        return chargerList;
    }

    public void setChargerList(HashMap<Long, ChargerDTO> chargerList) {
        this.chargerList = chargerList;
    }
}
