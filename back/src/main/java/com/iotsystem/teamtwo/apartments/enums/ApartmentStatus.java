package com.iotsystem.teamtwo.apartments.enums;

public enum ApartmentStatus {
    NEED_CONFIRMATION, APPROVED, REJECTED;
}
