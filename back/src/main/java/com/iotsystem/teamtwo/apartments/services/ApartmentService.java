package com.iotsystem.teamtwo.apartments.services;

import com.iotsystem.teamtwo.apartments.DTO.AdminReportDTO;
import com.iotsystem.teamtwo.apartments.DTO.ApartmentDTO;
import com.iotsystem.teamtwo.apartments.DTO.CreateApartmentDTO;
import com.iotsystem.teamtwo.apartments.enums.ApartmentStatus;
import com.iotsystem.teamtwo.apartments.repositories.IAllApartmentsListCache;
import com.iotsystem.teamtwo.apartments.repositories.IApartmentCache;
import com.iotsystem.teamtwo.apartments.repositories.IApartmentRepository;
import com.iotsystem.teamtwo.apartments.repositories.ICitiesCache;
import com.iotsystem.teamtwo.cache.ApartmentForList;
import com.iotsystem.teamtwo.cache.CityForList;
import com.iotsystem.teamtwo.cache.UserApartments;
import com.iotsystem.teamtwo.geospatial.services.IGeospatialService;
import com.iotsystem.teamtwo.helper.IHelperService;
import com.iotsystem.teamtwo.models.*;
import com.iotsystem.teamtwo.user.DTOs.RegularUserDTO;
import com.iotsystem.teamtwo.user.Services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class ApartmentService implements IApartmentService {
    @Autowired
    private IApartmentRepository apartmentRepository;
    @Autowired
    private IGeospatialService geospatialService;
    @Autowired
    private IHelperService helperService;
    @Autowired
    private IUserService userService;
    @Autowired
    private IApartmentCache apartmentCache;
    @Autowired
    private IAllApartmentsListCache allApartmentsListCache;
    @Autowired
    private ICitiesCache allCitiesListCache;

    @Value("${company.email}")
    private String companyEmail;

    @Override
    public Apartment add(CreateApartmentDTO apartmentDTO, Principal user) {
        RegularUser owner = userService.getByUsername(user.getName()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found! Only common users can add estate."));
        City city = geospatialService.getCity(apartmentDTO.getCity(), apartmentDTO.getCountry());
        Location location = city.getLocation();
        if (apartmentDTO.getLocation() != null) {
            location = geospatialService.add(apartmentDTO.getLocation());
        }
        String base64image = apartmentDTO.getImageUrl();
        apartmentDTO.setImageUrl("");
        Apartment apartment = new Apartment(
                apartmentDTO,
                owner,
                location,
                city
        );

        apartmentRepository.saveAndFlush(apartment);
        apartmentCache.save(new UserApartments(owner.getId(), apartmentRepository.findAllByOwner(owner).stream().map(ApartmentDTO::new).toList()));
        helperService.saveImage(base64image, "apartment/" + apartment.getId() + ".jpg");
        return apartment;
    }

    @Override
    public List<Apartment> listConfirmationNeeded(Principal admin) {
        Optional<Admin> adminUser = userService.getAdminByUsername(admin.getName());
        if (adminUser.isPresent()) {
            return apartmentRepository.findByStatus(ApartmentStatus.NEED_CONFIRMATION);
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found! Only admins can confirm estates.");
    }

    @Override
    public void confirm(Long id) throws Exception {
        Optional<Apartment> apartment = apartmentRepository.findById(id);
        if (apartment.isPresent()) {
            Apartment apartmentExisted = apartment.get();
            apartmentExisted.setStatus(ApartmentStatus.APPROVED);
            List<ApartmentForList> cachedApartments = (List<ApartmentForList>) allApartmentsListCache.findAll();
            cachedApartments = cachedApartments.stream().filter(item -> !Objects.equals(item.getApartmentId(), id)).collect(Collectors.toList());
            cachedApartments.add(new ApartmentForList(apartmentExisted));
            allApartmentsListCache.saveAll(cachedApartments);
            apartmentRepository.save(apartment.get());
            helperService.sendHtmlEmail(
                    companyEmail,
                    apartmentExisted.getOwner().getUsername(),
                    "Apartment Confirmation",
                    "Your apartment " + apartmentExisted.getName() +
                            " has been confirmed by our admin team."
            );
            return;
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Apartment not found!");
    }

    @Override
    public void reject(Long id, String reason) throws Exception {
        Optional<Apartment> apartment = apartmentRepository.findById(id);
        if (apartment.isPresent()) {
            Apartment apartmentExisted = apartment.get();
            apartmentExisted.setStatus(ApartmentStatus.REJECTED);
            List<ApartmentForList> cachedApartments = (List<ApartmentForList>) allApartmentsListCache.findAll();
            cachedApartments = cachedApartments.stream().filter(item -> !Objects.equals(item.getApartmentId(), id)).collect(Collectors.toList());
            cachedApartments.add(new ApartmentForList(apartmentExisted));
            allApartmentsListCache.saveAll(cachedApartments);
            apartmentRepository.save(apartmentExisted);
            helperService.sendHtmlEmail(
                    companyEmail,
                    apartmentExisted.getOwner().getUsername(),
                    "Apartment Rejection",
                    "Your apartment " + apartmentExisted.getName() +
                            " has been rejected by our admin team. Reason: <br>" +
                            reason
            );
            return;
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Apartment not found!");
    }

    @Override
    public RegularUserDTO getOwner(Long apartmentId) {
        Optional<Apartment> apartment = apartmentRepository.findById(apartmentId);
        if (apartment.isPresent()) {
            return new RegularUserDTO(apartment.get().getOwner());
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Apartment not found!");
    }

    @Override
    public Optional<Apartment> findById(Long id) {
        return apartmentRepository.findById(id);
    }

    @Override
    public ApartmentDTO getConfirmationNeeded(Long id, Principal admin) {
        Optional<Admin> adminUser = userService.getAdminByUsername(admin.getName());
        if (adminUser.isPresent()) {
            return new ApartmentDTO(apartmentRepository.findByStatusAndId(ApartmentStatus.NEED_CONFIRMATION, id));
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found! Only admins can confirm estates.");
    }

    @Override
    public RegularUserDTO giveApartmentAccessTo(Long apartmentId, String toUsername, Principal owner) {
        Apartment apartmentToShare = apartmentRepository.findById(apartmentId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Apartment with that id does not exists!"));
        RegularUser userToGrantAccessTo = userService.getByUsername(toUsername).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User with that username does not exists!"));
        if (userToGrantAccessTo.getUsername().equals(owner.getName())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You cannot give additional access to yourself");
        }
        apartmentToShare.getUsersWithAccess().add(userToGrantAccessTo);
        giveAccessToAllDevicesInApartmentToUser(apartmentToShare, userToGrantAccessTo);
        try {
            apartmentRepository.saveAndFlush(apartmentToShare);
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "User already has access to this apartment");
        }
        RegularUser ownerRegularUser = userService.getByUsername(owner.getName()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found! Only common users can list their estates."));
        apartmentCache.save(new UserApartments(ownerRegularUser.getId(), apartmentRepository.findAllByOwner(ownerRegularUser).stream().map(ApartmentDTO::new).toList()));
        return new RegularUserDTO(userToGrantAccessTo);
    }

    @Override
    public void removeApartmentAccessTo(Long apartmentId, String toUsername, Principal owner) {
        Apartment apartmentToRemoveAccessTo = apartmentRepository.findById(apartmentId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Apartment with that id does not exists!"));
        if (apartmentToRemoveAccessTo.getUsersWithAccess().stream().noneMatch(user -> user.getUsername().equals(toUsername))) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "This user can't be removed from apartment");
        }
        apartmentToRemoveAccessTo.setUsersWithAccess(apartmentToRemoveAccessTo.getUsersWithAccess().stream().filter(item -> !item.getUsername().equals(toUsername)).collect(Collectors.toSet()));
        apartmentToRemoveAccessTo.getAllDevices().forEach(device -> device.setUsersWithAccess(device.getUsersWithAccess().stream().filter(user -> !user.getUsername().equals(toUsername)).collect(Collectors.toSet())));
        apartmentRepository.saveAndFlush(apartmentToRemoveAccessTo);
        RegularUser ownerRegularUser = userService.getByUsername(owner.getName()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found! Only common users can list their estates."));
        apartmentCache.save(new UserApartments(ownerRegularUser.getId(), apartmentRepository.findAllByOwner(ownerRegularUser).stream().map(ApartmentDTO::new).toList()));
    }

    private void giveAccessToAllDevicesInApartmentToUser(Apartment apartment, RegularUser shareWith) {
        apartment.getAmbientConditionsList().forEach(ambientConditions -> ambientConditions.getUsersWithAccess().add(shareWith));
        apartment.getAirConditioningList().forEach(airConditioning -> airConditioning.getUsersWithAccess().add(shareWith));
        apartment.getWashingMachineList().forEach(washingMachine -> washingMachine.getUsersWithAccess().add(shareWith));
        apartment.getChargerList().forEach(charger -> charger.getUsersWithAccess().add(shareWith));
        apartment.getBatteryList().forEach(battery -> battery.getUsersWithAccess().add(shareWith));
        apartment.getSolarSystemList().forEach(solarSystem -> solarSystem.getUsersWithAccess().add(shareWith));
        apartment.getVehicleGateList().forEach(vehicleGate -> vehicleGate.getUsersWithAccess().add(shareWith));
        apartment.getLampList().forEach(lamp -> lamp.getUsersWithAccess().add(shareWith));
        apartment.getSprinklerSystemList().forEach(sprinklerSystem -> sprinklerSystem.getUsersWithAccess().add(shareWith));
    }

    @Override
    public List<ApartmentDTO> listAsDTOs(Principal user) {
        RegularUser ownerRegularUser = userService.getByUsername(user.getName()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found! Only common users can list their estates."));
        List<ApartmentDTO> ownerApartments = new ArrayList<>();
        Optional<UserApartments> userApartmentsFromCacheOptional = apartmentCache.findById(ownerRegularUser.getId());
        if (userApartmentsFromCacheOptional.isPresent()) {
            ownerApartments = userApartmentsFromCacheOptional.get().getApartments();
        } else {
            List<ApartmentDTO> apartmentsList = apartmentRepository.findAllByOwner(ownerRegularUser).stream().map(ApartmentDTO::new).toList();
            if (!apartmentsList.isEmpty()) {
                ownerApartments = apartmentsList;
                apartmentCache.save(new UserApartments(ownerRegularUser.getId(), ownerApartments));
            }
        }
        return ownerApartments;
    }

    @Override
    public List<ApartmentDTO> getSharedApartments(Principal user) {
        RegularUser userWithSharedApartments = userService.getByUsername(user.getName()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found! Only common users can list their estates."));
        return userWithSharedApartments.getAccessToApartments().stream().map(ApartmentDTO::new).toList();
    }

    public List<AdminReportDTO> getAllApartmentsForAdmin(Principal user) {
        List<ApartmentForList> apartments = (List<ApartmentForList>) allApartmentsListCache.findAll();
        if (apartments.isEmpty() || apartments.get(0) == null) {
            apartments = apartmentRepository.findAll().stream().map(ApartmentForList::new).toList();
            allApartmentsListCache.saveAll(apartments);
        }
        return apartments.stream().map(AdminReportDTO::new).toList();
    }

    @Override
    public List<AdminReportDTO> getAllCitiesForAdmin(Principal user) {
        List<CityForList> cities = (List<CityForList>) allCitiesListCache.findAll();
        if (cities.isEmpty() || cities.get(0) == null) {
            cities = apartmentRepository.findAll().stream().map(CityForList::new).toList();
            allCitiesListCache.saveAll(cities);
        }
        return cities.stream().map(AdminReportDTO::new).toList();
    }

    @Override
    public List<ApartmentDTO> listConfirmationNeededAsDTOs(Principal admin) {
        return listConfirmationNeeded(admin).stream().map(ApartmentDTO::new).collect(Collectors.toList());
    }
}
