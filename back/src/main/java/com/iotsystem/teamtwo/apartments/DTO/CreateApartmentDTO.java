package com.iotsystem.teamtwo.apartments.DTO;

import com.iotsystem.teamtwo.apartments.enums.ApartmentStatus;
import com.iotsystem.teamtwo.geospatial.DTO.CityDTO;
import com.iotsystem.teamtwo.geospatial.DTO.LocationDTO;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CreateApartmentDTO {
    @NotBlank( message = "is required")
    private String name;
    @NotBlank( message = "is required")
    private String address;

    @NotBlank( message = "is required")
    private String city;
    @NotBlank( message = "is required")
    private String country;

    private LocationDTO location;

    private Integer squareFootage;
    private Integer numberOfFloors;
    private Integer numberOfRooms;

    private String imageUrl;

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public LocationDTO getLocation() {
        return location;
    }

    public Integer getSquareFootage() {
        return squareFootage;
    }

    public Integer getNumberOfFloors() {
        return numberOfFloors;
    }

    public Integer getNumberOfRooms() {
        return numberOfRooms;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
