package com.iotsystem.teamtwo.apartments.repositories;

import com.iotsystem.teamtwo.cache.CityForList;
import org.springframework.data.repository.CrudRepository;

public interface ICitiesCache extends CrudRepository<CityForList,Long> {
}
