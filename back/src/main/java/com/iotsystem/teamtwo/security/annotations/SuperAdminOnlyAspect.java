package com.iotsystem.teamtwo.security.annotations;

import com.iotsystem.teamtwo.admin.Services.IAdminService;
import com.iotsystem.teamtwo.security.jwt.JwtTokenUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@Aspect
@Component
public class SuperAdminOnlyAspect {
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private IAdminService adminService;

    @Before("@annotation(SuperAdminOnly)")
    public void checkIfSuperAdmin(JoinPoint joinPoint) {
        String token;
        try {
            HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
            token = request.getHeader("Authorization").split(" ")[1];
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Unauthorized");
        }

        String role = jwtTokenUtil.getRoleFromToken(token);
        Long id = jwtTokenUtil.getUserIdFromToken(token);

        if(!role.equals("ROLE_ADMIN") || !Objects.equals(adminService.getSuperAdmin().orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Super Admin not found"))
                .getId(), id)){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Only Super Admin can create new admins");
        }
    }
}
