package com.iotsystem.teamtwo.security.annotations;

import com.iotsystem.teamtwo.devices.Services.IDeviceService;
import com.iotsystem.teamtwo.models.RegularUser;
import com.iotsystem.teamtwo.security.jwt.JwtTokenUtil;
import com.iotsystem.teamtwo.user.Services.IUserService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Aspect
@Component
public class CheckHasAccessAspect {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private IUserService userService;
    @Autowired
    private IDeviceService deviceService;

    @Before("@annotation(CheckHasAccess)")
    public void checkIfSuperAdmin(JoinPoint joinPoint) {
        String token = extractToken();
        Long id = jwtTokenUtil.getUserIdFromToken(token);
        RegularUser user = userService.getById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found"));
        List<Integer> idIndexes = findIndexOfIdParameter(joinPoint);
        if (idIndexes.isEmpty()) {
            return;
        }
        Long requestedItemId = (Long) joinPoint.getArgs()[idIndexes.get(0)];

        CheckHasAccess s = ((MethodSignature) joinPoint.getSignature()).getMethod().getAnnotation(CheckHasAccess.class);

        if (Objects.equals(s.on(), "Apartment")) {
            checkIfUserHasAccessToApartment(user, requestedItemId);
        } else if (Objects.equals(s.on(), "Device")) {
            Long apartmentId = (Long) joinPoint.getArgs()[idIndexes.get(1)];
            checkIfUserHasAccessToDevice(user, apartmentId, requestedItemId);
        }
    }

    private String extractToken() {
        try {
            HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
            return request.getHeader("Authorization").split(" ")[1];
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Unauthorized");
        }
    }

    private void checkIfUserHasAccessToApartment(RegularUser user, Long apartmentId) {
        if (user.getApartments().stream().anyMatch(apartment -> apartment.getId().equals(apartmentId)) || user.getAccessToApartments().stream().anyMatch(sharedApartment -> sharedApartment.getId().equals(apartmentId))) {
            return;
        }
        throw new ResponseStatusException(HttpStatus.FORBIDDEN, "User does not have access to this apartment");
    }

    private void checkIfUserHasAccessToDevice(RegularUser user, Long apartmentId, Long deviceId) {
        if (deviceService.findAllDevicesInApartment(apartmentId).stream().anyMatch(device -> device.getId().equals(deviceId)) || user.getAccessToDevices().stream().anyMatch(sharedDevice -> sharedDevice.getId().equals(deviceId))) {
            return;
        }
        throw new ResponseStatusException(HttpStatus.FORBIDDEN, "User does not have access to this device");
    }

    private List<Integer> findIndexOfIdParameter(JoinPoint joinPoint) {
        List<Integer> ids = new ArrayList<>();
        String[] tokens = joinPoint.getSignature().toString().replace(")", "").split("\\(")[1].split(",");
        for (int i = 0; i < tokens.length; i++) {
            if (tokens[i].equals("Long")) {
                ids.add(i);
            }
        }
        return ids;
    }
}
