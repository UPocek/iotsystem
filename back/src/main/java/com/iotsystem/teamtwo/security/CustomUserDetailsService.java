package com.iotsystem.teamtwo.security;

import com.iotsystem.teamtwo.admin.Repositories.IAdminRepository;
import com.iotsystem.teamtwo.models.Admin;
import com.iotsystem.teamtwo.models.RegularUser;
import com.iotsystem.teamtwo.user.Repositories.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private IAdminRepository adminRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<RegularUser> user = userRepository.findByUsername(username);
        if (user.isPresent()) {
            return org.springframework.security.core.userdetails.User.withUsername(user.get().getUsername()).password(user.get().getPassword()).roles("USER").build();
        }
        Optional<Admin> admin = adminRepository.findByUsername(username);
        if (admin.isPresent()) {
            return org.springframework.security.core.userdetails.User.withUsername(admin.get().getUsername()).password(admin.get().getPassword()).roles("ADMIN").build();
        }
        throw new UsernameNotFoundException("User not found with this username: " + username);
    }
}
