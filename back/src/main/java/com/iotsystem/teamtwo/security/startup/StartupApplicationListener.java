package com.iotsystem.teamtwo.security.startup;

import com.iotsystem.teamtwo.admin.Services.IAdminService;
import com.iotsystem.teamtwo.helper.IHelperService;
import com.iotsystem.teamtwo.models.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Component
public class StartupApplicationListener {

    @Value("${superadmin.email}")
    private String superAdminEmail;
    @Value("${company.email}")
    private String companyEmail;
    @Value("company.url")
    private String companyUrl;

    @Autowired
    private IHelperService helperService;
    @Autowired
    private IAdminService adminService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    private static final Logger LOG
            = Logger.getLogger(String.valueOf(StartupApplicationListener.class));

    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (adminService.isSuperAdminPresent()) {
            return;
        }
        String password = helperService.generateSecretCode(8);
        boolean ok = sendStartupEmail(password);
        if (!ok) {
            throw new RuntimeException("Error sending startup email. Check SendGrid settings.");
        }
        password = passwordEncoder.encode(password);
        adminService.save(new Admin("admin", password, true, false));
        LOG.info("Application started. Super admin password was sent to " + superAdminEmail);
    }

    private boolean sendStartupEmail(String password) {
        if (superAdminEmail.isEmpty()) {
            throw new RuntimeException("Super admin email is empty. Please add super admin email in application.properties under superadmin.email");
        }
        try {
            List<String> data = new ArrayList<>();
            data.add("Your admin account has been created on IOT");
            data.add("Username is admin and your super admin password is " + password);
            data.add(companyUrl);
            String htmlTemplate = helperService.prepareMailTemplate(data, "src/main/resources/templates/email.html", "Username is admin, and password is: " + password);
            helperService.sendHtmlEmail(companyEmail, superAdminEmail, "IOT - Super admin password", htmlTemplate);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return true;
    }
}

