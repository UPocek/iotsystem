package com.iotsystem.teamtwo.devices.DTOs;

import com.iotsystem.teamtwo.models.Schedule;

public class ScheduleDTO {

    private Long id;
    private String command;
    private String initiatorUsername;
    private String scheduledFrom;
    private String scheduledTo;

    public ScheduleDTO() {
    }

    public ScheduleDTO(Schedule schedule) {
        this.id = schedule.getId();
        this.command = schedule.getCommand();
        this.initiatorUsername = schedule.getInitiatorUsername();
        this.scheduledFrom = schedule.getFromTime().toString();
        this.scheduledTo = schedule.getToTime().toString();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getInitiatorUsername() {
        return initiatorUsername;
    }

    public void setInitiatorUsername(String initiatorUsername) {
        this.initiatorUsername = initiatorUsername;
    }

    public String getScheduledFrom() {
        return scheduledFrom;
    }

    public void setScheduledFrom(String scheduledFrom) {
        this.scheduledFrom = scheduledFrom;
    }

    public String getScheduledTo() {
        return scheduledTo;
    }

    public void setScheduledTo(String scheduledTo) {
        this.scheduledTo = scheduledTo;
    }
}
