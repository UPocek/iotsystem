package com.iotsystem.teamtwo.devices.Repositories;

import com.iotsystem.teamtwo.models.VehicleGate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IVehicleGateRepository extends JpaRepository<VehicleGate, Long> {
}
