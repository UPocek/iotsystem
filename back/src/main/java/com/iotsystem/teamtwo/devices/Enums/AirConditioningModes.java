package com.iotsystem.teamtwo.devices.Enums;

public enum AirConditioningModes {
    AUTO,
    COOL,
    HEAT,
    DRY,
}
