package com.iotsystem.teamtwo.devices.DTOs;

import com.iotsystem.teamtwo.models.SolarSystem;

public class SolarSystemDTO {
    private Integer numberOfPanels;
    private Integer panelSize;
    private Float panelEfficiency;
    private Integer powerUsage;

    public SolarSystemDTO() {
    }

    public SolarSystemDTO(SolarSystem solarSystem) {
        this.numberOfPanels = solarSystem.getNumberOfPanels();
        this.panelSize = solarSystem.getPanelSize();
        this.panelEfficiency = solarSystem.getPanelEfficiency();
        this.powerUsage = solarSystem.getPowerUsage();
    }

    public SolarSystemDTO(String name, Integer panelSize, Float panelEfficiency, Integer powerUsage, Integer numberOfPanels) {
        this.panelSize = panelSize;
        this.panelEfficiency = panelEfficiency;
        this.powerUsage = powerUsage;
        this.numberOfPanels = numberOfPanels;
    }

    public Integer getPanelSize() {
        return panelSize;
    }

    public void setPanelSize(Integer panelSize) {
        this.panelSize = panelSize;
    }

    public Float getPanelEfficiency() {
        return panelEfficiency;
    }

    public void setPanelEfficiency(Float panelEfficiency) {
        this.panelEfficiency = panelEfficiency;
    }

    public Integer getPowerUsage() {
        return powerUsage;
    }

    public void setPowerUsage(Integer powerUsage) {
        this.powerUsage = powerUsage;
    }

    public Integer getNumberOfPanels() {
        return numberOfPanels;
    }

    public void setNumberOfPanels(Integer numberOfPanels) {
        this.numberOfPanels = numberOfPanels;
    }

}
