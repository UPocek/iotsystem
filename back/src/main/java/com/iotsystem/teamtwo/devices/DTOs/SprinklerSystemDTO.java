package com.iotsystem.teamtwo.devices.DTOs;

import com.iotsystem.teamtwo.devices.Enums.PowerSupplyType;
import com.iotsystem.teamtwo.models.SprinklerSystem;

public class SprinklerSystemDTO {
    private PowerSupplyType powerSupplyType;
    private Integer powerUsage;
    private Boolean automaticMode;
    private String wateringTime;
    private String wateringSchedule;

    public SprinklerSystemDTO() {
    }

    public SprinklerSystemDTO(SprinklerSystem sprinklerSystem) {
        this.powerSupplyType = sprinklerSystem.getPowerSupplyType();
        this.powerUsage = sprinklerSystem.getPowerUsage();
        this.automaticMode = sprinklerSystem.getAutomaticMode();
        this.wateringTime = sprinklerSystem.getWateringTime();
    }

    public SprinklerSystemDTO(PowerSupplyType powerSupplyType, Integer powerUsage, Boolean automaticMode, String wateringTime, String wateringSchedule) {
        this.powerSupplyType = powerSupplyType;
        this.powerUsage = powerUsage;
        this.automaticMode = automaticMode;
        this.wateringTime = wateringTime;
        this.wateringSchedule = wateringSchedule;
    }

    public PowerSupplyType getPowerSupplyType() {
        return powerSupplyType;
    }

    public void setPowerSupplyType(PowerSupplyType powerSupplyType) {
        this.powerSupplyType = powerSupplyType;
    }

    public Integer getPowerUsage() {
        return powerUsage;
    }

    public void setPowerUsage(Integer powerUsage) {
        this.powerUsage = powerUsage;
    }

    public Boolean getAutomaticMode() {
        return automaticMode;
    }

    public void setAutomaticMode(Boolean automaticMode) {
        this.automaticMode = automaticMode;
    }

    public String getWateringTime() {
        return wateringTime;
    }

    public void setWateringTime(String wateringTime) {
        this.wateringTime = wateringTime;
    }

    public String getWateringSchedule() {
        return wateringSchedule;
    }

    public void setWateringSchedule(String wateringSchedule) {
        this.wateringSchedule = wateringSchedule;
    }
}
