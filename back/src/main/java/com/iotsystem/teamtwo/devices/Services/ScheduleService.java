package com.iotsystem.teamtwo.devices.Services;

import com.iotsystem.teamtwo.devices.DTOs.CommandDTO;
import com.iotsystem.teamtwo.devices.Enums.WashingMachineProgram;
import com.iotsystem.teamtwo.devices.Repositories.IScheduleRepository;
import com.iotsystem.teamtwo.devices.Repositories.IWashingMachineRepository;
import com.iotsystem.teamtwo.models.RegularUser;
import com.iotsystem.teamtwo.models.Schedule;
import com.iotsystem.teamtwo.models.WashingMachine;
import com.iotsystem.teamtwo.user.Services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.Optional;

@Service
public class ScheduleService {

    @Autowired
    private IDeviceService deviceService;
    @Autowired
    private IUserService userService;
    @Autowired
    private IScheduleRepository scheduleRepository;
    @Autowired
    private IWashingMachineRepository washingMachineRepository;

    @Async
    @Scheduled(fixedRate = 59000, initialDelayString = "${timing.initialScheduledDelay}")
    protected void executeScheduleDeviceActions() {

        //        Start scheduled program if now == fromTime
        for (Schedule schedule : scheduleRepository.findAllByActiveTrueAndFromTime(LocalTime.now().withSecond(0).withNano(0))) {
            Optional<RegularUser> schedulerOption = userService.getByUsername(schedule.getInitiatorUsername());
            if (schedulerOption.isEmpty()) {
                scheduleRepository.delete(schedule);
                continue;
            }
            RegularUser scheduler = schedulerOption.get();
            CommandDTO commandTurnOn = new CommandDTO("turn_on", String.format("%s_%s", scheduler.getName(), scheduler.getSurname()), scheduler.getId());
            deviceService.turnOffOn(schedule.getDeviceId(), commandTurnOn, schedule.getInitiatorUsername());

            CommandDTO commandScheduledProgram = new CommandDTO(schedule.getCommand(), String.format("%s_%s", scheduler.getName(), scheduler.getSurname()), scheduler.getId());
            switch (schedule.getDeviceOption()) {
                case AIRCONDITIONING ->
                        deviceService.switchAirConditioningMode(schedule.getDeviceId(), commandScheduledProgram, schedule.getInitiatorUsername());
                case WASHINGMACHINE -> {
                    WashingMachine washingMachine = washingMachineRepository.findById(schedule.getDeviceId()).get();
                    if (washingMachine.getCurrentProgram() == WashingMachineProgram.OFF) {
                        deviceService.switchWashingMachineMode(schedule.getDeviceId(), commandScheduledProgram, schedule.getInitiatorUsername());
                    }
                }
                default -> System.err.println("New unsupported device type schedule in ScheduleService");
            }
        }

        //        End scheduled program (by turning device off) if now == toTime
        for (Schedule schedule : scheduleRepository.findAllByActiveTrueAndToTime(LocalTime.now().withSecond(0).withNano(0))) {
            Optional<RegularUser> schedulerOption = userService.getByUsername(schedule.getInitiatorUsername());
            if (schedulerOption.isEmpty()) {
                scheduleRepository.delete(schedule);
                continue;
            }
            RegularUser scheduler = schedulerOption.get();
            CommandDTO commandTurnOff = new CommandDTO("turn_off", String.format("%s_%s", scheduler.getName(), scheduler.getSurname()), scheduler.getId());
            deviceService.turnOffOn(schedule.getDeviceId(), commandTurnOff, schedule.getInitiatorUsername());
        }
    }
}
