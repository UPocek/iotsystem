package com.iotsystem.teamtwo.devices.Enums;

public enum WashingMachineProgram {
    BLACK30, BLACK45, WHITE60, WHITE90, COLOR45, OFF
}
