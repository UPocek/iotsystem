package com.iotsystem.teamtwo.devices.Repositories;

import com.iotsystem.teamtwo.devices.RedisModels.Reading;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IReadingsRepository extends CrudRepository<Reading, String> {
}
