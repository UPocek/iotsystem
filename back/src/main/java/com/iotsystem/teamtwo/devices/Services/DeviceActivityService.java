package com.iotsystem.teamtwo.devices.Services;

import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.QueryApi;
import com.influxdb.query.FluxRecord;
import com.influxdb.query.FluxTable;
import com.iotsystem.teamtwo.devices.Repositories.IDeviceRepository;
import com.iotsystem.teamtwo.models.Device;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@Service
public class DeviceActivityService implements IDeviceActivityService{
    @Autowired
    private InfluxDBClient influxDBClient;

    @Autowired
    private IDeviceRepository deviceRepository;

    @Value("${influxdb.bucket}")
    private String bucket;

    @Async
    @Scheduled(fixedRate = 5000, initialDelayString = "${timing.initialScheduledDelay}")
    protected void checkOnDevices(){
            List<Long> result = new ArrayList<>();
            String fluxQuery = String.format(
                    "from(bucket: \"%s\")\n" +
                            "  |> range(start: -10s)\n" +
                            "  |> filter(fn: (r) => r[\"_measurement\"] == \"heartbeat\")\n" +
                            "  |> distinct()\n" +
                            "  |> yield(name: \"distinct\")",
                    this.bucket);
            QueryApi queryApi = this.influxDBClient.getQueryApi();
            List<FluxTable> tables = queryApi.query(fluxQuery);
            for (FluxTable fluxTable : tables) {
                List<FluxRecord> records = fluxTable.getRecords();
                for (FluxRecord fluxRecord : records) {
                    result.add(Long.valueOf(fluxRecord.getValueByKey("device_id").toString()));
                }
            }
            List<Long> activeDevices = getAllActive();
            List<Long> devicesToTurnOn = result.stream().filter(id -> !activeDevices.contains(id)).toList();
            List<Long> devicesToTurnOff = activeDevices.stream().filter(id -> !result.contains(id)).toList();

            devicesToTurnOn.forEach(this::changeActivity);
            devicesToTurnOff.forEach(this::changeActivity);
        }

    @Override
    public List<Long> getAllActive() {
        return deviceRepository.findAllByOnlineTrue().stream().map(Device::getId).toList();
    }

    @Override
    public void changeActivity(Long id) {
        Device device = deviceRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Device does not exist."));
        device.setOnline(!device.isOnline());
        deviceRepository.save(device);
    }
}
