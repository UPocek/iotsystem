package com.iotsystem.teamtwo.devices.Repositories;

import com.iotsystem.teamtwo.models.Battery;
import com.iotsystem.teamtwo.models.Device;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface IBatteryRepository extends JpaRepository<Battery, Long> {
    Set<Battery> findAllByApartmentId(Long apartmentId);
}
