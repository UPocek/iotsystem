package com.iotsystem.teamtwo.devices.Services;

import java.util.List;

public interface IDeviceActivityService {
    List<Long> getAllActive();
    void changeActivity(Long id);
}
