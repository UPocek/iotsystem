package com.iotsystem.teamtwo.devices.Enums;

public enum DeviceOption {
    SOLAR_SYSTEM,
    CHARGER,
    BATTERY,
    AIRCONDITIONING,
    AMBIENTSENSORS,
    WASHINGMACHINE,
    LAMP,
    GATE,
    SPRINKLERS
}
