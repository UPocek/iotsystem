package com.iotsystem.teamtwo.devices.Repositories;

import com.iotsystem.teamtwo.models.Charger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IChargerRepository extends JpaRepository<Charger, Long> {
}
