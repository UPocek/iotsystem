package com.iotsystem.teamtwo.devices.DTOs;

import com.iotsystem.teamtwo.models.Battery;

public class BatteryDTO {
    private Integer batteryCapacity;
    private Float level;

    public BatteryDTO() {
    }

    public BatteryDTO(Battery battery) {
        this.batteryCapacity = battery.getBatteryCapacity();
        this.level = battery.getLevel();
    }

    public Integer getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(Integer batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public Float getLevel() {
        return level;
    }

    public void setLevel(Float level) {
        this.level = level;
    }
}
