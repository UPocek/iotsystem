package com.iotsystem.teamtwo.devices.Repositories;

import com.iotsystem.teamtwo.models.Device;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface IDeviceRepository extends JpaRepository<Device, Long> {
    Set<Device> findAllByOnlineTrue();

    Set<Device> findAllByApartmentId(Long apartmentId);
}
