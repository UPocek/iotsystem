package com.iotsystem.teamtwo.devices.DTOs;

public class SimulatorInitDTO {
    private Long deviceId;
    private Long propertyId;

    public SimulatorInitDTO() {
    }

    public SimulatorInitDTO(Long deviceId, Long propertyId) {
        this.deviceId = deviceId;
        this.propertyId = propertyId;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Long getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Long propertyId) {
        this.propertyId = propertyId;
    }
}
