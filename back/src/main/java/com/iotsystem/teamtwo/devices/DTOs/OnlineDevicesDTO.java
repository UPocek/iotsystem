package com.iotsystem.teamtwo.devices.DTOs;

public class OnlineDevicesDTO {
    private String devices;

    public OnlineDevicesDTO() {
    }

    public OnlineDevicesDTO(String devices) {
        this.devices = devices;
    }

    public String getDevices() {
        return devices;
    }

    public void setDevices(String devices) {
        this.devices = devices;
    }
}
