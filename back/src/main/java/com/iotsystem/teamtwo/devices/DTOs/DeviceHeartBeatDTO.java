package com.iotsystem.teamtwo.devices.DTOs;

public class DeviceHeartBeatDTO {
    private String device;

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }
}
