package com.iotsystem.teamtwo.devices.DTOs;

import com.iotsystem.teamtwo.devices.Enums.PowerSupplyType;
import com.iotsystem.teamtwo.models.WashingMachine;

public class WashingMachineDTO {
    private PowerSupplyType powerSupplyType;
    private Integer powerUsage;
    private String currentProgram;
    private String programEndDateTime;

    public WashingMachineDTO() {
    }

    public WashingMachineDTO(WashingMachine washingMachine) {
        this.powerSupplyType = washingMachine.getPowerSupplyType();
        this.powerUsage = washingMachine.getPowerUsage();
        this.currentProgram = washingMachine.getCurrentProgram().name();
        this.programEndDateTime = washingMachine.getProgramEnd().toString();
    }

    public WashingMachineDTO(PowerSupplyType powerSupplyType, Integer powerUsage, String currentProgram, String programEndDateTime) {
        this.powerSupplyType = powerSupplyType;
        this.powerUsage = powerUsage;
        this.currentProgram = currentProgram;
        this.programEndDateTime = programEndDateTime;
    }

    public PowerSupplyType getPowerSupplyType() {
        return powerSupplyType;
    }

    public void setPowerSupplyType(PowerSupplyType powerSupplyType) {
        this.powerSupplyType = powerSupplyType;
    }

    public Integer getPowerUsage() {
        return powerUsage;
    }

    public void setPowerUsage(Integer powerUsage) {
        this.powerUsage = powerUsage;
    }

    public String getCurrentProgram() {
        return currentProgram;
    }

    public void setCurrentProgram(String currentProgram) {
        this.currentProgram = currentProgram;
    }

    public String getProgramEndDateTime() {
        return programEndDateTime;
    }

    public void setProgramEndDateTime(String programEndDateTime) {
        this.programEndDateTime = programEndDateTime;
    }
}
