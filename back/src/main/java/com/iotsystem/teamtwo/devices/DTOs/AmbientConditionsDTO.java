package com.iotsystem.teamtwo.devices.DTOs;

import com.iotsystem.teamtwo.devices.Enums.PowerSupplyType;
import com.iotsystem.teamtwo.models.AmbientConditions;

public class AmbientConditionsDTO {
    private PowerSupplyType powerSupplyType;
    private Integer powerUsage;

    public AmbientConditionsDTO() {
    }

    public AmbientConditionsDTO(AmbientConditions ambientConditions) {
        this.powerSupplyType = ambientConditions.getPowerSupplyType();
        this.powerUsage = ambientConditions.getPowerUsage();
    }

    public AmbientConditionsDTO(PowerSupplyType powerSupplyType, Integer powerUsage) {
        this.powerSupplyType = powerSupplyType;
        this.powerUsage = powerUsage;
    }

    public PowerSupplyType getPowerSupplyType() {
        return powerSupplyType;
    }

    public void setPowerSupplyType(PowerSupplyType powerSupplyType) {
        this.powerSupplyType = powerSupplyType;
    }

    public Integer getPowerUsage() {
        return powerUsage;
    }

    public void setPowerUsage(Integer powerUsage) {
        this.powerUsage = powerUsage;
    }
}
