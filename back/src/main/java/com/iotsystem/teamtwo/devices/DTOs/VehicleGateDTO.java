package com.iotsystem.teamtwo.devices.DTOs;

import com.iotsystem.teamtwo.devices.Enums.PowerSupplyType;
import com.iotsystem.teamtwo.models.VehicleGate;

public class VehicleGateDTO {
    private PowerSupplyType powerSupplyType;
    private Integer powerUsage;
    private Boolean privateMode;
    private String allowedNumbers;

    public VehicleGateDTO() {
    }

    public VehicleGateDTO(VehicleGate vehicleGate) {
        this.powerSupplyType = vehicleGate.getPowerSupplyType();
        this.powerUsage = vehicleGate.getPowerUsage();
        this.privateMode = vehicleGate.getPrivateMode();
        this.allowedNumbers = vehicleGate.getAllowedNumbers();
    }

    public VehicleGateDTO(PowerSupplyType powerSupplyType, Integer powerUsage, Boolean privateMode, String allowedNumbers) {
        this.powerSupplyType = powerSupplyType;
        this.powerUsage = powerUsage;
        this.privateMode = privateMode;
        this.allowedNumbers = allowedNumbers;
    }

    public PowerSupplyType getPowerSupplyType() {
        return powerSupplyType;
    }

    public void setPowerSupplyType(PowerSupplyType powerSupplyType) {
        this.powerSupplyType = powerSupplyType;
    }

    public Integer getPowerUsage() {
        return powerUsage;
    }

    public void setPowerUsage(Integer powerUsage) {
        this.powerUsage = powerUsage;
    }

    public Boolean getPrivateMode() {
        return privateMode;
    }

    public void setPrivateMode(Boolean privateMode) {
        this.privateMode = privateMode;
    }

    public String getAllowedNumbers() {
        return allowedNumbers;
    }

    public void setAllowedNumbers(String allowedNumbers) {
        this.allowedNumbers = allowedNumbers;
    }
}
