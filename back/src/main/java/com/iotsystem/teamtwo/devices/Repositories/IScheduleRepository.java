package com.iotsystem.teamtwo.devices.Repositories;

import com.iotsystem.teamtwo.models.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Set;

@Repository
public interface IScheduleRepository extends JpaRepository<Schedule, Long> {

    Set<Schedule> findAllByActiveTrueAndFromTime(LocalTime now);

    Set<Schedule> findAllByActiveTrueAndToTime(LocalTime now);

    List<Schedule> findAllByDeviceId(Long deviceId);
}
