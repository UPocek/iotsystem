package com.iotsystem.teamtwo.devices.Repositories;

import com.iotsystem.teamtwo.models.SolarSystem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ISolarSystemRepository extends JpaRepository<SolarSystem, Long> {
}
