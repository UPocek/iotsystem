package com.iotsystem.teamtwo.devices.DTOs;

import java.util.List;

public class ChargerActionsDTO {
    private List<ActionDTO> carChargingActions;
    private List<ActionDTO> chargingLimitActions;
    private List<ActionDTO> chargingTurnOnOffActions;

    public ChargerActionsDTO() {
    }

    public ChargerActionsDTO(List<ActionDTO> carChargingActions, List<ActionDTO> chargingLimitActions, List<ActionDTO> chargingTurnOnOffActions) {
        this.carChargingActions = carChargingActions;
        this.chargingLimitActions = chargingLimitActions;
        this.chargingTurnOnOffActions = chargingTurnOnOffActions;
    }

    public List<ActionDTO> getCarChargingActions() {
        return carChargingActions;
    }

    public void setCarChargingActions(List<ActionDTO> carChargingActions) {
        this.carChargingActions = carChargingActions;
    }

    public List<ActionDTO> getChargingLimitActions() {
        return chargingLimitActions;
    }

    public void setChargingLimitActions(List<ActionDTO> chargingLimitActions) {
        this.chargingLimitActions = chargingLimitActions;
    }

    public List<ActionDTO> getChargingTurnOnOffActions() {
        return chargingTurnOnOffActions;
    }

    public void setChargingTurnOnOffActions(List<ActionDTO> chargingTurnOnOffActions) {
        this.chargingTurnOnOffActions = chargingTurnOnOffActions;
    }
}
