package com.iotsystem.teamtwo.devices.Services;

import com.iotsystem.teamtwo.devices.DTOs.CommandDTO;
import com.iotsystem.teamtwo.devices.Enums.WashingMachineProgram;
import com.iotsystem.teamtwo.devices.Repositories.IWashingMachineRepository;
import com.iotsystem.teamtwo.models.RegularUser;
import com.iotsystem.teamtwo.user.Services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class WashingCycleService {
    @Autowired
    private IDeviceService deviceService;
    @Autowired
    private IUserService userService;
    @Autowired
    private IWashingMachineRepository washingMachineRepository;

    @Async
    @Scheduled(fixedRate = 61000, initialDelayString = "${timing.initialScheduledDelay}")
    protected void turnOffMachineWhenProgramIsOver() {
        for (var washingMachine : washingMachineRepository.findAllByCurrentProgramIsNot(WashingMachineProgram.OFF)) {
            if (washingMachine.getProgramEnd().isBefore(LocalDateTime.now())) {
                Optional<RegularUser> initiatorOption = userService.getById(washingMachine.getProgramInitiatorId());
                if (initiatorOption.isPresent()) {
                    CommandDTO commandTurnOff = new CommandDTO("turn_off", String.format("%s_%s", initiatorOption.get().getName(), initiatorOption.get().getSurname()), initiatorOption.get().getId());
                    deviceService.turnOffOn(washingMachine.getId(), commandTurnOff, initiatorOption.get().getUsername());
                }
                washingMachine.setCurrentProgram(WashingMachineProgram.OFF);
                washingMachineRepository.save(washingMachine);
            }
        }
    }
}
