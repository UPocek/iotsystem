package com.iotsystem.teamtwo.devices.Repositories;

import com.iotsystem.teamtwo.models.AirConditioning;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IAirConditioningRepository extends JpaRepository<AirConditioning, Long> {
}
