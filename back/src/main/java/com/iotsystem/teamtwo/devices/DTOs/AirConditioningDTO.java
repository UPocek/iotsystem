package com.iotsystem.teamtwo.devices.DTOs;

import com.iotsystem.teamtwo.devices.Enums.PowerSupplyType;
import com.iotsystem.teamtwo.models.AirConditioning;
import com.iotsystem.teamtwo.models.AmbientConditions;

public class AirConditioningDTO {
    private PowerSupplyType powerSupplyType;
    private Integer powerUsage;
    private Integer minTemp;
    private Integer maxTemp;
    private Integer currentTemp;
    private String mode;

    public AirConditioningDTO() {
    }

    public AirConditioningDTO(AirConditioning airConditioning) {
        this.powerSupplyType = airConditioning.getPowerSupplyType();
        this.powerUsage = airConditioning.getPowerUsage();
        this.minTemp = airConditioning.getMinTemp();
        this.maxTemp = airConditioning.getMaxTemp();
        this.currentTemp = airConditioning.getCurrentTemp();
        this.mode = String.valueOf(airConditioning.getMode());
    }

    public AirConditioningDTO(PowerSupplyType powerSupplyType, Integer powerUsage) {
        this.powerSupplyType = powerSupplyType;
        this.powerUsage = powerUsage;
    }

    public PowerSupplyType getPowerSupplyType() {
        return powerSupplyType;
    }

    public void setPowerSupplyType(PowerSupplyType powerSupplyType) {
        this.powerSupplyType = powerSupplyType;
    }

    public Integer getPowerUsage() {
        return powerUsage;
    }

    public void setPowerUsage(Integer powerUsage) {
        this.powerUsage = powerUsage;
    }

    public Integer getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(Integer minTemp) {
        this.minTemp = minTemp;
    }

    public Integer getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(Integer maxTemp) {
        this.maxTemp = maxTemp;
    }

    public Integer getCurrentTemp() {
        return currentTemp;
    }

    public void setCurrentTemp(Integer currentTemp) {
        this.currentTemp = currentTemp;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }
}
