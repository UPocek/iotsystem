package com.iotsystem.teamtwo.devices.Repositories;

import com.iotsystem.teamtwo.devices.Enums.WashingMachineProgram;
import com.iotsystem.teamtwo.models.WashingMachine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface IWashingMachineRepository extends JpaRepository<WashingMachine, Long> {

    Set<WashingMachine> findAllByCurrentProgramIsNot(WashingMachineProgram program);
}
