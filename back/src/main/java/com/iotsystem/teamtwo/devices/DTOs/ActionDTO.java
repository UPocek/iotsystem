package com.iotsystem.teamtwo.devices.DTOs;

public class ActionDTO {
    private String action;
    private String userFullName;
    private Long userId;
    private String date;
    private Double value;

    public ActionDTO() {
    }

    public ActionDTO(String action, String userFullName, String date, Long userId) {
        this.action = action;
        this.userFullName = userFullName;
        this.date = date;
        this.userId = userId;
    }

    public ActionDTO(Double value, String userFullName, String date, Long userId) {
        this.userFullName = userFullName;
        this.date = date;
        this.userId = userId;
        this.value = value;
    }

    public ActionDTO( String date, Double value) {
        this.date = date;
        this.value = value;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
