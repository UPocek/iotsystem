package com.iotsystem.teamtwo.devices.DTOs;

import com.iotsystem.teamtwo.models.Device;
import com.iotsystem.teamtwo.user.DTOs.RegularUserDTO;

import java.util.List;

public class DeviceWithTypeDTO {

    private Long id;
    private String name;
    private Boolean online;
    private List<RegularUserDTO> usersWithAccess;
    private String type;
    private String ownerUsername;
    private Long apartmentId;

    public DeviceWithTypeDTO() {
    }

    public DeviceWithTypeDTO(Device device) {
        this.id = device.getId();
        this.name = device.getName();
        this.online = device.isOnline();
        this.usersWithAccess = device.getUsersWithAccess().stream().map(RegularUserDTO::new).toList();
        this.type = extractDeviceType(device);
        this.ownerUsername = device.getApartment().getOwner().getUsername();
        this.apartmentId = device.getApartment().getId();
    }

    private String extractDeviceType(Device device) {
        return device.getClass().getSimpleName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getOnline() {
        return online;
    }

    public void setOnline(Boolean online) {
        this.online = online;
    }

    public List<RegularUserDTO> getUsersWithAccess() {
        return usersWithAccess;
    }

    public void setUsersWithAccess(List<RegularUserDTO> usersWithAccess) {
        this.usersWithAccess = usersWithAccess;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOwnerUsername() {
        return ownerUsername;
    }

    public void setOwnerUsername(String ownerUsername) {
        this.ownerUsername = ownerUsername;
    }

    public Long getApartmentId() {
        return apartmentId;
    }

    public void setApartmentId(Long apartmentId) {
        this.apartmentId = apartmentId;
    }
}
