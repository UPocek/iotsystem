package com.iotsystem.teamtwo.devices.DTOs;

import com.iotsystem.teamtwo.devices.Enums.PowerSupplyType;
import com.iotsystem.teamtwo.models.Lamp;

public class LampDTO {
    private PowerSupplyType powerSupplyType;
    private Integer powerUsage;
    private Boolean automaticMode;
    private Boolean lightOn;

    public LampDTO() {
    }

    public LampDTO(Lamp lamp) {
        this.powerSupplyType = lamp.getPowerSupplyType();
        this.powerUsage = lamp.getPowerUsage();
        this.automaticMode = lamp.getAutomaticMode();
        this.lightOn = lamp.getLightOn();
    }

    public LampDTO(PowerSupplyType powerSupplyType, Integer powerUsage, Boolean automaticMode, Boolean lightOn) {
        this.powerSupplyType = powerSupplyType;
        this.powerUsage = powerUsage;
        this.automaticMode = automaticMode;
        this.lightOn = lightOn;
    }

    public PowerSupplyType getPowerSupplyType() {
        return powerSupplyType;
    }

    public void setPowerSupplyType(PowerSupplyType powerSupplyType) {
        this.powerSupplyType = powerSupplyType;
    }

    public Integer getPowerUsage() {
        return powerUsage;
    }

    public void setPowerUsage(Integer powerUsage) {
        this.powerUsage = powerUsage;
    }

    public Boolean getAutomaticMode() {
        return automaticMode;
    }

    public void setAutomaticMode(Boolean automaticMode) {
        this.automaticMode = automaticMode;
    }

    public Boolean getLightOn() {
        return lightOn;
    }

    public void setLightOn(Boolean lightOn) {
        this.lightOn = lightOn;
    }
}
