package com.iotsystem.teamtwo.devices.DTOs;

import javax.validation.constraints.NotNull;

public class DeviceDTO {
    @NotNull(message = "Device option is required")
    private String deviceOption;
    @NotNull(message = "Device code is required")
    private String deviceCode;
    @NotNull(message = "Device property is required")
    private Long property;
    @NotNull(message = "Device image is required")
    private String image;
    @NotNull(message = "Device name is required")
    private String name;
    private String powerSupply;
    private Integer powerUsage;
    private Integer numberOfConnections;
    private Integer power;
    private Integer batteryCapacity;
    private Integer numberOfPanels;
    private Integer panelSize;
    private Float panelEfficiency;
    private Integer minTemp;
    private Integer maxTemp;
    private Boolean automaticMode;
    private Boolean lightOn;
    private Boolean privateMode;
    private String allowedNumbers;
    private String wateringTime;
    private String wateringSchedule;

    public DeviceDTO() {
    }

    public String getDeviceCode() {
        return deviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }

    public Long getProperty() {
        return property;
    }

    public void setProperty(Long property) {
        this.property = property;
    }

    public String getDeviceOption() {
        return deviceOption;
    }

    public void setDeviceOption(String deviceOption) {
        this.deviceOption = deviceOption;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPowerSupply() {
        return powerSupply;
    }

    public void setPowerSupply(String powerSupply) {
        this.powerSupply = powerSupply;
    }

    public Integer getPowerUsage() {
        return powerUsage;
    }

    public void setPowerUsage(Integer powerUsage) {
        this.powerUsage = powerUsage;
    }

    public Integer getNumberOfConnections() {
        return numberOfConnections;
    }

    public void setNumberOfConnections(Integer numberOfConnections) {
        this.numberOfConnections = numberOfConnections;
    }

    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public Integer getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(Integer batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public Integer getPanelSize() {
        return panelSize;
    }

    public void setPanelSize(Integer panelSize) {
        this.panelSize = panelSize;
    }

    public Float getPanelEfficiency() {
        return panelEfficiency;
    }

    public void setPanelEfficiency(Float panelEfficiency) {
        this.panelEfficiency = panelEfficiency;
    }

    public Integer getNumberOfPanels() {
        return numberOfPanels;
    }

    public void setNumberOfPanels(Integer numberOfPanels) {
        this.numberOfPanels = numberOfPanels;
    }

    public Integer getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(Integer minTemp) {
        this.minTemp = minTemp;
    }

    public Integer getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(Integer maxTemp) {
        this.maxTemp = maxTemp;
    }

    public Boolean getAutomaticMode() {
        return automaticMode;
    }

    public void setAutomaticMode(Boolean automaticMode) {
        this.automaticMode = automaticMode;
    }

    public Boolean getLightOn() {
        return lightOn;
    }

    public void setLightOn(Boolean lightOn) {
        this.lightOn = lightOn;
    }

    public Boolean getPrivateMode() {
        return privateMode;
    }

    public void setPrivateMode(Boolean privateMode) {
        this.privateMode = privateMode;
    }

    public String getAllowedNumbers() {
        return allowedNumbers;
    }

    public void setAllowedNumbers(String allowedNumbers) {
        this.allowedNumbers = allowedNumbers;
    }

    public String getWateringTime() {
        return wateringTime;
    }

    public void setWateringTime(String wateringTime) {
        this.wateringTime = wateringTime;
    }

    public String getWateringSchedule() {
        return wateringSchedule;
    }

    public void setWateringSchedule(String wateringSchedule) {
        this.wateringSchedule = wateringSchedule;
    }
}
