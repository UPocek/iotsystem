package com.iotsystem.teamtwo.devices.DTOs;

public class CommandDTO {
    private String command;
    private String userFullName;
    private Long userId;

    public CommandDTO() {
    }

    public CommandDTO(String command, String userFullName, Long userId) {
        this.command = command;
        this.userFullName = userFullName;
        this.userId = userId;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
