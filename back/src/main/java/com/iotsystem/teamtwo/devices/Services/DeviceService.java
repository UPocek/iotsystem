package com.iotsystem.teamtwo.devices.Services;

import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.QueryApi;
import com.influxdb.query.FluxRecord;
import com.influxdb.query.FluxTable;
import com.iotsystem.teamtwo.apartments.DTO.ApartmentInfoDTO;
import com.iotsystem.teamtwo.apartments.repositories.IApartmentRepository;
import com.iotsystem.teamtwo.devices.DTOs.*;
import com.iotsystem.teamtwo.devices.Enums.AirConditioningModes;
import com.iotsystem.teamtwo.devices.Enums.DeviceOption;
import com.iotsystem.teamtwo.devices.Enums.WashingMachineProgram;
import com.iotsystem.teamtwo.devices.RedisModels.Status;
import com.iotsystem.teamtwo.devices.Repositories.*;
import com.iotsystem.teamtwo.helper.IHelperService;
import com.iotsystem.teamtwo.models.*;
import com.iotsystem.teamtwo.user.DTOs.RegularUserDTO;
import com.iotsystem.teamtwo.user.Services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;


@Service
public class DeviceService implements IDeviceService {
    @Autowired
    private IStatusRepository statusRepository;
    @Autowired
    private IHelperService helperService;
    @Autowired
    private IUserService userService;
    @Autowired
    private IDeviceRepository deviceRepository;
    @Autowired
    private IAmbientConditionsRepository ambientConditionsRepository;
    @Autowired
    private IAirConditioningRepository airConditioningRepository;
    @Autowired
    private IWashingMachineRepository washingMachineRepository;
    @Autowired
    private IVehicleGateRepository vehicleGateRepository;
    @Autowired
    private ILampRepository lampRepository;
    @Autowired
    private ISprinklersRepository sprinklersRepository;
    @Autowired
    private IBatteryRepository batteryRepository;
    @Autowired
    private ISolarSystemRepository solarSystemRepository;
    @Autowired
    private IChargerRepository chargerRepository;
    @Autowired
    private IApartmentRepository apartmentRepository;
    @Autowired
    private InfluxDBClient influxDBClient;
    @Autowired
    private IScheduleRepository scheduleRepository;
    @Value("${influxdb.bucket}")
    private String bucket;


    @Override
    public Long addNewDevice(DeviceDTO deviceDTO) {
        Long generatedId = -1L;
        Apartment apartment = apartmentRepository.findById(deviceDTO.getProperty()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Provided property doesn't exist."));

        switch (DeviceOption.valueOf(deviceDTO.getDeviceOption())) {
            case AMBIENTSENSORS:
                basicValidation(deviceDTO);

                AmbientConditions newAmbientConditions = new AmbientConditions(deviceDTO, apartment);

                ambientConditionsRepository.saveAndFlush(newAmbientConditions);
                generatedId = newAmbientConditions.getId();
                apartment.getAmbientConditionsList().add(newAmbientConditions);
                break;
            case AIRCONDITIONING:
                basicValidation(deviceDTO);

                AirConditioning newAirConditioning = new AirConditioning(deviceDTO, apartment);

                airConditioningRepository.saveAndFlush(newAirConditioning);
                generatedId = newAirConditioning.getId();
                apartment.getAirConditioningList().add(newAirConditioning);
                break;
            case WASHINGMACHINE:
                basicValidation(deviceDTO);

                WashingMachine newWashingMachine = new WashingMachine(deviceDTO, apartment);

                washingMachineRepository.saveAndFlush(newWashingMachine);
                generatedId = newWashingMachine.getId();
                apartment.getWashingMachineList().add(newWashingMachine);
                break;
            case LAMP:
                basicValidation(deviceDTO);

                Lamp newLamp = new Lamp(deviceDTO, apartment);

                lampRepository.saveAndFlush(newLamp);
                generatedId = newLamp.getId();
                apartment.getLampList().add(newLamp);
                break;
            case SPRINKLERS:
                basicValidation(deviceDTO);

                SprinklerSystem newSprinklerSystem = new SprinklerSystem(deviceDTO, apartment);

                sprinklersRepository.saveAndFlush(newSprinklerSystem);
                generatedId = newSprinklerSystem.getId();
                apartment.getSprinklerSystemList().add(newSprinklerSystem);
                break;
            case GATE:
                basicValidation(deviceDTO);

                VehicleGate newVehicleGate = new VehicleGate(deviceDTO, apartment);

                vehicleGateRepository.saveAndFlush(newVehicleGate);
                generatedId = newVehicleGate.getId();
                apartment.getVehicleGateList().add(newVehicleGate);
                break;
            case SOLAR_SYSTEM:
                if (deviceDTO.getPanelEfficiency() == null) {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Device does not provide panel efficiency amount.");
                }
                if (deviceDTO.getPanelSize() == null) {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Device does not provide panel size.");
                }
                if (deviceDTO.getNumberOfPanels() == null) {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Device does not provide number of panels.");
                }

                SolarSystem newSolarSystem = new SolarSystem(deviceDTO, apartment);

                solarSystemRepository.saveAndFlush(newSolarSystem);
                generatedId = newSolarSystem.getId();
                apartment.getSolarSystemList().add(newSolarSystem);
                break;
            case BATTERY:
                if (deviceDTO.getBatteryCapacity() == null) {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Device does not provide battery capacity.");
                }

                Battery newBattery = new Battery(deviceDTO, apartment);

                batteryRepository.saveAndFlush(newBattery);
                generatedId = newBattery.getId();
                apartment.getBatteryList().add(newBattery);
                break;
            case CHARGER:
                if (deviceDTO.getNumberOfConnections() == null) {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Device does not provide number of charging connections.");
                }
                if (deviceDTO.getPower() == null) {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Device does not provide charging power.");
                }

                Charger newCharger = new Charger(deviceDTO, apartment);

                chargerRepository.saveAndFlush(newCharger);
                generatedId = newCharger.getId();
                apartment.getChargerList().add(newCharger);
                break;
        }

        if (generatedId == -1L) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Couldn't create device.");
        }
        sendCredentials(deviceDTO.getDeviceCode(), generatedId, deviceDTO.getProperty());
        apartmentRepository.saveAndFlush(apartment);
        sendPropertyInfo(apartment);
        helperService.saveImage(deviceDTO.getImage(), "device/" + generatedId + ".jpg");

        return generatedId;
    }

    @Override
    public boolean turnOffOn(Long deviceId, CommandDTO commandDTO, String username) {
        RegularUser u = userService.getByUsername(username).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        Device device = deviceRepository.findById(deviceId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        boolean newStatus = Objects.equals(commandDTO.getCommand(), "turn_on");
        device.setOnline(newStatus);
        deviceRepository.saveAndFlush(device);
        commandDTO.setUserFullName(u.getName() + "_" + u.getSurname());
        commandDTO.setUserId(u.getId());
        this.helperService.publishToMqttBroker("server-command/" + deviceId, commandDTO, false);
        return newStatus;
    }

    @Override
    public void setFullnessOnCharger(Long deviceId, Long apartmentId, CommandDTO commandDTO, Principal user) {
        RegularUser u = userService.getByUsername(user.getName()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        Charger charger = chargerRepository.findById(deviceId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        charger.setFullness(Double.valueOf(commandDTO.getCommand().split("/")[1]));
        chargerRepository.saveAndFlush(charger);
        commandDTO.setUserFullName(u.getName() + "_" + u.getSurname());
        commandDTO.setUserId(u.getId());
        this.helperService.publishToMqttBroker("server-command/" + deviceId, commandDTO, false);
        Apartment apartment = apartmentRepository.findById(apartmentId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        sendPropertyInfo(apartment);
    }

    public void deleteSchedule(Long scheduleId) {
        scheduleRepository.deleteById(scheduleId);
    }

    @Override
    public boolean switchAirConditioningMode(Long deviceId, CommandDTO commandDTO, String userUsername) {
        RegularUser u = userService.getByUsername(userUsername).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        AirConditioning device = airConditioningRepository.findById(deviceId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        String[] tokens = commandDTO.getCommand().split("_");
        if (tokens.length < 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        switch (tokens[0].toLowerCase()) {
            case "cool":
                device.setMode(AirConditioningModes.COOL);
                break;
            case "heat":
                device.setMode(AirConditioningModes.HEAT);
                break;
            case "dry":
                device.setMode(AirConditioningModes.DRY);
                break;
            case "auto":
                device.setMode(AirConditioningModes.AUTO);
                break;
            default:
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid command.");
        }
        try {
            device.setCurrentTemp(Integer.valueOf(tokens[1]));
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid command.");
        }
        airConditioningRepository.saveAndFlush(device);
        commandDTO.setUserFullName(u.getName() + "_" + u.getSurname());
        commandDTO.setUserId(u.getId());
        this.helperService.publishToMqttBroker("server-command/" + deviceId, commandDTO, false);
        return true;
    }

    @Override
    public WashingMachineDTO switchWashingMachineMode(Long deviceId, CommandDTO commandDTO, String userUsername) {
        RegularUser initiator = userService.getByUsername(userUsername).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        WashingMachine device = washingMachineRepository.findById(deviceId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        if (device.getCurrentProgram() != WashingMachineProgram.OFF && !commandDTO.getCommand().equalsIgnoreCase("off")) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Program already running");
        }
        switch (commandDTO.getCommand()) {
            case "black30":
                device.setCurrentProgram(WashingMachineProgram.BLACK30);
                break;
            case "black45":
                device.setCurrentProgram(WashingMachineProgram.BLACK45);
                break;
            case "white60":
                device.setCurrentProgram(WashingMachineProgram.WHITE60);
                break;
            case "white90":
                device.setCurrentProgram(WashingMachineProgram.WHITE90);
                break;
            case "color45":
                device.setCurrentProgram(WashingMachineProgram.COLOR45);
                break;
            case "off":
                device.setCurrentProgram(WashingMachineProgram.OFF);
                break;
            default:
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid command.");
        }
        device.setProgramEnd(LocalDateTime.now().plusHours(2));
        device.setProgramInitiatorId(initiator.getId());
        washingMachineRepository.saveAndFlush(device);
        commandDTO.setUserFullName(initiator.getName() + "_" + initiator.getSurname());
        commandDTO.setUserId(initiator.getId());
        this.helperService.publishToMqttBroker("server-command/" + deviceId, commandDTO, false);
        return new WashingMachineDTO(device);
    }

    @Override
    public LampDTO switchLampMode(Long deviceId, CommandDTO commandDTO, String userUsername) {
        RegularUser initiator = userService.getByUsername(userUsername).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        Lamp device = lampRepository.findById(deviceId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        switch (commandDTO.getCommand()) {
            case "manual" -> {
                device.setAutomaticMode(false);
                device.setLightOn(false);
            }
            case "auto" -> device.setAutomaticMode(true);
            case "light-off" -> device.setLightOn(false);
            case "light-on" -> device.setLightOn(true);
            default -> throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid command.");
        }
        lampRepository.saveAndFlush(device);
        commandDTO.setUserFullName(initiator.getName() + "_" + initiator.getSurname());
        commandDTO.setUserId(initiator.getId());
        this.helperService.publishToMqttBroker("server-command/" + deviceId, commandDTO, false);
        return new LampDTO(device);
    }

    @Override
    public VehicleGateDTO switchGatesMode(Long deviceId, CommandDTO commandDTO, String name) {
        RegularUser initiator = userService.getByUsername(name).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        VehicleGate device = vehicleGateRepository.findById(deviceId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        String command = commandDTO.getCommand();
        if (command.equals("private")) {
            device.setPrivateMode(true);
        } else if (command.equals("public")) {
            device.setPrivateMode(false);
        } else if (command.startsWith("add-number/")) {
            String number = command.split("/")[1];
            if (!device.getAllowedNumbers().contains("[")) {
                device.setAllowedNumbers("[\"" + number + "\"]");
            } else {
                String prefix = device.getAllowedNumbers().split("]")[0];
                device.setAllowedNumbers(prefix + ",\"" + number + "\"]");
            }
        } else if (command.startsWith("remove-number/")) {
            String number = command.split("/")[1];
            String numbers = device.getAllowedNumbers();
            numbers = numbers.replace("\"" + number + "\",", "");
            numbers = numbers.replace("\"" + number + "\"]", "]");
            device.setAllowedNumbers(numbers);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        vehicleGateRepository.saveAndFlush(device);
        commandDTO.setUserFullName(initiator.getName() + "_" + initiator.getSurname());
        commandDTO.setUserId(initiator.getId());
        this.helperService.publishToMqttBroker("server-command/" + deviceId, commandDTO, false);
        return new VehicleGateDTO(device);
    }

    @Override
    public SprinklerSystemDTO switchSprinklerMode(Long deviceId, CommandDTO commandDTO, String name) {
        RegularUser initiator = userService.getByUsername(name).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        SprinklerSystem device = sprinklersRepository.findById(deviceId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        switch (commandDTO.getCommand()) {
            case "manual" -> {
                device.setAutomaticMode(false);
            }
            case "auto" -> device.setAutomaticMode(true);
        }
        if (commandDTO.getCommand().startsWith("new-time/")) {
            String time = commandDTO.getCommand().split("/")[1];
            device.setWateringTime(time);
        }
        if (commandDTO.getCommand().startsWith("new-schedule/")) {
            String schedule = commandDTO.getCommand().split("/")[1];
            device.setWateringSchedule(schedule);
        }
        sprinklersRepository.saveAndFlush(device);
        commandDTO.setUserFullName(initiator.getName() + "_" + initiator.getSurname());
        commandDTO.setUserId(initiator.getId());
        this.helperService.publishToMqttBroker("server-command/" + deviceId, commandDTO, false);
        return new SprinklerSystemDTO(device);
    }

    @Override
    public VehicleGateDTO getGatesSettings(Long deviceId) {
        return new VehicleGateDTO(vehicleGateRepository.findById(deviceId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)));
    }

    @Override
    public SprinklerSystemDTO getSprinklerSettings(Long deviceId) {
        return new SprinklerSystemDTO(sprinklersRepository.findById(deviceId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)));
    }

    @Override
    public ScheduleDTO scheduleAirConditioning(Long deviceId, Integer temperature, String mode, String from, String to, Principal user) {
        LocalTime fromTime = LocalTime.parse(from);
        LocalTime toTime = LocalTime.parse(to);
        Schedule newSchedule = new Schedule(deviceId, String.format("%s_%d", mode, temperature), user.getName(), fromTime, toTime, DeviceOption.AIRCONDITIONING);
        scheduleRepository.save(newSchedule);
        return new ScheduleDTO(newSchedule);
    }

    @Override
    public ScheduleDTO scheduleWashingMachine(Long deviceId, String program, String from, Principal user) {
        LocalTime fromTime = LocalTime.parse(from);
        LocalTime toTime = fromTime.plusHours(2);
        Schedule newSchedule = new Schedule(deviceId, program, user.getName(), fromTime, toTime, DeviceOption.WASHINGMACHINE);
        scheduleRepository.save(newSchedule);
        return new ScheduleDTO(newSchedule);
    }

    @Override
    public List<ScheduleDTO> getDeviceSchedules(Long deviceId) {
        return scheduleRepository.findAllByDeviceId(deviceId).stream().map(ScheduleDTO::new).toList();
    }

    @Override
    public AirConditioningDTO getAirConditioningSettings(Long deviceId) {
        return new AirConditioningDTO(airConditioningRepository.findById(deviceId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)));
    }

    @Override
    public WashingMachineDTO getWashingMachineSettings(Long deviceId) {
        return new WashingMachineDTO(washingMachineRepository.findById(deviceId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)));
    }

    @Override
    public LampDTO getLampSettings(Long deviceId) {
        return new LampDTO(lampRepository.findById(deviceId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)));
    }

    @Override
    public Set<Device> findAllDevicesInApartment(Long apartmentId) {
        return deviceRepository.findAllByApartmentId(apartmentId);
    }

    @Override
    public RegularUserDTO giveAccessToDevice(Long deviceId, String toUsername, Principal owner) {
        Device deviceToShare = deviceRepository.findById(deviceId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Device with that id doesn't exists"));
        RegularUser userToShareWith = userService.getByUsername(toUsername).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User with that username does not exists"));
        if (userToShareWith.getUsername().equals(owner.getName())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You can't share device with yourself");
        }
        deviceToShare.getUsersWithAccess().add(userToShareWith);
        try {
            deviceRepository.save(deviceToShare);
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE);
        }

        return new RegularUserDTO(userToShareWith);
    }

    @Override
    public void removeAccessToDevice(Long deviceId, String toUsername, Principal owner) {
        Device deviceToShare = deviceRepository.findById(deviceId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Device with that id doesn't exists"));
        if (deviceToShare.getUsersWithAccess().stream().noneMatch(user -> user.getUsername().equals(toUsername))) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Can't remove access to this user!");
        }
        deviceToShare.setUsersWithAccess(deviceToShare.getUsersWithAccess().stream().filter(item -> !item.getUsername().equals(toUsername)).collect(Collectors.toSet()));
        deviceRepository.save(deviceToShare);
    }


    @Override
    public ApartmentStatusDTO subscribeToDevicesStatus(Long apartmentId, Principal user) {
        if (statusRepository.existsByUser(user.getName())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User already subscribed to this apartment.");
        }
        statusRepository.save(new Status(user.getName(), apartmentId));
        Apartment apartmentToSubscribeTo = apartmentRepository.findById(apartmentId).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Apartment not found."));
        if (apartmentToSubscribeTo.getOwner().getUsername().equals(user.getName())) {
            return new ApartmentStatusDTO(apartmentToSubscribeTo);
        } else {
            RegularUser guest = userService.getByUsername(user.getName()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "User not found."));
            ApartmentStatusDTO apartmentStatusDTO = new ApartmentStatusDTO();
            apartmentStatusDTO.setAmbientConditionsList(apartmentToSubscribeTo.getAmbientConditionsList().stream().filter(device -> device.getUsersWithAccess().stream().anyMatch(user1 -> user1.getUsername().equals(guest.getUsername()))).map(DeviceStatusDTO::new).collect(Collectors.toList()));
            apartmentStatusDTO.setAirConditioningList(apartmentToSubscribeTo.getAirConditioningList().stream().filter(device -> device.getUsersWithAccess().stream().anyMatch(user1 -> user1.getUsername().equals(guest.getUsername()))).map(DeviceStatusDTO::new).collect(Collectors.toList()));
            apartmentStatusDTO.setWashingMachineList(apartmentToSubscribeTo.getWashingMachineList().stream().filter(device -> device.getUsersWithAccess().stream().anyMatch(user1 -> user1.getUsername().equals(guest.getUsername()))).map(DeviceStatusDTO::new).collect(Collectors.toList()));
            apartmentStatusDTO.setChargerList(apartmentToSubscribeTo.getChargerList().stream().filter(device -> device.getUsersWithAccess().stream().anyMatch(user1 -> user1.getUsername().equals(guest.getUsername()))).map(DeviceStatusDTO::new).collect(Collectors.toList()));
            apartmentStatusDTO.setSolarSystemList(apartmentToSubscribeTo.getSolarSystemList().stream().filter(device -> device.getUsersWithAccess().stream().anyMatch(user1 -> user1.getUsername().equals(guest.getUsername()))).map(DeviceStatusDTO::new).collect(Collectors.toList()));
            apartmentStatusDTO.setBatteryList(apartmentToSubscribeTo.getBatteryList().stream().filter(device -> device.getUsersWithAccess().stream().anyMatch(user1 -> user1.getUsername().equals(guest.getUsername()))).map(DeviceStatusDTO::new).collect(Collectors.toList()));
            apartmentStatusDTO.setVehicleGateList(apartmentToSubscribeTo.getVehicleGateList().stream().filter(device -> device.getUsersWithAccess().stream().anyMatch(user1 -> user1.getUsername().equals(guest.getUsername()))).map(DeviceStatusDTO::new).collect(Collectors.toList()));
            apartmentStatusDTO.setLampList(apartmentToSubscribeTo.getLampList().stream().filter(device -> device.getUsersWithAccess().stream().anyMatch(user1 -> user1.getUsername().equals(guest.getUsername()))).map(DeviceStatusDTO::new).collect(Collectors.toList()));
            apartmentStatusDTO.setSprinklerSystemList(apartmentToSubscribeTo.getSprinklerSystemList().stream().filter(device -> device.getUsersWithAccess().stream().anyMatch(user1 -> user1.getUsername().equals(guest.getUsername()))).map(DeviceStatusDTO::new).collect(Collectors.toList()));
            return apartmentStatusDTO;
        }
    }

    private void sendPropertyInfo(Apartment apartment) {
        ApartmentInfoDTO apartmentInfoDTO = new ApartmentInfoDTO(apartment);
        helperService.publishToMqttBroker("server-status/" + apartment.getId().toString(), apartmentInfoDTO, true);
    }

    private void sendCredentials(String deviceCode, Long deviceId, Long propertyId) {
        helperService.publishToMqttBroker("server-init/" + deviceCode, new SimulatorInitDTO(deviceId, propertyId), false);
    }

    private void basicValidation(DeviceDTO deviceDTO) {
        if (deviceDTO.getPowerSupply() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Device does not provide power supply type.");
        }
        if (deviceDTO.getPowerUsage() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Device does not provide power usage amount.");
        }
    }

    @Override
    public List<ActionDTO> getBasicActions(Long deviceId, Long userId, String startDate, String endDate, String deviceType) {
        List<ActionDTO> result = new ArrayList<>();
        List<FluxTable> tables = getActions(deviceId, userId, startDate, endDate, deviceType);
        for (FluxTable fluxTable : tables) {
            List<FluxRecord> records = fluxTable.getRecords();
            for (FluxRecord fluxRecord : records) {
                Long id;
                try {
                    id = Long.valueOf(fluxRecord.getValueByKey("user_id").toString());
                } catch (Exception e) {
                    id = -1L;
                }

                result.add(new ActionDTO(fluxRecord.getValue() != null ? (String) fluxRecord.getValue() : null, fluxRecord.getValueByKey("user_full_name").toString(), fluxRecord.getTime().toString(), id));
            }
        }
        return result;
    }

    @Override
    public ChargerActionsDTO getChargerActions(Long deviceId, Long userId, String startDate, String endDate) {
        List<ActionDTO> carChargingActions = new ArrayList<>();
        List<ActionDTO> chargingLimitActions = new ArrayList<>();
        List<ActionDTO> chargingTurnOnOffActions = new ArrayList<>();
        List<FluxTable> tables = getActions(deviceId, userId, startDate, endDate, "charger");
        for (FluxTable fluxTable : tables) {
            List<FluxRecord> records = fluxTable.getRecords();
            for (FluxRecord fluxRecord : records) {
                long id;
                try {
                    id = Long.parseLong(fluxRecord.getValueByKey("user_id").toString());
                } catch (Exception e) {
                    id = -1L;
                }

                if (fluxRecord.getValue().toString().startsWith("turn")) {
                    chargingTurnOnOffActions.add(new ActionDTO(fluxRecord.getValue() != null ? (String) fluxRecord.getValue() : null, fluxRecord.getValueByKey("user_full_name").toString(), fluxRecord.getTime().toString(), id));
                } else if (fluxRecord.getValue().toString().startsWith("percentage_battery_full")) {
                    chargingLimitActions.add(new ActionDTO(fluxRecord.getValue() != null ? Double.valueOf(fluxRecord.getValue().toString().split("/")[1]) : null, fluxRecord.getValueByKey("user_full_name").toString(), fluxRecord.getTime().toString(), id));
                } else if (fluxRecord.getValue().toString().startsWith("charging_ended")) {
                    carChargingActions.add(new ActionDTO(fluxRecord.getValue() != null ? Double.valueOf(fluxRecord.getValue().toString().split("/")[1]) : null, fluxRecord.getValueByKey("user_full_name").toString(), fluxRecord.getTime().toString(), id));
                }
            }
        }

        return new ChargerActionsDTO(userId == null ? aggregateCarCharging(carChargingActions) : carChargingActions, chargingLimitActions, chargingTurnOnOffActions);
    }

    @Override
    public List<DeviceWithTypeDTO> getDevicesSharedIndependentlyFromApartment(Principal user) {
        RegularUser userWithSharedDevices = userService.getByUsername(user.getName()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found"));
        List<Long> deviceIdsSharedThroughApartment = new ArrayList<>();
        userWithSharedDevices.getAccessToApartments().forEach(apartment -> deviceIdsSharedThroughApartment.addAll(apartment.getAllDevices().stream().map(Device::getId).toList()));
        return userWithSharedDevices.getAccessToDevices().stream().filter(device -> !deviceIdsSharedThroughApartment.contains(device.getId())).map(DeviceWithTypeDTO::new).sorted(Comparator.comparing(DeviceWithTypeDTO::getType)).toList();
    }

    private List<FluxTable> getActions(Long deviceId, Long userId, String startDate, String endDate, String deviceType) {
        String start = "1900-01-01";
        String end = "";
        String user = "";
        if (startDate != null && validStartDate(startDate)) {
            start = startDate;
        }
        if (endDate != null && validEndDate(startDate, endDate)) {
            end = String.format(", stop: %s", endDate);
        }
        if (userId != null) {
            user = String.format("  |> filter(fn: (r) => r[\"user_id\"] == \"%d\")\n", userId);
        }

        String fluxQuery = String.format(
                "from(bucket: \"%s\")\n" +
                        "  |> range(start: %s%s)\n" +
                        "  |> filter(fn: (r) => r[\"_measurement\"] == \"command\")\n" +
                        "  |> filter(fn: (r) => r[\"_field\"] == \"command\")\n" + "%s" +
                        "  |> filter(fn: (r) => r[\"device_type\"] == \"%s\")"+
                        "  |> filter(fn: (r) => r[\"device_id\"] == \"%d\")", this.bucket, start, end, user, deviceType, deviceId);

        QueryApi queryApi = this.influxDBClient.getQueryApi();
        return queryApi.query(fluxQuery);
    }

    private List<ActionDTO> aggregateCarCharging(List<ActionDTO> actions) {
        List<ActionDTO> result = new ArrayList<>();

        if (!actions.isEmpty()) {
            String date = actions.get(0).getDate().split("T")[0];
            ActionDTO currentActionDay = new ActionDTO(date, 0.0);
            for (ActionDTO action : actions) {
                if (!action.getDate().split("T")[0].equals(date)) {
                    result.add(currentActionDay);
                    date = action.getDate().split("T")[0];
                    currentActionDay = new ActionDTO(date, 0.0);
                }
                currentActionDay.setValue(currentActionDay.getValue() + action.getValue());
            }
            result.add(currentActionDay);
        }

        return result;
    }

    private boolean validStartDate(String startDate) {
        try {
            LocalDate date = LocalDate.parse(startDate);

            return date.isBefore(LocalDate.now());
        } catch (Exception e) {
            return false;
        }
    }

    private boolean validEndDate(String startDate, String endDate) {
        try {
            LocalDate dateStart = LocalDate.parse(startDate);
            LocalDate dateEnd = LocalDate.parse(endDate);

            return dateStart.isBefore(dateEnd);
        } catch (Exception e) {
            return false;
        }
    }
}


