package com.iotsystem.teamtwo.devices.DTOs;

import com.iotsystem.teamtwo.models.Charger;

public class ChargerDTO {
    private Integer numberOfConnections;
    private Integer power;
    private Double fullness;

    public ChargerDTO() {
    }

    public ChargerDTO(Charger  charger){
        this.numberOfConnections = charger.getNumberOfConnections();
        this.power = charger.getPower();
        this.fullness = charger.getFullness();
    }

    public ChargerDTO(String name, Integer numberOfConnections, Integer power, Double fullness) {
        this.numberOfConnections = numberOfConnections;
        this.power = power;
        this.fullness = fullness;
    }

    public Integer getNumberOfConnections() {
        return numberOfConnections;
    }

    public void setNumberOfConnections(Integer numberOfConnections) {
        this.numberOfConnections = numberOfConnections;
    }

    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public Double getFullness() {
        return fullness;
    }

    public void setFullness(Double fullness) {
        this.fullness = fullness;
    }
}
