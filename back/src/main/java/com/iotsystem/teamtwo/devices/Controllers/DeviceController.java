package com.iotsystem.teamtwo.devices.Controllers;

import com.iotsystem.teamtwo.devices.DTOs.*;
import com.iotsystem.teamtwo.devices.Services.*;
import com.iotsystem.teamtwo.security.annotations.CheckHasAccess;
import com.iotsystem.teamtwo.user.DTOs.RegularUserDTO;
import org.eclipse.paho.mqttv5.client.IMqttClient;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/device")
public class DeviceController {

    @Autowired
    private IMqttClient mqttClient;
    @Autowired
    private IDeviceService deviceService;

    @GetMapping("/schedule/{apartmentId}/{deviceId}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public List<ScheduleDTO> getDeviceSchedules(@PathVariable Long deviceId, @PathVariable Long apartmentId) {
        return deviceService.getDeviceSchedules(deviceId);
    }

    @GetMapping("/airConditioning/{apartmentId}/{deviceId}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public AirConditioningDTO getAirConditioningSettings(@PathVariable Long deviceId, @PathVariable Long apartmentId) {
        return deviceService.getAirConditioningSettings(deviceId);
    }

    @GetMapping("/washingMachine/{apartmentId}/{deviceId}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public WashingMachineDTO getWashingMachineSettings(@PathVariable Long deviceId, @PathVariable Long apartmentId) {
        return deviceService.getWashingMachineSettings(deviceId);
    }

    @GetMapping("/lamp/{apartmentId}/{deviceId}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public LampDTO getLampSettings(@PathVariable Long deviceId, @PathVariable Long apartmentId) {
        return deviceService.getLampSettings(deviceId);
    }

    @GetMapping("/gate/{apartmentId}/{deviceId}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public VehicleGateDTO getGatesSettings(@PathVariable Long deviceId, @PathVariable Long apartmentId) {
        return deviceService.getGatesSettings(deviceId);
    }

    @GetMapping("/sprinkler/{apartmentId}/{deviceId}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public SprinklerSystemDTO getSprinklerSettings(@PathVariable Long deviceId, @PathVariable Long apartmentId) {
        return deviceService.getSprinklerSettings(deviceId);
    }

    @GetMapping("/actions/solar/{apartmentId}/{deviceId}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public List<ActionDTO> getSolarActions(@PathVariable Long deviceId, @PathVariable Long apartmentId, @RequestParam(required = false) Long userId, @RequestParam(required = false) String startDate, @RequestParam(required = false) String endDate) {
        return deviceService.getBasicActions(deviceId, userId, startDate, endDate,"solarsystem");
    }

    @GetMapping("/actions/charge/{apartmentId}/{deviceId}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public ChargerActionsDTO getChargerActions(@PathVariable Long deviceId, @PathVariable Long apartmentId, @RequestParam(required = false) Long userId, @RequestParam(required = false) String startDate, @RequestParam(required = false) String endDate) {
        return deviceService.getChargerActions(deviceId, userId, startDate, endDate);
    }

    @GetMapping("/actions/airConditioning/{apartmentId}/{deviceId}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public List<ActionDTO> getAirConditioningActions(@PathVariable Long deviceId, @PathVariable Long apartmentId, @RequestParam(required = false) Long userId, @RequestParam(required = false) String startDate, @RequestParam(required = false) String endDate) {
        return deviceService.getBasicActions(deviceId, userId, startDate, endDate,"air_conditioning");
    }

    @GetMapping("/actions/washingMachine/{apartmentId}/{deviceId}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public List<ActionDTO> getWashingMachineActions(@PathVariable Long deviceId, @PathVariable Long apartmentId, @RequestParam(required = false) Long userId, @RequestParam(required = false) String startDate, @RequestParam(required = false) String endDate) {
        return deviceService.getBasicActions(deviceId, userId, startDate, endDate,"washing_machine");
    }

    @GetMapping("/shared")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public List<DeviceWithTypeDTO> getDevicesSharedIndependentlyFromApartment(Principal user) {
        return deviceService.getDevicesSharedIndependentlyFromApartment(user);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<Long> addNewDevice(@RequestBody DeviceDTO deviceDTO) {
        return new ResponseEntity<>(deviceService.addNewDevice(deviceDTO), HttpStatus.CREATED);
    }

    @PostMapping("/connect-device")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<Object> connectDevice(@RequestBody TopicSubscriptionDTO topic) throws MqttException {
        this.mqttClient.subscribe("device-setup/" + topic.getTopic(), 1);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/subscribeStatus/{apartmentId}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Apartment")
    public ApartmentStatusDTO subscribeToDevicesStatus(@PathVariable Long apartmentId, Principal user) {
        return deviceService.subscribeToDevicesStatus(apartmentId, user);
    }

    @PostMapping("/on-off/{apartmentId}/{deviceId}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public boolean turnOffOn(@PathVariable Long deviceId, @PathVariable Long apartmentId, @RequestBody CommandDTO commandDTO, Principal user) {
        return deviceService.turnOffOn(deviceId, commandDTO, user.getName());
    }

    @PostMapping("/fullness/{apartmentId}/{deviceId}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public ResponseEntity<String> setFullnessOnCharger(@PathVariable Long deviceId, @PathVariable Long apartmentId, @RequestBody CommandDTO commandDTO, Principal user) {
        deviceService.setFullnessOnCharger(deviceId, apartmentId, commandDTO, user);
        return new ResponseEntity<>("Fullness set successfully!", HttpStatus.OK);
    }

    @PostMapping("/airConditioning/{apartmentId}/{deviceId}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public boolean switchAirConditioningMode(@PathVariable Long deviceId, @PathVariable Long apartmentId, @RequestBody CommandDTO commandDTO, Principal user) {
        return deviceService.switchAirConditioningMode(deviceId, commandDTO, user.getName());
    }

    @PostMapping("/washingMachine/{apartmentId}/{deviceId}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public WashingMachineDTO switchWashingMachineMode(@PathVariable Long deviceId, @PathVariable Long apartmentId, @RequestBody CommandDTO commandDTO, Principal user) {
        return deviceService.switchWashingMachineMode(deviceId, commandDTO, user.getName());
    }

    @PostMapping("/lamp/{apartmentId}/{deviceId}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public LampDTO switchLampAutoMode(@PathVariable Long deviceId, @PathVariable Long apartmentId, @RequestBody CommandDTO commandDTO, Principal user) {
        return deviceService.switchLampMode(deviceId, commandDTO, user.getName());
    }

    @PostMapping("/gates/{apartmentId}/{deviceId}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public VehicleGateDTO switchGatesPrivateMode(@PathVariable Long deviceId, @PathVariable Long apartmentId, @RequestBody CommandDTO commandDTO, Principal user) {
        return deviceService.switchGatesMode(deviceId, commandDTO, user.getName());
    }

    @PostMapping("/sprinkler/{apartmentId}/{deviceId}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public SprinklerSystemDTO switchSprinklerMode(@PathVariable Long deviceId, @PathVariable Long apartmentId, @RequestBody CommandDTO commandDTO, Principal user) {
        return deviceService.switchSprinklerMode(deviceId, commandDTO, user.getName());
    }

    @PostMapping("/airConditioning/schedule/{apartmentId}/{deviceId}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public ScheduleDTO scheduleAirConditioning(@PathVariable Long deviceId, @PathVariable Long apartmentId, @RequestParam Integer temp, @RequestParam String mode, @RequestParam String from, @RequestParam String to, Principal user) {
        return deviceService.scheduleAirConditioning(deviceId, temp, mode, from, to, user);
    }

    @PostMapping("/washingMachine/schedule/{apartmentId}/{deviceId}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public ScheduleDTO scheduleWashingMachine(@PathVariable Long deviceId, @PathVariable Long apartmentId, @RequestParam String program, @RequestParam String from, Principal user) {
        return deviceService.scheduleWashingMachine(deviceId, program, from, user);
    }

    @PostMapping("/giveAccess/{apartmentId}/{deviceId}/{toUsername}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public RegularUserDTO giveAccessToDevice(@PathVariable Long deviceId, @PathVariable Long apartmentId, @PathVariable String toUsername, Principal owner) {
        return deviceService.giveAccessToDevice(deviceId, toUsername, owner);
    }

    @PutMapping("/removeAccess/{apartmentId}/{deviceId}/{toUsername}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public ResponseEntity<Void> removeAccessToDevice(@PathVariable Long deviceId, @PathVariable Long apartmentId, @PathVariable String toUsername, Principal owner) {
        deviceService.removeAccessToDevice(deviceId, toUsername, owner);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/schedule/{scheduleId}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<String> deleteSchedule(@PathVariable Long scheduleId) {
        deviceService.deleteSchedule(scheduleId);
        return ResponseEntity.ok("Schedule successfully deleted");
    }
}
