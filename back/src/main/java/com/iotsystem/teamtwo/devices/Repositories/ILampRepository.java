package com.iotsystem.teamtwo.devices.Repositories;

import com.iotsystem.teamtwo.models.Lamp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ILampRepository extends JpaRepository<Lamp, Long> {
}
