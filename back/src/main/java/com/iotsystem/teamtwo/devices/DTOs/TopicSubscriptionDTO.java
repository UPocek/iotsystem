package com.iotsystem.teamtwo.devices.DTOs;

public class TopicSubscriptionDTO {
    private String topic;

    public TopicSubscriptionDTO() {
    }

    public TopicSubscriptionDTO(String topic) {
        this.topic = topic;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    @Override
    public String toString() {
        return "TopicSubscription{" +
                "topic='" + topic + '\'' +
                '}';
    }

}
