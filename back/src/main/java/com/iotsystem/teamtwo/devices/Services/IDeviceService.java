package com.iotsystem.teamtwo.devices.Services;

import com.iotsystem.teamtwo.devices.DTOs.*;
import com.iotsystem.teamtwo.models.Device;
import com.iotsystem.teamtwo.user.DTOs.RegularUserDTO;

import java.security.Principal;

import java.util.List;

import java.util.Set;

public interface IDeviceService {

    Long addNewDevice(DeviceDTO deviceDTO);

    ApartmentStatusDTO subscribeToDevicesStatus(Long apartmentId, Principal user);

    boolean turnOffOn(Long deviceId, CommandDTO commandDTO, String username);

    void setFullnessOnCharger(Long deviceId, Long apartmentId, CommandDTO commandDTO, Principal user);

    void deleteSchedule(Long scheduleId);

    boolean switchAirConditioningMode(Long deviceId, CommandDTO commandDTO, String userUsername);

    WashingMachineDTO switchWashingMachineMode(Long deviceId, CommandDTO commandDTO, String userUsername);

    LampDTO switchLampMode(Long deviceId, CommandDTO commandDTO, String name);

    ScheduleDTO scheduleAirConditioning(Long deviceId, Integer temperature, String mode, String from, String to, Principal user);

    ScheduleDTO scheduleWashingMachine(Long deviceId, String program, String from, Principal user);

    List<ScheduleDTO> getDeviceSchedules(Long deviceId);

    AirConditioningDTO getAirConditioningSettings(Long deviceId);

    WashingMachineDTO getWashingMachineSettings(Long deviceId);

    LampDTO getLampSettings(Long deviceId);

    ChargerActionsDTO getChargerActions(Long deviceId, Long userId, String startDate, String endDate);

    List<ActionDTO> getBasicActions(Long deviceId, Long userId, String startDate, String endDate, String deviceType);

    List<DeviceWithTypeDTO> getDevicesSharedIndependentlyFromApartment(Principal user);

    Set<Device> findAllDevicesInApartment(Long apartmentId);

    RegularUserDTO giveAccessToDevice(Long deviceId, String toUsername, Principal owner);

    void removeAccessToDevice(Long deviceId, String toUsername, Principal owner);

    VehicleGateDTO switchGatesMode(Long deviceId, CommandDTO commandDTO, String name);

    SprinklerSystemDTO switchSprinklerMode(Long deviceId, CommandDTO commandDTO, String name);

    VehicleGateDTO getGatesSettings(Long deviceId);

    SprinklerSystemDTO getSprinklerSettings(Long deviceId);
}
