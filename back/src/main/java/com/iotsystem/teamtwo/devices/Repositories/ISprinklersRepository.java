package com.iotsystem.teamtwo.devices.Repositories;

import com.iotsystem.teamtwo.models.SprinklerSystem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ISprinklersRepository extends JpaRepository<SprinklerSystem, Long> {
}
