package com.iotsystem.teamtwo.devices.Enums;

public enum PowerSupplyType {
    EXTERNAL_POWER, INTERNAL_POWER
}
