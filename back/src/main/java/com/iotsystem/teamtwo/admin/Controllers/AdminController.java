package com.iotsystem.teamtwo.admin.Controllers;

import com.iotsystem.teamtwo.admin.DTOs.AdminDTO;
import com.iotsystem.teamtwo.admin.DTOs.NewAdminDTO;
import com.iotsystem.teamtwo.admin.DTOs.PasswordChangedDTO;
import com.iotsystem.teamtwo.admin.Services.IAdminService;
import com.iotsystem.teamtwo.security.annotations.SuperAdminOnly;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/admin")
public class AdminController {

    @Autowired
    private IAdminService adminService;

    @PostMapping
    @SuperAdminOnly
    public ResponseEntity<String> register(@Valid @RequestBody NewAdminDTO newAdminDTO) {
        adminService.register(newAdminDTO);
        return new ResponseEntity<>("New Admin added successfully!", HttpStatus.CREATED);
    }

    @GetMapping
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public List<AdminDTO> getAll() {
        return adminService.getAll();
    }

    @GetMapping("/checkPassword")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<PasswordChangedDTO> checkPassword(Principal user) {
        return ResponseEntity.ok(adminService.checkPasswordChanged(user));
    }
}
