package com.iotsystem.teamtwo.admin.Services;

import com.iotsystem.teamtwo.admin.DTOs.AdminDTO;
import com.iotsystem.teamtwo.admin.DTOs.NewAdminDTO;
import com.iotsystem.teamtwo.admin.DTOs.PasswordChangedDTO;
import com.iotsystem.teamtwo.admin.Repositories.IAdminRepository;
import com.iotsystem.teamtwo.helper.IHelperService;
import com.iotsystem.teamtwo.models.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AdminService implements IAdminService {

    @Value("${company.email}")
    public String companyEmail;
    @Value("${company.url}")
    public String companyUrl;

    @Autowired
    private IHelperService helperService;
    @Autowired
    private IAdminRepository adminRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void register(NewAdminDTO adminCredentials) {
        if (existsByUsername(adminCredentials.getUsername())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Admin with that username already exists");
        }
        String password = helperService.generateSecretCode(8);
        String encodedPassword = passwordEncoder.encode(password);

        try {
            List<String> data = new ArrayList<>();
            data.add("Your admin account has been created on IOT");
            data.add("Login to " + companyUrl + " with username: " + adminCredentials.getUsername() + " and password: " + password);
            data.add(companyUrl);
            String htmlTemplate = helperService.prepareMailTemplate(data, "src/main/resources/templates/email.html", "Use this email as username for access, your password is: " + password);
            helperService.sendHtmlEmail(companyEmail, adminCredentials.getUsername(), "IOT - Verify your admin profile", htmlTemplate);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        adminRepository.save(new Admin(adminCredentials.getUsername(), encodedPassword, false, false));
    }

    @Override
    public List<AdminDTO> getAll() {
        return adminRepository.findAll().stream().map(AdminDTO::new).toList();
    }

    @Override
    public Optional<Admin> getSuperAdmin() {
        return adminRepository.findByIsSuper(true);
    }

    @Override
    public Optional<Admin> getByUsername(String username) {
        return adminRepository.findByUsername(username);
    }

    @Override
    public PasswordChangedDTO checkPasswordChanged(Principal user) {
        return new PasswordChangedDTO(adminRepository.findByUsername(user.getName()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)).getPasswordChanged());
    }

    @Override
    public void save(Admin admin) {
        adminRepository.save(admin);
    }

    @Override
    public boolean isSuperAdminPresent() {
        return adminRepository.existsByIsSuperTrue();
    }

    private boolean existsByUsername(String username) {
        return adminRepository.existsByUsername(username);
    }
}
