package com.iotsystem.teamtwo.admin.Services;


import com.iotsystem.teamtwo.admin.DTOs.AdminDTO;
import com.iotsystem.teamtwo.admin.DTOs.NewAdminDTO;
import com.iotsystem.teamtwo.admin.DTOs.PasswordChangedDTO;
import com.iotsystem.teamtwo.models.Admin;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

public interface IAdminService {

    void register(NewAdminDTO adminCredentials);

    List<AdminDTO> getAll();

    Optional<Admin> getSuperAdmin();

    Optional<Admin> getByUsername(String username);

    PasswordChangedDTO checkPasswordChanged(Principal user);

    void save(Admin admin);

    boolean isSuperAdminPresent();
}
