package com.iotsystem.teamtwo.admin.DTOs;

import com.iotsystem.teamtwo.models.Admin;

public class AdminDTO {
    private String username;

    public AdminDTO() {
    }

    public AdminDTO(Admin admin) {
        this.username = admin.getUsername();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
