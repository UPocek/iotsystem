package com.iotsystem.teamtwo.admin.DTOs;

public class PasswordChangedDTO {

    private Boolean passwordChanged;

    public PasswordChangedDTO() {
    }

    public PasswordChangedDTO(Boolean passwordChanged) {
        this.passwordChanged = passwordChanged;
    }

    public Boolean getPasswordChanged() {
        return passwordChanged;
    }

    public void setPasswordChanged(Boolean passwordChanged) {
        this.passwordChanged = passwordChanged;
    }
}
