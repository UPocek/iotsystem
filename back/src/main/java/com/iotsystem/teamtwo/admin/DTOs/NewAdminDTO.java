package com.iotsystem.teamtwo.admin.DTOs;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class NewAdminDTO {

    @NotBlank
    @Email(message = "Invalid email format")
    private String username;

    public NewAdminDTO() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
