package com.iotsystem.teamtwo.admin.Repositories;

import com.iotsystem.teamtwo.models.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IAdminRepository extends JpaRepository<Admin, Long> {
    Optional<Admin> findByUsername(String username);

    Optional<Admin> findByIsSuper(Boolean isSuper);

    boolean existsByIsSuperTrue();

    boolean existsByUsername(String username);
}
