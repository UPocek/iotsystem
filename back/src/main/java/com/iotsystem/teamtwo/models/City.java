package com.iotsystem.teamtwo.models;

import javax.persistence.*;


@Entity
public class City {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String city;
    private String cityAscii;
    @OneToOne
    private Location location;
    private String country;
    private String iso2;
    private String iso3;
    private String admin_name;
    private String capital;
    private String population;
    private boolean predefined;

    public City(String city, String cityAscii, Location location, String country, String iso2, String iso3, String admin_name, String capital, String population, boolean predefined) {
        this.city = city;
        this.cityAscii = cityAscii;
        this.location = location;
        this.country = country;
        this.iso2 = iso2;
        this.iso3 = iso3;
        this.admin_name = admin_name;
        this.capital = capital;
        this.population = population;
        this.predefined = predefined;
    }

    public City(String city, String country) {
        this.city = city;
        this.country = country;
    }

    public City() {

    }

    public Location getLocation() {
        return location;
    }

    public Integer getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public String getCityAscii() {
        return cityAscii;
    }

    public String getCountry() {
        return country;
    }

    public String getIso2() {
        return iso2;
    }

    public String getIso3() {
        return iso3;
    }

    public String getAdmin_name() {
        return admin_name;
    }

    public String getCapital() {
        return capital;
    }

    public String getPopulation() {
        return population;
    }

    public boolean isPredefined() {
        return predefined;
    }
}
