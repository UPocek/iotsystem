package com.iotsystem.teamtwo.models;

import com.iotsystem.teamtwo.devices.DTOs.DeviceDTO;
import com.iotsystem.teamtwo.devices.Enums.PowerSupplyType;

import javax.persistence.Entity;

@Entity
public class Battery extends Device{
    private Integer batteryCapacity;
    private Float level;

    public Battery() {
    }

    public Battery(DeviceDTO deviceDTO){
        super(deviceDTO.getName());
        this.batteryCapacity = deviceDTO.getBatteryCapacity();
        this.level = 0f;
    }

    public Battery(DeviceDTO deviceDTO, Apartment apartment){
        super(deviceDTO.getName());
        this.batteryCapacity = deviceDTO.getBatteryCapacity();
        this.level = 0f;
        this.setApartment(apartment);
    }

    public Integer getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(Integer batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public Float getLevel() {
        return level;
    }

    public void setLevel(Float level) {
        this.level = level;
    }
}
