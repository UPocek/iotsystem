package com.iotsystem.teamtwo.models;

import com.iotsystem.teamtwo.devices.DTOs.DeviceDTO;
import com.iotsystem.teamtwo.devices.Enums.PowerSupplyType;

import javax.persistence.Entity;

@Entity
public class Lamp extends Device{
    private PowerSupplyType powerSupplyType;
    private Integer powerUsage;
    private Boolean automaticMode;
    private Boolean lightOn;

    public Lamp() {
    }

    public Lamp(String name, PowerSupplyType powerSupplyType, Integer powerUsage, Boolean automaticMode, Boolean lightOn) {
        super(name);
        this.powerSupplyType = powerSupplyType;
        this.powerUsage = powerUsage;
        this.automaticMode = automaticMode;
        this.lightOn = lightOn;
    }

    public Lamp(DeviceDTO deviceDTO){
        super(deviceDTO.getName());
        this.powerSupplyType = PowerSupplyType.valueOf(deviceDTO.getPowerSupply());
        this.powerUsage = deviceDTO.getPowerUsage();
        this.automaticMode = deviceDTO.getAutomaticMode();
        this.lightOn = deviceDTO.getLightOn();
    }

    public Lamp(DeviceDTO deviceDTO, Apartment apartment){
        super(deviceDTO.getName());
        this.powerSupplyType = PowerSupplyType.valueOf(deviceDTO.getPowerSupply());
        this.powerUsage = deviceDTO.getPowerUsage();
        this.automaticMode = deviceDTO.getAutomaticMode();
        this.lightOn = deviceDTO.getLightOn();
        this.setApartment(apartment);
    }

    public PowerSupplyType getPowerSupplyType() {
        return powerSupplyType;
    }

    public void setPowerSupplyType(PowerSupplyType powerSupplyType) {
        this.powerSupplyType = powerSupplyType;
    }

    public Integer getPowerUsage() {
        return powerUsage;
    }

    public void setPowerUsage(Integer powerUsage) {
        this.powerUsage = powerUsage;
    }

    public Boolean getAutomaticMode() {
        return automaticMode;
    }

    public void setAutomaticMode(Boolean automaticMode) {
        this.automaticMode = automaticMode;
    }

    public Boolean getLightOn() {
        return lightOn;
    }

    public void setLightOn(Boolean lightOn) {
        this.lightOn = lightOn;
    }
}
