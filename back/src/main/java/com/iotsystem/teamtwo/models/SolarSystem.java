package com.iotsystem.teamtwo.models;

import com.iotsystem.teamtwo.devices.DTOs.DeviceDTO;

import javax.persistence.Entity;

@Entity
public class SolarSystem extends Device {
    private Integer numberOfPanels;
    private Integer panelSize;
    private Float panelEfficiency;
    private Integer powerUsage;

    public SolarSystem() {
    }

    public SolarSystem(String name, Integer panelSize, Float panelEfficiency, Integer powerUsage, Integer numberOfPanels) {
        super(name);
        this.panelSize = panelSize;
        this.panelEfficiency = panelEfficiency;
        this.powerUsage = powerUsage;
        this.numberOfPanels = numberOfPanels;
    }

    public SolarSystem(DeviceDTO deviceDTO) {
        super(deviceDTO.getName());
        this.panelSize = deviceDTO.getPanelSize();
        this.panelEfficiency = deviceDTO.getPanelEfficiency();
        this.powerUsage = deviceDTO.getPowerUsage();
        this.numberOfPanels = deviceDTO.getNumberOfPanels();
    }

    public SolarSystem(DeviceDTO deviceDTO, Apartment apartment) {
        super(deviceDTO.getName());
        this.panelSize = deviceDTO.getPanelSize();
        this.panelEfficiency = deviceDTO.getPanelEfficiency();
        this.powerUsage = deviceDTO.getPowerUsage();
        this.numberOfPanels = deviceDTO.getNumberOfPanels();
        this.setApartment(apartment);
    }

    public Integer getPanelSize() {
        return panelSize;
    }

    public void setPanelSize(Integer panelSize) {
        this.panelSize = panelSize;
    }

    public Float getPanelEfficiency() {
        return panelEfficiency;
    }

    public void setPanelEfficiency(Float panelEfficiency) {
        this.panelEfficiency = panelEfficiency;
    }

    public Integer getPowerUsage() {
        return powerUsage;
    }

    public void setPowerUsage(Integer powerUsage) {
        this.powerUsage = powerUsage;
    }

    public Integer getNumberOfPanels() {
        return numberOfPanels;
    }

    public void setNumberOfPanels(Integer numberOfPanels) {
        this.numberOfPanels = numberOfPanels;
    }
}
