package com.iotsystem.teamtwo.models;

import com.iotsystem.teamtwo.user.DTOs.NewUserDTO;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class RegularUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String surname;
    @Column(unique = true)
    private String username;
    private String password;
    private Boolean active;
    @OneToMany(mappedBy = "owner")
    private Set<Apartment> apartments;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "apartment_access",
            joinColumns = @JoinColumn(name = "person_with_access", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "shared_apartment", referencedColumnName = "id"))
    private Set<Apartment> accessToApartments = new HashSet<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "device_access",
            joinColumns = @JoinColumn(name = "person_with_access", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "shared_device", referencedColumnName = "id"))
    private Set<Device> accessToDevices = new HashSet<>();

    public RegularUser() {
    }

    public RegularUser(NewUserDTO newUser) {
        this.name = newUser.getName();
        this.surname = newUser.getSurname();
        this.username = newUser.getUsername();
        this.password = newUser.getPassword();
        this.active = false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Set<Apartment> getApartments() {
        return apartments;
    }

    public void setApartments(Set<Apartment> apartments) {
        this.apartments = apartments;
    }

    public Set<Apartment> getAccessToApartments() {
        return accessToApartments;
    }

    public void setAccessToApartments(Set<Apartment> accessToApartments) {
        this.accessToApartments = accessToApartments;
    }

    public Set<Device> getAccessToDevices() {
        return accessToDevices;
    }

    public void setAccessToDevices(Set<Device> accessToDevices) {
        this.accessToDevices = accessToDevices;
    }
}
