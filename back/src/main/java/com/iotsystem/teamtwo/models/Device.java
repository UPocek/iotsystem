package com.iotsystem.teamtwo.models;


import javax.persistence.*;

import java.util.HashSet;
import java.util.Set;

import static javax.persistence.InheritanceType.TABLE_PER_CLASS;

@Entity
@Inheritance(strategy = TABLE_PER_CLASS)
public abstract class Device {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    private String name;
    private boolean online;
    @ManyToOne
    private Apartment apartment;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "device_access",
            joinColumns = @JoinColumn(name = "shared_device", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "person_with_access", referencedColumnName = "id"))
    private Set<RegularUser> usersWithAccess = new HashSet<>();

    public Device() {
    }

    public Device(String name) {
        this.name = name;
        this.online = true;
    }

    public Device(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Device(Long id, String name, boolean online) {
        this.id = id;
        this.name = name;
        this.online = online;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public Apartment getApartment() {
        return apartment;
    }

    public void setApartment(Apartment apartment) {
        this.apartment = apartment;
    }

    public Set<RegularUser> getUsersWithAccess() {
        return usersWithAccess;
    }

    public void setUsersWithAccess(Set<RegularUser> usersWithAccess) {
        this.usersWithAccess = usersWithAccess;
    }
}
