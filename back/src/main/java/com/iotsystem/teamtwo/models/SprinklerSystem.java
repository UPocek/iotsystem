package com.iotsystem.teamtwo.models;

import com.iotsystem.teamtwo.devices.DTOs.DeviceDTO;
import com.iotsystem.teamtwo.devices.Enums.PowerSupplyType;

import javax.persistence.Entity;

@Entity
public class SprinklerSystem extends Device {
    private PowerSupplyType powerSupplyType;
    private Integer powerUsage;
    private Boolean automaticMode;
    private String wateringTime;
    private String wateringSchedule;

    public SprinklerSystem() {
    }

    public SprinklerSystem(String name, PowerSupplyType powerSupplyType, Integer powerUsage, Boolean automaticMode, String wateringTime, String wateringSchedule) {
        super(name);
        this.powerSupplyType = powerSupplyType;
        this.powerUsage = powerUsage;
        this.automaticMode = automaticMode;
        this.wateringTime = wateringTime;
        this.wateringSchedule = wateringSchedule;
    }

    public SprinklerSystem(DeviceDTO deviceDTO) {
        super(deviceDTO.getName());
        this.powerSupplyType = PowerSupplyType.valueOf(deviceDTO.getPowerSupply());
        this.powerUsage = deviceDTO.getPowerUsage();
        this.automaticMode = deviceDTO.getAutomaticMode();
        this.wateringTime = deviceDTO.getWateringTime();
        this.wateringSchedule = deviceDTO.getWateringSchedule();
    }

    public SprinklerSystem(DeviceDTO deviceDTO, Apartment apartment) {
        super(deviceDTO.getName());
        this.powerSupplyType = PowerSupplyType.valueOf(deviceDTO.getPowerSupply());
        this.powerUsage = deviceDTO.getPowerUsage();
        this.automaticMode = deviceDTO.getAutomaticMode();
        this.wateringTime = deviceDTO.getWateringTime();
        this.wateringSchedule = deviceDTO.getWateringSchedule();
        this.setApartment(apartment);
    }

    public PowerSupplyType getPowerSupplyType() {
        return powerSupplyType;
    }

    public void setPowerSupplyType(PowerSupplyType powerSupplyType) {
        this.powerSupplyType = powerSupplyType;
    }

    public Integer getPowerUsage() {
        return powerUsage;
    }

    public void setPowerUsage(Integer powerUsage) {
        this.powerUsage = powerUsage;
    }

    public Boolean getAutomaticMode() {
        return automaticMode;
    }

    public void setAutomaticMode(Boolean automaticMode) {
        this.automaticMode = automaticMode;
    }

    public String getWateringTime() {
        return wateringTime;
    }

    public void setWateringTime(String wateringTime) {
        this.wateringTime = wateringTime;
    }

    public String getWateringSchedule() {
        return wateringSchedule;
    }

    public void setWateringSchedule(String wateringSchedule) {
        this.wateringSchedule = wateringSchedule;
    }
}
