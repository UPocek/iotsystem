package com.iotsystem.teamtwo.models;

import com.iotsystem.teamtwo.apartments.DTO.CreateApartmentDTO;
import com.iotsystem.teamtwo.apartments.enums.ApartmentStatus;

import javax.persistence.*;
import javax.persistence.CascadeType;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Entity
public class Apartment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String address;
    @ManyToOne
    private City city;
    @OneToOne
    private Location location;
    @ManyToOne
    private RegularUser owner;
    private Integer squareFootage;
    private Integer numberOfFloors;
    private Integer numberOfRooms;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "apartment_access",
            joinColumns = @JoinColumn(name = "shared_apartment", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "person_with_access", referencedColumnName = "id"))
    private Set<RegularUser> usersWithAccess = new HashSet<>();
    @OneToMany(cascade = CascadeType.ALL)
    private Set<AmbientConditions> ambientConditionsList;
    @OneToMany(cascade = CascadeType.ALL)
    private Set<AirConditioning> airConditioningList;
    @OneToMany(cascade = CascadeType.ALL)
    private Set<WashingMachine> washingMachineList;
    @OneToMany(cascade = CascadeType.ALL)
    private Set<Lamp> lampList;
    @OneToMany(cascade = CascadeType.ALL)
    private Set<VehicleGate> vehicleGateList;
    @OneToMany(cascade = CascadeType.ALL)
    private Set<SprinklerSystem> sprinklerSystemList;
    @OneToMany(cascade = CascadeType.ALL)
    private Set<SolarSystem> solarSystemList;
    @OneToMany(cascade = CascadeType.ALL)
    private Set<Battery> batteryList;
    @OneToMany(cascade = CascadeType.ALL)
    private Set<Charger> chargerList;
    @Enumerated(EnumType.STRING)
    private ApartmentStatus status;

    public Apartment() {
    }

    public Apartment(String name, String address, City city, Location location, Integer squareFootage, Integer numberOfFloors, Integer numberOfRooms, ApartmentStatus status, RegularUser owner) {
        this.name = name;
        this.owner = owner;
        this.address = address;
        this.city = city;
        this.location = location;
        this.squareFootage = squareFootage;
        this.numberOfFloors = numberOfFloors;
        this.numberOfRooms = numberOfRooms;
        this.status = status;
        this.ambientConditionsList = new HashSet<>();
        this.airConditioningList = new HashSet<>();
        this.washingMachineList = new HashSet<>();
        this.lampList = new HashSet<>();
        this.vehicleGateList = new HashSet<>();
        this.sprinklerSystemList = new HashSet<>();
        this.solarSystemList = new HashSet<>();
        this.batteryList = new HashSet<>();
        this.chargerList = new HashSet<>();
    }

    public Apartment(CreateApartmentDTO apartmentDTO, RegularUser owner, Location location, City city) {
        this.name = apartmentDTO.getName();
        this.address = apartmentDTO.getAddress();
        this.squareFootage = apartmentDTO.getSquareFootage();
        this.numberOfFloors = apartmentDTO.getNumberOfFloors();
        this.numberOfRooms = apartmentDTO.getNumberOfRooms();

        this.owner = owner;
        this.location = location;
        this.city = city;

        this.status = ApartmentStatus.NEED_CONFIRMATION;
        this.ambientConditionsList = new HashSet<>();
        this.airConditioningList = new HashSet<>();
        this.washingMachineList = new HashSet<>();
        this.lampList = new HashSet<>();
        this.vehicleGateList = new HashSet<>();
        this.sprinklerSystemList = new HashSet<>();
        this.solarSystemList = new HashSet<>();
        this.batteryList = new HashSet<>();
        this.chargerList = new HashSet<>();
    }

    public List<Device> getAllDevices() {
        List<Device> devices = new ArrayList<>();
        devices.addAll(getAmbientConditionsList());
        devices.addAll(getAirConditioningList());
        devices.addAll(getWashingMachineList());
        devices.addAll(getLampList());
        devices.addAll(getVehicleGateList());
        devices.addAll(getSprinklerSystemList());
        devices.addAll(getSolarSystemList());
        devices.addAll(getBatteryList());
        devices.addAll(getChargerList());
        return devices;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public City getCity() {
        return city;
    }

    public Location getLocation() {
        return location;
    }

    public Integer getSquareFootage() {
        return squareFootage;
    }

    public Integer getNumberOfFloors() {
        return numberOfFloors;
    }

    public Integer getNumberOfRooms() {
        return numberOfRooms;
    }

    public ApartmentStatus getStatus() {
        return status;
    }

    public void setStatus(ApartmentStatus status) {
        this.status = status;
    }

    public RegularUser getOwner() {
        return owner;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setOwner(RegularUser owner) {
        this.owner = owner;
    }

    public void setSquareFootage(Integer squareFootage) {
        this.squareFootage = squareFootage;
    }

    public void setNumberOfFloors(Integer numberOfFloors) {
        this.numberOfFloors = numberOfFloors;
    }

    public void setNumberOfRooms(Integer numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    public Set<AmbientConditions> getAmbientConditionsList() {
        return ambientConditionsList;
    }

    public void setAmbientConditionsList(Set<AmbientConditions> ambientConditionsList) {
        this.ambientConditionsList = ambientConditionsList;
    }

    public Set<AirConditioning> getAirConditioningList() {
        return airConditioningList;
    }

    public void setAirConditioningList(Set<AirConditioning> airConditioningList) {
        this.airConditioningList = airConditioningList;
    }

    public Set<WashingMachine> getWashingMachineList() {
        return washingMachineList;
    }

    public void setWashingMachineList(Set<WashingMachine> washingMachineList) {
        this.washingMachineList = washingMachineList;
    }

    public Set<Lamp> getLampList() {
        return lampList;
    }

    public void setLampList(Set<Lamp> lampList) {
        this.lampList = lampList;
    }

    public Set<VehicleGate> getVehicleGateList() {
        return vehicleGateList;
    }

    public void setVehicleGateList(Set<VehicleGate> vehicleGateList) {
        this.vehicleGateList = vehicleGateList;
    }

    public Set<SprinklerSystem> getSprinklerSystemList() {
        return sprinklerSystemList;
    }

    public void setSprinklerSystemList(Set<SprinklerSystem> sprinklerSystemList) {
        this.sprinklerSystemList = sprinklerSystemList;
    }

    public Set<SolarSystem> getSolarSystemList() {
        return solarSystemList;
    }

    public void setSolarSystemList(Set<SolarSystem> solarSystemList) {
        this.solarSystemList = solarSystemList;
    }

    public Set<Battery> getBatteryList() {
        return batteryList;
    }

    public void setBatteryList(Set<Battery> batteryList) {
        this.batteryList = batteryList;
    }

    public Set<Charger> getChargerList() {
        return chargerList;
    }

    public void setChargerList(Set<Charger> chargerList) {
        this.chargerList = chargerList;
    }

    public Set<RegularUser> getUsersWithAccess() {
        return usersWithAccess;
    }

    public void setUsersWithAccess(Set<RegularUser> usersWithAccess) {
        this.usersWithAccess = usersWithAccess;
    }
}
