package com.iotsystem.teamtwo.models;

import com.iotsystem.teamtwo.devices.Enums.DeviceOption;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.time.LocalTime;

@Entity
public class Schedule {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @NotNull
    private Long deviceId;
    @NotNull
    private String command;
    @NotNull
    private String initiatorUsername;
    @NotNull
    private LocalTime fromTime;
    @NotNull
    private LocalTime toTime;
    @NotNull
    private Boolean active;
    @NotNull
    private DeviceOption deviceOption;

    public Schedule() {
    }

    public Schedule(Long deviceId, String command, String initiatorUsername, LocalTime fromTime, LocalTime toTime, DeviceOption deviceOption) {
        this.deviceId = deviceId;
        this.command = command;
        this.initiatorUsername = initiatorUsername;
        this.fromTime = fromTime;
        this.toTime = toTime;
        this.active = true;
        this.deviceOption = deviceOption;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getInitiatorUsername() {
        return initiatorUsername;
    }

    public void setInitiatorUsername(String initiatorUsername) {
        this.initiatorUsername = initiatorUsername;
    }

    public LocalTime getFromTime() {
        return fromTime;
    }

    public void setFromTime(LocalTime fromTime) {
        this.fromTime = fromTime;
    }

    public LocalTime getToTime() {
        return toTime;
    }

    public void setToTime(LocalTime toTime) {
        this.toTime = toTime;
    }

    public DeviceOption getDeviceOption() {
        return deviceOption;
    }

    public void setDeviceOption(DeviceOption deviceOption) {
        this.deviceOption = deviceOption;
    }
}
