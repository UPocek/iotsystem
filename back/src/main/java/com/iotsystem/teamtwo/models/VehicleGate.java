package com.iotsystem.teamtwo.models;

import com.iotsystem.teamtwo.devices.DTOs.DeviceDTO;
import com.iotsystem.teamtwo.devices.Enums.PowerSupplyType;

import javax.persistence.Entity;

@Entity
public class VehicleGate extends Device{
    private PowerSupplyType powerSupplyType;
    private Integer powerUsage;
    private Boolean privateMode;
    private String allowedNumbers;

    public VehicleGate() {
    }

    public VehicleGate(String name, PowerSupplyType powerSupplyType, Integer powerUsage, Boolean privateMode, String allowedNumbers) {
        super(name);
        this.powerSupplyType = powerSupplyType;
        this.powerUsage = powerUsage;
        this.privateMode = privateMode;
        this.allowedNumbers = allowedNumbers;
    }

    public VehicleGate(DeviceDTO deviceDTO, Apartment apartment){
        super(deviceDTO.getName());
        this.powerSupplyType = PowerSupplyType.valueOf(deviceDTO.getPowerSupply());
        this.powerUsage = deviceDTO.getPowerUsage();
        this.privateMode = deviceDTO.getPrivateMode();
        this.allowedNumbers = deviceDTO.getAllowedNumbers();
        this.setApartment(apartment);
    }

    public PowerSupplyType getPowerSupplyType() {
        return powerSupplyType;
    }

    public void setPowerSupplyType(PowerSupplyType powerSupplyType) {
        this.powerSupplyType = powerSupplyType;
    }

    public Integer getPowerUsage() {
        return powerUsage;
    }

    public void setPowerUsage(Integer powerUsage) {
        this.powerUsage = powerUsage;
    }

    public Boolean getPrivateMode() {
        return privateMode;
    }

    public void setPrivateMode(Boolean privateMode) {
        this.privateMode = privateMode;
    }

    public String getAllowedNumbers() {
        return allowedNumbers;
    }

    public void setAllowedNumbers(String allowedNumbers) {
        this.allowedNumbers = allowedNumbers;
    }
}
