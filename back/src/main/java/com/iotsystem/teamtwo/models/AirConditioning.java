package com.iotsystem.teamtwo.models;

import com.iotsystem.teamtwo.devices.DTOs.DeviceDTO;
import com.iotsystem.teamtwo.devices.Enums.AirConditioningModes;
import com.iotsystem.teamtwo.devices.Enums.PowerSupplyType;

import javax.persistence.Entity;

@Entity
public class AirConditioning extends Device {
    private PowerSupplyType powerSupplyType;
    private Integer powerUsage;
    private Integer minTemp;
    private Integer maxTemp;
    private Integer currentTemp;
    private AirConditioningModes mode;

    public AirConditioning() {
    }

    public AirConditioning(DeviceDTO deviceDTO) {
        super(deviceDTO.getName());
        this.powerSupplyType = PowerSupplyType.valueOf(deviceDTO.getPowerSupply());
        this.powerUsage = deviceDTO.getPowerUsage();
        this.minTemp = deviceDTO.getMinTemp();
        this.maxTemp = deviceDTO.getMaxTemp();
        this.currentTemp = deviceDTO.getMinTemp() + (deviceDTO.getMaxTemp() - deviceDTO.getMinTemp()) / 2;
        this.mode = AirConditioningModes.AUTO;
    }

    public AirConditioning(DeviceDTO deviceDTO, Apartment apartment) {
        super(deviceDTO.getName());
        this.powerSupplyType = PowerSupplyType.valueOf(deviceDTO.getPowerSupply());
        this.powerUsage = deviceDTO.getPowerUsage();
        this.minTemp = deviceDTO.getMinTemp();
        this.maxTemp = deviceDTO.getMaxTemp();
        this.currentTemp = deviceDTO.getMinTemp() + (deviceDTO.getMaxTemp() - deviceDTO.getMinTemp()) / 2;
        this.mode = AirConditioningModes.AUTO;
        this.setApartment(apartment);
    }

    public PowerSupplyType getPowerSupplyType() {
        return powerSupplyType;
    }

    public void setPowerSupplyType(PowerSupplyType powerSupplyType) {
        this.powerSupplyType = powerSupplyType;
    }

    public Integer getPowerUsage() {
        return powerUsage;
    }

    public void setPowerUsage(Integer powerUsage) {
        this.powerUsage = powerUsage;
    }

    public Integer getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(Integer minTemp) {
        this.minTemp = minTemp;
    }

    public Integer getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(Integer maxTemp) {
        this.maxTemp = maxTemp;
    }

    public Integer getCurrentTemp() {
        return currentTemp;
    }

    public void setCurrentTemp(Integer currentTemp) {
        this.currentTemp = currentTemp;
    }

    public AirConditioningModes getMode() {
        return mode;
    }

    public void setMode(AirConditioningModes mode) {
        this.mode = mode;
    }
}
