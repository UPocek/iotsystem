package com.iotsystem.teamtwo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeamtwoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeamtwoApplication.class, args);
	}

}
