package com.iotsystem.teamtwo.helper;

import com.iotsystem.teamtwo.apartments.DTO.ApartmentInfoDTO;
import com.iotsystem.teamtwo.devices.DTOs.CommandDTO;
import com.sendgrid.helpers.mail.Mail;

import java.io.IOException;
import java.util.List;

public interface IHelperService {

    void sendHtmlEmail(String emailFrom, String emailTo, String subject, String contentHtml) throws Exception;
    String prepareMailTemplate(List<String> data, String mailTemplatePath, String fallBackEmailString);

    String generateSecretCode(int length);

    String saveImage(String base64ImageString, String imageName);

    void publishToMqttBroker(String topic, Object content, Boolean retain);
}
