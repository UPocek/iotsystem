package com.iotsystem.teamtwo.helper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iotsystem.teamtwo.apartments.DTO.ApartmentInfoDTO;
import com.iotsystem.teamtwo.devices.DTOs.CommandDTO;
import com.iotsystem.teamtwo.devices.DTOs.MessageDTO;
import com.iotsystem.teamtwo.devices.DTOs.SimulatorInitDTO;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import org.apache.commons.codec.binary.Base64;
import org.eclipse.paho.mqttv5.client.IMqttClient;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@Service
public class HelperService implements IHelperService {
    @Autowired
    private IMqttClient mqttClient;

    @Autowired
    private ObjectMapper objectMapper;

    @Value("${sendgrid.key}")
    private String sendGridKey;

    @Value("${imagesFolder.location}")
    private String imagesFolderLocation;

    @Override
    public void sendHtmlEmail(String emailFrom, String emailTo, String subject, String contentHtml) throws Exception {
//        Email from = new Email(emailFrom);
//        Email to = new Email(emailTo);
//        Content content = new Content("text/html", contentHtml);
//        Mail mail = new Mail(from, subject, to, content);
//
//        SendGrid sg = new SendGrid(sendGridKey);
//        Request request = new Request();
//
//        request.setMethod(Method.POST);
//        request.setEndpoint("mail/send");
//        request.setBody(mail.build());
//        Response response = sg.api(request);
//        if (response.getStatusCode() != 200 && response.getStatusCode() != 202) {
//            throw new Exception("Email sending failed");
//        }
    }

    @Override
    public String prepareMailTemplate(List<String> data, String mailTemplatePath, String fallBackEmailString) {
        String html;
        try {
            html = Files.readString(Paths.get(mailTemplatePath));
            for (int i = 0; i < data.size(); i++) {
                html = html.replace("#?" + i, data.get(i));
            }
        } catch (IOException ex) {
            html = fallBackEmailString;
        }
        return html;
    }

    @Override
    public String generateSecretCode(int length) {
        String code = generateRandomString();
        return code.substring(0, length);
    }

    @Override
    public String saveImage(String base64ImageString, String imageName) {
        String imageDataBytes = base64ImageString.substring(base64ImageString.indexOf(",") + 1);

        byte[] data = Base64.decodeBase64(imageDataBytes);
        File imagesFolder = new File(imagesFolderLocation);

        if (!imagesFolder.exists()) {
            File apartmentFolder = new File(imagesFolderLocation + "apartment/");
            File deviceFolder = new File(imagesFolderLocation + "device/");
            File profileFolder = new File(imagesFolderLocation + "profile/");
            var result0 = imagesFolder.mkdir();
            var result1 = apartmentFolder.mkdir();
            var result2 = deviceFolder.mkdir();
            var result3 = profileFolder.mkdir();
            if (!result0 || !result1 || !result2 || !result3) {
                System.err.println("Images folder failed to create!");
            }
        }
        String localPath = imagesFolderLocation + imageName;
        try (OutputStream stream = new FileOutputStream(localPath)) {
            stream.write(data);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return imageName;
    }

    private String generateRandomString() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

    @Override
    public void publishToMqttBroker(String topic, Object content, Boolean retain) {
        try {
            byte[] messageContent = objectMapper.writeValueAsString(content).getBytes();
            MqttMessage message = new MqttMessage(messageContent);
            if (retain) {
                message.setRetained(true);
            }
            this.mqttClient.publish(topic, message);
        } catch (MqttException | JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
