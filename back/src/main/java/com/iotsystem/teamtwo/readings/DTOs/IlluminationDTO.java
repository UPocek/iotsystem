package com.iotsystem.teamtwo.readings.DTOs;

import java.util.List;

public class IlluminationDTO {
    List<GenericReadingDTO<Double>> illumination;
    List<GenericReadingDTO<Boolean>> on;
    List<GenericReadingDTO<Boolean>> auto;
    List<GenericReadingDTO<String>> command;

    public IlluminationDTO() {
    }

    public IlluminationDTO(List<GenericReadingDTO<Double>> illumination, List<GenericReadingDTO<Boolean>> on, List<GenericReadingDTO<Boolean>> auto, List<GenericReadingDTO<String>> command) {
        this.illumination = illumination;
        this.on = on;
        this.auto = auto;
        this.command = command;
    }

    public List<GenericReadingDTO<Double>> getIllumination() {
        return illumination;
    }

    public void setIllumination(List<GenericReadingDTO<Double>> illumination) {
        this.illumination = illumination;
    }

    public List<GenericReadingDTO<Boolean>> getOn() {
        return on;
    }

    public void setOn(List<GenericReadingDTO<Boolean>> on) {
        this.on = on;
    }

    public List<GenericReadingDTO<Boolean>> getAuto() {
        return auto;
    }

    public void setAuto(List<GenericReadingDTO<Boolean>> auto) {
        this.auto = auto;
    }

    public List<GenericReadingDTO<String>> getCommand() {
        return command;
    }

    public void setCommand(List<GenericReadingDTO<String>> command) {
        this.command = command;
    }
}
