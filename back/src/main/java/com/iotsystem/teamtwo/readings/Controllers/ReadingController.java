package com.iotsystem.teamtwo.readings.Controllers;

import com.iotsystem.teamtwo.readings.DTOs.*;
import com.iotsystem.teamtwo.readings.Services.IReadingService;
import com.iotsystem.teamtwo.security.annotations.CheckHasAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/api/readings")
public class ReadingController {

    @Autowired
    private IReadingService readingService;

    @GetMapping("/temperatureAndHumidity/live")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public TemperatureAndHumidityDTO getTemperatureAndHumidityReadingsLive(@RequestParam Long deviceId, @RequestParam Long apartmentId, Principal user) {
        return readingService.getTemperatureAndHumidityReadings(deviceId, user);
    }

    @GetMapping("/temperatureAndHumidity/history/{start}/{end}/{step}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public TemperatureAndHumidityDTO getTemperatureAndHumidityReadingsHistory(@PathVariable String start, @PathVariable String end, @PathVariable Integer step, @RequestParam Long deviceId, @RequestParam Long apartmentId) {
        return readingService.getTemperatureAndHumidityHistory(deviceId, start, end, step);
    }

    @GetMapping("/batterySystem/live")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Apartment")
    public BatterySystemDTO getBatterySystemReadingsLive(@RequestParam Long apartmentId,@RequestParam Long deviceId, Principal user) {
        return readingService.getGenerationAndConsumptionReadings(apartmentId,deviceId, user);
    }

    @GetMapping("/batterySystem/history/{propertyId}/{start}/{end}/{step}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Apartment")
    public BatterySystemDTO getBatterySystemReadingsHistory(@PathVariable Long propertyId, @PathVariable String start, @PathVariable String end, @PathVariable Integer step) {
        return readingService.getGenerationAndConsumptionHistory(propertyId, start, end, step);
    }

    @GetMapping("/batterySystem/apartment/{propertyId}/{start}/{end}/{step}")
    //@PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public BatterySystemDTO getBatterySystemReadingsForApartment(@PathVariable Long propertyId, @PathVariable String start, @PathVariable String end, @PathVariable Integer step) {
        return readingService.getGenerationAndConsumptionForApartment(propertyId, start, end, step);
    }

    @GetMapping("/batterySystem/city/{cityId}/{start}/{end}/{step}")
    //@PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public BatterySystemDTO getBatterySystemReadingsForCity(@PathVariable Long cityId, @PathVariable String start, @PathVariable String end, @PathVariable Integer step) {
        return readingService.getGenerationAndConsumptionForCity(cityId, start, end, step);
    }

    @GetMapping("/illumination")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public IlluminationDTO getIlluminationLive(@RequestParam Long deviceId, @RequestParam Long apartmentId, @RequestParam String fromTimestamp, @RequestParam String toTimestamp, Principal user) {
        return readingService.getIlluminationLiveReadings(deviceId, apartmentId, fromTimestamp, toTimestamp, user);
    }

    @GetMapping("/availability")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public AvailabilityDTO getAvailability(@RequestParam Long deviceId, @RequestParam Long apartmentId, @RequestParam String fromTimestamp, @RequestParam String toTimestamp, Principal user) {
        return readingService.getAvailability(deviceId, apartmentId, user, fromTimestamp, toTimestamp);
    }

    @GetMapping("/gates")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @CheckHasAccess(on = "Device")
    public GatesLogDTO getGates(@RequestParam Long deviceId, @RequestParam Long apartmentId, @RequestParam String fromTimestamp, @RequestParam String toTimestamp, Principal user) {
        return readingService.getGatesLog(deviceId, apartmentId, user, fromTimestamp, toTimestamp);
    }
}
