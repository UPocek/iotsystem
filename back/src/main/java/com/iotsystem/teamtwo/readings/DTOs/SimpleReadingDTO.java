package com.iotsystem.teamtwo.readings.DTOs;

public class SimpleReadingDTO {

    private Double value;
    private String timestamp;

    public SimpleReadingDTO() {
    }

    public SimpleReadingDTO(Double value, String timestamp) {
        this.value = value;
        this.timestamp = timestamp;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
