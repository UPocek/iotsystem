package com.iotsystem.teamtwo.readings.DTOs;

import java.util.List;

public class GatesLogDTO {
    List<GenericReadingDTO<String>> message;

    public GatesLogDTO() {
    }

    public GatesLogDTO(List<GenericReadingDTO<String>> message) {
        this.message = message;
    }

    public List<GenericReadingDTO<String>> getMessage() {
        return message;
    }

    public void setMessage(List<GenericReadingDTO<String>> message) {
        this.message = message;
    }
}
