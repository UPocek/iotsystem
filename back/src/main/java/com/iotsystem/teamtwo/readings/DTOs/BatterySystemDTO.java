package com.iotsystem.teamtwo.readings.DTOs;
import java.util.List;
public class BatterySystemDTO {
    List<SimpleReadingDTO> generation;
    List<SimpleReadingDTO> consumption;

    List<BatteryStatusDTO> batteries;

    public BatterySystemDTO() {
    }

    public BatterySystemDTO(List<SimpleReadingDTO> generation, List<SimpleReadingDTO> consumption, List<BatteryStatusDTO> batteries) {
        this.generation = generation;
        this.consumption = consumption;
        this.batteries = batteries;
    }

    public BatterySystemDTO(List<SimpleReadingDTO> generation, List<SimpleReadingDTO> consumption) {
        this.generation = generation;
        this.consumption = consumption;
        this.batteries = null;
    }

    public List<SimpleReadingDTO> getGeneration() {
        return generation;
    }

    public void setGeneration(List<SimpleReadingDTO> generation) {
        this.generation = generation;
    }

    public List<SimpleReadingDTO> getConsumption() {
        return consumption;
    }

    public void setConsumption(List<SimpleReadingDTO> consumption) {
        this.consumption = consumption;
    }

    public List<BatteryStatusDTO> getBatteries() {
        return batteries;
    }

    public void setBatteries(List<BatteryStatusDTO> batteries) {
        this.batteries = batteries;
    }
}
