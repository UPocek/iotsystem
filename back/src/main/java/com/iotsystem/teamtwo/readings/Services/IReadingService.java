package com.iotsystem.teamtwo.readings.Services;

import com.iotsystem.teamtwo.readings.DTOs.*;

import java.security.Principal;

public interface IReadingService {

    TemperatureAndHumidityDTO getTemperatureAndHumidityReadings(Long deviceId, Principal user);
    TemperatureAndHumidityDTO getTemperatureAndHumidityHistory(Long deviceId, String startDate, String endDate, Integer step);
    BatterySystemDTO getGenerationAndConsumptionReadings(Long apartmentId, Long deviceId,Principal user);
    BatterySystemDTO getGenerationAndConsumptionHistory(Long propertyId, String startDate, String endDate, Integer step);
    BatterySystemDTO getGenerationAndConsumptionForApartment(Long propertyId, String startDate, String endDate, Integer step);
    BatterySystemDTO getGenerationAndConsumptionForCity(Long cityId, String startDate, String endDate, Integer step);
    IlluminationDTO getIlluminationLiveReadings(Long deviceId, Long apartmentId, String fromTimestamp, String toTimestamp, Principal user);
    AvailabilityDTO getAvailability(Long deviceId, Long apartmentId, Principal user, String fromTimestamp, String toTimestamp);
    GatesLogDTO getGatesLog(Long deviceId, Long apartmentId, Principal user, String fromTimestamp, String toTimestamp);
}
