package com.iotsystem.teamtwo.readings.DTOs;

import java.util.List;

public class AvailabilityDTO {
    List<GenericReadingDTO<Double>> availability;

    public AvailabilityDTO() {
    }

    public AvailabilityDTO(List<GenericReadingDTO<Double>> availability) {
        this.availability = availability;
    }

    public List<GenericReadingDTO<Double>> getAvailability() {
        return availability;
    }

    public void setAvailability(List<GenericReadingDTO<Double>> availability) {
        this.availability = availability;
    }
}
