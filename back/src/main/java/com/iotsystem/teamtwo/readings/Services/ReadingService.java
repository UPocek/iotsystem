package com.iotsystem.teamtwo.readings.Services;

import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.QueryApi;
import com.influxdb.query.FluxRecord;
import com.influxdb.query.FluxTable;
import com.iotsystem.teamtwo.devices.RedisModels.Reading;
import com.iotsystem.teamtwo.devices.Repositories.IBatteryRepository;
import com.iotsystem.teamtwo.devices.Repositories.IReadingsRepository;
import com.iotsystem.teamtwo.readings.DTOs.BatteryStatusDTO;
import com.iotsystem.teamtwo.readings.DTOs.BatterySystemDTO;
import com.iotsystem.teamtwo.readings.DTOs.SimpleReadingDTO;
import com.iotsystem.teamtwo.readings.DTOs.TemperatureAndHumidityDTO;
import com.iotsystem.teamtwo.readings.DTOs.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class ReadingService implements IReadingService {

    @Value("${influxdb.readings.bucket.1m}")
    private String readingsBucket1m;
    @Value("${influxdb.readings.bucket.1h}")
    private String readingsBucket1h;
    @Value("${influxdb.energy_consumption.bucket.1m}")
    private String energyConsumptionBucket1m;
    @Value("${influxdb.energy_consumption.bucket.1h}")
    private String energyConsumptionBucket1h;
    @Value("${influxdb.bucket}")
    private String bucket;
    @Autowired
    private InfluxDBClient influxDBClient;
    @Autowired
    private IReadingsRepository readingsRepository;

    @Autowired
    private IBatteryRepository batteryRepository;

    @Override
    public TemperatureAndHumidityDTO getTemperatureAndHumidityReadings(Long deviceId, Principal user) {

        subscribeOnReadings(deviceId, user.getName(), "temphumid");

        List<SimpleReadingDTO> temperatures = new ArrayList<>();
        List<SimpleReadingDTO> humidities = new ArrayList<>();

        String fluxQuery = String.format(
                "from(bucket: \"%s\")\n" +
                        "  |> range(start: -1h)\n" +
                        "  |> filter(fn: (r) => r[\"_measurement\"] == \"readings\")\n" +
                        "  |> filter(fn: (r) => r[\"_field\"] == \"humidity\" or r[\"_field\"] == \"temperature\" )\n" +
                        "  |> filter(fn: (r) => r[\"device_id\"] == \"%d\")", this.readingsBucket1m, deviceId);
        QueryApi queryApi = this.influxDBClient.getQueryApi();
        List<FluxTable> tables = queryApi.query(fluxQuery);
        for (FluxTable fluxTable : tables) {
            List<FluxRecord> records = fluxTable.getRecords();
            for (FluxRecord fluxRecord : records) {
                if (Objects.equals(fluxRecord.getField(), "temperature")) {
                    temperatures.add(new SimpleReadingDTO(fluxRecord.getValue() != null ? (Double) fluxRecord.getValue() : null, fluxRecord.getTime().plusSeconds(3600).toString()));
                } else if (Objects.equals(fluxRecord.getField(), "humidity")) {
                    humidities.add(new SimpleReadingDTO(fluxRecord.getValue() != null ? (Double) fluxRecord.getValue() : null, fluxRecord.getTime().plusSeconds(3600).toString()));
                }
            }
        }

        return new TemperatureAndHumidityDTO(temperatures, humidities);
    }

    @Override
    public TemperatureAndHumidityDTO getTemperatureAndHumidityHistory(Long deviceId, String startDate, String endDate, Integer step) {
        List<SimpleReadingDTO> temperatures = new ArrayList<>();
        List<SimpleReadingDTO> humidities = new ArrayList<>();

        String fluxQuery = String.format(
                "from(bucket: \"%s\")\n" +
                        "  |> range(start: %s, stop: %s)\n" +
                        "  |> filter(fn: (r) => r[\"_measurement\"] == \"readings\")\n" +
                        "  |> filter(fn: (r) => r[\"_field\"] == \"humidity\" or r[\"_field\"] == \"temperature\" )\n" +
                        "  |> filter(fn: (r) => r[\"device_id\"] == \"%d\")" +
                        "  |> aggregateWindow(every: %dm, fn: mean)", step < 30 ? this.readingsBucket1m : this.readingsBucket1h, startDate, endDate, deviceId, step);
        QueryApi queryApi = this.influxDBClient.getQueryApi();
        List<FluxTable> tables = queryApi.query(fluxQuery);
        for (FluxTable fluxTable : tables) {
            List<FluxRecord> records = fluxTable.getRecords();
            for (FluxRecord fluxRecord : records) {
                if (Objects.equals(fluxRecord.getField(), "temperature")) {
                    temperatures.add(new SimpleReadingDTO(fluxRecord.getValue() != null ? (Double) fluxRecord.getValue() : null, fluxRecord.getTime().toString()));
                } else if (Objects.equals(fluxRecord.getField(), "humidity")) {
                    humidities.add(new SimpleReadingDTO(fluxRecord.getValue() != null ? (Double) fluxRecord.getValue() : null, fluxRecord.getTime().toString()));
                }
            }
        }

        return new TemperatureAndHumidityDTO(temperatures, humidities);
    }

    @Override
    public BatterySystemDTO getGenerationAndConsumptionReadings(Long apartmentId, Long deviceId, Principal user) {
        subscribeOnReadings(deviceId, user.getName(), "genconsum");

        List<SimpleReadingDTO> generations = new ArrayList<>();
        List<SimpleReadingDTO> consumptions = new ArrayList<>();

        String fluxQuery = String.format(
                "from(bucket: \"%s\")\n" +
                        "  |> range(start: -1h)\n" +
                        "  |> filter(fn: (r) => r[\"_measurement\"] == \"energy_consumption\")\n" +
                        "  |> filter(fn: (r) => r[\"_field\"] == \"power_transfer\" )\n" +
                        "  |> filter(fn: (r) => r[\"property_id\"] == \"%d\")", this.energyConsumptionBucket1m, apartmentId);
        QueryApi queryApi = this.influxDBClient.getQueryApi();
        List<FluxTable> tables = queryApi.query(fluxQuery);
        for (FluxTable fluxTable : tables) {
            List<FluxRecord> records = fluxTable.getRecords();
            for (FluxRecord fluxRecord : records) {
                if (Objects.equals(fluxRecord.getValueByKey("power_generate"), "True")) {
                    generations.add(new SimpleReadingDTO(fluxRecord.getValue() != null ? (Double) fluxRecord.getValue() : null, fluxRecord.getTime().plusSeconds(3600).toString()));
                } else if (Objects.equals(fluxRecord.getValueByKey("power_generate"), "False")) {
                    consumptions.add(new SimpleReadingDTO(fluxRecord.getValue() != null ? (Double) fluxRecord.getValue() : null, fluxRecord.getTime().plusSeconds(3600).toString()));
                }
            }
        }

        List<BatteryStatusDTO> batteries = getBatteryStatuses(apartmentId);

        return new BatterySystemDTO(generations, consumptions, batteries);
    }

    @Override
    public BatterySystemDTO getGenerationAndConsumptionHistory(Long apartmentId, String startDate, String endDate, Integer step) {
        List<SimpleReadingDTO> generations = new ArrayList<>();
        List<SimpleReadingDTO> consumptions = new ArrayList<>();
        String start = "1900-01-01";
        String end = "";
        if (startDate != null) {
            start = startDate;
        }
        if (endDate != null) {
            end = String.format(", stop: %s", endDate);
        }

        String fluxQuery = String.format(
                "from(bucket: \"%s\")\n" +
                        "  |> range(start: %s%s)\n" +
                        "  |> filter(fn: (r) => r[\"_measurement\"] == \"energy_consumption\")\n" +
                        "  |> filter(fn: (r) => r[\"_field\"] == \"power_transfer\" )\n" +
                        "  |> filter(fn: (r) => r[\"property_id\"] == \"%d\")\n" +
                        "  |> aggregateWindow(every: %dm, fn: mean, createEmpty: false)", step < 30 ? this.energyConsumptionBucket1m : this.energyConsumptionBucket1h, start, end, apartmentId, step);
        QueryApi queryApi = this.influxDBClient.getQueryApi();
        List<FluxTable> tables = queryApi.query(fluxQuery);
        for (FluxTable fluxTable : tables) {
            List<FluxRecord> records = fluxTable.getRecords();
            for (FluxRecord fluxRecord : records) {
                if (Objects.equals(fluxRecord.getValueByKey("power_generate"), "True")) {
                    generations.add(new SimpleReadingDTO(fluxRecord.getValue() != null ? (Double) fluxRecord.getValue() : null, fluxRecord.getTime().plusSeconds(3600).toString()));
                } else if (Objects.equals(fluxRecord.getValueByKey("power_generate"), "False")) {
                    consumptions.add(new SimpleReadingDTO(fluxRecord.getValue() != null ? (Double) fluxRecord.getValue() : null, fluxRecord.getTime().plusSeconds(3600).toString()));
                }
            }
        }

        List<BatteryStatusDTO> batteries = getBatteryStatuses(apartmentId);

        return new BatterySystemDTO(generations, consumptions, batteries);
    }

    @Override
    public BatterySystemDTO getGenerationAndConsumptionForApartment(Long apartmentId, String startDate, String endDate, Integer step) {
        List<SimpleReadingDTO> generations = new ArrayList<>();
        List<SimpleReadingDTO> consumptions = new ArrayList<>();
        String start = "1900-01-01";
        String end = "";
        if (startDate != null) {
            start = startDate;
        }
        if (endDate != null) {
            end = String.format(", stop: %s", endDate);
        }



        String fluxQuery = String.format(
                "from(bucket: \"%s\")\n" +
                        "  |> range(start: %s%s)\n" +
                        "  |> filter(fn: (r) => r[\"_measurement\"] == \"energy_consumption\")\n" +
                        "  |> filter(fn: (r) => r[\"_field\"] == \"power_transfer\" )\n" +
                        "  |> filter(fn: (r) => r[\"property_id\"] == \"%d\")\n" +
                        "  |> aggregateWindow(every: %dm, fn: mean, createEmpty: false)", step < 30 ? this.energyConsumptionBucket1m : this.energyConsumptionBucket1h, start, end, apartmentId, step);
        QueryApi queryApi = this.influxDBClient.getQueryApi();
        List<FluxTable> tables = queryApi.query(fluxQuery);
        for (FluxTable fluxTable : tables) {
            List<FluxRecord> records = fluxTable.getRecords();
            for (FluxRecord fluxRecord : records) {
                if (Objects.equals(fluxRecord.getValueByKey("power_generate"), "True")) {
                    generations.add(new SimpleReadingDTO(fluxRecord.getValue() != null ? (Double) fluxRecord.getValue() : null, fluxRecord.getTime().plusSeconds(3600).toString()));
                } else if (Objects.equals(fluxRecord.getValueByKey("power_generate"), "False")) {
                    consumptions.add(new SimpleReadingDTO(fluxRecord.getValue() != null ? (Double) fluxRecord.getValue() : null, fluxRecord.getTime().plusSeconds(3600).toString()));
                }
            }
        }

        return new BatterySystemDTO(generations, consumptions);
    }

    @Override
    public BatterySystemDTO getGenerationAndConsumptionForCity(Long cityId, String startDate, String endDate, Integer step) {
        List<SimpleReadingDTO> generations = new ArrayList<>();
        List<SimpleReadingDTO> consumptions = new ArrayList<>();
        String start = "1900-01-01";
        String end = "";
        if (startDate != null) {
            start = startDate;
        }
        if (endDate != null) {
            end = String.format(", stop: %s", endDate);
        }

        String fluxQuery = String.format(
                "from(bucket: \"%s\")\n" +
                        "  |> range(start: %s%s)\n" +
                        "  |> filter(fn: (r) => r[\"_measurement\"] == \"energy_consumption\")\n" +
                        "  |> filter(fn: (r) => r[\"_field\"] == \"power_transfer\" )\n" +
                        "  |> filter(fn: (r) => r[\"cityId\"] == \"%s\")\n" +
                        "  |> aggregateWindow(every: %dm, fn: mean, createEmpty: false)", step < 30 ? this.energyConsumptionBucket1m : this.energyConsumptionBucket1h, start, end, cityId.toString(), step);
        QueryApi queryApi = this.influxDBClient.getQueryApi();
        List<FluxTable> tables = queryApi.query(fluxQuery);
        for (FluxTable fluxTable : tables) {
            List<FluxRecord> records = fluxTable.getRecords();
            for (FluxRecord fluxRecord : records) {
                if (Objects.equals(fluxRecord.getValueByKey("power_generate"), "True")) {
                    generations.add(new SimpleReadingDTO(fluxRecord.getValue() != null ? (Double) fluxRecord.getValue() : null, fluxRecord.getTime().plusSeconds(3600).toString()));
                } else if (Objects.equals(fluxRecord.getValueByKey("power_generate"), "False")) {
                    consumptions.add(new SimpleReadingDTO(fluxRecord.getValue() != null ? (Double) fluxRecord.getValue() : null, fluxRecord.getTime().plusSeconds(3600).toString()));
                }
            }
        }

        return new BatterySystemDTO(generations, consumptions);
    }

    private List<BatteryStatusDTO> getBatteryStatuses(Long apartmentId) {
        return batteryRepository.findAllByApartmentId(apartmentId).stream().map(BatteryStatusDTO::new).toList();
    }

    @Override
    public IlluminationDTO getIlluminationLiveReadings(Long deviceId, Long apartmentId, String fromTimestamp, String toTimestamp, Principal user) {
        if (!readingsRepository.existsById(user.getName())) {
            subscribeOnReadings(deviceId, user.getName(), "illumination");
        }

        List<GenericReadingDTO<Double>> illumination = new ArrayList<>();
        List<GenericReadingDTO<Boolean>> on = new ArrayList<>();
        List<GenericReadingDTO<Boolean>> auto = new ArrayList<>();
        List<GenericReadingDTO<String>> command = new ArrayList<>();

        String fluxQuery = String.format(
                """
                        from(bucket: "iot")
                            |> range(start: %s, stop: %s)
                            |> filter(fn: (r) => r["_field"] == "command" or r["_field"] == "illumination" or r["_field"] == "on" or r["_field"] == "auto")
                            |> filter(fn: (r) => r["device_type"] == "lamp")
                            |> filter(fn: (r) => r["device_id"] == "%s")
                            |> filter(fn: (r) => r["property_id"] == "%s")
                            |> aggregateWindow(every: 10s, fn: last, createEmpty: false)
                        """, fromTimestamp, toTimestamp, deviceId, apartmentId);
        QueryApi queryApi = this.influxDBClient.getQueryApi();
        List<FluxTable> tables = queryApi.query(fluxQuery);
        for (FluxTable fluxTable : tables) {
            List<FluxRecord> records = fluxTable.getRecords();
            for (FluxRecord fluxRecord : records) {
                if (Objects.equals(fluxRecord.getField(), "illumination")) {
                    illumination.add(
                            new GenericReadingDTO<>(
                                    fluxRecord.getValue() != null ? (Double) fluxRecord.getValue() : null,
                                    Objects.requireNonNull(fluxRecord.getTime()).plusSeconds(3600).toString()
                            )
                    );
                }
                if (Objects.equals(fluxRecord.getField(), "on")) {
                    on.add(
                            new GenericReadingDTO<>(
                                    fluxRecord.getValue() != null ? (Boolean) fluxRecord.getValue() : null,
                                    Objects.requireNonNull(fluxRecord.getTime()).plusSeconds(3600).toString()
                            )
                    );
                }
                if (Objects.equals(fluxRecord.getField(), "auto")) {
                    auto.add(
                            new GenericReadingDTO<>(
                                    fluxRecord.getValue() != null ? (Boolean) fluxRecord.getValue() : null,
                                    Objects.requireNonNull(fluxRecord.getTime()).plusSeconds(3600).toString()
                            )
                    );
                }
                if (Objects.equals(fluxRecord.getField(), "command")) {
                    command.add(
                            new GenericReadingDTO<>(
                                    fluxRecord.getValue() != null ? (String) fluxRecord.getValue() : null,
                                    Objects.requireNonNull(fluxRecord.getTime()).plusSeconds(3600).toString()
                            )
                    );
                }
            }
        }

        return new IlluminationDTO(illumination, on, auto, command);
    }

    @Override
    public AvailabilityDTO getAvailability(Long deviceId, Long apartmentId, Principal user, String fromTimestamp, String toTimestamp) {
        List<GenericReadingDTO<Double>> availability = new ArrayList<>();

        String aggregateWindow = "1h";
        String windowInInterval = "1.0";

        Instant fromInstant = Instant.parse(fromTimestamp);
        Instant toInstant = Instant.parse(toTimestamp);

        if (toInstant.toEpochMilli() - fromInstant.toEpochMilli() > 86400000) {
            aggregateWindow = "1d";
            windowInInterval = "24.0";
        }

        String fluxQuery = String.format(
                """
                        from(bucket: "heartbeat_downsampled")
                            |> range(start: %s, stop: %s)
                            |> filter(fn: (r) => r["_measurement"] == "heartbeat")
                            |> filter(fn: (r) => r["device_id"] == "%s")
                            |> aggregateWindow(every: 3m, fn: count)
                            |> map(fn: (r) => ({_time: r._time, _value: if r._value > 0.0 then 1.0 else 0.0}))
                            |> aggregateWindow(every: %s, fn: sum)
                            |> map(fn: (r) => ({_time: r._time, _value: r._value / %s / 20.0 * 100.0}))
                        """, fromTimestamp, toTimestamp, deviceId, aggregateWindow, windowInInterval);
        QueryApi queryApi = this.influxDBClient.getQueryApi();
        List<FluxTable> tables = queryApi.query(fluxQuery);
        for (FluxTable fluxTable : tables) {
            List<FluxRecord> records = fluxTable.getRecords();
            for (FluxRecord fluxRecord : records) {
                availability.add(
                        new GenericReadingDTO<>(
                                fluxRecord.getValue() != null ? (Double) fluxRecord.getValue() : null,
                                Objects.requireNonNull(fluxRecord.getTime()).plusSeconds(3600).toString()
                        )
                );
            }
        }

        return new AvailabilityDTO(availability);
    }

    @Override
    public GatesLogDTO getGatesLog(Long deviceId, Long apartmentId, Principal user, String fromTimestamp, String toTimestamp) {
        return null;
    }

    private void subscribeOnReadings(Long id, String username, String readingType) {
        if (readingsRepository.existsById(username)) {
            throw new ResponseStatusException(HttpStatus.ALREADY_REPORTED, "User already subscribed to this reading.");
        }
        readingsRepository.save(new Reading(username, readingType, id));
    }
}
