package com.iotsystem.teamtwo.user.DTOs;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class LoginCredentialsDTO {

    @NotNull( message = "is required")
    private String username;

    @NotNull( message = "is required")
    @Size(min = 6, message = "must be longer then 6 characters")
    private String password;

    public LoginCredentialsDTO() {
    }
    public LoginCredentialsDTO(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
