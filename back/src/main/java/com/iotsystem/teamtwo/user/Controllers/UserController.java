package com.iotsystem.teamtwo.user.Controllers;

import com.iotsystem.teamtwo.admin.DTOs.PasswordChangedDTO;
import com.iotsystem.teamtwo.admin.Services.IAdminService;
import com.iotsystem.teamtwo.models.Admin;
import com.iotsystem.teamtwo.models.RegularUser;
import com.iotsystem.teamtwo.security.jwt.JwtTokenUtil;
import com.iotsystem.teamtwo.user.DTOs.LoginCredentialsDTO;
import com.iotsystem.teamtwo.user.DTOs.LoginUserDTO;
import com.iotsystem.teamtwo.user.DTOs.NewPasswordDTO;
import com.iotsystem.teamtwo.user.DTOs.NewUserDTO;
import com.iotsystem.teamtwo.user.Services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Optional;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private IUserService userService;
    @Autowired
    private IAdminService adminService;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @PostMapping("/registration")
    public ResponseEntity<String> register(@Valid @RequestBody NewUserDTO newUser) {
        userService.register(newUser);
        return ResponseEntity.ok("User registered successfully.");
    }

    @PostMapping(value = "/login")
    public LoginUserDTO login(@Valid @RequestBody LoginCredentialsDTO credentialsDTO) {
        UsernamePasswordAuthenticationToken authReq = new UsernamePasswordAuthenticationToken(credentialsDTO.getUsername(),
                credentialsDTO.getPassword());

        Authentication auth = authenticationManager.authenticate(authReq);

        SecurityContext sc = SecurityContextHolder.getContext();
        sc.setAuthentication(auth);
        Optional<RegularUser> user = userService.getByUsername(credentialsDTO.getUsername());
        String token;
        String refreshToken;

        if (user.isPresent() && user.get().getActive()) {
            token = jwtTokenUtil.generateToken(user.get().getUsername(), sc.getAuthentication().getAuthorities().toArray()[0].toString(), user.get().getId());
            refreshToken = jwtTokenUtil.generateRefreshToken(credentialsDTO.getUsername(), sc.getAuthentication().getAuthorities().toArray()[0].toString(), user.get().getId());
        } else {
            Admin admin = adminService.getByUsername(credentialsDTO.getUsername()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
            token = jwtTokenUtil.generateToken(admin.getUsername(), sc.getAuthentication().getAuthorities().toArray()[0].toString(), admin.getId());
            refreshToken = jwtTokenUtil.generateRefreshToken(admin.getUsername(), sc.getAuthentication().getAuthorities().toArray()[0].toString(), admin.getId());
        }
        LoginUserDTO loginUserDTO = new LoginUserDTO();
        loginUserDTO.setAccessToken(token);
        loginUserDTO.setRefreshToken(refreshToken);
        return loginUserDTO;
    }

    @PostMapping(value = "/refreshToken")
    public LoginUserDTO refreshToken(@Valid @RequestBody LoginUserDTO dto) {
        if (dto.getRefreshToken() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Refresh token is required");
        }
        if (jwtTokenUtil.isTokenExpired(dto.getRefreshToken()))
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Your refresh token has expired, please log in again");

        String newJwt = jwtTokenUtil.generateToken(jwtTokenUtil.getUsernameFromToken(dto.getRefreshToken()), jwtTokenUtil.getRoleFromToken(dto.getRefreshToken()), jwtTokenUtil.getUserIdFromToken(dto.getRefreshToken()));
        dto.setAccessToken(newJwt);
        String newRefreshToken = jwtTokenUtil.generateRefreshToken(jwtTokenUtil.getUsernameFromToken(dto.getRefreshToken()), jwtTokenUtil.getRoleFromToken(dto.getRefreshToken()), jwtTokenUtil.getUserIdFromToken(dto.getRefreshToken()));
        dto.setRefreshToken(newRefreshToken);
        return dto;
    }

    @PutMapping("/verify/{code}")
    public ResponseEntity<String> verify(@PathVariable String code) {
        userService.verify(code);
        return ResponseEntity.ok("User verified successfully.");
    }

    @PutMapping("/change-password")
    @PreAuthorize("hasAuthority('ROLE_USER') || hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<String> changePassword(@RequestBody @Valid NewPasswordDTO newPasswordDTO, Principal user) {
        userService.changePassword(newPasswordDTO, user);
        return ResponseEntity.ok("Password changed successfully.");
    }

}
