package com.iotsystem.teamtwo.user.DTOs;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class NewPasswordDTO {

    @NotNull
    @Size(min = 6, message = "must be at least 6 characters long")
    private String password;

    public NewPasswordDTO() {
    }

    public NewPasswordDTO(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
