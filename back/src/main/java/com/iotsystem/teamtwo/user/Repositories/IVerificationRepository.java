package com.iotsystem.teamtwo.user.Repositories;

import com.iotsystem.teamtwo.models.Verification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IVerificationRepository extends JpaRepository<Verification, String>
{
}
