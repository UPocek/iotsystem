package com.iotsystem.teamtwo.user.Repositories;

import com.iotsystem.teamtwo.models.RegularUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IUserRepository extends JpaRepository<RegularUser, Long> {

    Optional<RegularUser> findByUsername(String username);

    boolean existsByUsername(String username);

}
