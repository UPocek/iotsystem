package com.iotsystem.teamtwo.user.DTOs;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class NewUserDTO {

    @NotBlank( message = "is required")
    @Size(min = 1, max = 20, message = "must be between 1 and 20 characters")
    private String name;
    @NotBlank( message = "is required")
    @Size(min = 1, max = 20, message = "must be between 1 and 20 characters")
    private String surname;
    @NotBlank( message = "is required")
    @Pattern(regexp = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$", message = "Invalid email format")
    private String username;
    @NotBlank( message = "is required")
    @Size(min = 6, max = 20, message = "must be between 6 and 20 characters")
    private String password;
    @NotNull( message = "is required")
    private String profilePicture;

    public NewUserDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }
}
