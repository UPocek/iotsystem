package com.iotsystem.teamtwo.user.Services;

import com.iotsystem.teamtwo.admin.Services.IAdminService;
import com.iotsystem.teamtwo.helper.IHelperService;
import com.iotsystem.teamtwo.models.Admin;
import com.iotsystem.teamtwo.models.RegularUser;
import com.iotsystem.teamtwo.models.Verification;
import com.iotsystem.teamtwo.user.DTOs.NewPasswordDTO;
import com.iotsystem.teamtwo.user.DTOs.NewUserDTO;
import com.iotsystem.teamtwo.user.Repositories.IUserRepository;
import com.iotsystem.teamtwo.user.Repositories.IVerificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService implements IUserService {

    @Value("${company.email}")
    public String companyEmail;
    @Value("${company.url}")
    public String companyUrl;

    @Autowired
    private IHelperService helperService;
    @Autowired
    private IAdminService adminService;
    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private IVerificationRepository verificationRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void register(NewUserDTO newUser) {
        if (userRepository.existsByUsername(newUser.getUsername())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User with that username already exists");
        }
        String verificationCode = helperService.generateSecretCode(36);
        try {
            List<String> data = new ArrayList<>();
            data.add("Your user account has been created on IoT4Home");
            data.add("Activate your profile by clicking the link: " + companyUrl + "verify?code=" + verificationCode);
            data.add(companyUrl);
            String htmlTemplate = helperService.prepareMailTemplate(data, "src/main/resources/templates/email.html", "Activate your profile by clicking the link: " + companyUrl + "verify?code=" + verificationCode);
//            helperService.sendHtmlEmail(companyEmail, newUser.getUsername(), "IOT - Verify your user profile", htmlTemplate);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        verificationRepository.save(new Verification(verificationCode, newUser.getUsername()));

        newUser.setPassword(passwordEncoder.encode(newUser.getPassword()));
        userRepository.save(new RegularUser(newUser));
        helperService.saveImage(newUser.getProfilePicture(), "profile/" + newUser.getUsername() + ".jpg");
    }

    @Override
    public void verify(String code) {
        Verification verification = verificationRepository.findById(code).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        RegularUser user = userRepository.findByUsername(verification.getUsername()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        user.setActive(true);
        userRepository.saveAndFlush(user);
    }

    @Override
    public void save(RegularUser user) {
        userRepository.save(user);
    }

    @Override
    public Optional<RegularUser> getByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public Optional<RegularUser> getById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public void changePassword(NewPasswordDTO newPasswordDTO, Principal user) {
        Optional<RegularUser> userToChange = getByUsername(user.getName());
        if (userToChange.isPresent()) {
            RegularUser regularUser = userToChange.get();
            if (passwordEncoder.matches(newPasswordDTO.getPassword(), regularUser.getPassword())) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "New password cannot be the same as the old one");
            }
            regularUser.setPassword(passwordEncoder.encode(newPasswordDTO.getPassword()));
            userRepository.saveAndFlush(regularUser);
        } else {
            Admin admin = adminService.getByUsername(user.getName()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
            if (passwordEncoder.matches(newPasswordDTO.getPassword(), admin.getPassword())) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "New password cannot be the same as the old one");
            }
            admin.setPassword(passwordEncoder.encode(newPasswordDTO.getPassword()));
            admin.setPasswordChanged(true);
            adminService.save(admin);
        }
    }

    @Override
    public Optional<Admin> getAdminByUsername(String name) {
        return adminService.getByUsername(name);
    }
}
