package com.iotsystem.teamtwo.user.Services;

import com.iotsystem.teamtwo.models.Admin;
import com.iotsystem.teamtwo.models.RegularUser;
import com.iotsystem.teamtwo.user.DTOs.NewPasswordDTO;
import com.iotsystem.teamtwo.user.DTOs.NewUserDTO;

import java.security.Principal;
import java.util.Optional;

public interface IUserService {
    void register(NewUserDTO newUser);

    void verify(String code);

    void save(RegularUser user);

    Optional<RegularUser> getByUsername(String username);

    Optional<RegularUser> getById(Long id);

    void changePassword(NewPasswordDTO newPasswordDTO, Principal user);

    Optional<Admin> getAdminByUsername(String name);
}
