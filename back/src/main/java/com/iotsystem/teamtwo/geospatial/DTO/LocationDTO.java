package com.iotsystem.teamtwo.geospatial.DTO;

import com.iotsystem.teamtwo.models.Location;

import java.io.Serializable;

public class LocationDTO implements Serializable {
    private Double latitude;
    private Double longitude;

    public LocationDTO() {
    }

    public LocationDTO(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public LocationDTO(Location location) {
        this.latitude = location.getLatitude();
        this.longitude = location.getLongitude();
    }

    public Double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
