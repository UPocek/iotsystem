package com.iotsystem.teamtwo.geospatial.DTO;

import com.iotsystem.teamtwo.models.City;

import javax.annotation.Nullable;
import java.io.Serializable;

public class CityDTO implements Serializable {
    private Integer id;
    private String city;
    @Nullable
    private LocationDTO location;
    private String country;

    public CityDTO() {

    }

    public CityDTO(City city) {
        this.id = city.getId();
        this.city = city.getCity();
        if (city.getLocation() != null) this.location = new LocationDTO(city.getLocation());
        this.country = city.getCountry();
    }

    public Integer getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    @org.jetbrains.annotations.Nullable
    public LocationDTO getLocation() {
        return location;
    }

    public String getCountry() {
        return country;
    }
}
