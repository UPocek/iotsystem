package com.iotsystem.teamtwo.geospatial.repositories;

import com.iotsystem.teamtwo.models.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ILocationRepository extends JpaRepository<Location, Integer> {
}
