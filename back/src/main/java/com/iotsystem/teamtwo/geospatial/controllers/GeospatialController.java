package com.iotsystem.teamtwo.geospatial.controllers;

import com.iotsystem.teamtwo.geospatial.services.IGeospatialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/geospatial")
public class GeospatialController {
    @Autowired
    private IGeospatialService geospatialService;

    @GetMapping("/country/{substring}")
    public List<String> countriesThatContains(@PathVariable String substring) {
        return geospatialService.countriesThatContains(substring);
    }

    @GetMapping("/country/{substring}/cities")
    public List<String> citiesByCountryThatContains(@PathVariable String substring) {
        return geospatialService.citiesByCountry(substring);
    }
}
