package com.iotsystem.teamtwo.geospatial.services;

import com.iotsystem.teamtwo.geospatial.DTO.CityDTO;
import com.iotsystem.teamtwo.geospatial.DTO.LocationDTO;
import com.iotsystem.teamtwo.models.City;
import com.iotsystem.teamtwo.models.Location;

import java.util.List;

public interface IGeospatialService {
    City getCity(String city, String country);
    Location add(LocationDTO locationDTO);

    List<String> countriesThatContains(String substring);
    List<String> citiesByCountry(String substring);
}
