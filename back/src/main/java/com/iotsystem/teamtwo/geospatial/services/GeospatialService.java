package com.iotsystem.teamtwo.geospatial.services;

import com.iotsystem.teamtwo.geospatial.DTO.CityDTO;
import com.iotsystem.teamtwo.geospatial.DTO.LocationDTO;
import com.iotsystem.teamtwo.geospatial.repositories.ICityRepository;
import com.iotsystem.teamtwo.geospatial.repositories.ILocationRepository;
import com.iotsystem.teamtwo.models.City;
import com.iotsystem.teamtwo.models.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.ValidationException;
import java.util.List;


@Service
public class GeospatialService implements IGeospatialService {
    @Autowired
    private ILocationRepository locationRepository;
    @Autowired
    private ICityRepository cityRepository;

    @Override
    public City getCity(String city, String country) {
        if (!cityRepository.existsCityByCountry(country)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Country does not exist!");
        }
        if (!cityRepository.existsCityByCity(city)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "City does not exist!");
        }
        return cityRepository.findCityByCity(city);
    }

    @Override
    public Location add(LocationDTO locationDTO) {
        return locationRepository.save(new Location(locationDTO.getLatitude(), locationDTO.getLongitude()));
    }

    @Override
    public List<String> countriesThatContains(String name) {
        return cityRepository.countriesThatContains(name).stream().toList();
    }

    @Override
    public List<String> citiesByCountry(String name) {
        return cityRepository.citiesByCountry(name).stream().toList();
    }
}
