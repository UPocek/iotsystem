package com.iotsystem.teamtwo.geospatial.repositories;

import com.iotsystem.teamtwo.models.City;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface ICityRepository extends JpaRepository<City, Integer> {
    boolean existsCityByCityAscii(String s);
    boolean existsCityByCity(String s);
    City findCityByCity(String s);
    boolean existsCityByCountry(String country);

    @Query(nativeQuery = true, value = "SELECT c.country FROM City c WHERE c.predefined = true AND " +
            "UPPER(c.country) LIKE '%' || UPPER(?1) || '%' LIMIT 10")
    Set<String> countriesThatContains(String substring);

    @Query(nativeQuery = true, value = "SELECT c.city FROM City c WHERE c.predefined = true AND " +
            "UPPER(c.country) LIKE '%' || UPPER(?1) || '%'")
    Set<String> citiesByCountry(String country);

    @Query("SELECT CONCAT(c.iso3, ':',  c.cityAscii) FROM City c")
    Set<String> getAllCitySet();
}
