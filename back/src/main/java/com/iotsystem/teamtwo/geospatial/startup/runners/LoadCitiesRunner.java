package com.iotsystem.teamtwo.geospatial.startup.runners;
import com.iotsystem.teamtwo.geospatial.services.ParseCSVService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class LoadCitiesRunner implements CommandLineRunner {
    private final ParseCSVService parseCSVService;

    @Autowired
    public LoadCitiesRunner(ParseCSVService parseCSVService) {
        this.parseCSVService = parseCSVService;
    }

    @Override
    public void run(String... args) {
        parseCSVService.importDataFromCSV();
    }
}
