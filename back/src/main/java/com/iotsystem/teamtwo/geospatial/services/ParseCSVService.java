package com.iotsystem.teamtwo.geospatial.services;

import com.iotsystem.teamtwo.models.City;
import com.iotsystem.teamtwo.models.Location;
import com.iotsystem.teamtwo.geospatial.repositories.ICityRepository;
import com.iotsystem.teamtwo.geospatial.repositories.ILocationRepository;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.Set;

@Service
public class ParseCSVService {
    private final ICityRepository cityRepository;
    private final ILocationRepository locationRepository;

    @Autowired
    public ParseCSVService(ICityRepository cityRepository, ILocationRepository locationRepository) {
        this.cityRepository = cityRepository;
        this.locationRepository = locationRepository;
    }

    public void importDataFromCSV() {
        final Set<String> citySet = cityRepository.getAllCitySet();

        try (
                CSVReader reader = new CSVReader(
                        new FileReader(
                                ResourceUtils.getFile("classpath:constants/worldcities.csv")
                        )
                )
        ) {
            reader.readNext(); // Skip the header row
            String[] line;
            while ((line = reader.readNext()) != null) {
                int i = 0;
                if (citySet.contains(line[6] + ":" + line[1])) continue;

                City entity = new City(
                        line[i++],
                        line[i++],
                        new Location(
                                Double.parseDouble(line[i++]),
                                Double.parseDouble(line[i++])
                        ),
                        line[i++],
                        line[i++],
                        line[i++],
                        line[i++],
                        line[i++],
                        line[i],
                        true
                );
                locationRepository.save(entity.getLocation());
                cityRepository.save(entity);
            }
        } catch (IOException | CsvValidationException e) {
            e.printStackTrace();
        }
    }
}