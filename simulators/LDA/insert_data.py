import random
from datetime import datetime, timedelta
import influxdb_client
from influxdb_client.client.write_api import SYNCHRONOUS

bucket = "iot"
org = "iotream2"
token = "ukvDyhBvVF8K2l4EoBAlUXSRNiEqMV1wuBBc50o1tdaQm_FpHR0wEwcM_8ETWXv1FrEliHilfu87X9bX6yZL4w=="

url="http://localhost:8086"

def insert_heartbeats():
    global now, three_months_ago, heartbeat_devices
    points = []

    current = three_months_ago
    while current < now:
        for apartment in heartbeat_devices:
            for device in heartbeat_devices[apartment]:
                for id in device['ids']:
                    p = influxdb_client.Point("measurements").measurement("heartbeat").tag("device_id", str(id)).tag("device_type",device['device_type']).field("status", "ok").time(current,'ms')
                    points.append(p)

                    if len(points) > 900:
                        write_api.write(bucket=bucket, org=org, record=points)
                        points = []
        current = current + timedelta(seconds=9)
    if len(points) > 0:
        write_api.write(bucket=bucket, org=org, record=points)

def insert_on_off_command():
    global now, three_months_ago, turn_on_off_devices
    points = []

    users = [{'user_full_name':'name_surname', 'user_id':'1'}, {'user_full_name':'name_surname', 'user_id':'2'}]
    for apartment in turn_on_off_devices:
        for device in turn_on_off_devices[apartment]:
            for id in device['ids']:
                current = three_months_ago
                while current < now:
                    random_user = random.randint(0,1)
                    p = (influxdb_client.Point("measurements")
                    .measurement("command")
                    .tag("device_id", str(id))
                    .tag("device_type",device['device_type'])
                    .tag("user_full_name",users[random_user]['user_full_name'])
                    .tag("user_id",users[random_user]['user_id'])
                    .field("command", "turn_on")
                    .time(current,'ms'))
                    points.append(p)
                    current = current + timedelta(hours=20)

                    if len(points) > 900:
                        write_api.write(bucket=bucket, org=org, record=points)
                        points = []

                    random_user = random.randint(0,1)
                    p = (influxdb_client.Point("measurements")
                    .measurement("command")
                    .tag("device_id", str(id))
                    .tag("device_type",device['device_type'])
                    .tag("user_full_name",users[random_user]['user_full_name'])
                    .tag("user_id",users[random_user]['user_id'])
                    .field("command", "turn_off")
                    .time(current,'ms'))
                    points.append(p)
                    current = current + timedelta(hours=20)

                    if len(points) > 900:
                        write_api.write(bucket=bucket, org=org, record=points)
                        points = []
    if len(points) > 0:
        write_api.write(bucket=bucket, org=org, record=points)
    
def insert_solar_reading():
    global now, three_months_ago, solar_devices
    points = []

    for apartment in solar_devices:
        for device in solar_devices[apartment]:
            for id in device['ids']:
                current = three_months_ago
                while current < now:
                    if current.hour > 19:
                        current = current + timedelta(days=1)
                        current.replace(hour=7)
                    random_num = random.uniform(0.1267, 0.267)
                    p = (influxdb_client.Point("measurements")
                    .measurement("readings")
                    .tag("device_id", str(id))
                    .tag("device_type",device['device_type'])
                    .tag("property_id",str(apartment))
                    .field("power_generated", random_num)
                    .time(current,'ms'))
                    points.append(p)
                    current = current + timedelta(minutes=1)

                    if len(points) > 900:
                        write_api.write(bucket=bucket, org=org, record=points)
                        points = []
    if len(points) > 0:
        write_api.write(bucket=bucket, org=org, record=points)

def insert_consumtion_generation():
    global now, three_months_ago, consume_generate
    points = []

    for apartment in consume_generate:
        current = three_months_ago
        while current < now:
            random_consumtion = random.uniform(0.00083, 0.03083)
            random_generation = random.uniform(0.001267, 0.00267)
            if current.hour > 19 or current.hour < 7:
                random_generation = 0.0
            p = (influxdb_client.Point("measurements")
            .measurement("energy_consumption")
            .tag("property_id", str(apartment))
            .tag("cityId",str(consume_generate[apartment]['cityId']))
            .tag("power_generate",False)
            .field("power_transfer", random_consumtion)
            .time(current,'ms'))
            points.append(p)
            if len(points) > 900:
                        write_api.write(bucket=bucket, org=org, record=points)
                        points = []

            p = (influxdb_client.Point("measurements")
            .measurement("energy_consumption")
            .tag("property_id", str(apartment))
            .tag("cityId",str(consume_generate[apartment]['cityId']))
            .tag("power_generate",True)
            .field("power_transfer", random_generation)
            .time(current,'ms'))
            points.append(p)
            current = current + timedelta(minutes=1)

            if len(points) > 900:
                        write_api.write(bucket=bucket, org=org, record=points)
                        points = []
    if len(points) > 0:
        write_api.write(bucket=bucket, org=org, record=points)

def insert_charging_command():
    global now, three_months_ago, charge_devices
    points = []

    for apartment in charge_devices:
        for device in charge_devices[apartment]:
            for id in device['ids']:
                current = three_months_ago
                while current < now:
                    p = (influxdb_client.Point("measurements")
                    .measurement("command")
                    .tag("device_id", str(id))
                    .tag("device_type",device['device_type'])
                    .tag("user_full_name","device")
                    .tag("user_id","-1")
                    .field("command", "charging_started")
                    .time(current,'ms'))
                    points.append(p)
                    random_offset = random.randint(20, 180)
                    current = current + timedelta(minutes=random_offset)
                    if len(points) > 900:
                        write_api.write(bucket=bucket, org=org, record=points)
                        points = []

                    random_consumtion = random.uniform(2.0, 4.0)
                    p = (influxdb_client.Point("measurements")
                    .measurement("command")
                    .tag("device_id", str(id))
                    .tag("device_type",device['device_type'])
                    .tag("user_full_name","device")
                    .tag("user_id","-1")
                    .field("command", f'charging_ended/{str(random_consumtion)}')
                    .time(current,'ms'))
                    points.append(p)

                    random_offset = random.randint(3, 18)
                    current = current + timedelta(hours=random_offset)

                    if len(points) > 900:
                        write_api.write(bucket=bucket, org=org, record=points)
                        points = []
    if len(points) > 0:
        write_api.write(bucket=bucket, org=org, record=points)

def insert_percentage_charger():
    global now, three_months_ago, charge_devices
    points = []

    for apartment in charge_devices:
        for device in charge_devices[apartment]:
            for id in device['ids']:
                current = three_months_ago
                while current < now:
                    if current.hour > 19:
                        current = current + timedelta(days=1)
                        current.replace(hour=7)
                    random_num = random.uniform(0.5, 1.0)
                    p = (influxdb_client.Point("measurements")
                    .measurement("command")
                    .tag("device_id", str(id))
                    .tag("device_type","charger")
                    .tag("user_full_name","name_surname")
                    .tag("user_id","1")
                    .field("command", f'percentage_battery_full/{str(random_num)}')
                    .time(current,'ms'))
                    points.append(p)

                    if len(points) > 900:
                        write_api.write(bucket=bucket, org=org, record=points)
                        points = []

                    random_offset = random.randint(10, 25)
                    current = current + timedelta(hours=random_offset)

                    random_num = random.uniform(0.5, 1.0)
                    p = (influxdb_client.Point("measurements")
                    .measurement("command")
                    .tag("device_id", str(id))
                    .tag("device_type","charger")
                    .tag("user_full_name","name_surname")
                    .tag("user_id","2")
                    .field("command", f'percentage_battery_full/{str(random_num)}')
                    .time(current,'ms'))
                    points.append(p)
                    random_offset = random.randint(10, 25)
                    current = current + timedelta(hours=random_offset)

                    if len(points) > 900:
                        write_api.write(bucket=bucket, org=org, record=points)
                        points = []
    if len(points) > 0:
        write_api.write(bucket=bucket, org=org, record=points)

def insert_ambient_reading():
    global now, three_months_ago, ambient_devices
    points = []

    for apartment in ambient_devices:
        for device in ambient_devices[apartment]:
            for id in device['ids']:
                current = three_months_ago
                while current < now:
                    random_temp = random.uniform(10, 20)
                    random_humid = random.uniform(0, 100)
                    p = (influxdb_client.Point("measurements")
                    .measurement("readings")
                    .tag("device_id", str(id))
                    .tag("device_type",device['device_type'])
                    .field("temperature", random_temp)
                    .field("humidity", random_humid)
                    .time(current,'ms'))
                    points.append(p)
                    current = current + timedelta(seconds=29)

                    if len(points) > 900:
                        write_api.write(bucket=bucket, org=org, record=points)
                        points = []
    if len(points) > 0:
        write_api.write(bucket=bucket, org=org, record=points)

def insert_air_mode_command():
    global now, three_months_ago, air_cond_devices
    points = []

    modes = ['heat','cool','auto','dry']
    for apartment in air_cond_devices:
        for device in air_cond_devices[apartment]:
            for id in device['ids']:
                current = three_months_ago
                while current < now:
                    mode = modes[random.randint(0,3)]
                    temp = random.randint(20,25)
                    p = (influxdb_client.Point("measurements")
                    .measurement("command")
                    .tag("device_id", str(id))
                    .tag("device_type",device['device_type'])
                    .tag("user_full_name","name_surname")
                    .tag("user_id","2")
                    .field("command", f'{mode}_{str(temp)}')
                    .time(current,'ms'))
                    points.append(p)
                    current = current + timedelta(hours=10)

                    if len(points) > 900:
                        write_api.write(bucket=bucket, org=org, record=points)
                        points = []

                    mode = modes[random.randint(0,3)]
                    temp = random.randint(20,25)
                    p = (influxdb_client.Point("measurements")
                    .measurement("command")
                    .tag("device_id", str(id))
                    .tag("device_type",device['device_type'])
                    .tag("user_full_name","name_surname")
                    .tag("user_id","1")
                    .field("command", f'{mode}_{str(temp)}')
                    .time(current,'ms'))
                    points.append(p)
                    current = current + timedelta(hours=10)

                    if len(points) > 900:
                        write_api.write(bucket=bucket, org=org, record=points)
                        points = []
    if len(points) > 0:
        write_api.write(bucket=bucket, org=org, record=points)

def insert_wash_command():
    global now, three_months_ago, wash_devices
    points = []

    modes = ['black30','black45','white60','white90','color45']
    for apartment in wash_devices:
        for device in wash_devices[apartment]:
            for id in device['ids']:
                current = three_months_ago
                while current < now:
                    mode = modes[random.randint(0,4)]
                    p = (influxdb_client.Point("measurements")
                    .measurement("command")
                    .tag("device_id", str(id))
                    .tag("device_type",device['device_type'])
                    .tag("user_full_name","name_surname")
                    .tag("user_id","1")
                    .field("command", mode)
                    .time(current,'ms'))
                    points.append(p)
                    current = current + timedelta(days=1)

                    if len(points) > 900:
                        write_api.write(bucket=bucket, org=org, record=points)
                        points = []

                    mode = modes[random.randint(0,4)]
                    p = (influxdb_client.Point("measurements")
                    .measurement("command")
                    .tag("device_id", str(id))
                    .tag("device_type",device['device_type'])
                    .tag("user_full_name","name_surname")
                    .tag("user_id","2")
                    .field("command", mode)
                    .time(current,'ms'))
                    points.append(p)
                    current = current + timedelta(days=1)

                    if len(points) > 900:
                        write_api.write(bucket=bucket, org=org, record=points)
                        points = []
    if len(points) > 0:
        write_api.write(bucket=bucket, org=org, record=points)

if __name__ == "__main__":
    heartbeat_devices = {
        1:[
            {
                'device_type':'solarsystem', 
                'ids':[25,28]
            },
            {
                'device_type':'charger', 
                'ids':[27,30]
            },
            {
                'device_type':'batterysystem', 
                'ids':[26,29]
            }
        ]
    }

    turn_on_off_devices = {
        1:[
            {
                'device_type':'solarsystem', 
                'ids':[25,34]
            },
            {
                'device_type':'charger', 
                'ids':[27,36]
            },
            {
                'device_type':'ambient_conditions_sensor', 
                'ids':[20,29]
            },
            {
                'device_type':'air_conditioning', 
                'ids':[19,28]
            },
            {
                'device_type':'washing_machine', 
                'ids':[21,20]
            },
        ]
    }

    solar_devices = {
        1:[
            {
                'device_type':'solarsystem', 
                'ids':[25,34]
            },
        ]
    }

    ambient_devices = {
        1:[
            {
                'device_type':'ambient_conditions_sensor', 
                'ids':[20,29]
            },
        ]
    }

    air_cond_devices = {
        1:[
            {
                'device_type':'air_conditioning', 
                'ids':[19,28]
            },
        ]
    }

    wash_devices ={
        1:[
            {
                'device_type':'washing_machine', 
                'ids':[21,30]
            },
        ]
    }

    consume_generate = {
        2:{
                'cityId':8421, 
            },
        
    }

    charge_devices = {
        1:[
            {
                'device_type':'charger', 
                'ids':[27,36]
            },
        ]
    }

    now = datetime.now()
    three_months_ago = now - timedelta(days=95)

    client = influxdb_client.InfluxDBClient(
        url=url,
        token=token,
        org=org
    )

    write_api = client.write_api(write_options=SYNCHRONOUS)
    # insert_heartbeats()
    insert_on_off_command()
    insert_solar_reading()
    insert_percentage_charger()
    insert_consumtion_generation()
    insert_ambient_reading()
    insert_air_mode_command()
    insert_wash_command()
    insert_charging_command()

    
    
    
 

    
   