import sys
import uuid
import json
import time
import paho.mqtt.client as paho
from threading import Lock,Thread

def connect_mqtt():
    client = paho.Client()
    client.username_pw_set("devuser", password="changeme")
    client.on_message = on_message_received
    if client.connect("localhost", 1883, 60) != 0:
        print("Couldn't connect to the mqtt broker")
        sys.exit(1)
    return client

def publish_heartbeat():
   global batteries
   while True:
    with battery_lock:
        for battery in batteries:
            client.publish(
                "measurements",
                f'heartbeat,device_id={str(battery)},device_type=batterysystem status="ok"',
                0,
            )
    time.sleep(1)

def on_message_received(client, userdata, msg):
    topic = msg.topic
    if topic == f"server-status/{property_id}":
        parse_property_details_from_message(msg)
    if topic == f'devices-consume/{property_id}':
        devices_consumption(msg)

def subscribe_to_property_info():
    global property_id
    client.subscribe(f'server-status/{property_id}')
    client.loop_start()

def subscribe_to_consumption_devices():
    global property_id
    client.subscribe(f'devices-consume/{property_id}')
    client.loop_start()
    
def parse_property_details_from_message(msg):
    global batteries, city
    try:
        with battery_lock:
            batteries = json.loads(msg.payload.decode())["apartmentDevices"]["batteryList"]
        city = json.loads(msg.payload.decode())["city"].replace(" ", "")
    except Exception:
        pass

def devices_consumption(msg):
    global consumption, generation, batteries
    consume = float(json.loads(msg.payload.decode()))
    with battery_lock:
        if consume >= get_current_batteries_level():
            print("Battery exhusted")
            consumption += consume - get_current_batteries_level()
            for battery in batteries.values():
                battery['level'] = 0 
            
        #multiplying with -1 because when solar panles sends energy consumtion it sends it with negative sign
        elif consume < 0 and -1*consume + get_current_batteries_level() > get_batteries_maximum():
            generation += (get_current_batteries_level() - get_batteries_maximum()) - consume
            for battery in batteries.values():
                battery['level'] = battery['batteryCapacity']

        else:
            if get_current_batteries_level()>0:
                print("Battery above zero")
                for battery in batteries.values():
                    battery['level'] -= consume/get_current_batteries_level()*battery['level']
                    print("Battery level ", battery['level'])
            else:
                print("Battery below zero")
                for battery in batteries.values():
                    battery['level'] -= consume/get_batteries_maximum()*battery['batteryCapacity']
                    print("Battery level ", battery['level'])
                    


def get_current_batteries_level():
    global batteries
    if len(batteries.values()) == 0:
        return 0
    return  sum([battery['level'] for battery in batteries.values()])

def get_batteries_maximum():
    global batteries
    if len(batteries.values()) == 0:
        return 0
    return sum(battery['batteryCapacity'] for battery in batteries.values())

def simulate():
    global consumption, generation,city
    while True:
        with battery_lock:
            for battery in batteries:
                print(batteries[battery]["level"])
                client.publish(
                    f'battery-level/{str(battery)}',
                    f'{batteries[battery]["level"]}',
                    0,
                )

            client.publish(
                "measurements",
                f'energy_consumption,property_id={str(property_id)},city={city},power_generate={False} power_transfer={consumption}',
                0,
            )
            print('Consumption: ', consumption)
            consumption = 0
            
            client.publish(
                "measurements",
                f'energy_consumption,property_id={str(property_id)},city={city},power_generate={True} power_transfer={generation}',
                0,
            )
            print('Generated: ', generation)
            generation = 0
            
        
        time.sleep(60)


if __name__ == "__main__":
    batteries = {}
    consumption = 0
    generation = 0
    property_id = None
    city = None
    battery_lock = Lock()
    client = connect_mqtt()
    t = Thread(target=publish_heartbeat, daemon=True)
    property_id = sys.argv[1]
    subscribe_to_property_info()
    subscribe_to_consumption_devices()
    t.start()
    simulate()
    client.disconnect()
