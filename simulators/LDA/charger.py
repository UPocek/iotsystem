import sys
import time
import uuid
import json
import paho.mqtt.client as paho
from threading import Lock,Thread
import random

def connect_mqtt():
    client = paho.Client()
    client.username_pw_set("devuser", password="changeme")
    client.on_message = on_message_received
    if client.connect("localhost", 1883, 60) != 0:
        print("Couldn't connect to the mqtt broker")
        sys.exit(1)
    return client

def try_connect():
    global init_code
    init_code = str(uuid.uuid4())
    print(init_code)
    client.publish(f"device-setup/{init_code}", "New device", 0, retain=True)
    time.sleep(1)
    client.subscribe(f"server-init/{init_code}")
    client.loop_start()

def publish_heartbeat():
   global device_id, device_turned_on
   while True:
    if device_turned_on:
        client.publish(
                        "measurements",
                        f"heartbeat,device_id={str(device_id)},device_type=charger status=\"ok\"",
                        0
                    )
    time.sleep(1)

def on_message_received(client, userdata, msg):
    topic = msg.topic
    if topic == f"server-init/{init_code}":
        get_credentials(msg)
    if topic == f"server-command/{device_id}":
        execute_command(msg)
    if topic == f"server-status/{property_id}":
        parse_property_details_from_message(msg)

def get_credentials(msg):
    get_device_id(msg)
    subscribe_to_property_info()
    subscribe_to_commands()
    client.unsubscribe(f"server-init/{init_code}")

def get_device_id(msg):
    global device_id,property_id
    device_id = json.loads(msg.payload.decode())["deviceId"]
    property_id = json.loads(msg.payload.decode())["propertyId"]

def subscribe_to_property_info():
    global property_id
    client.subscribe(f'server-status/{property_id}')
    client.loop_start()

def subscribe_to_commands():
    global device_id
    client.subscribe(f'server-command/{device_id}')
    client.loop_start()

def execute_command(msg):
    global device_turned_on, device_id
    executed_user_name = json.loads(msg.payload.decode())["userFullName"]
    executed_user_id = json.loads(msg.payload.decode())["userId"]
    command = json.loads(msg.payload.decode())["command"]

    if command == "turn_off":
       turn_off_on(False)
    if command == "turn_on":
        turn_off_on(True)
    if command.split('/')[0] == "percentage_battery_full":
        set_occupancy(command.split('/')[1])
    
    client.publish("measurements",
            f'command,device_id={str(device_id)},device_type=charger,user_full_name={executed_user_name},user_id={str(executed_user_id)} command=\"{command}\"',
            0,
        )
    
def turn_off_on(turn):
    global device_turned_on
    with device_lock:
            device_turned_on = turn

def set_occupancy(new_percentage):
    global percentage_battery_full
    with percentage_battery_full_lock:
        percentage_battery_full = float(new_percentage)

def parse_property_details_from_message(msg):
    global device_turned_on, number_of_connections, power, percentage_battery_full

    try:
        device = json.loads(msg.payload.decode())["apartmentDevices"]["chargerList"][str(device_id)]

        number_of_connections = device["numberOfConnections"]
        power = device["power"]
        percentage_battery_full = device["fullness"]

        device_turned_on = True
    except Exception:
        pass
    

def simulate():
    global device_id, device_turned_on

    while True:
        if device_turned_on:
            print('turned on')
            timestamp = time.time()
            
            if int(timestamp)%4 == 0:
                num_cars_came = int(timestamp)%number_of_connections if int(timestamp)%number_of_connections > 0 else 1
                car_event(num_cars_came)
        else:
            print('turned off')
        time.sleep(60)

def car_event(num_cars_came):
    for _ in range(num_cars_came):
        t = Thread(target=simulate_charging, daemon=True)
        t.start()

def simulate_charging():
    global percentage_battery_full,log_file
    car_capacity = random.randint(30,60)
    print("Car capacity: ", str(car_capacity))
    car_fullnes = (random.randint(10,100)/100) * car_capacity
    print("Car fullnes: ", str(car_fullnes))
    with percentage_battery_full_lock:
        percentage_battery_full_copy = percentage_battery_full

    print("Car will be fueled till it reaches: ", str(percentage_battery_full_copy * car_capacity))
    if car_fullnes < percentage_battery_full_copy * car_capacity:
        
        client.publish("measurements",
            f'command,device_id={str(device_id)},device_type=charger,user_full_name=device,user_id={str(-1)} command=\"charging_started\"',
            0,
        )

        accumulated_power_usage = 0
        while car_fullnes < percentage_battery_full_copy * car_capacity:
            current_power_usage = power if car_fullnes + power <= percentage_battery_full_copy * car_capacity else percentage_battery_full_copy * car_capacity - car_fullnes
            accumulated_power_usage += current_power_usage
            client.publish(f'devices-consume/{property_id}',str(current_power_usage), 0)
            car_fullnes += current_power_usage
            print("Current car fullnes: ", str(car_fullnes))
            
            time.sleep(1)

        print("Ended charging and consumed: ",str(accumulated_power_usage))
        output = 'charging_ended/'+ str(accumulated_power_usage)
        client.publish("measurements",
                f'command,device_id={str(device_id)},device_type=charger,user_full_name=device,user_id=device command=\"{output}\"',
                0,
            )
    

if __name__ == "__main__":
    number_of_connections = None
    power = None
    percentage_battery_full = 100
    init_code = None
    device_id = None
    property_id = None
    device_turned_on = False
    client = connect_mqtt()
    device_lock = Lock()
    percentage_battery_full_lock = Lock()
    t = Thread(target=publish_heartbeat, daemon=True)
    
    try:
        device_id = sys.argv[1]
        property_id = sys.argv[2]
        subscribe_to_property_info()
        subscribe_to_commands()
        t.start()
        simulate()
    except IndexError:
        try_connect()
        t.start()
        simulate()
    finally:
        client.disconnect()