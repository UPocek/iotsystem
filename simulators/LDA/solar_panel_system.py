import sys
import time
import uuid
import json
import paho.mqtt.client as paho
import requests
from threading import Lock,Thread


def connect_mqtt():
    client = paho.Client()
    client.username_pw_set("devuser", password="changeme")
    client.on_message = on_message_received
    if client.connect("localhost", 1883, 60) != 0:
        print("Couldn't connect to the mqtt broker")
        sys.exit(1)
    return client

def try_connect():
    global init_code
    init_code = str(uuid.uuid4())
    print(init_code)
    client.publish(f"device-setup/{init_code}", "New device", 0, retain=True)
    time.sleep(1)
    client.subscribe(f"server-init/{init_code}")
    client.loop_start()

def publish_heartbeat():
   global device_id, device_turned_on
   while True:
    if device_turned_on:
        client.publish(
                        "measurements",
                        f"heartbeat,device_id={str(device_id)},device_type=solarsystem status=\"ok\"",
                        0
                    )
    time.sleep(1)

def on_message_received(client, userdata, msg):
    topic = msg.topic
    print(topic)
    if topic == f"server-init/{init_code}":
        get_credentials(msg)
    if topic == f"server-command/{device_id}":
        execute_command(msg)
    if topic == f"server-status/{property_id}":
        parse_property_details_from_message(msg)

def get_credentials(msg):
    get_device_id(msg)
    subscribe_to_property_info()
    subscribe_to_commands()
    client.unsubscribe(f"server-init/{init_code}")

def get_device_id(msg):
    global device_id,property_id
    device_id = json.loads(msg.payload.decode())["deviceId"]
    property_id = json.loads(msg.payload.decode())["propertyId"]

def subscribe_to_property_info():
    global property_id
    client.subscribe(f'server-status/{property_id}')
    client.loop_start()

def subscribe_to_commands():
    global device_id
    client.subscribe(f'server-command/{device_id}')
    client.loop_start()

def execute_command(msg):
    global device_turned_on, device_id
    executed_user_name = json.loads(msg.payload.decode())["userFullName"]
    executed_user_id = json.loads(msg.payload.decode())["userId"]
    command = json.loads(msg.payload.decode())["command"]
    if command == "turn_off":
        with device_lock:
            device_turned_on = False
    if command == "turn_on":
        with device_lock:
            device_turned_on = True

    client.publish("measurements",
            f'command,device_id={str(device_id)},device_type=solarsystem,user_full_name={executed_user_name},user_id={str(executed_user_id)} command=\"{command}\"',
            0,
        )
    
def parse_property_details_from_message(msg):
    global device_turned_on, number_of_panels, solar_panel_size, solar_panel_efficiency, latitude, longitude

    try:
        device = json.loads(msg.payload.decode())["apartmentDevices"]["solarSystemList"][str(device_id)]
        location = json.loads(msg.payload.decode())['location']

        number_of_panels = device["numberOfPanels"]
        solar_panel_size = device["panelSize"]
        solar_panel_efficiency = device["panelEfficiency"]

        latitude = location['latitude']
        longitude = location['longitude']
        device_turned_on = True
    except Exception:
        pass
    

def simulate():
    global device_id, device_turned_on

    while True:
        if device_turned_on:
            print('turned on')
            irradiance = fetch_irradiance_data()
            power = simulate_power(irradiance)
            print(power)
            client.publish(
                "measurements",
                f'readings,device_id={str(device_id)},property_id={str(property_id)},device_type=solarsystem power_generated={power}',
                0,
            )
            client.publish(f'devices-consume/{property_id}',str(-power), 0)
        else:
            print('turned off')
        time.sleep(60)

def fetch_irradiance_data():
  global latitude, longitude
  url = f'https://api.open-meteo.com/v1/forecast?latitude={str(round(latitude,2))}&longitude={str(round(longitude,2))}&hourly=direct_normal_irradiance'
  #url = f'https://api.open-meteo.com/v1/forecast?latitude=-18&longitude=178&hourly=direct_normal_irradiance'
  response= requests.get(url)
  irradiances = json.loads(response.content)['hourly']['direct_normal_irradiance']
  current_hour = int(time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()).split(' ')[1].split(':')[0])
  return irradiances[current_hour]

def simulate_power(irradiance):
  return (solar_panel_size * solar_panel_efficiency *  irradiance * number_of_panels)/(60*24)

if __name__ == "__main__":
    number_of_panels = None
    solar_panel_size = None
    solar_panel_efficiency = None
    init_code = None
    device_id = None
    property_id = None
    device_turned_on = False
    latitude = None
    longitude = None
    client = connect_mqtt()
    device_lock = Lock()
    t = Thread(target=publish_heartbeat, daemon=True)
    
    try:
        device_id = sys.argv[1]
        property_id = sys.argv[2]
        subscribe_to_property_info()
        subscribe_to_commands()
        t.start()
        simulate()
    except IndexError:
        try_connect()
        t.start()
        simulate()
    finally:
        client.disconnect()