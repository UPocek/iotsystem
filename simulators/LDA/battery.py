import sys
import uuid
import json
import time
import paho.mqtt.client as paho

def connect_mqtt():
    client = paho.Client()
    client.username_pw_set("devuser", password="changeme")
    client.on_message = get_credentials
    if client.connect("localhost", 1883, 60) != 0:
        print("Couldn't connect to the mqtt broker")
        sys.exit(1)
    return client

def try_connect():
    global init_code
    init_code = str(uuid.uuid4())
    print(init_code)
    client.publish(f"device-setup/{init_code}", "New device", 0, retain=True)
    time.sleep(1)
    client.subscribe(f"server-init/{init_code}")
    client.loop_start()

def get_credentials(msg):
    client.loop_stop()

if __name__ == '__main__':
    client = connect_mqtt()
    try_connect()