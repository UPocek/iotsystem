import requests
import uuid
import time
import random
import json

def create_users(number_of_users):
    global users

    for i in range(number_of_users):
        user = { 'username': f'{i}@gmail.com', 'password': f'password', 'name': f'name', 'surname': f'surname', 'profilePicture': '/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAP//////////////////////////////////////////////////////////////////////////////////////wgALCAABAAEBAREA/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxA=' }
        make_post_request(user, url_create_user,'')
        users.append({ 'username': f'{i}@gmail.com', 'password': f'password'})
    time.sleep(15)

def login_users():
   global users
   for user in users:
      response = make_post_request(user, url_login,'')
      user['token'] = response.json()["accessToken"]

def create_apartments():
    global locations, users
    
    for user in users:
        user['apartments'] = []
        for i in range(2):
            location = random.choice(locations)
            apartment = {'name':'apartment-test','location':None,'address':location['address'],'city':location['city'],'country':location['country'],'squareFootage':100,'numberOfFloors':2,'numberOfRooms':5, 'imageUrl':'/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAP//////////////////////////////////////////////////////////////////////////////////////wgALCAABAAEBAREA/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxA='}
            response = make_post_request(apartment, url_create_apartment,user['token'])
            user['apartments'].append({'apartment_id':response.json(),'devices':[]})
    time.sleep(15)
           

def create_devices():
    global users, air_args1, air_args2, am_args1,am_args2, wm_args1, wm_args2, lm_args1, lm_args2,ga_args1,ga_args2,sp_args1,sp_args2,so_args1,so_args2,bat_args1,bat_args2,ch_args1,ch_args2

    for user in users:
       for apartment in user['apartments']:
        for i in range(2):

            device = make_air_conditioning_device(apartment['apartment_id'])
            response = make_post_request(device, url_create_device,user['token'])
            apartment['devices'].append(response.json())
            air_args1 += f' {apartment["apartment_id"]}'
            air_args2 += f' {response.json()}'
            
            device = make_ambient_device(apartment['apartment_id'])
            response = make_post_request(device, url_create_device,user['token'])
            apartment['devices'].append(response.json())
            am_args1 += f' {apartment["apartment_id"]}'
            am_args2 += f' {response.json()}'

            device = make_washing_machine_device(apartment['apartment_id'])
            response = make_post_request(device,url_create_device,user['token'])
            apartment['devices'].append(response.json())
            wm_args1 += f' {apartment["apartment_id"]}'
            wm_args2 += f' {response.json()}'
            
            device = make_lamp_device(apartment['apartment_id'])
            response = make_post_request(device,url_create_device,user['token'])
            apartment['devices'].append(response.json())
            lm_args1 += f' {apartment["apartment_id"]}'
            lm_args2 += f' {response.json()}'
        
            device = make_gate_device(apartment['apartment_id'])
            response = make_post_request(device,url_create_device,user['token'])
            apartment['devices'].append(response.json())
            ga_args1 += f' {apartment["apartment_id"]}'
            ga_args2 += f' {response.json()}'
            
            device = make_sprinklers_device(apartment['apartment_id'])
            response = make_post_request(device,url_create_device,user['token'])
            apartment['devices'].append(response.json())
            sp_args1 += f' {apartment["apartment_id"]}'
            sp_args2 += f' {response.json()}'
            
            device = make_panels_device(apartment['apartment_id'])
            response = make_post_request(device,url_create_device,user['token'])
            apartment['devices'].append(response.json())
            so_args1 += f' {apartment["apartment_id"]}'
            so_args2 += f' {response.json()}'
            
            device = make_battery_device(apartment['apartment_id'])
            response = make_post_request(device,url_create_device, user['token'])
            apartment['devices'].append(response.json())
            bat_args1 += f' {apartment["apartment_id"]}'
            bat_args2 += f' {response.json()}'

            device = make_charger_device(apartment['apartment_id'])
            response = make_post_request(device,url_create_device, user['token'])
            apartment['devices'].append(response.json())
            ch_args1 += f' {apartment["apartment_id"]}'
            ch_args2 += f' {response.json()}'
    
    air_args1 += ')\n'
    air_args2 += ')\n'

    am_args1 += ')\n'
    am_args2 += ')\n'

    wm_args1 += ')\n'
    wm_args2 += ')\n'

    lm_args1 += ')\n'
    lm_args2 = ')\n'

    ga_args1 += ')\n'
    ga_args2 += ')\n'

    sp_args1 += ')\n'
    sp_args2 += ')\n'

    so_args1 += ')\n'
    so_args2 += ')\n'

    bat_args1 += ')\n'
    bat_args2 += ')\n'

    ch_args1 += ')\n'
    ch_args2 += ')\n'

def save_txt():
    global air_args1, air_args2, am_args1,am_args2, wm_args1, wm_args2, lm_args1, lm_args2,ga_args1,ga_args2,sp_args1,sp_args2,so_args1,so_args2,bat_args1,bat_args2,ch_args1,ch_args2

    args1 = [air_args1,am_args1, wm_args1, lm_args1, ga_args1, sp_args2, so_args1, bat_args1, ch_args1]
    args2 = [air_args2,am_args2, wm_args2,lm_args2, ga_args2, sp_args2, so_args2, bat_args2, ch_args2]
    data = []

    for i, (arg1, arg2) in enumerate(zip(args1, args2)):
        name = ["air", "am", "wm", "lm", "ga", "sp", "so", "bat", "ch"][i]
        data.append({'name': name, 'arg1': arg1, 'arg2': arg2})

    for item in data:
        with open(f'../output_{item["name"]}.txt', 'w') as f:
            f.write(item['arg1'])
            f.write(item['arg2'])

def save_to_json():
    global users
    json_string = json.dumps(users)
    with open('data.json', 'w') as f:
        json.dump(json_string, f)


def make_post_request(data, url, access_token):
  try:
    if access_token != '':
        r  = requests.post(url, headers={'Authorization': f'Bearer {access_token}'},json=data)
    else:
       r  = requests.post(url,json=data)
  except Exception as e:
    print(e)
    return
  print(r.status_code)
  return r

def make_get_request(data, url, token):
    try:
        if token:
            r  = requests.get(url, headers={'Authorization': f'Bearer {access_token}'},)
        else:
            r  = requests.get(url,json=data)
    except Exception as e:
        print(e)
        return
    print(r.status_code)

def make_air_conditioning_device(apartment_id):
   code = str(uuid.uuid4())
   return {
            'deviceCode': code,
            'deviceOption': 'AIRCONDITIONING',
            'property': apartment_id,
            'image': '/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAP//////////////////////////////////////////////////////////////////////////////////////wgALCAABAAEBAREA/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxA=',
            'name': f'test-device-air-con {apartment_id}',
            'powerUsage': 1,
            'powerSupply': 'INTERNAL_POWER',
            'panelSize': 0,
            'panelEfficiency': 0.0,
            'batteryCapacity': 0,
            'power': 0,
            'numberOfConnections': 0,
            'numberOfPanels': 0,
            'minTemp': 0,
            'maxTemp': 30
        }

def make_ambient_device(apartment_id):
   code = str(uuid.uuid4())
   return {
            'deviceCode': code,
            'deviceOption': 'AMBIENTSENSORS',
            'property': apartment_id,
            'image': '/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAP//////////////////////////////////////////////////////////////////////////////////////wgALCAABAAEBAREA/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxA=',
            'name': f'test-device-ambient {apartment_id}',
            'powerUsage': 1,
            'powerSupply': 'INTERNAL_POWER',
            'panelSize': 0,
            'panelEfficiency': 0.0,
            'batteryCapacity': 0,
            'power': 0,
            'numberOfConnections': 0,
            'numberOfPanels': 0,
            'minTemp': 0,
            'maxTemp': 0
        }

def make_washing_machine_device(apartment_id):
   code = str(uuid.uuid4())
   return {
            'deviceCode':code,
            'deviceOption': 'WASHINGMACHINE',
            'property': apartment_id,
            'image': '/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAP//////////////////////////////////////////////////////////////////////////////////////wgALCAABAAEBAREA/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxA=',
            'name': f'test-device-wash {apartment_id}',
            'powerUsage': 1,
            'powerSupply': 'INTERNAL_POWER',
            'panelSize': 0,
            'panelEfficiency': 0.0,
            'batteryCapacity': 0,
            'power': 0,
            'numberOfConnections': 0,
            'numberOfPanels': 0,
            'minTemp': 0,
            'maxTemp': 0
        }

def make_lamp_device( apartment_id):
   code = str(uuid.uuid4())
   return {
            'deviceCode': code,
            'deviceOption': 'LAMP',
            'property': apartment_id,
            'image': '/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAP//////////////////////////////////////////////////////////////////////////////////////wgALCAABAAEBAREA/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxA=',
            'name': f'test-device-lamp {apartment_id}',
            'powerUsage': 1,
            'powerSupply': 'INTERNAL_POWER',
            'panelSize': 0,
            'panelEfficiency': 0.0,
            'batteryCapacity': 0,
            'power': 0,
            'numberOfConnections': 0,
            'numberOfPanels': 0,
            'minTemp': 0,
            'maxTemp': 0
        }

def make_gate_device(apartment_id):
   code = str(uuid.uuid4())
   return {
            'deviceCode': code,
            'deviceOption': 'GATE',
            'property': apartment_id,
            'image': '/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAP//////////////////////////////////////////////////////////////////////////////////////wgALCAABAAEBAREA/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxA=',
            'name': f'test-device-gate {apartment_id}',
            'powerUsage': 1,
            'powerSupply': 'INTERNAL_POWER',
            'panelSize': 0,
            'panelEfficiency': 0.0,
            'batteryCapacity': 0,
            'power': 0,
            'numberOfConnections': 0,
            'numberOfPanels': 0,
            'minTemp': 0,
            'maxTemp': 0
        }

def make_sprinklers_device(apartment_id):
   code = str(uuid.uuid4())
   return {
            'deviceCode': code,
            'deviceOption': 'SPRINKLERS',
            'property': apartment_id,
            'image': '/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAP//////////////////////////////////////////////////////////////////////////////////////wgALCAABAAEBAREA/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxA=',
            'name': f'test-device-sprinklers {apartment_id}',
            'powerUsage': 1,
            'powerSupply': 'INTERNAL_POWER',
            'panelSize': 0,
            'panelEfficiency': 0.0,
            'batteryCapacity': 0,
            'power': 0,
            'numberOfConnections': 0,
            'numberOfPanels': 0,
            'minTemp': 0,
            'maxTemp': 0
        }

def make_panels_device(apartment_id):
   code = str(uuid.uuid4())
   return {
            'deviceCode': code,
            'deviceOption': 'SOLAR_SYSTEM',
            'property': apartment_id,
            'image': '/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAP//////////////////////////////////////////////////////////////////////////////////////wgALCAABAAEBAREA/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxA=',
            'name': f'test-device-panels {apartment_id}',
            'powerUsage': 0,
            'powerSupply': 'INTERNAL_POWER',
            'panelSize': 1,
            'panelEfficiency': 0.8,
            'batteryCapacity': 0,
            'power': 0,
            'numberOfConnections': 0,
            'numberOfPanels': 2,
            'minTemp': 0,
            'maxTemp': 0
        }

def make_battery_device( apartment_id):
   code = str(uuid.uuid4())
   return {
            'deviceCode': code,
            'deviceOption': 'BATTERY',
            'property': apartment_id,
            'image': '/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAP//////////////////////////////////////////////////////////////////////////////////////wgALCAABAAEBAREA/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxA=',
            'name': f'test-device-battery {apartment_id}',
            'powerUsage': 0,
            'powerSupply': 'INTERNAL_POWER',
            'panelSize': 0,
            'panelEfficiency': 0.0,
            'batteryCapacity': 5,
            'power': 0,
            'numberOfConnections': 0,
            'numberOfPanels': 0,
            'minTemp': 0,
            'maxTemp': 0
        }

def make_charger_device(apartment_id):
   code = str(uuid.uuid4())
   return {
            'deviceCode': code,
            'deviceOption': 'CHARGER',
            'property': apartment_id,
            'image': '/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAP//////////////////////////////////////////////////////////////////////////////////////wgALCAABAAEBAREA/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxA=',
            'name': f'test-device-charger {apartment_id}',
            'powerUsage': 0,
            'powerSupply': 'INTERNAL_POWER',
            'panelSize': 0,
            'panelEfficiency': 0.0,
            'batteryCapacity': 0,
            'power': 2,
            'numberOfConnections': 2,
            'numberOfPanels': 0,
            'minTemp': 0,
            'maxTemp': 0
        }

if __name__ == '__main__':
    access_token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0YW1hcmFpbGljMTErMTFAZ21haWwuY29tIiwicm9sZSI6IlJPTEVfVVNFUiIsImlkIjozLCJleHAiOjE3MDY3NTQ2MDIsImlhdCI6MTcwNjczNjYwMn0.Eykl_CGPkLhQcl-fX0U-_qhS6lB3Uqz7cecWUxjN_--mpqXSQSq07tn1P23wJ4dqbLMErV3pyNGrtbkpu3h9hQ"
    base_url = "http://localhost:9080/api"
    
    url_create_user = f'{base_url}/user/registration'
    url_login = f'{base_url}/user/login'
    url_create_apartment = f'{base_url}/apartment'
    url_create_device = f'{base_url}/device'

    locations = [
       {
          'address':'Laze Nančića 34, Novi Sad',
          'city':'Novi Sad',
          'country':'Serbia'
       },
       {
          'address':'Žikice Jovanovića Španca, Subotica 24111',
          'city':'Subotica',
          'country':'Serbia'
       },
       {
          'address':'Žikice Jovanovića Španca, Subotica 24111',
          'city':'Subotica',
          'country':'Serbia'
       },
       {
          'address':'Laze Nančića 34, Novi Sad',
          'city':'Novi Sad',
          'country':'Serbia'
       },
       {
          'address':'Ulitsa Severnaya 20-Ya, 2-16, Omsk, Omskaya oblast',
          'city':'Omsk',
          'country':'Russia'
       },
    ]

    air_args1 = 'arg1=('
    air_args2 = 'arg2=('

    am_args1 = 'arg1=('
    am_args2 = 'arg2=('

    wm_args1 = 'arg1=('
    wm_args2 = 'arg2=('

    lm_args1 = 'arg1=('
    lm_args2 = 'arg2=('

    ga_args1 = 'arg1=('
    ga_args2 = 'arg2=('

    sp_args1 = 'arg1=('
    sp_args2 = 'arg2=('

    so_args1 = 'arg1=('
    so_args2 = 'arg2=('

    bat_args1 = 'arg1=('
    bat_args2 = 'arg2=('

    ch_args1 = 'arg1=('
    ch_args2 = 'arg2=('


    users = []
    create_users(2)
    login_users()
    create_apartments()
    create_devices()
    save_txt()
    save_to_json()