import json
import math
import random
import sys
import time
from pprint import pprint
from time import sleep

import paho.mqtt.client as paho
import uuid as uuid_lib
import logging


logging.basicConfig(level=logging.DEBUG)


class GarageDoor:
    def __init__(self, client: paho.Client):
        self.uuid = str(uuid_lib.uuid4())
        self.client = client
        self.client.on_message = self._route_message
        self.open = False
        self.mode = 'public'
        self.allowedNumbers = []
        self.propertyId = None
        self.deviceId = None
        self.powerSupplyType = None
        self.powerUsage = None
        self.shutDown = False
        self.carsCount = 0

        logging.info(self.uuid)

    def register(self, property_id, device_id) -> str:
        if not property_id or not device_id:
            self.client.publish(f"device-setup/{self.uuid}", "New device", 0, retain=True)
            self.client.subscribe(f"server-init/{self.uuid}")
        else:
            self.propertyId = property_id
            self.deviceId = device_id
            self.client.subscribe(f"server-command/{self.deviceId}")
            self.client.subscribe(f"server-status/{self.propertyId}")
        self.client.loop_start()
        return self.uuid

    def _route_message(self, client, userdata, msg: paho.MQTTMessage):
        print(msg.topic)

        if msg.topic == f"server-init/{self.uuid}":
            self._get_credentials(msg)
            sleep(2.0)
            self.client.unsubscribe(f"server-init/{self.uuid}")
            self.client.subscribe(f"server-command/{self.deviceId}")
            self.client.subscribe(f"server-status/{self.propertyId}")
        if msg.topic == f"server-status/{self.propertyId}":
            self._configure_device(msg)
        if msg.topic == f"server-command/{self.deviceId}":
            self._handle_command(msg)

    def _get_credentials(self, msg: paho.MQTTMessage):
        msg_data = json.loads(msg.payload.decode())
        self.deviceId = msg_data["deviceId"]
        self.propertyId = msg_data["propertyId"]

        logging.debug(f"Device registered with id {self.deviceId} in property {self.propertyId}.")

    def _configure_device(self, msg: paho.MQTTMessage):
        msg_data = json.loads(msg.payload.decode())

        self.powerSupplyType = msg_data["apartmentDevices"]["vehicleGateList"][str(self.deviceId)]["powerSupplyType"]
        self.powerUsage = msg_data["apartmentDevices"]["vehicleGateList"][str(self.deviceId)]["powerUsage"]
        self.mode = "private" if msg_data["apartmentDevices"]["vehicleGateList"][str(self.deviceId)]["privateMode"] else "public"
        numbers = msg_data["apartmentDevices"]["vehicleGateList"][str(self.deviceId)]["allowedNumbers"]
        self.allowedNumbers = json.loads(numbers if numbers else "[]")
        logging.debug(f"Gates configured to use {self.powerUsage} from {self.powerSupplyType}.")

    def _handle_command(self, msg: paho.MQTTMessage):
        msg_data = json.loads(msg.payload.decode())

        print(msg_data["command"])
        client.publish(
            "measurements",
            f'command,device_id={self.deviceId},device_type=gate,user_id={msg_data["userId"]} '
            f'command="{msg_data["command"]}"',
            0,
        )

        if msg_data["command"] == "turn_off":
            self.shutDown = True
            return

        if msg_data["command"] == "turn_on":
            pass
        elif msg_data["command"] == "private":
            self.mode = "private"
        elif msg_data["command"] == "public":
            self.mode = "public"
        elif msg_data["command"].startswith("add-number/"):
            number = msg_data["command"].split("/")[-1]
            self.allowedNumbers.append(number)
        elif msg_data["command"].startswith("remove-number/"):
            number = msg_data["command"].split("/")[-1]
            self.allowedNumbers.remove(number)
        elif msg_data["command"] == "close-gate":
            if self.open:
                self.open = False
                client.publish(
                    "measurements",
                    f'gate_log,device_type=gate,property_id={str(self.propertyId)},device_id={self.deviceId} '
                    f'msg="Closing",mode="{self.mode}"',
                    0,
                )
        elif msg_data["command"] == "open-gate":
            number = random.choice(self.allowedNumbers + ['RANDOM2NUMBER', 'HE1HE1HE'])
            client.publish(
                "measurements",
                f'gate_log,device_type=gate,property_id={str(self.propertyId)},device_id={self.deviceId} '
                f'msg="number detected: {number}",mode="{self.mode}"',
                0,
            )
            self.carsCount += 1
            client.publish(
                "measurements",
                f'gate_log,device_type=gate,property_id={str(self.propertyId)},device_id={self.deviceId} '
                f'msg="Opening",mode="{self.mode}"',
                0,
            )

        if not self.propertyId or not self.deviceId or not self.powerSupplyType or not self.powerUsage:
            print("Skipping command: not configured yet")
            return

    def simulate(self):
        while True:
            if self.shutDown:
                break
            sleep(1.0)

            if not self.propertyId or not self.deviceId or not self.powerSupplyType or not self.powerUsage:
                continue

            consumption = self.powerUsage * 0.01 * random.random()
            if self.open:
                consumption += self.powerUsage
                self.open = False
                client.publish(
                    "measurements",
                    f'gate_log,device_type=gate,property_id={str(self.propertyId)},device_id={self.deviceId} '
                    f'msg="Closing",mode="{self.mode}"',
                    0,
                )

            if random.random() < 0.3:
                number = random.choice(self.allowedNumbers + ['RANDOM2NUMBER', 'HE1HE1HE'])
                client.publish(
                    "measurements",
                    f'gate_log,device_type=gate,property_id={str(self.propertyId)},device_id={self.deviceId} '
                    f'msg="number detected: {number}",mode="{self.mode}"',
                    0,
                )
                if self.mode == "public" or number in self.allowedNumbers:
                    client.publish(
                        "measurements",
                        f'gate_log,device_type=gate,property_id={str(self.propertyId)},device_id={self.deviceId} '
                        f'msg="Opening",mode="{self.mode}"',
                        0,
                    )
                    self.open = True
                    self.carsCount += 1
                    consumption += self.powerUsage
                else:
                    client.publish(
                        "measurements",
                        f'gate_log,device_type=gate,property_id={str(self.propertyId)},device_id={self.deviceId} '
                        f'msg="Number not allowed",mode="{self.mode}"',
                        0,
                    )

            if random.random() < 0.4 and self.carsCount > 0:
                consumption += self.powerUsage
                client.publish(
                    "measurements",
                    f'gate_log,device_type=gate,property_id={str(self.propertyId)},device_id={self.deviceId} '
                    f'msg="Opening",mode="{self.mode}"',
                    0,
                )
                self.open = True
                self.carsCount -= 1
                client.publish(
                    "measurements",
                    f'gate_log,device_type=gate,property_id={str(self.propertyId)},device_id={self.deviceId} '
                    f'msg="Car left",mode="{self.mode}"',
                    0,
                )

            client.publish(
                f"devices-consume/{self.propertyId}",
                str(consumption),
                0,
                retain=False
            )

            client.publish(
                "measurements",
                f'readings,device_type=gate,property_id={str(self.propertyId)},device_id={self.deviceId} '
                f'cars_count={self.carsCount}',
                0,
            )
            client.publish(
                "measurements",
                f'gates_open,device_type=gate,property_id={str(self.propertyId)},device_id={self.deviceId} '
                f'gates_open={self.open}',
                0,
            )
            client.publish(
                "measurements",
                f'heartbeat,device_id={self.deviceId},device_type=gate '
                f'status="ok"',
                0,
            )


if __name__ == '__main__':
    client = paho.Client()
    client.username_pw_set("devuser", password="changeme")
    if client.connect("localhost", 1883, 60) != 0:
        logging.error("Couldn't connect to the mqtt broker")
        sys.exit(1)

    gates = GarageDoor(client)

    try:
        device_id = sys.argv[1]
        property_id = sys.argv[2]
        gates.register(
            device_id=device_id,
            property_id=property_id
        )
    except IndexError:
        gates.register(None, None)

    try:
        gates.simulate()
    except KeyboardInterrupt:
        gates.client.loop_stop()
        gates.client.disconnect()
        sys.exit(0)

