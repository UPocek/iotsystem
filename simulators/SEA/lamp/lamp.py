import json
import math
import random
import sys
import time
from pprint import pprint
from time import sleep

import paho.mqtt.client as paho
import uuid as uuid_lib
import logging


logging.basicConfig(level=logging.DEBUG)


class ExternalLamp:
    def __init__(self, client: paho.Client, minimalLight: float = 100):
        self.uuid = str(uuid_lib.uuid4())
        self.client = client
        self.client.on_message = self._route_message
        self.on = False
        self.mode = 'auto'
        self.illuminationLevel = 0.0
        self.propertyId = None
        self.deviceId = None
        self.powerSupplyType = None
        self.powerUsage = None
        self.shutDown = False

        logging.info(self.uuid)

    def register(self, property_id, device_id) -> str:
        if not property_id or not device_id:
            self.client.publish(f"device-setup/{self.uuid}", "New device", 0, retain=True)
            self.client.subscribe(f"server-init/{self.uuid}")
        else:
            self.propertyId = property_id
            self.deviceId = device_id
            self.client.subscribe(f"server-command/{self.deviceId}")
            self.client.subscribe(f"server-status/{self.propertyId}")
        self.client.loop_start()
        return self.uuid

    def _route_message(self, client, userdata, msg: paho.MQTTMessage):
        print(msg.topic)

        if msg.topic == f"server-init/{self.uuid}":
            self._get_credentials(msg)
            sleep(2.0)
            self.client.unsubscribe(f"server-init/{self.uuid}")
            self.client.subscribe(f"server-command/{self.deviceId}")
            self.client.subscribe(f"server-status/{self.propertyId}")
        if msg.topic == f"server-status/{self.propertyId}":
            self._configure_device(msg)
        if msg.topic == f"server-command/{self.deviceId}":
            self._handle_command(msg)

    def _get_credentials(self, msg: paho.MQTTMessage):
        msg_data = json.loads(msg.payload.decode())
        self.deviceId = msg_data["deviceId"]
        self.propertyId = msg_data["propertyId"]

        logging.debug(f"Device registered with id {self.deviceId} in property {self.propertyId}.")

    def _configure_device(self, msg: paho.MQTTMessage):
        msg_data = json.loads(msg.payload.decode())

        self.powerSupplyType = msg_data["apartmentDevices"]["lampList"][str(self.deviceId)]["powerSupplyType"]
        self.powerUsage = msg_data["apartmentDevices"]["lampList"][str(self.deviceId)]["powerUsage"]
        self.mode = "auto" if msg_data["apartmentDevices"]["lampList"][str(self.deviceId)]["automaticMode"] else "manual"
        logging.debug(f"Lamp configured to use {self.powerUsage} from {self.powerSupplyType}.")

    def _handle_command(self, msg: paho.MQTTMessage):
        msg_data = json.loads(msg.payload.decode())

        print(msg_data["command"])
        client.publish(
            "measurements",
            f'command,device_id={self.deviceId},device_type=lamp,user_id={msg_data["userId"]} '
            f'command="{msg_data["command"]}"',
            0,
        )

        if msg_data["command"] == "turn_off":
            self.shutDown = True
            return

        if msg_data["command"] == "turn_on":
            pass
        elif msg_data["command"] == "auto":
            self.mode = "auto"
        elif msg_data["command"] == "manual":
            self.mode = "manual"
        elif msg_data["command"] == "light-off":
            self.on = False
        elif msg_data["command"] == "light-on":
            self.on = True

        if not self.propertyId or not self.deviceId or not self.powerSupplyType or not self.powerUsage:
            print("Skipping command: not configured yet")
            return

    def simulate(self):
        while True:
            if self.shutDown:
                break
            sleep(1.0)

            if not self.propertyId or not self.deviceId or not self.powerSupplyType or not self.powerUsage:
                continue

            if self.on:
                consumption = self.powerUsage + self.powerUsage * 0.1 * (random.random() - 0.5)
            else:
                consumption = self.powerUsage * 0.01 * random.random()

            illumination = math.sin(time.time() / 60 * math.pi) * 500 + 500
            if self.on:
                illumination += 200

            if self.mode == "auto":
                if illumination < 300:
                    self.on = True
                elif illumination > 400:
                    self.on = False

            client.publish(
                f"devices-consume/{self.propertyId}",
                str(consumption),
                0,
                retain=False
            )

            client.publish(
                "measurements",
                f'readings,device_type=lamp,property_id={str(self.propertyId)},device_id={self.deviceId} '
                f'illumination={illumination},auto={self.mode == "auto"},on={self.on}',
                0,
            )
            client.publish(
                "measurements",
                f'heartbeat,device_id={self.deviceId},device_type=lamp '
                f'status="ok"',
                0,
            )


if __name__ == '__main__':
    client = paho.Client()
    client.username_pw_set("devuser", password="changeme")
    if client.connect("localhost", 1883, 60) != 0:
        logging.error("Couldn't connect to the mqtt broker")
        sys.exit(1)

    lamp = ExternalLamp(client)

    try:
        device_id = sys.argv[1]
        property_id = sys.argv[2]
        lamp.register(
            device_id=device_id,
            property_id=property_id
        )
    except IndexError:
        lamp.register(None, None)

    try:
        lamp.simulate()
    except KeyboardInterrupt:
        lamp.client.loop_stop()
        lamp.client.disconnect()
        sys.exit(0)
