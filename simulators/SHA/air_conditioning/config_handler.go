package main

import (
	"fmt"
	"os"

	"gopkg.in/yaml.v2"
)

type Config struct {
	DeviceID   int `yaml:"device_id"`
	PropertyID int `yaml:"property_id"`
}

func writeToConfigFile(config Config) {
	yamlData, err := yaml.Marshal(&config)
	if err != nil {
		fmt.Println("Error marshalling YAML:", err)
		return
	}

	err = os.WriteFile("config.yaml", yamlData, 0644)
	if err != nil {
		fmt.Println("Error writing to YAML file:", err)
		return
	}
}

func loadConfigFile() Config {
	var config Config

	yamlFile, err := os.ReadFile("config.yaml")
	if err != nil {
		panic("Error reading YAML file:")
	}

	err = yaml.Unmarshal(yamlFile, &config)
	if err != nil {
		panic("Error unmarshalling YAML:")
	}

	return config
}

func loadDeviceConfigurations(config Config) {
	DEVICE_ID = config.DeviceID
	PROPERTY_ID = config.PropertyID
	ACTIVE = true
}
