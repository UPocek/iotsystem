package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"sync"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

type InitMessage struct {
	DeviceId   int `json:"deviceId"`
	PropertyId int `json:"propertyId"`
}

type Command struct {
	UserId       int    `json:"userId"`
	UserFullName string `json:"userFullName"`
	Command      string `json:"command"`
}

type AmbientCondition struct {
	PowerSupplyType string `json:"powerSupplyType"`
	PowerUsage      int    `json:"powerUsage"`
}

type AirConditioning struct {
	PowerSupplyType string `json:"powerSupplyType"`
	PowerUsage      int    `json:"powerUsage"`
	MinTemp         int    `json:"minTemp"`
	MaxTemp         int    `json:"maxTemp"`
	CurrentTemp     int    `json:"currentTemp"`
	CurrentMode     string `json:"mode"`
}

type WashingMachine struct {
	PowerSupplyType    string `json:"powerSupplyType"`
	PowerUsage         int    `json:"powerUsage"`
	CurrentProgram     string `json:"currentProgram"`
	ProgramEndDateTime string `json:"programEndDateTime"`
}

type Lamp struct {
	PowerSupplyType string `json:"powerSupplyType"`
	PowerUsage      int    `json:"powerUsage"`
}

type VehicleGate struct {
	PowerSupplyType string `json:"powerSupplyType"`
	PowerUsage      int    `json:"powerUsage"`
}

type SprinklerSystem struct {
	PowerSupplyType string `json:"powerSupplyType"`
	PowerUsage      int    `json:"powerUsage"`
}

type SolarSystem struct {
	NumberOfPanels  int     `json:"numberOfPanels"`
	PanelSize       int     `json:"panelSize"`
	PanelEfficiency float64 `json:"panelEfficiency"`
	PowerUsage      int     `json:"powerUsage"`
}

type Battery struct {
	BatteryCapacity int     `json:"batteryCapacity"`
	Level           float64 `json:"level"`
}

type Charger struct {
	NumberOfConnections int     `json:"numberOfConnections"`
	Power               int     `json:"power"`
	Fullness            float64 `json:"fullness"`
}

type ApartmentDevices struct {
	AmbientConditionsList map[string]AmbientCondition `json:"ambientConditionsList"`
	AirConditioningList   map[string]AirConditioning  `json:"airConditioningList"`
	WashingMachineList    map[string]WashingMachine   `json:"washingMachineList"`
	LampList              map[string]Lamp             `json:"lampList"`
	VehicleGateList       map[string]VehicleGate      `json:"vehicleGateList"`
	SprinklerSystemList   map[string]SprinklerSystem  `json:"sprinklerSystemList"`
	SolarSystemList       map[string]SolarSystem      `json:"solarSystemList"`
	BatteryList           map[string]Battery          `json:"batteryList"`
	ChargerList           map[string]Charger          `json:"chargerList"`
}

type Location struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

type Status struct {
	ApartmentId      int              `json:"id"`
	ApartmentDevices ApartmentDevices `json:"apartmentDevices"`
	Location         Location         `json:"location"`
	City             string           `json:"city"`
}

var messagePubHandler mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	fmt.Printf("Message received: %s - %s\n", msg.Topic(), msg.Payload())

	switch {
	case strings.HasPrefix(msg.Topic(), SERVER_INIT_TOPIC):
		var message InitMessage
		err := json.Unmarshal(msg.Payload(), &message)
		if err != nil {
			fmt.Println("Invalid server message: ", err)
			return
		}
		DEVICE_ID = message.DeviceId
		PROPERTY_ID = message.PropertyId
		ACTIVE = true
		unsub(client, fmt.Sprintf("%s/%s", SERVER_INIT_TOPIC, DEVICE_CODE))
		sub(client, fmt.Sprintf("%s/%d", SERVER_COMMAND_TOPIC, DEVICE_ID))
		sub(client, fmt.Sprintf("%s/%d", SERVER_STATUS_TOPIC, PROPERTY_ID))
	case strings.HasPrefix(msg.Topic(), SERVER_COMMAND_TOPIC):
		var command Command
		err := json.Unmarshal(msg.Payload(), &command)
		if err != nil {
			fmt.Println("Invalid server message: ", err)
			return
		}
		switch {
		case strings.HasPrefix(command.Command, "turn_on"):
			ACTIVE = true
		case strings.HasPrefix(command.Command, "turn_off"):
			ACTIVE = false
		case strings.HasPrefix(command.Command, "heat"):
			DEVICE_CURRENT_MODE = "HEAT"
			DEVICE_CURRENT_TEMP, _ = strconv.Atoi(strings.Split(command.Command, "_")[1])
		case strings.HasPrefix(command.Command, "cool"):
			DEVICE_CURRENT_MODE = "COOL"
			DEVICE_CURRENT_TEMP, _ = strconv.Atoi(strings.Split(command.Command, "_")[1])
		case strings.HasPrefix(command.Command, "dry"):
			DEVICE_CURRENT_MODE = "DRY"
			DEVICE_CURRENT_TEMP, _ = strconv.Atoi(strings.Split(command.Command, "_")[1])
		case strings.HasPrefix(command.Command, "auto"):
			DEVICE_CURRENT_MODE = "AUTO"
			DEVICE_CURRENT_TEMP, _ = strconv.Atoi(strings.Split(command.Command, "_")[1])
		}
		acqknowledgeCommand(client, command.UserFullName, command.UserId, command.Command)
	case strings.HasPrefix(msg.Topic(), SERVER_STATUS_TOPIC):
		var status Status
		err := json.Unmarshal(msg.Payload(), &status)
		if err != nil {
			panic(err)
		}
		POWER_SUPPLY_TYPE = status.ApartmentDevices.AirConditioningList[strconv.Itoa(DEVICE_ID)].PowerSupplyType
		POWER_USAGE_IN_KWH = status.ApartmentDevices.AirConditioningList[strconv.Itoa(DEVICE_ID)].PowerUsage
		DEVICES_LATITUDE = float32(status.Location.Latitude)
		DEVICES_LONGITUDE = float32(status.Location.Longitude)
		DEVICE_MIN_TEMP = status.ApartmentDevices.AirConditioningList[strconv.Itoa(DEVICE_ID)].MinTemp
		DEVICE_MAX_TEMP = status.ApartmentDevices.AirConditioningList[strconv.Itoa(DEVICE_ID)].MaxTemp
		DEVICE_CURRENT_TEMP = status.ApartmentDevices.AirConditioningList[strconv.Itoa(DEVICE_ID)].CurrentTemp
		DEVICE_CURRENT_MODE = status.ApartmentDevices.AirConditioningList[strconv.Itoa(DEVICE_ID)].CurrentMode
		ACTIVE = true
	}

}

func heartBeat(client mqtt.Client, wg *sync.WaitGroup) {
	defer wg.Done()

	for !SHUTDOWN {
		message := fmt.Sprintf("heartbeat,device_id=%d,device_type=%s status=\"ok\"", DEVICE_ID, DEVICE_TYPE)
		if ACTIVE {
			publish(client, DB_TOPIC, message, false)
		}
		time.Sleep(time.Second * time.Duration(HEARTBEAT_INTERVAL_IN_SECONDS))
	}
}

func acqknowledgeCommand(client mqtt.Client, userFullName string, userId int, command string) {

	message := fmt.Sprintf("command,device_id=%d,device_type=%s,user_full_name=%s,user_id=%d command=\"%s\"", DEVICE_ID, DEVICE_TYPE, userFullName, userId, command)
	publish(client, DB_TOPIC, message, false)
	time.Sleep(time.Second * time.Duration(1))

}

func energyConsumption(client mqtt.Client, wg *sync.WaitGroup) {
	defer wg.Done()

	for !SHUTDOWN {
		if ACTIVE && POWER_SUPPLY_TYPE == POWER_SUPPLY_TYPE_INTERNAL {
			publish(client, fmt.Sprintf("%s/%d", ENERGY_CONSUMPTION_TOPIC, PROPERTY_ID), fmt.Sprintf("%f", (float64(POWER_USAGE_IN_KWH))/3600.0*float64(ENERGY_CONSUMPTION_INTERVAL_IN_SECONDS)), false)
		}
		time.Sleep(time.Second * time.Duration(ENERGY_CONSUMPTION_INTERVAL_IN_SECONDS))
	}
}

var connectHandler mqtt.OnConnectHandler = func(client mqtt.Client) {
	fmt.Println("Connected")
}

var connectLostHandler mqtt.ConnectionLostHandler = func(client mqtt.Client, err error) {
	fmt.Printf("Connect lost: %v", err)
}

func getClient() mqtt.Client {
	broker := "localhost"
	port := 1883
	opts := mqtt.NewClientOptions()
	opts.AddBroker(fmt.Sprintf("tcp://%s:%d", broker, port))
	// opts.SetClientID("device1")
	opts.SetUsername("devuser")
	opts.SetPassword("changeme")
	opts.SetDefaultPublishHandler(messagePubHandler)
	opts.OnConnect = connectHandler
	opts.OnConnectionLost = connectLostHandler
	client := mqtt.NewClient(opts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
	return client
}

func publish(client mqtt.Client, topic string, message string, retained bool) {
	token := client.Publish(topic, 1, retained, message)
	token.Wait()
	fmt.Printf("Published message %s to topic: %s\n", message, topic)
}

func sub(client mqtt.Client, topic string) {
	token := client.Subscribe(topic, 1, nil)
	token.Wait()
	fmt.Printf("Subscribed to topic: %s\n", topic)
}

func unsub(client mqtt.Client, topic string) {
	token := client.Unsubscribe(topic)
	token.Wait()
	fmt.Printf("Unsubscribed from topic: %s\n", topic)
}
