package main

import (
	"fmt"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/google/uuid"
)

func startDeviceSetup(client mqtt.Client) {
	fmt.Println("*** Ambient conditions sensor ***")

	DEVICE_CODE = uuid.New().String()
	fmt.Printf("\nDevice connection code is: %s \n\n", DEVICE_CODE)

	publish(client, fmt.Sprintf("%s/%s", SERVER_DEVICE_SETUP_TOPIC, DEVICE_CODE), "New device", true)
	sub(client, fmt.Sprintf("%s/%s", SERVER_INIT_TOPIC, DEVICE_CODE))
}
