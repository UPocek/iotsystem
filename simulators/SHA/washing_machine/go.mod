module device1/main

go 1.21.4

require github.com/eclipse/paho.mqtt.golang v1.4.3

require (
	github.com/google/uuid v1.4.0
	github.com/gorilla/websocket v1.5.0 // indirect
	golang.org/x/net v0.8.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
	gopkg.in/yaml.v2 v2.4.0
)
