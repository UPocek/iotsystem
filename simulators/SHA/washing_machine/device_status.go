package main

import (
	"fmt"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

func loadCurrentStatus(client mqtt.Client) {
	sub(client, fmt.Sprintf("%s/%d", SERVER_STATUS_TOPIC, PROPERTY_ID))
	sub(client, fmt.Sprintf("%s/%d", SERVER_COMMAND_TOPIC, DEVICE_ID))
}
