package main

import (
	"fmt"
	"sync"
)

func printDeviceOptions() {
	fmt.Printf("Device ID:%d - active: %t\nOptions(enter option to execute command):\nq) Quit\ns) On/Off\n>> ", DEVICE_ID, ACTIVE)
}

func userInput(wg *sync.WaitGroup) {
	defer wg.Done()

	var input string

	for !SHUTDOWN {
		printDeviceOptions()
		fmt.Scanln(&input)
		switch input {
		case "s":
			ACTIVE = !ACTIVE
		case "q":
			SHUTDOWN = true
			writeToConfigFile(Config{DeviceID: DEVICE_ID, PropertyID: PROPERTY_ID})
		}
	}
}
