from locust import HttpUser, task, between
import random
from datetime import datetime, timedelta

username="0@gmail.com"
password="password"
apartment_id = 2
solar_device_ids = [25,34]
en_device_ids = [26,35]
user_id = 1
max_days_ago = 3
max_range_days = 30

class WebsiteUser(HttpUser):
    wait_time = between(1, 5)
    host = 'http://localhost:9080'

    def __init__(self, parent):
        super(WebsiteUser, self).__init__(parent)
        self.token = ''


    def login(self):
        response = self.client.post("/api/user/login", json={
        "username": username, 
        "password": password
        })
        
        if "accessToken" not in response.json():
            print(response.json())
        else:
            self.token = response.json()['accessToken']

    def on_start(self):
        self.login()

    @task
    def get_all_solar_actions(self):
        random_device_id = random.choice(solar_device_ids)
        response = self.client.get(f"/api/device/actions/solar/{apartment_id}/{random_device_id}", headers={'Authorization':'Bearer ' + self.token}, name='/api/device/actions/solar/{apartmentId}/{deviceId}')
        print(response.json())

    @task
    def get_all_energy_report(self):
        random_device_id = random.choice(en_device_ids)
        response = self.client.get(f"/api/readings/batterySystem/live", headers={'Authorization':'Bearer ' + self.token}, name='/api/readings/batterySystem/live', params={'apartmentId':apartment_id, 'deviceId':random_device_id})
        print(response.json())