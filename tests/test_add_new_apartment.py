from locust import FastHttpUser, constant_throughput, task, constant
import random

class RegistrationTest(FastHttpUser):
    # wait_time = constant_throughput(0.1)
    wait_time = constant(0.1)
    host = "http://localhost:9080"
    accessToken = ''

    @task
    def insert_apartments(self):
        with self.client.post("/api/user/login", json=random.choice(users), catch_response=True) as response:
            if "accessToken" not in response.json():
                response.failure(f"{response.status_code}")
            else:
                self.accessToken = response.json()['accessToken']
            for _ in range(100):
                location = random.choice(locations)
                apartment = {'name':'apartment-test','location':None,'address':location['address'],'city':location['city'],'country':location['country'],'squareFootage':100,'numberOfFloors':2,'numberOfRooms':5, 'imageUrl':'/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAP//////////////////////////////////////////////////////////////////////////////////////wgALCAABAAEBAREA/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxA='}
                response = self.client.post("/api/apartment", json=apartment, headers={"Authorization": f"Bearer {self.accessToken}"})
        


users = [
{"username": "uros.pocek+1@gmail.com", "password": "uros123"},
{"username": "uros.pocek+test@gmail.com", "password": "test123"}
]
locations = [
       {
          'address':'Laze Nančića 34, Novi Sad',
          'city':'Novi Sad',
          'country':'Serbia'
       },
       {
          'address':'Žikice Jovanovića Španca, Subotica 24111',
          'city':'Subotica',
          'country':'Serbia'
       },
       {
          'address':'Žikice Jovanovića Španca, Subotica 24111',
          'city':'Subotica',
          'country':'Serbia'
       },
       {
          'address':'Laze Nančića 34, Novi Sad',
          'city':'Novi Sad',
          'country':'Serbia'
       },
       {
          'address':'Ulitsa Severnaya 20-Ya, 2-16, Omsk, Omskaya oblast',
          'city':'Omsk',
          'country':'Russia'
       },
    ]