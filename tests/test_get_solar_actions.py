from locust import HttpUser, task, between
import random
from datetime import datetime, timedelta

username="0@gmail.com"
password="password"
apartment_ids = [2]
device_ids = [25,34]
user_id = 1
max_days_ago = 3
max_range_days = 30

class WebsiteUser(HttpUser):
    wait_time = between(1, 5)
    host = 'http://localhost:9080'

    def __init__(self, parent):
        super(WebsiteUser, self).__init__(parent)
        self.token = ''


    def login(self):
        response = self.client.post("/api/user/login", json={
        "username": username, 
        "password": password
        })
        
        if "accessToken" not in response.json():
            print(response.json())
        else:
            self.token = response.json()['accessToken']

    def on_start(self):
        self.login()

    @task
    def get_all_solar_actions(self):
        random_device_id = random.choice(device_ids)
        random_apartment_id = random.choice(apartment_ids)
        response = self.client.get(f"/api/device/actions/solar/{random_apartment_id}/{random_device_id}", headers={'Authorization':'Bearer ' + self.token}, name='/api/device/actions/solar/{apartmentId}/{deviceId}')
        print(response.json())

    @task
    def get_filtered_solar_actions(self):
        random_days_ago = random.randint(1, max_days_ago)
        random_days_range = random.randint(1, max_range_days)
        random_start_date = datetime.today() - timedelta(days=random_days_ago)
        random_end_date = random_start_date - timedelta(days=random_days_range)
        random_device_id = random.choice(device_ids)
        random_apartment_id = random.choice(apartment_ids)
        response = self.client.get(f"/api/device/actions/solar/{random_apartment_id}/{random_device_id}", headers={'Authorization':'Bearer ' + self.token}, name='/api/device/actions/solar/{apartmentId}/{deviceId}', params={'startDate': random_start_date, 'endDate': random_end_date, 'userId': user_id})
        print(response.json())