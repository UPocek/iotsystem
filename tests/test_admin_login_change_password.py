from locust import FastHttpUser, constant_throughput, task, constant

class RegistrationTest(FastHttpUser):
    wait_time = constant_throughput(100)
    # wait_time = constant(1.0)
    host = "http://localhost:9080"
    accessToken = ''
    password_index = 0
    passowords = ['admin123', 'admin1234', 'admin12345']

    @task
    def admin_login_change_passowrd(self):
        self.client.get('/login')
        with self.client.post("/api/user/login", json={'username': 'admin', 'password': self.passowords[self.password_index]}, catch_response=True) as response:
            if "accessToken" not in response.json():
                response.failure(f"{response.status_code}")
            else:
                self.accessToken = response.json()['accessToken']
        self.client.get("/")
        self.client.get("/change-password")
        with self.client.put("/api/user/change-password", json={'password': self.passowords[(self.password_index+1) % 3]}, headers={"Authorization": f"Bearer {self.accessToken}"}, catch_response=True) as cpResponse:
            if cpResponse.status_code != 200:
                cpResponse.failure(f"{cpResponse.status_code}")
        self.password_index = (self.password_index + 1) % 3