from locust import HttpUser, task, between
import random
from datetime import datetime, timedelta

username="0@gmail.com"
password="password"
apartment_id = 2
device_ids = [27,36]
users = [1,2]
commands = ['turn_on, turn_off']

class WebsiteUser(HttpUser):
    wait_time = between(1, 2)
    host = 'http://localhost:9080'

    def __init__(self, parent):
        super(WebsiteUser, self).__init__(parent)
        self.token = ''


    def login(self):
        response = self.client.post("/api/user/login", json={
        "username": username, 
        "password": password
        })
        
        if "accessToken" not in response.json():
            print(response.json())
        else:
            self.token = response.json()['accessToken']

    def on_start(self):
        self.login()

    @task
    def send_command(self):
        random_device_id = random.choice(device_ids)
        random_command = random.choice(commands)
        random_user = random.choice(users)
        command = {
                'command':random_command,
                'userFullName':"name_surname",
                'userId':random_user
            }
        
        response = self.client.post(f"/api/device/on-off/{apartment_id}/{random_device_id}", headers={'Authorization':'Bearer ' + self.token}, name='/api/device/on-off/{apartment_id}/{random_device_id}',json=command)
        print(response.json())

    @task
    def send_fullness(self):
        random_device_id = random.choice(device_ids)
        random_user = random.choice(users)
        random_command = f'percentage_battery_full/{random.randint(50,100) / 100}'
        command = {
                'command':random_command,
                'userFullName':"name_surname",
                'userId':random_user
            }
        
        response = self.client.post(f"/api/device/fullness/{apartment_id}/{random_device_id}", headers={'Authorization':'Bearer ' + self.token}, name='/api/device/fullness/{apartmentId}/{deviceId}',json=command)
        print(response.json())

