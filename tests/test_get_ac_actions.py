from locust import FastHttpUser, task, constant

class RegistrationTest(FastHttpUser):
    wait_time = constant(0.1)
    host = "http://localhost:9080"
    accessToken = ''

    @task
    def get_ac_actions(self):
        self.client.get('/')
        responseApartments = self.client.get("/api/apartment", headers={"Authorization": f"Bearer {self.accessToken}"})
        self.client.get(f'/apartments/{responseApartments.json()[0]["id"]}')
        responseDevices = self.client.post(f"/api/device/subscribeStatus/{responseApartments.json()[0]['id']}", headers={"Authorization": f"Bearer {self.accessToken}"})
        self.client.get(f'/devices/{responseDevices.json()["airConditioningList"][0]["id"]}?apartment={responseApartments.json()[0]["id"]}&device_type=airConditioning')
        self.client.get(f'/api/device/actions/airConditioning/{responseApartments.json()[0]["id"]}/{responseDevices.json()["airConditioningList"][0]["id"]}/', headers={"Authorization": f"Bearer {self.accessToken}"})


    def on_start(self):
        with self.client.post("/api/user/login", json={"username": "uros.pocek+1@gmail.com", "password": "uros123"}, catch_response=True) as response:
            if "accessToken" not in response.json():
                response.failure(f"{response.status_code}")
            else:
                self.accessToken = response.json()['accessToken']