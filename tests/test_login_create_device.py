from locust import HttpUser, task, between
import uuid

air_conditioning_device= {
                    'deviceCode': str(uuid.uuid4()),
                    'deviceOption': 'AIRCONDITIONING',
                    'property': 1,
                    'image': '/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAP//////////////////////////////////////////////////////////////////////////////////////wgALCAABAAEBAREA/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxA=',
                    'name': f'test-device-air-con {str(uuid.uuid4())}',
                    'powerUsage': 1,
                    'powerSupply': 'INTERNAL_POWER',
                    'panelSize': 0,
                    'panelEfficiency': 0.0,
                    'batteryCapacity': 0,
                    'power': 0,
                    'numberOfConnections': 0,
                    'numberOfPanels': 0,
                    'minTemp': 0,
                    'maxTemp': 30
                }

ambient_device= {
                    'deviceCode': str(uuid.uuid4()),
                    'deviceOption': 'AMBIENTSENSORS',
                    'property': 1,
                    'image': '/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAP//////////////////////////////////////////////////////////////////////////////////////wgALCAABAAEBAREA/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxA=',
                    'name': f'test-device-ambient {str(uuid.uuid4())}',
                    'powerUsage': 1,
                    'powerSupply': 'INTERNAL_POWER',
                    'panelSize': 0,
                    'panelEfficiency': 0.0,
                    'batteryCapacity': 0,
                    'power': 0,
                    'numberOfConnections': 0,
                    'numberOfPanels': 0,
                    'minTemp': 0,
                    'maxTemp': 0
                }

washing_machine_device ={
                    'deviceCode': str(uuid.uuid4()),
                    'deviceOption': 'WASHINGMACHINE',
                    'property': 1,
                    'image': '/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAP//////////////////////////////////////////////////////////////////////////////////////wgALCAABAAEBAREA/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxA=',
                    'name': f'test-device-wash {str(uuid.uuid4())}',
                    'powerUsage': 1,
                    'powerSupply': 'INTERNAL_POWER',
                    'panelSize': 0,
                    'panelEfficiency': 0.0,
                    'batteryCapacity': 0,
                    'power': 0,
                    'numberOfConnections': 0,
                    'numberOfPanels': 0,
                    'minTemp': 0,
                    'maxTemp': 0
                }

lamp_device= {
                    'deviceCode': str(uuid.uuid4()),
                    'deviceOption': 'LAMP',
                    'property': 1,
                    'image': '/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAP//////////////////////////////////////////////////////////////////////////////////////wgALCAABAAEBAREA/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxA=',
                    'name': f'test-device-lamp {str(uuid.uuid4())}',
                    'powerUsage': 1,
                    'powerSupply': 'INTERNAL_POWER',
                    'panelSize': 0,
                    'panelEfficiency': 0.0,
                    'batteryCapacity': 0,
                    'power': 0,
                    'numberOfConnections': 0,
                    'numberOfPanels': 0,
                    'minTemp': 0,
                    'maxTemp': 0
                }

gate_device = {
                    'deviceCode': str(uuid.uuid4()),
                    'deviceOption': 'GATE',
                    'property': 1,
                    'image': '/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAP//////////////////////////////////////////////////////////////////////////////////////wgALCAABAAEBAREA/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxA=',
                    'name': f'test-device-gate {str(uuid.uuid4())}',
                    'powerUsage': 1,
                    'powerSupply': 'INTERNAL_POWER',
                    'panelSize': 0,
                    'panelEfficiency': 0.0,
                    'batteryCapacity': 0,
                    'power': 0,
                    'numberOfConnections': 0,
                    'numberOfPanels': 0,
                    'minTemp': 0,
                    'maxTemp': 0
                }

sprinklers_device = {
                    'deviceCode': str(uuid.uuid4()),
                    'deviceOption': 'SPRINKLERS',
                    'property': 1,
                    'image': '/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAP//////////////////////////////////////////////////////////////////////////////////////wgALCAABAAEBAREA/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxA=',
                    'name': f'test-device-sprinklers {str(uuid.uuid4())}',
                    'powerUsage': 1,
                    'powerSupply': 'INTERNAL_POWER',
                    'panelSize': 0,
                    'panelEfficiency': 0.0,
                    'batteryCapacity': 0,
                    'power': 0,
                    'numberOfConnections': 0,
                    'numberOfPanels': 0,
                    'minTemp': 0,
                    'maxTemp': 0
                }

panels_device = {
                    'deviceCode': str(uuid.uuid4()),
                    'deviceOption': 'SOLAR_SYSTEM',
                    'property': 1,
                    'image': '/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAP//////////////////////////////////////////////////////////////////////////////////////wgALCAABAAEBAREA/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxA=',
                    'name': f'test-device-panels {str(uuid.uuid4())}',
                    'powerUsage': 0,
                    'powerSupply': 'INTERNAL_POWER',
                    'panelSize': 1,
                    'panelEfficiency': 0.8,
                    'batteryCapacity': 0,
                    'power': 0,
                    'numberOfConnections': 0,
                    'numberOfPanels': 2,
                    'minTemp': 0,
                    'maxTemp': 0
                }

battery_device = {
                    'deviceCode': str(uuid.uuid4()),
                    'deviceOption': 'BATTERY',
                    'property': 1,
                    'image': '/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAP//////////////////////////////////////////////////////////////////////////////////////wgALCAABAAEBAREA/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxA=',
                    'name': f'test-device-battery {str(uuid.uuid4())}',
                    'powerUsage': 0,
                    'powerSupply': 'INTERNAL_POWER',
                    'panelSize': 0,
                    'panelEfficiency': 0.0,
                    'batteryCapacity': 5,
                    'power': 0,
                    'numberOfConnections': 0,
                    'numberOfPanels': 0,
                    'minTemp': 0,
                    'maxTemp': 0
                }

charger_device ={
                    'deviceCode': str(uuid.uuid4()),
                    'deviceOption': 'CHARGER',
                    'property': 1,
                    'image': '/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAP//////////////////////////////////////////////////////////////////////////////////////wgALCAABAAEBAREA/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxA=',
                    'name': f'test-device-charger {str(uuid.uuid4())}',
                    'powerUsage': 0,
                    'powerSupply': 'INTERNAL_POWER',
                    'panelSize': 0,
                    'panelEfficiency': 0.0,
                    'batteryCapacity': 0,
                    'power': 2,
                    'numberOfConnections': 2,
                    'numberOfPanels': 0,
                    'minTemp': 0,
                    'maxTemp': 0
                }

class WebsiteUser(HttpUser):
    wait_time = between(1, 5)
    host = 'http://localhost:9080'

    def __init__(self, parent):
        super(WebsiteUser, self).__init__(parent)
        self.token = ''


    def login(self):
        response = self.client.post("/api/user/login", json={
        "username": "0@gmail.com", 
        "password": "password"
        })
        
        if "accessToken" not in response.json():
            print(response.json())
        else:
            self.token = response.json()['accessToken']

    def on_start(self):
        self.login()

    @task
    def create_device(self):
        device = charger_device
        response = self.client.post("/api/device", headers={'Authorization':'Bearer ' + self.token}, json=device)
        print(response.json())
