from locust import FastHttpUser, task, constant
from datetime import datetime

class RegistrationTest(FastHttpUser):
    wait_time = constant(0.1)
    host = "http://localhost:9080"
    accessToken = ''

    @task
    def schedule_wm(self):
        self.client.get('/')
        responseApartments = self.client.get("/api/apartment", headers={"Authorization": f"Bearer {self.accessToken}"})
        self.client.get(f'/apartments/{responseApartments.json()[0]["id"]}')
        responseDevices = self.client.post(f"/api/device/subscribeStatus/{responseApartments.json()[0]['id']}", headers={"Authorization": f"Bearer {self.accessToken}"})
        self.client.get(f'/devices/{responseDevices.json()["washingMachineList"][0]["id"]}?apartment={responseApartments.json()[0]["id"]}&device_type=washingMachine')
        with self.client.post(f'/api/device/washingMachine/schedule/{responseApartments.json()[0]["id"]}/{responseDevices.json()["washingMachineList"][0]["id"]}?program={"black30"}&from={datetime.now().strftime("%H:%M:%S")}', headers={"Authorization": f"Bearer {self.accessToken}"}, catch_response=True) as scheduleResponse:
            if scheduleResponse.status_code != 200:
                scheduleResponse.failure(f"{scheduleResponse.status_code}")
            else:
                scheduleResponse.success()


    def on_start(self):
        with self.client.post("/api/user/login", json={"username": "uros.pocek+1@gmail.com", "password": "uros123"}, catch_response=True) as response:
            if "accessToken" not in response.json():
                response.failure(f"{response.status_code}")
            else:
                self.accessToken = response.json()['accessToken']