from locust import HttpUser, task, between
import random
from datetime import datetime, timedelta

username="0@gmail.com"
password="password"
apartment_id = 2
city_id = 8421
device_ids = [25,28]
user_ids = [1,2]
max_days_ago = 3
max_range_days = 30

class WebsiteUser(HttpUser):
    wait_time = between(0.5,1)
    host = 'http://localhost:9080'

    def __init__(self, parent):
        super(WebsiteUser, self).__init__(parent)
        self.token = ''


    def login(self):
        response = self.client.post("/api/user/login", json={
        "username": username, 
        "password": password
        })
        
        if "accessToken" not in response.json():
            print(response.json())
        else:
            self.token = response.json()['accessToken']

    def on_start(self):
        self.login()

    @task
    def get_filtered_energy_report_step_60_apartment(self):
        random_days_ago = random.randint(1, max_days_ago)
        random_days_range = random.randint(1, max_range_days)
        random_start_date = datetime.today() - timedelta(days=random_days_ago)
        random_end_date = random_start_date - timedelta(days=random_days_range)
        step = 60
        print(f"/api/readings/batterySystem/city/{city_id}/{str(random_start_date.isoformat())}/{str(random_end_date.isoformat())}/{step}")
        response = self.client.get(f"/api/readings/batterySystem/apartment/{apartment_id}/{random_end_date.isoformat()+ 'Z'}/{random_start_date.isoformat()+ 'Z'}/{step}", headers={'Authorization':'Bearer ' + self.token}, name='api/readings/batterySystem/apartment/{apartment_id}/{random_start_date}/{random_start_date}/{step}' )
        print(response.json())

    @task
    def get_filtered_energy_report_step_15_apartment(self):
        random_hours_ago = random.randint(1, 12)
        random_start_date = datetime.today()
        random_end_date = datetime.today() - timedelta(hours=random_hours_ago)
        step = 15
        print(f"/api/readings/batterySystem/city/{city_id}/{str(random_start_date)}/{str(random_end_date)}/{step}")
        
        response = self.client.get(f"/api/readings/batterySystem/apartment/{apartment_id}/{random_end_date.isoformat()+ 'Z'}/{random_start_date.isoformat()+ 'Z'}/{step}", headers={'Authorization':'Bearer ' + self.token}, name='api/readings/batterySystem/apartment/{apartment_id}/{random_start_date}/{random_start_date}/{step}' )
        print(response.json())

    @task
    def get_filtered_energy_report_step_60_city(self):
        random_days_ago = random.randint(1, max_days_ago)
        random_days_range = random.randint(1, max_range_days)
        random_start_date = datetime.today() - timedelta(days=random_days_ago)
        random_end_date = random_start_date - timedelta(days=random_days_range)
        step = 60

        response = self.client.get(f"/api/readings/batterySystem/city/{city_id}/{random_end_date.isoformat() + 'Z'}/{random_start_date.isoformat()+ 'Z'}/{step}", headers={'Authorization':'Bearer ' + self.token}, name='api/readings/batterySystem/city/{cityId}/{random_start_date}/{random_start_date}/{step}' )
        print(response.json())

    @task
    def get_filtered_energy_report_step_15_city(self):
        random_hours_ago = random.randint(1, 12)
        random_start_date = datetime.today()
        random_end_date = datetime.today() - timedelta(hours=random_hours_ago)
        step = 15
        
        response = self.client.get(f"/api/readings/batterySystem/city/{city_id}/{random_end_date.isoformat()+ 'Z'}/{random_start_date.isoformat()+ 'Z'}/{step}", headers={'Authorization':'Bearer ' + self.token}, name='api/readings/batterySystem/city/{city_id}/{random_start_date}/{random_start_date}/{step}' )
        print(response.json())