from locust import FastHttpUser, constant_throughput, task, constant
import random

class RegistrationTest(FastHttpUser):
    # wait_time = constant_throughput(0.1)
    wait_time = constant(1.0)
    host = "http://localhost:8080"
    accessToken = ''

    @task
    def get_apartments(self):
        # self.client.get('/login')
        with self.client.post("/api/user/login", json=random.choice(users), catch_response=True) as response:
            if "accessToken" not in response.json():
                response.failure(f"{response.status_code}")
            else:
                self.accessToken = response.json()['accessToken']
        # self.client.get("/")
        self.client.get("/api/apartment", headers={"Authorization": f"Bearer {self.accessToken}"})
        self.client.get("/api/apartment/shared", headers={"Authorization": f"Bearer {self.accessToken}"})
        self.client.get("/api/device/shared", headers={"Authorization": f"Bearer {self.accessToken}"})
        

    def on_start(self):
        with self.client.post("/api/user/login", json={"username": "uros.pocek+test@gmail.com", "password": "test123"}, catch_response=True) as response:
            if "accessToken" not in response.json():
                response.failure(f"{response.status_code}")
            else:
                self.accessToken = response.json()['accessToken']
    
            for _ in range(1):
                location = random.choice(locations)
                apartment = {'name':'apartment-test','location':None,'address':location['address'],'city':location['city'],'country':location['country'],'squareFootage':100,'numberOfFloors':2,'numberOfRooms':5, 'imageUrl':'/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAP//////////////////////////////////////////////////////////////////////////////////////wgALCAABAAEBAREA/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxA='}
                response = self.client.post("/api/apartment", json=apartment, headers={"Authorization": f"Bearer {self.accessToken}"})

users = [
{"username": "uros.pocek+1@gmail.com", "password": "uros123"},
{"username": "uros.pocek+test@gmail.com", "password": "test123"}
]
locations = [
       {
          'address':'Laze Nančića 34, Novi Sad',
          'city':'Novi Sad',
          'country':'Serbia'
       },
       {
          'address':'Žikice Jovanovića Španca, Subotica 24111',
          'city':'Subotica',
          'country':'Serbia'
       },
       {
          'address':'Žikice Jovanovića Španca, Subotica 24111',
          'city':'Subotica',
          'country':'Serbia'
       },
       {
          'address':'Laze Nančića 34, Novi Sad',
          'city':'Novi Sad',
          'country':'Serbia'
       },
       {
          'address':'Ulitsa Severnaya 20-Ya, 2-16, Omsk, Omskaya oblast',
          'city':'Omsk',
          'country':'Russia'
       },
    ]