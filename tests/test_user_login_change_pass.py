from locust import FastHttpUser, constant_throughput, task

class RegistrationTest(FastHttpUser):
    wait_time = constant_throughput(100)
    host = "http://localhost:9080"
    accessToken = ''
    password_index = 0
    passowords = ['alex123', 'user1234', 'user1235']

    @task
    def login_change_password(self):
        self.client.get('/login')
        response = self.client.post("/api/user/login", json={'username': 'aleksandar+1@gmail.com', 'password': self.passowords[self.password_index]})
        if "accessToken" not in response.json():
            print('No jwt')
        else:
            self.accessToken = response.json()['accessToken']
        self.client.get("/change-password")
        self.client.put("/api/user/change-password", json={'password': self.passowords[(self.password_index+1) % 3]}, headers={"Authorization": f"Bearer {self.accessToken}"})
        self.password_index = (self.password_index + 1) % 3