from locust import FastHttpUser, task, constant
import uuid

class RegistrationTest(FastHttpUser):
    wait_time = constant(1.0)
    host = "http://localhost:9080"

    @task
    def registration(self):
        self.client.get("/registration")
        with self.client.post("/api/user/registration", catch_response=True , json={ 'username': f'{uuid.uuid4()}@gmail.com', 'password': f'password', 'name': f'name', 'surname': f'surname', 'profilePicture': '' }) as response:
            if response.status_code == 200:
                response.success()
            else:
                response.failure(f'{response.status_code}')
