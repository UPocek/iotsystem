from locust import FastHttpUser, task, constant
import uuid

# locust -f test_add_new_ac.py -u 1 -r 1

class RegistrationTest(FastHttpUser):
    wait_time = constant(0.1)
    host = "http://localhost:9080"
    accessToken = ''

    @task
    def create_ac(self):
        self.client.get('/')
        self.client.get("/api/apartment", headers={"Authorization": f"Bearer {self.accessToken}"})
        self.client.get("/devices/add")
        with self.client.post("/api/device", headers={'Authorization':'Bearer ' + self.accessToken}, json=air_conditioning_device) as response:
            if response.status_code == 201:
                print(f"Device {air_conditioning_device['name']} created")


    def on_start(self):
        with self.client.post("/api/user/login", json={"username": "uros.pocek+test@gmail.com", "password": "test123"}, catch_response=True) as response:
            if "accessToken" not in response.json():
                response.failure(f"{response.status_code}")
            else:
                self.accessToken = response.json()['accessToken']

air_conditioning_device= {
                    'deviceCode': str(uuid.uuid4()),
                    'deviceOption': 'AIRCONDITIONING',
                    'property': 1,
                    'image': '/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAP//////////////////////////////////////////////////////////////////////////////////////wgALCAABAAEBAREA/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxA=',
                    'name': f'test-device-air-con {str(uuid.uuid4())}',
                    'powerUsage': 1,
                    'powerSupply': 'INTERNAL_POWER',
                    'panelSize': 0,
                    'panelEfficiency': 0.0,
                    'batteryCapacity': 0,
                    'power': 0,
                    'numberOfConnections': 0,
                    'numberOfPanels': 0,
                    'minTemp': 0,
                    'maxTemp': 30
                }