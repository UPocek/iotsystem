from locust import FastHttpUser, task, constant

class RegistrationTest(FastHttpUser):
    wait_time = constant(0.1)
    host = "http://localhost:9080"
    accessToken = ''

    @task
    def share_apartments(self):
        self.client.get('/')
        response = self.client.get("/api/apartment", headers={"Authorization": f"Bearer {self.accessToken}"})
        with self.client.post(f'/api/apartment/giveAccess/{response.json()[0]["id"]}/{users[1]["username"]}', headers={"Authorization": f"Bearer {self.accessToken}"}, catch_response=True) as giveResponse:
            if giveResponse.status_code == 406:
                giveResponse.success()
        with self.client.put(f'/api/apartment/removeAccess/{response.json()[0]["id"]}/{users[1]["username"]}', headers={"Authorization": f"Bearer {self.accessToken}"}, catch_response=True) as removeResponse:
            if removeResponse.status_code == 400:
                removeResponse.success()


    def on_start(self):
        with self.client.post("/api/user/login", json=users[0], catch_response=True) as response:
            if "accessToken" not in response.json():
                response.failure(f"{response.status_code}")
            else:
                self.accessToken = response.json()['accessToken']

users = [
{"username": "uros.pocek+1@gmail.com", "password": "uros123"},
{"username": "uros.pocek+test@gmail.com", "password": "test123"}
]