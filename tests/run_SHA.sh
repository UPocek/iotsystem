#!/bin/bash

# Function to handle cleanup when script is interrupted
cleanup() {
    echo "Script interrupted. Cleaning up..."
    pkill -P $$
    exit 1
}

# Trap the INT signal (Ctrl+C) to invoke the cleanup function
trap cleanup INT

# Check if the config file argument is provided
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <config_file>"
    exit 1
fi

# Get the config file from command-line argument
config_file="$1"

# Check if the config file exists
if [ ! -f "$config_file" ]; then
    echo "Error: Config file '$config_file' not found."
    exit 1
fi

# Source the config file to load variables
source "$config_file"

# Check if the required variables are set
if [ -z "$folder_path" ] || [ -z "${arg1[*]}" ] || [ -z "${arg2[*]}" ]; then
    echo "Error: Config file is missing required values."
    exit 1
fi

if [ ! -d "$folder_path" ]; then
    echo "Error: Folder '$folder_path' not found."
    exit 1
fi

# Get the length of the lists
num_args=${#arg1[@]}

# Change working directory to the folder containing the Go script
cd "$folder_path" || exit

# Iterate over the lists and run the Go file
for ((i=0; i<num_args; i++)); do
    (
        echo "Running: go run . ${arg2[$i]} ${arg1[$i]}"
        go run . "${arg2[$i]}" "${arg1[$i]}"
        echo "Finished: go run . ${arg2[$i]} ${arg1[$i]}"
    ) &
done

wait

# Exit normally
exit 0