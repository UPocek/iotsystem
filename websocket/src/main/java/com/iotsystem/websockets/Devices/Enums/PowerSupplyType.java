package com.iotsystem.websockets.Devices.Enums;

public enum PowerSupplyType {
    EXTERNAL_POWER, INTERNAL_POWER
}
