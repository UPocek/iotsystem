package com.iotsystem.websockets.Devices.WS;

import com.iotsystem.websockets.Devices.Repositories.IReadingsRepository;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class ReadingsSocketHandler extends TextWebSocketHandler {

    private final IReadingsRepository readingsRepository;

    public ReadingsSocketHandler(IReadingsRepository readingsRepository) {
        this.readingsRepository = readingsRepository;
    }

    public static final Map<String, WebSocketSession> READINGS_SESSIONS = new ConcurrentHashMap<>();

    @Override
    public void afterConnectionEstablished(WebSocketSession session) {
        Optional<String> u = Objects.requireNonNull(session.getHandshakeHeaders().get("cookie")).stream().filter(cookie -> cookie.contains("X-Authorization=")).findFirst();
        if (u.isPresent()) {
            var userCredentials = u.get().split("X-Authorization=")[1];
            System.out.println("User connected " + userCredentials);
            READINGS_SESSIONS.putIfAbsent(userCredentials, session);
        }
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, @NotNull CloseStatus status) {
        System.out.println("User disconnected " + session.getId());
        Optional<String> authCookie = Objects.requireNonNull(session.getHandshakeHeaders().get("cookie"))
                .stream()
                .filter(cookie -> cookie.startsWith("X-Authorization="))
                .findFirst();

        String userCredentials = authCookie
                .map(cookie -> cookie.split("=")[1])
                .orElseGet(() -> READINGS_SESSIONS.entrySet()
                        .stream()
                        .filter(entry -> entry.getValue().getId().equals(session.getId()))
                        .map(Map.Entry::getKey)
                        .findFirst()
                        .orElse(null));

        if (userCredentials != null) {
            READINGS_SESSIONS.remove(userCredentials);
            readingsRepository.deleteById(userCredentials);
        }
    }
}
