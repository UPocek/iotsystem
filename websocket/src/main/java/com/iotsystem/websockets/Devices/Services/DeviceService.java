package com.iotsystem.websockets.Devices.Services;

import com.google.gson.Gson;
import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.QueryApi;
import com.influxdb.query.FluxRecord;
import com.influxdb.query.FluxTable;
import com.iotsystem.websockets.Devices.DTOs.*;
import com.iotsystem.websockets.Devices.Models.Apartment;
import com.iotsystem.websockets.Devices.Models.Device;
import com.iotsystem.websockets.Devices.Models.RegularUser;
import com.iotsystem.websockets.Devices.RedisModels.Reading;
import com.iotsystem.websockets.Devices.Repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.iotsystem.websockets.Devices.WS.ReadingsSocketHandler.READINGS_SESSIONS;
import static com.iotsystem.websockets.Devices.WS.SocketHandler.SESSIONS;

@Service
public class DeviceService {

    @Autowired
    IStatusRepository statusRepository;
    @Autowired
    IReadingsRepository readingsRepository;
    @Autowired
    IRegularUserRepository regularUserRepository;
    @Autowired
    IApartmentRepository apartmentRepository;
    @Autowired
    IDeviceRepository deviceRepository;

    @Autowired
    IBatteryRepository batteryRepository;
    @Autowired
    private InfluxDBClient influxDBClient;

    @Value("${influxdb.readings.bucket.1m}")
    private String readingsBucket;

    @Value("${influxdb.energy_consumption.bucket.1m}")
    private String energyConsumptionBucket;

    @Value("${influxdb.bucket}")
    private String bucket;

    @Async
    @Scheduled(fixedRate = 11000, initialDelayString = "${timing.initialScheduledDelay}")
    protected void notifyUserOnDeviceOnOffStatus() throws IOException {
        for (String userCredentials : SESSIONS.keySet()) {
            var statusOptional = statusRepository.findById(userCredentials);
            if (statusOptional.isEmpty()) {
                SESSIONS.get(userCredentials).close();
                continue;
            }
            Optional<RegularUser> watcherOptional = regularUserRepository.findByUsername(userCredentials);
            if (watcherOptional.isEmpty()) {
                SESSIONS.get(userCredentials).close();
                continue;
            }
            Optional<Apartment> userApartmentOptional = apartmentRepository.findById(statusOptional.get().getApartmentId());
            if (userApartmentOptional.isEmpty()) {
                SESSIONS.get(userCredentials).close();
                continue;
            }
            RegularUser watcher = watcherOptional.get();
            Apartment apartmentToGetDevicesStatusesOf = userApartmentOptional.get();

            String apartmentStatus;

            if (watcher.getApartments().stream().anyMatch(apartment -> apartment.getId().equals(apartmentToGetDevicesStatusesOf.getId())) || watcher.getAccessToApartments().stream().anyMatch(apartment -> apartment.getId().equals(statusOptional.get().getApartmentId()))) {
                apartmentStatus = new Gson().toJson(new ApartmentStatusDTO(apartmentToGetDevicesStatusesOf));
            } else {
                ApartmentStatusDTO apartmentStatusDTO = new ApartmentStatusDTO();
                apartmentStatusDTO.setAmbientConditionsList(apartmentToGetDevicesStatusesOf.getAmbientConditionsList().stream().filter(device -> device.getUsersWithAccess().stream().anyMatch(user1 -> user1.getUsername().equals(watcher.getUsername()))).map(DeviceStatusDTO::new).collect(Collectors.toList()));
                apartmentStatusDTO.setAirConditioningList(apartmentToGetDevicesStatusesOf.getAirConditioningList().stream().filter(device -> device.getUsersWithAccess().stream().anyMatch(user1 -> user1.getUsername().equals(watcher.getUsername()))).map(DeviceStatusDTO::new).collect(Collectors.toList()));
                apartmentStatusDTO.setWashingMachineList(apartmentToGetDevicesStatusesOf.getWashingMachineList().stream().filter(device -> device.getUsersWithAccess().stream().anyMatch(user1 -> user1.getUsername().equals(watcher.getUsername()))).map(DeviceStatusDTO::new).collect(Collectors.toList()));
                apartmentStatusDTO.setChargerList(apartmentToGetDevicesStatusesOf.getChargerList().stream().filter(device -> device.getUsersWithAccess().stream().anyMatch(user1 -> user1.getUsername().equals(watcher.getUsername()))).map(DeviceStatusDTO::new).collect(Collectors.toList()));
                apartmentStatusDTO.setSolarSystemList(apartmentToGetDevicesStatusesOf.getSolarSystemList().stream().filter(device -> device.getUsersWithAccess().stream().anyMatch(user1 -> user1.getUsername().equals(watcher.getUsername()))).map(DeviceStatusDTO::new).collect(Collectors.toList()));
                apartmentStatusDTO.setBatteryList(apartmentToGetDevicesStatusesOf.getBatteryList().stream().filter(device -> device.getUsersWithAccess().stream().anyMatch(user1 -> user1.getUsername().equals(watcher.getUsername()))).map(DeviceStatusDTO::new).collect(Collectors.toList()));
                apartmentStatusDTO.setVehicleGateList(apartmentToGetDevicesStatusesOf.getVehicleGateList().stream().filter(device -> device.getUsersWithAccess().stream().anyMatch(user1 -> user1.getUsername().equals(watcher.getUsername()))).map(DeviceStatusDTO::new).collect(Collectors.toList()));
                apartmentStatusDTO.setLampList(apartmentToGetDevicesStatusesOf.getLampList().stream().filter(device -> device.getUsersWithAccess().stream().anyMatch(user1 -> user1.getUsername().equals(watcher.getUsername()))).map(DeviceStatusDTO::new).collect(Collectors.toList()));
                apartmentStatusDTO.setSprinklerSystemList(apartmentToGetDevicesStatusesOf.getSprinklerSystemList().stream().filter(device -> device.getUsersWithAccess().stream().anyMatch(user1 -> user1.getUsername().equals(watcher.getUsername()))).map(DeviceStatusDTO::new).collect(Collectors.toList()));
                apartmentStatus = new Gson().toJson(apartmentStatusDTO);
            }

            try {
                SESSIONS.get(userCredentials).sendMessage(new TextMessage(apartmentStatus));
            } catch (Exception e) {
                SESSIONS.remove(userCredentials);
            }
        }
    }

    @Async
    @Scheduled(fixedRate = 59000, initialDelayString = "${timing.initialScheduledDelay}")
    protected void notifyUserOnNewReadings() throws IOException {
        for (String userCredentials : READINGS_SESSIONS.keySet()) {
            System.out.println(userCredentials);
            var readingOptional = readingsRepository.findById(userCredentials);
            if (readingOptional.isEmpty()) {
                READINGS_SESSIONS.get(userCredentials).close();
                continue;
            }

            Reading reading = readingOptional.get();
            Optional<Device> userDeviceOptional = deviceRepository.findById(reading.getDeviceId());
            if (userDeviceOptional.isEmpty()) {
                READINGS_SESSIONS.get(userCredentials).close();
                continue;
            }

            String deviceReadings;

            if (reading.getReadingType().equals("temphumid")) {
                deviceReadings = getUpdateForHumidityAndTemperature(userDeviceOptional.get().getId());
                try {
                    READINGS_SESSIONS.get(userCredentials).sendMessage(new TextMessage(deviceReadings));
                } catch (Exception e) {
                    READINGS_SESSIONS.remove(userCredentials);
                }
            } else if (reading.getReadingType().equals("genconsum")) {
                Optional<Apartment> userApartmentOptional = apartmentRepository.findByOwnerUsername(reading.getUser());
                if (userApartmentOptional.isPresent()) {
                    deviceReadings = sendUpdateForBatterySystem(userApartmentOptional.get().getId());
                    try {
                        READINGS_SESSIONS.get(userCredentials).sendMessage(new TextMessage(deviceReadings));
                    } catch (Exception e) {
                        READINGS_SESSIONS.remove(userCredentials);
                    }
                }
            }
        }
    }

    private String getUpdateForHumidityAndTemperature(Long deviceId) throws IOException {
        List<SimpleReadingDTO> temperatures = new ArrayList<>();
        List<SimpleReadingDTO> humidities = new ArrayList<>();

        String fluxQuery = String.format(
                "from(bucket: \"%s\")\n" +
                        "  |> range(start: -1m)\n" +
                        "  |> filter(fn: (r) => r[\"_measurement\"] == \"readings\")\n" +
                        "  |> filter(fn: (r) => r[\"_field\"] == \"humidity\" or r[\"_field\"] == \"temperature\" )\n" +
                        "  |> filter(fn: (r) => r[\"device_id\"] == \"%d\")", this.readingsBucket, deviceId);
        QueryApi queryApi = this.influxDBClient.getQueryApi();
        List<FluxTable> tables = queryApi.query(fluxQuery);
        for (FluxTable fluxTable : tables) {
            List<FluxRecord> records = fluxTable.getRecords();
            for (FluxRecord fluxRecord : records) {
                if (Objects.equals(fluxRecord.getField(), "temperature")) {
                    temperatures.add(new SimpleReadingDTO(Float.parseFloat(fluxRecord.getValueByKey("_value").toString()), fluxRecord.getTime().plusSeconds(3600).toString()));
                } else if (Objects.equals(fluxRecord.getField(), "humidity")) {
                    humidities.add(new SimpleReadingDTO(Float.parseFloat(fluxRecord.getValueByKey("_value").toString()), fluxRecord.getTime().plusSeconds(3600).toString()));
                }
            }
        }

        return new Gson().toJson(new TemperatureAndHumidityDTO(temperatures, humidities));
    }

    private String sendUpdateForBatterySystem(Long apartmentId) throws IOException {
        List<SimpleReadingDTO> generations = new ArrayList<>();
        List<SimpleReadingDTO> consumptions = new ArrayList<>();

        String fluxQuery = String.format(
                "from(bucket: \"%s\")\n" +
                        "  |> range(start: -1h)\n" +
                        "  |> filter(fn: (r) => r[\"_measurement\"] == \"energy_consumption\")\n" +
                        "  |> filter(fn: (r) => r[\"_field\"] == \"power_transfer\" )\n" +
                        "  |> filter(fn: (r) => r[\"property_id\"] == \"%d\")", this.energyConsumptionBucket, apartmentId);
        QueryApi queryApi = this.influxDBClient.getQueryApi();
        List<FluxTable> tables = queryApi.query(fluxQuery);
        for (FluxTable fluxTable : tables) {
            List<FluxRecord> records = fluxTable.getRecords();
            for (FluxRecord fluxRecord : records) {
                if (Objects.equals(fluxRecord.getValueByKey("power_generate"), "True")) {
                    generations.add(new SimpleReadingDTO(Float.parseFloat(fluxRecord.getValueByKey("_value").toString()), fluxRecord.getTime().plusSeconds(3600).toString()));
                } else if (Objects.equals(fluxRecord.getValueByKey("power_generate"), "False")) {
                    consumptions.add(new SimpleReadingDTO(Float.parseFloat(fluxRecord.getValueByKey("_value").toString()), fluxRecord.getTime().plusSeconds(3600).toString()));
                }
            }
        }

        List<BatteryStatusDTO> batteries = getBatteryStatuses(apartmentId);
        return new Gson().toJson(new BatterySystemDTO(generations, consumptions, batteries));
    }

    private List<BatteryStatusDTO> getBatteryStatuses(Long apartmentId) {
        return batteryRepository.findAllByApartmentId(apartmentId).stream().map(BatteryStatusDTO::new).toList();
    }

    @Async
    @Scheduled(fixedRate = 100, initialDelayString = "${timing.initialScheduledDelay}")
    protected void notifyUserOnNewIllumination() throws IOException {
        for (String userCredentials : READINGS_SESSIONS.keySet()) {
            var readingOptional = readingsRepository.findById(userCredentials);
            if (readingOptional.isEmpty()) {
                READINGS_SESSIONS.get(userCredentials).close();
                continue;
            }
            Optional<Device> userDeviceOptional = deviceRepository.findById(readingOptional.get().getDeviceId());
            if (userDeviceOptional.isEmpty()) {
                READINGS_SESSIONS.get(userCredentials).close();
                continue;
            }

            System.out.println("Illumination device id: " + userDeviceOptional.get().getId());


            List<GenericReadingDTO<Double>> illumination = new ArrayList<>();
            List<GenericReadingDTO<Boolean>> on = new ArrayList<>();
            List<GenericReadingDTO<Boolean>> auto = new ArrayList<>();
            List<GenericReadingDTO<String>> command = new ArrayList<>();

            String fluxQuery = String.format(
                    """
                            from(bucket: "iot")
                                |> range(start: -20s)
                                |> filter(fn: (r) => r["_field"] == "command" or r["_field"] == "illumination" or r["_field"] == "on" or r["_field"] == "auto")
                                |> filter(fn: (r) => r["device_type"] == "lamp")
                                |> filter(fn: (r) => r["device_id"] == "%s")
                                |> aggregateWindow(every: 5s, fn: last, createEmpty: false)
                            """, userDeviceOptional.get().getId());
            QueryApi queryApi = this.influxDBClient.getQueryApi();
            List<FluxTable> tables = queryApi.query(fluxQuery);
            for (FluxTable fluxTable : tables) {
                List<FluxRecord> records = fluxTable.getRecords();
                for (FluxRecord fluxRecord : records) {
                    if (Objects.equals(fluxRecord.getField(), "illumination")) {
                        illumination.add(
                                new GenericReadingDTO<>(
                                        fluxRecord.getValue() != null ? (Double) fluxRecord.getValue() : null,
                                        Objects.requireNonNull(fluxRecord.getTime()).plusSeconds(3600).toString()
                                )
                        );
                    }
                    if (Objects.equals(fluxRecord.getField(), "on")) {
                        on.add(
                                new GenericReadingDTO<>(
                                        fluxRecord.getValue() != null ? (Boolean) fluxRecord.getValue() : null,
                                        Objects.requireNonNull(fluxRecord.getTime()).plusSeconds(3600).toString()
                                )
                        );
                    }
                    if (Objects.equals(fluxRecord.getField(), "auto")) {
                        auto.add(
                                new GenericReadingDTO<>(
                                        fluxRecord.getValue() != null ? (Boolean) fluxRecord.getValue() : null,
                                        Objects.requireNonNull(fluxRecord.getTime()).plusSeconds(3600).toString()
                                )
                        );
                    }
                    if (Objects.equals(fluxRecord.getField(), "command")) {
                        command.add(
                                new GenericReadingDTO<>(
                                        fluxRecord.getValue() != null ? (String) fluxRecord.getValue() : null,
                                        Objects.requireNonNull(fluxRecord.getTime()).plusSeconds(3600).toString()
                                )
                        );
                    }
                }
            }

            String deviceReadings = new Gson().toJson(new IlluminationDTO(illumination, on, auto, command));
            READINGS_SESSIONS.get(userCredentials).sendMessage(new TextMessage(deviceReadings));
        }
    }
}
