package com.iotsystem.websockets.Devices.DTOs;

import com.iotsystem.websockets.Devices.Models.RegularUser;

import java.io.Serializable;

public class RegularUserDTO implements Serializable {
    private Long id;
    private String name;
    private String surname;
    private String username;
    private Boolean active;

    public RegularUserDTO() {
    }

    public RegularUserDTO(Long id, String name, String surname, String username, Boolean active) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.active = active;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getUsername() {
        return username;
    }

    public Boolean getActive() {
        return active;
    }

    public RegularUserDTO(RegularUser regularUser) {
        this.id = regularUser.getId();
        this.name = regularUser.getName();
        this.surname = regularUser.getSurname();
        this.username = regularUser.getUsername();
        this.active = regularUser.getActive();
    }
}
