package com.iotsystem.websockets.Devices.DTOs;

import com.iotsystem.websockets.Devices.Models.Device;

import java.util.ArrayList;
import java.util.List;

public class DeviceStatusDTO {

    private Long id;
    private String name;
    private Boolean online;
    private List<RegularUserDTO> usersWithAccess;

    public DeviceStatusDTO() {
    }

    public DeviceStatusDTO(Device device) {
        this.id = device.getId();
        this.name = device.getName();
        this.online = device.isOnline();
        this.usersWithAccess = device.getUsersWithAccess().stream().map(RegularUserDTO::new).toList();
    }

    public DeviceStatusDTO(Long id, String name, Boolean online) {
        this.id = id;
        this.name = name;
        this.online = online;
        this.usersWithAccess = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getOnline() {
        return online;
    }

    public void setOnline(Boolean online) {
        this.online = online;
    }

    public List<RegularUserDTO> getUsersWithAccess() {
        return usersWithAccess;
    }

    public void setUsersWithAccess(List<RegularUserDTO> usersWithAccess) {
        this.usersWithAccess = usersWithAccess;
    }
}
