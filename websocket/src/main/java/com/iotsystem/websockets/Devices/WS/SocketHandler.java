package com.iotsystem.websockets.Devices.WS;

import com.iotsystem.websockets.Devices.Repositories.IStatusRepository;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

public class SocketHandler extends TextWebSocketHandler {

    private final IStatusRepository statusRepository;

    public SocketHandler(IStatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

    public static final Map<String, WebSocketSession> SESSIONS = new ConcurrentHashMap<>();

    @Override
    public void handleTextMessage(@NotNull WebSocketSession session, @NotNull TextMessage message)
            throws IOException {

//        for(WebSocketSession webSocketSession : sessions) {
//            Map value = new Gson().fromJson(message.getPayload(), Map.class);
//            webSocketSession.sendMessage(new TextMessage("Hello " + value.get("name") + " !"));
//        }
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) {
        Optional<String> u = Objects.requireNonNull(session.getHandshakeHeaders().get("cookie")).stream().filter(cookie -> cookie.startsWith("X-Authorization=")).findFirst();
        if (u.isPresent()) {
            var userCredentials = u.get().split("=")[1];
            SESSIONS.putIfAbsent(userCredentials, session);
        }
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, @NotNull CloseStatus status) {
        Optional<String> authCookie = Objects.requireNonNull(session.getHandshakeHeaders().get("cookie"))
                .stream()
                .filter(cookie -> cookie.startsWith("X-Authorization="))
                .findFirst();

        String userCredentials = authCookie
                .map(cookie -> cookie.split("=")[1])
                .orElseGet(() -> SESSIONS.entrySet()
                        .stream()
                        .filter(entry -> entry.getValue().getId().equals(session.getId()))
                        .map(Map.Entry::getKey)
                        .findFirst()
                        .orElse(null));

        if (userCredentials != null) {
            SESSIONS.remove(userCredentials);
            statusRepository.deleteById(userCredentials);
        }
    }
}
