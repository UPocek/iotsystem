package com.iotsystem.websockets.Devices.Repositories;

import com.iotsystem.websockets.Devices.RedisModels.Status;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IStatusRepository extends CrudRepository<Status, String> {
}
