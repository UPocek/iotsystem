package com.iotsystem.websockets.Devices.RedisModels;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import java.io.Serializable;

@RedisHash("Status")
public class Status implements Serializable {
    @Id
    private String user;
    private Long apartmentId;

    public Status() {
    }

    public Status(String user, Long apartmentId) {
        this.user = user;
        this.apartmentId = apartmentId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Long getApartmentId() {
        return apartmentId;
    }

    public void setApartmentId(Long apartmentId) {
        this.apartmentId = apartmentId;
    }
}
