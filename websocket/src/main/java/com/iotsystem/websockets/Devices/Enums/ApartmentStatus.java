package com.iotsystem.websockets.Devices.Enums;

public enum ApartmentStatus {
    NEED_CONFIRMATION, APPROVED, REJECTED;
}
