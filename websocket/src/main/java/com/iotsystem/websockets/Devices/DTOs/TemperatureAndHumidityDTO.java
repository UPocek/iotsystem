package com.iotsystem.websockets.Devices.DTOs;

import java.util.List;

public class TemperatureAndHumidityDTO {

    List<SimpleReadingDTO> temperature;
    List<SimpleReadingDTO> humidity;

    public TemperatureAndHumidityDTO() {
    }

    public TemperatureAndHumidityDTO(List<SimpleReadingDTO> temperature, List<SimpleReadingDTO> humidity) {
        this.temperature = temperature;
        this.humidity = humidity;
    }

    public List<SimpleReadingDTO> getTemperature() {
        return temperature;
    }

    public void setTemperature(List<SimpleReadingDTO> temperature) {
        this.temperature = temperature;
    }

    public List<SimpleReadingDTO> getHumidity() {
        return humidity;
    }

    public void setHumidity(List<SimpleReadingDTO> humidity) {
        this.humidity = humidity;
    }
}
