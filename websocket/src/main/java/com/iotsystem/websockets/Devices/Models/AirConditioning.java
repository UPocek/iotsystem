package com.iotsystem.websockets.Devices.Models;

import com.iotsystem.websockets.Devices.Enums.AirConditioningModes;
import com.iotsystem.websockets.Devices.Enums.PowerSupplyType;

import javax.persistence.Entity;

@Entity
public class AirConditioning extends Device {
    private PowerSupplyType powerSupplyType;
    private Integer powerUsage;
    private Integer minTemp;
    private Integer maxTemp;
    private Integer currentTemp;
    private AirConditioningModes mode;

    public AirConditioning() {
    }

    public PowerSupplyType getPowerSupplyType() {
        return powerSupplyType;
    }

    public void setPowerSupplyType(PowerSupplyType powerSupplyType) {
        this.powerSupplyType = powerSupplyType;
    }

    public Integer getPowerUsage() {
        return powerUsage;
    }

    public void setPowerUsage(Integer powerUsage) {
        this.powerUsage = powerUsage;
    }

    public Integer getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(Integer minTemp) {
        this.minTemp = minTemp;
    }

    public Integer getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(Integer maxTemp) {
        this.maxTemp = maxTemp;
    }

    public Integer getCurrentTemp() {
        return currentTemp;
    }

    public void setCurrentTemp(Integer currentTemp) {
        this.currentTemp = currentTemp;
    }

    public AirConditioningModes getMode() {
        return mode;
    }

    public void setMode(AirConditioningModes mode) {
        this.mode = mode;
    }
}
