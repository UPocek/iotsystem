package com.iotsystem.websockets.Devices.Models;

import com.iotsystem.websockets.Devices.Enums.PowerSupplyType;
import com.iotsystem.websockets.Devices.Enums.WashingMachineProgram;

import javax.persistence.Entity;
import java.time.LocalDateTime;

@Entity
public class WashingMachine extends Device {
    private PowerSupplyType powerSupplyType;
    private Integer powerUsage;
    private WashingMachineProgram currentProgram;
    private LocalDateTime programEnd;
    private Long programInitiatorId;

    public WashingMachine() {
    }

    public WashingMachine(Long id, String name, boolean online, PowerSupplyType powerSupplyType, Integer powerUsage, WashingMachineProgram currentProgram, LocalDateTime programEnd) {
        super(id, name, online);
        this.powerSupplyType = powerSupplyType;
        this.powerUsage = powerUsage;
        this.currentProgram = currentProgram;
        this.programEnd = programEnd;
    }

    public PowerSupplyType getPowerSupplyType() {
        return powerSupplyType;
    }

    public void setPowerSupplyType(PowerSupplyType powerSupplyType) {
        this.powerSupplyType = powerSupplyType;
    }

    public Integer getPowerUsage() {
        return powerUsage;
    }

    public void setPowerUsage(Integer powerUsage) {
        this.powerUsage = powerUsage;
    }

    public WashingMachineProgram getCurrentProgram() {
        return currentProgram;
    }

    public void setCurrentProgram(WashingMachineProgram currentProgram) {
        this.currentProgram = currentProgram;
    }

    public LocalDateTime getProgramEnd() {
        return programEnd;
    }

    public void setProgramEnd(LocalDateTime programEnd) {
        this.programEnd = programEnd;
    }

    public Long getProgramInitiatorId() {
        return programInitiatorId;
    }

    public void setProgramInitiatorId(Long programInitiatorId) {
        this.programInitiatorId = programInitiatorId;
    }
}
