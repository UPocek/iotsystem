package com.iotsystem.websockets.Devices.Models;

import javax.persistence.Entity;

@Entity
public class Charger extends Device {
    private Integer numberOfConnections;
    private Integer power;
    private Integer powerUsage;
    private Double fullness;

    public Charger() {
    }

    public Charger(String name, Integer numberOfConnections, Integer power, Integer powerUsage, Double fullness) {
        super(name);
        this.numberOfConnections = numberOfConnections;
        this.power = power;
        this.powerUsage = powerUsage;
        this.fullness = fullness;
    }

    public Integer getNumberOfConnections() {
        return numberOfConnections;
    }

    public void setNumberOfConnections(Integer numberOfConnections) {
        this.numberOfConnections = numberOfConnections;
    }

    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public Integer getPowerUsage() {
        return powerUsage;
    }

    public void setPowerUsage(Integer powerUsage) {
        this.powerUsage = powerUsage;
    }

    public Double getFullness() {
        return fullness;
    }

    public void setFullness(Double fullness) {
        this.fullness = fullness;
    }
}
