package com.iotsystem.websockets.Devices.Repositories;

import com.iotsystem.websockets.Devices.RedisModels.Reading;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IReadingsRepository extends CrudRepository<Reading, String> {
}
