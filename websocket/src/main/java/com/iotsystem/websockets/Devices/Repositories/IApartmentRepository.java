package com.iotsystem.websockets.Devices.Repositories;

import com.iotsystem.websockets.Devices.Models.Apartment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IApartmentRepository extends JpaRepository<Apartment, Long> {
    Optional<Apartment> findByOwnerUsername(String username);
}
