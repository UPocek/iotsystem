package com.iotsystem.websockets.Devices.Repositories;

import com.iotsystem.websockets.Devices.Models.Battery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface IBatteryRepository extends JpaRepository<Battery, Long> {
    Set<Battery> findAllByApartmentId(Long apartmentId);
}
