package com.iotsystem.websockets.Devices.RedisModels;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;

@RedisHash("Reading")
public class Reading implements Serializable {

    @Id
    private String user;
    private String readingType;
    private Long deviceId;

    public Reading() {
    }

    public Reading(String user, String readingType, Long deviceId) {
        this.user = user;
        this.readingType = readingType;
        this.deviceId = deviceId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getReadingType() {
        return readingType;
    }

    public void setReadingType(String readingType) {
        this.readingType = readingType;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }
}
