package com.iotsystem.websockets.Devices.Models;


import com.iotsystem.websockets.Devices.Enums.PowerSupplyType;

import javax.persistence.Entity;

@Entity
public class VehicleGate extends Device {
    private PowerSupplyType powerSupplyType;
    private Integer powerUsage;

    public VehicleGate() {
    }

    public VehicleGate(String name, PowerSupplyType powerSupplyType, Integer powerUsage) {
        super(name);
        this.powerSupplyType = powerSupplyType;
        this.powerUsage = powerUsage;
    }

    public PowerSupplyType getPowerSupplyType() {
        return powerSupplyType;
    }

    public void setPowerSupplyType(PowerSupplyType powerSupplyType) {
        this.powerSupplyType = powerSupplyType;
    }

    public Integer getPowerUsage() {
        return powerUsage;
    }

    public void setPowerUsage(Integer powerUsage) {
        this.powerUsage = powerUsage;
    }
}
