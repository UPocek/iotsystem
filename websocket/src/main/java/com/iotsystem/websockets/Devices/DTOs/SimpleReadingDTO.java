package com.iotsystem.websockets.Devices.DTOs;

public class SimpleReadingDTO {

    private Float value;
    private String timestamp;

    public SimpleReadingDTO() {
    }

    public SimpleReadingDTO(Float value, String timestamp) {
        this.value = value;
        this.timestamp = timestamp;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
