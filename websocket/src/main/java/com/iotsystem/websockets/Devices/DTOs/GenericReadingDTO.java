package com.iotsystem.websockets.Devices.DTOs;

public class GenericReadingDTO<T> {

    private T value;
    private String timestamp;

    public GenericReadingDTO() {
    }

    public GenericReadingDTO(T value, String timestamp) {
        this.value = value;
        this.timestamp = timestamp;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
