package com.iotsystem.websockets.Devices.Models;

import javax.persistence.Entity;

@Entity
public class Battery extends Device {
    private Integer batteryCapacity;
    private Float level;

    public Battery() {
    }

    public Battery(String name, Integer batteryCapacity) {
        super(name);
        this.batteryCapacity = batteryCapacity;
    }

    public Integer getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(Integer batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public Float getLevel() {
        return level;
    }

    public void setLevel(Float level) {
        this.level = level;
    }
}
