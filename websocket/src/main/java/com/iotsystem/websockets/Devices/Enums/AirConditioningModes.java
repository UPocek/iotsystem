package com.iotsystem.websockets.Devices.Enums;

public enum AirConditioningModes {
    AUTO,
    COOL,
    HEAT,
    DRY,
}
