package com.iotsystem.websockets.Devices.Repositories;

import com.iotsystem.websockets.Devices.Models.RegularUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IRegularUserRepository extends JpaRepository<RegularUser, Long> {

    Optional<RegularUser> findByUsername(String username);
}
