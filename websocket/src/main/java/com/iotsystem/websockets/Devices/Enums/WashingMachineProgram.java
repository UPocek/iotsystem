package com.iotsystem.websockets.Devices.Enums;

public enum WashingMachineProgram {
    BLACK30, BLACK45, WHITE60, WHITE90, COLOR45, OFF
}
