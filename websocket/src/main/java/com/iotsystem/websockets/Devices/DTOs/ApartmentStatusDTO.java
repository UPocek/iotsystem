package com.iotsystem.websockets.Devices.DTOs;

import com.iotsystem.websockets.Devices.Models.Apartment;
import com.iotsystem.websockets.Devices.Models.Device;

import java.util.Comparator;
import java.util.List;

public class ApartmentStatusDTO {

    private List<DeviceStatusDTO> ambientConditionsList;

    private List<DeviceStatusDTO> airConditioningList;

    private List<DeviceStatusDTO> washingMachineList;

    private List<DeviceStatusDTO> lampList;

    private List<DeviceStatusDTO> vehicleGateList;

    private List<DeviceStatusDTO> sprinklerSystemList;

    private List<DeviceStatusDTO> solarSystemList;

    private List<DeviceStatusDTO> batteryList;

    private List<DeviceStatusDTO> chargerList;

    public ApartmentStatusDTO() {
    }

    public ApartmentStatusDTO(Apartment apartment) {
        this.ambientConditionsList = apartment.getAmbientConditionsList().stream().sorted(Comparator.comparing(Device::getName)).map(DeviceStatusDTO::new).toList();
        this.airConditioningList = apartment.getAirConditioningList().stream().sorted(Comparator.comparing(Device::getName)).map(DeviceStatusDTO::new).toList();
        this.washingMachineList = apartment.getWashingMachineList().stream().sorted(Comparator.comparing(Device::getName)).map(DeviceStatusDTO::new).toList();
        this.lampList = apartment.getLampList().stream().sorted(Comparator.comparing(Device::getName)).map(DeviceStatusDTO::new).toList();
        this.vehicleGateList = apartment.getVehicleGateList().stream().sorted(Comparator.comparing(Device::getName)).map(DeviceStatusDTO::new).toList();
        this.sprinklerSystemList = apartment.getSprinklerSystemList().stream().sorted(Comparator.comparing(Device::getName)).map(DeviceStatusDTO::new).toList();
        this.solarSystemList = apartment.getSolarSystemList().stream().sorted(Comparator.comparing(Device::getName)).map(DeviceStatusDTO::new).toList();
        this.batteryList = apartment.getBatteryList().stream().sorted(Comparator.comparing(Device::getName)).map(DeviceStatusDTO::new).toList();
        this.chargerList = apartment.getChargerList().stream().sorted(Comparator.comparing(Device::getName)).map(DeviceStatusDTO::new).toList();
    }

    public List<DeviceStatusDTO> getAmbientConditionsList() {
        return ambientConditionsList;
    }

    public void setAmbientConditionsList(List<DeviceStatusDTO> ambientConditionsList) {
        this.ambientConditionsList = ambientConditionsList;
    }

    public List<DeviceStatusDTO> getAirConditioningList() {
        return airConditioningList;
    }

    public void setAirConditioningList(List<DeviceStatusDTO> airConditioningList) {
        this.airConditioningList = airConditioningList;
    }

    public List<DeviceStatusDTO> getWashingMachineList() {
        return washingMachineList;
    }

    public void setWashingMachineList(List<DeviceStatusDTO> washingMachineList) {
        this.washingMachineList = washingMachineList;
    }

    public List<DeviceStatusDTO> getLampList() {
        return lampList;
    }

    public void setLampList(List<DeviceStatusDTO> lampList) {
        this.lampList = lampList;
    }

    public List<DeviceStatusDTO> getVehicleGateList() {
        return vehicleGateList;
    }

    public void setVehicleGateList(List<DeviceStatusDTO> vehicleGateList) {
        this.vehicleGateList = vehicleGateList;
    }

    public List<DeviceStatusDTO> getSprinklerSystemList() {
        return sprinklerSystemList;
    }

    public void setSprinklerSystemList(List<DeviceStatusDTO> sprinklerSystemList) {
        this.sprinklerSystemList = sprinklerSystemList;
    }

    public List<DeviceStatusDTO> getSolarSystemList() {
        return solarSystemList;
    }

    public void setSolarSystemList(List<DeviceStatusDTO> solarSystemList) {
        this.solarSystemList = solarSystemList;
    }

    public List<DeviceStatusDTO> getBatteryList() {
        return batteryList;
    }

    public void setBatteryList(List<DeviceStatusDTO> batteryList) {
        this.batteryList = batteryList;
    }

    public List<DeviceStatusDTO> getChargerList() {
        return chargerList;
    }

    public void setChargerList(List<DeviceStatusDTO> chargerList) {
        this.chargerList = chargerList;
    }
}
