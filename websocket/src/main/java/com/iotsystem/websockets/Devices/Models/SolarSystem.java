package com.iotsystem.websockets.Devices.Models;

import javax.persistence.Entity;

@Entity
public class SolarSystem extends Device {
    private Integer numberOfPanels;
    private Integer panelSize;
    private Float panelEfficiency;
    private Integer powerUsage;

    public SolarSystem() {
    }

    public SolarSystem(String name, Integer panelSize, Float panelEfficiency, Integer powerUsage, Integer numberOfPanels) {
        super(name);
        this.panelSize = panelSize;
        this.panelEfficiency = panelEfficiency;
        this.powerUsage = powerUsage;
        this.numberOfPanels = numberOfPanels;
    }

    public Integer getPanelSize() {
        return panelSize;
    }

    public void setPanelSize(Integer panelSize) {
        this.panelSize = panelSize;
    }

    public Float getPanelEfficiency() {
        return panelEfficiency;
    }

    public void setPanelEfficiency(Float panelEfficiency) {
        this.panelEfficiency = panelEfficiency;
    }

    public Integer getPowerUsage() {
        return powerUsage;
    }

    public void setPowerUsage(Integer powerUsage) {
        this.powerUsage = powerUsage;
    }

    public Integer getNumberOfPanels() {
        return numberOfPanels;
    }

    public void setNumberOfPanels(Integer numberOfPanels) {
        this.numberOfPanels = numberOfPanels;
    }
}
