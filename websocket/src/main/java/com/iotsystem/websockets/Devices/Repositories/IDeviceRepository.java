package com.iotsystem.websockets.Devices.Repositories;

import com.iotsystem.websockets.Devices.Models.Device;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IDeviceRepository extends JpaRepository<Device, Long> {
}
