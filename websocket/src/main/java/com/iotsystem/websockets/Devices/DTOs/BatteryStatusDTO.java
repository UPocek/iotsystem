package com.iotsystem.websockets.Devices.DTOs;

import com.iotsystem.websockets.Devices.Models.Battery;

public class BatteryStatusDTO {
    private Long id;
    private String name;
    private Float level;

    public BatteryStatusDTO(){

    }

    public BatteryStatusDTO(Battery battery) {
        this.id = battery.getId();
        this.name = battery.getName();
        this.level = battery.getLevel()/ battery.getBatteryCapacity()*100;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setLevel(Float level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getLevel() {
        return level;
    }
}
