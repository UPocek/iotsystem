package com.iotsystem.websockets;

import com.iotsystem.websockets.Devices.Repositories.IReadingsRepository;
import com.iotsystem.websockets.Devices.Repositories.IStatusRepository;
import com.iotsystem.websockets.Devices.WS.ReadingsSocketHandler;
import com.iotsystem.websockets.Devices.WS.SocketHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
@EnableWebSocket
public class WebSocketConfiguration implements WebSocketConfigurer {

    @Autowired
    private IStatusRepository statusRepository;
    @Autowired
    private IReadingsRepository readingsRepository;

    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(new SocketHandler(statusRepository), "/status").setAllowedOrigins("*");
        registry.addHandler(new ReadingsSocketHandler(readingsRepository), "/readings").setAllowedOrigins("*");
    }
}
