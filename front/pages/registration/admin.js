import { useEffect, useState } from "react"
import styles from '@/styles/Registration.module.css'
import { useRouter } from "next/router";
import { baseUrl } from "../_app";
import axios from "axios";
import { getUserUsername } from "@/helper/helper";
import NavBar from "@/components/Navbar";

export default function AdminRegistrationPage() {
    const router = useRouter();

    const [formInvalid, setFormInvalide] = useState(false);

    useEffect(() => {
        getUserUsername() != 'admin' && router.replace('/');
    }, [router])

    const handleSubmit = (event) => {
        event.preventDefault();
        const inputs = {
            "username": event.target.email.value,
        }

        if (isFormValid(inputs)) {
            register(inputs, router, setFormInvalide)
        } else {
            alert("You didn't fill out the form properly. Try again.");
        }
    }

    function isFormValid(inputs) {
        return Object.values(inputs).every(value => value.trim() !== '');
    }

    function register(inputs, router, setFormInvalide) {
        axios.post(`${baseUrl}/api/admin`, { 'username': inputs['username'] })
            .then(response => { alert("New admin created."); router.replace('/'); })
            .catch(err => setFormInvalide(true));
    }


    return (
        <div>
            <NavBar />
            <div className={styles.registrationDiv}>
                <form className={styles.registrationForm} onSubmit={handleSubmit}>
                    <div className={styles.titleDiv}>
                        <h1>Register new admin for IoT4Home</h1>
                        <p>Enter thir profile information here. They will get email with password associated.</p>
                    </div>
                    <div className={styles.inputDiv}>
                        <input className={styles.inputField} type="email" id="email" name="email" placeholder="Admin email"></input>
                    </div>
                    <div className={styles.inputDiv}>
                        <div className={styles.submitDiv}>
                            <input className={styles.submitBtn} type="submit" value="Register" />
                        </div>
                    </div>
                    {formInvalid && <p className={styles.err}>Email already taken. Try another one.</p>}
                </form>
            </div>
        </div>)
}