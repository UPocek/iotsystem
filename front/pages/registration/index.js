import { useState } from "react"
import styles from '@/styles/Registration.module.css'
import { useRouter } from "next/router";
import { baseUrl } from "../_app";
import axios from "axios";
import ImageUploadField from "@/components/ImageUploadField";
import Link from "next/link";

export default function RegistrationPage() {
    const router = useRouter();
    const [formInvalid, setFormInvalide] = useState(false);
    const [logo, setLogo] = useState('');

    const handleSubmit = (event) => {
        event.preventDefault();
        const inputs = {
            "name": event.target.name.value,
            "surname": event.target.surname.value,
            "username": event.target.email.value,
            "password": event.target.password.value,
            "repassword": event.target.repassword.value
        }

        if (isFormValid(inputs)) {
            registerNewUser(inputs, router, setFormInvalide)
        } else {
            alert("You didn't fill out the form properly. Try again.");
        }
    }

    function isFormValid(inputs) {
        return Object.values(inputs).every(value => value.trim() !== '') && inputs['password'] === inputs['repassword'];
    }

    function registerNewUser(inputs, router, setFormInvalide) {
        const reader = new FileReader();
        reader.addEventListener('load', (event) => {
            const content = event.target.result;
            register(content, inputs, router, setFormInvalide);
        });
        reader.readAsDataURL(logo);
    }

    function register(profilePicture, inputs, router, setFormInvalide) {
        axios.post(`${baseUrl}/api/user/registration`, { 'username': inputs['username'], 'password': inputs['password'], 'name': inputs['name'], 'surname': inputs['surname'], 'profilePicture': profilePicture })
            .then(response => { alert("New user created."); router.replace("/login") })
            .catch(err => setFormInvalide(true));
    }

    return <div className={styles.registrationDiv}>
        <form className={styles.registrationForm} onSubmit={handleSubmit}>
            <div className={styles.titleDiv}>
                <h1>Register new user for IoT4Home</h1>
                <p>Enter your profile information here.</p>
            </div>
            <div className={`${styles.nameDiv} ${styles.inputDiv}`}>
                <div className={styles.innerNameDiv}>
                    <input className={styles.inputField} type="text" id="name" name="name" placeholder="Name"></input>
                </div>
                <div>
                    <input className={styles.inputField} type="text" id="surname" name="surname" placeholder="Surame"></input>
                </div>
            </div>
            <div className={styles.inputDiv}>
                <input className={styles.inputField} type="email" id="email" name="email" placeholder="Email"></input>
            </div>
            <div className={styles.inputDiv}>
                <input className={styles.inputField} type="password" id="password" name="password" placeholder="Password"></input>
            </div>
            <div className={styles.inputDiv}>
                <input className={styles.inputField} type="password" id="repassword" name="repassword" placeholder="Repeat Password"></input>
            </div>
            <ImageUploadField image={logo} setImage={setLogo} />
            <div className={styles.inputDiv}>
                <div className={styles.submitDiv}>
                    <input className={styles.submitBtn} type="submit" value="Register" />
                </div>
            </div>
            <div>
                Already registered? Go to <Link href='/login'>login</Link>.
            </div>
            {formInvalid && <p className={styles.err}>Email already taken. Try another one.</p>}
        </form>
    </div>
}