import styles from '@/styles/Home.module.css'
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import NavBar from "@/components/Navbar";
import ActionsChart from '@/components/ActionsChart';
import Lamp from '@/components/Lamp';
import GenerationConsumptionChart from '@/components/GenerationConsumptionChart';
import TemperatureAndHumidityChart from '@/components/TemperatureAndHumidityChart';
import AirConditioning from '@/components/AirConditioning';
import ChargerCharts from '@/components/ChargerCharts';
import WashingMachine from '@/components/WashingMachine';
import Charger from '@/components/Charger';
import SolarPanels from '@/components/SolarPanels';
import BatterySystem from '@/components/BatterySystem';
import VehicleGate from "@/components/VehicleGate.jsx";
import SprinklerSystem from "@/components/SprinklerSystem.jsx";
import AvailabilityStatus from "../../components/AvailabilityStatus.jsx";

export default function Device() {
    const router = useRouter();

    const [apartmentId, setApartmentId] = useState();
    const [deviceId, setDeviceId] = useState();
    const [deviceType, setDeviceType] = useState("");


    useEffect(() => {
        setDeviceId(router.query.deviceId);
        setApartmentId(router.query.apartment);
        setDeviceType(router.query.device_type);
    }, [router.query])

    return (
        <div>
            <NavBar />
            {(deviceId && deviceType == 'ambientConditions') && <div className={styles.container}>
                <TemperatureAndHumidityChart deviceId={deviceId} apartmentId={apartmentId} />
            </div>}
            {(deviceId && deviceType == 'airConditioning') && <div className={styles.container}>
                <AirConditioning deviceId={deviceId} apartmentId={apartmentId} />
            </div>}
            {(deviceId && deviceType == 'solarSystem') && <div className={styles.container}>
                <SolarPanels deviceId={deviceId} apartmentId={apartmentId} />
            </div>}
            {(deviceId && deviceType == 'battery') && <div className={styles.container}>
                <BatterySystem deviceId={deviceId} apartmentId={apartmentId} />
            </div>}
            {(deviceId && deviceType == 'charger') && <div className={styles.container}>
                <Charger deviceId={deviceId} apartmentId={apartmentId} />
            </div>}
            {(deviceId && deviceType == 'washingMachine') && <div className={styles.container}>
                <WashingMachine deviceId={deviceId} apartmentId={apartmentId} />
            </div>}
            {(deviceId && deviceType == 'lamp') && <div className={styles.container}>
                <Lamp deviceId={deviceId} apartmentId={apartmentId}/>
            </div>}
            {(deviceId && deviceType == 'vehicleGate') && <div className={styles.container}>
                <VehicleGate deviceId={deviceId} apartmentId={apartmentId}/>
            </div>}
            {(deviceId && deviceType == 'sprinklerSystem') && <div className={styles.container}>
                <SprinklerSystem deviceId={deviceId} apartmentId={apartmentId}/>
            </div>}
            <div className={styles.availability}>
                <h3>Availability monitoring:</h3>
                {
                    deviceId && <AvailabilityStatus deviceId={deviceId} apartmentId={apartmentId} />
                }
            </div>
        </div>)
}