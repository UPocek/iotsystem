import styles from "@/styles/NewDevice.module.css"
import Stepper from "@/components/Stepper";
import { useRouter } from "next/router";
import { getUserRole } from "@/helper/helper";
import { ROLE_USER } from "../_app";
import NavBar from "@/components/Navbar";
import { useEffect, useState } from "react";

export default function AddNewDevice() {
    const router = useRouter();

    useEffect(() => {
        const userRole = getUserRole();
        if (userRole !== ROLE_USER) {
            router.replace('/');
        }
    }, [router]);

    return <div>
        <NavBar />
        <div className={styles.registrationDiv}>
            <div className={styles.registrationForm}>
                <Stepper />
            </div>
        </div>
    </div>
}