import { getUserAccessToken, getUserRefreshToken, logOut } from '@/helper/helper';
import '@/styles/globals.css'
import axios from 'axios';

export const baseUrl = "http://localhost:9080";
export const wsUrl = "ws://localhost:9080";

export const ROLE_ADMIN = "ROLE_ADMIN";
export const ROLE_USER = "ROLE_USER";

axios.interceptors.request.use(
  config => {
    const token = getUserAccessToken();
    if (token && !config.headers['skip']) {
      config.headers['Authorization'] = 'Bearer ' + token
    }
    // config.headers['Content-Type'] = 'application/json';
    return config
  },
  error => {
    Promise.reject(error)
  }
)

axios.interceptors.response.use(
  response => {
    return response;
  },
  async function (error) {
    const originalRequest = error.config;

    if (error.response && error.response.status === 401 && !originalRequest._retry) {
      console.log('Got 401. Trying to refresh token...');
      originalRequest._retry = true;
      const tokenEndpoint = `${baseUrl}/api/user/refreshToken`;
      const accessToken = getUserAccessToken();
      const refreshToken = getUserRefreshToken();

      return axios.post(tokenEndpoint, {
        'accessToken': accessToken,
        'refreshToken': refreshToken,
      }, {
        headers: {
          'Content-Type': 'application/json',
          'skip': true
        },
      }).then(response => {
        if (response.status === 200) {
          localStorage.setItem('accessToken', response.data['accessToken']);
          localStorage.setItem('refreshToken', response.data['refreshToken']);
          return axios(originalRequest);
        } else {
          console.log('Unable to refresh token. Performing log out...');
          logOut();
        }
      }).catch(err => {
        logOut();
      });
    }
    return Promise.reject(error)
  }
)

export default function App({ Component, pageProps }) {
  return <Component {...pageProps} />
}
