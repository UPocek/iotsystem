import VerifySection from "@/components/VerifySection";
import Head from "next/head";

export default function Verify() {
    return (
        <>
            <Head>
                <title>IOT - Verify</title>
                <meta name="description" content="IoT verify" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <main>
                <VerifySection />
            </main>
        </>
    )
}