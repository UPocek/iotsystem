import DevicesOverview from '@/components/DevicesOverview';
import NavBar from '@/components/Navbar';
import { getUserRole } from '@/helper/helper';
import styles from '@/styles/Home.module.css'

import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { ROLE_USER } from '../_app';

export default function DevicesApartmentOverview() {
    const router = useRouter();

    useEffect(() => {
        if (getUserRole() !== ROLE_USER) {
            router.replace('/');
        }
    }, [router])

    return (
        <div>
            <NavBar />
            <div className={styles.container}>
                {router.query.apartmentId && <DevicesOverview apartmentId={router.query.apartmentId} />}
            </div>
        </div>

    );
}