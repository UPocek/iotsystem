import styles from "@/styles/Registration.module.css"
import { useRouter } from "next/router";
import { useState } from "react";
import axios from "axios";
import { baseUrl } from "../_app";
import ImageUploadField from "@/components/ImageUploadField";
import LeafletPlacePicker from "@/components/LeafletPlacePicker";
import NavBar from "../../components/Navbar.jsx";


export default function ApartmentsAddPage() {
    const router = useRouter();

    const [formInvalid, setFormInvalide] = useState(false);
    const [logo, setLogo] = useState('');
    const [error, setError] = useState('');

    const [countries, setCountries] = useState([]);
    const [country, setCountry] = useState('');
    const [cities, setCities] = useState([]);
    const [city, setCity] = useState([]);

    function filterCountries(nameSubstring) {
        if (nameSubstring === '') {
            setCountries([]);
            return;
        }
        axios.get(`${baseUrl}/api/geospatial/country/${nameSubstring}`)
            .then(response => {
                setCountries(response.data);
                if (response.data.length === 1) {
                    filterCities(response.data[0]);
                } else {
                    setCities([]);
                    setCity('');
                }
            })
            .catch(err => {
                console.error(err);
            });
    }

    function filterCities(country) {
        axios.get(`${baseUrl}/api/geospatial/country/${country}/cities`)
            .then(response => {
                setCities(response.data);
            })
            .catch(err => {
                console.error(err);
            });
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        const inputs = {
            "name": event.target.name.value,

            "address": event.target.address.value,
            "city": event.target.city.value,
            "country": event.target.country.value,
            "squareFootage": event.target.squareFootage.value,
            "numberOfFloors": event.target.numberOfFloors.value,
            "numberOfRooms": event.target.numberOfRooms.value,
        }

        if (isFormValid(inputs)) {
            addApartment(inputs, router, setFormInvalide, setError)
        } else {
            alert("You didn't fill out the form properly. Try again.");
        }
    }

    function isFormValid(inputs) {
        return Object.values(inputs).every(value => value.trim() !== '');
    }

    function addApartment(inputs, router, setFormInvalide, setError) {
        const reader = new FileReader();

        if (logo === '') {
            addApartmentOnBackend(null, inputs, router, setFormInvalide, setError);
            return;
        }

        reader.addEventListener('load', (event) => {
            const content = event.target.result;
            addApartmentOnBackend(content, inputs, router, setFormInvalide, setError);
        });
        reader.readAsDataURL(logo);
    }

    function getLatLng() {
        if (localStorage.getItem("latlng")) {
            return JSON.parse(localStorage.getItem("latlng"));
        }
        return null;
    }

    function addApartmentOnBackend(profilePicture, inputs, router, setFormInvalide, setError) {
        axios.post(`${baseUrl}/api/apartment`, {
            "name": inputs["name"],
            "address": inputs["address"],
            "city": inputs["city"],
            "country": inputs["country"],
            "location": getLatLng(),
            "squareFootage": inputs["squareFootage"],
            "numberOfFloors": inputs["numberOfFloors"],
            "numberOfRooms": inputs["numberOfRooms"],

            "imageUrl": profilePicture
        })
            .then(response => {
                alert("New apartment created.");
                router.replace("/")
            })
            .catch(err => {
                setFormInvalide(true);
                setError(err.response?.data?.message ?? "Something went wrong.");
            });
    }

    return <div>
        <NavBar />
        <div className={styles.registrationDiv}>
            <form className={styles.registrationForm} onSubmit={handleSubmit}>
                <div className={styles.titleDiv}>
                    <h1>Register new apartment in IoT4Home</h1>
                    <p>Enter your apartment information here.</p>
                </div>
                <div className={styles.inputDiv}>
                    <input className={styles.inputField} type="text" id="name" name="name" placeholder="Name" />
                </div>
                <div className={styles.inputDiv}>
                    <input className={styles.inputField} type="text" id="address" name="address" placeholder="Address" />
                </div>
                <div className={styles.inputDiv}>
                    <input className={styles.inputField} type="text" id="country" list="country-l" name="country"
                        placeholder="Country"
                        onChange={
                            (event) => {
                                setCountry(event.target.value);
                                filterCountries(event.target.value);
                            }
                        } />
                    <datalist id="country-l">
                        {
                            countries.map(c => {
                                return <option value={c} key={c} />
                            })
                        }
                    </datalist>
                </div>
                <div className={styles.inputDiv}>
                    <input className={styles.inputField} type="text" list="city-l" name="city" placeholder="City"
                        disabled={
                            countries.length !== 1
                        } onChange={
                            (event) => {
                                setCity(event.target.value);
                            }
                        } value={
                            city
                        } />
                    <datalist id="city-l">
                        {
                            cities.map(c => {
                                return <option value={c} key={c} />
                            })
                        }
                    </datalist>
                </div>
                <LeafletPlacePicker />
                <div className={styles.inputDiv}>
                    <input className={styles.inputField} type="number" id="squareFootage" name="squareFootage"
                        placeholder="squareFootage" />
                </div>
                <div className={styles.inputDiv}>
                    <input className={styles.inputField} type="number" id="numberOfFloors" name="numberOfFloors"
                        placeholder="numberOfFloors" />
                </div>
                <div className={styles.inputDiv}>
                    <input className={styles.inputField} type="number" id="numberOfRooms" name="numberOfRooms"
                        placeholder="numberOfRooms" />
                </div>
                <ImageUploadField image={logo} setImage={setLogo} />
                <div className={styles.inputDiv}>
                    <div className={styles.submitDiv}>
                        <input className={styles.submitBtn} type="submit" value="Add Apartment" />
                    </div>
                </div>
                {formInvalid && <p className={styles.err}>{
                    error
                }</p>}
            </form>
        </div>
    </div>
}
