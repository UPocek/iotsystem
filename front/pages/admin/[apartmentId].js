/* eslint-disable @next/next/no-img-element */
import NavBar from '@/components/Navbar';
import { getUserRole } from '@/helper/helper';
import styles from "@/styles/Registration.module.css"

import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { baseUrl, ROLE_ADMIN } from '../_app';
import axios from "axios";
import LeafletPlacePicker from "../../components/LeafletPlacePicker.jsx";

export default function DevicesApartmentOverview() {
    const router = useRouter();

    const [apartment, setApartment] = useState(null);
    const [reason, setReason] = useState('');

    useEffect(() => {
        if (getUserRole() !== ROLE_ADMIN) {
            router.replace('/');
        }

        const apartmentId = router.query.apartmentId;
        if (apartmentId) {
            axios.get(`${baseUrl}/api/apartment/confirmation_needed/${apartmentId}/`)
                .then(response => {
                    setApartment(response.data);
                })
                .catch(err => {
                    console.error(err);
                });
        }
    }, [router])

    const handleSubmit = (event) => {
        event.preventDefault();
    }

    return (
        <div>
            <NavBar />
            {apartment && <div className={styles.apartmentDiv}>
                <form className={styles.registrationForm} onSubmit={handleSubmit}>
                    <div className={styles.titleSuperDiv}>
                        <img src={`${baseUrl}/apartment/${apartment.id}.jpg`}
                            width={120} height={120} alt={`${apartment.id}`} />
                        <div className={styles.titleDiv}>
                            <h1>Apartment waiting for verification</h1>
                            <p>Approve it, or reject with the reason.</p>
                        </div>
                    </div>
                    <div className={styles.inputDiv}>
                        <label>Name</label>
                        <input className={styles.inputField} value={apartment.name} disabled={true} />
                    </div>
                    <div className={styles.inputDiv}>
                        <label>Address</label>
                        <input className={styles.inputField} value={apartment.address} disabled={true} />
                    </div>
                    <div className={styles.inputDiv}>
                        <label>Country</label>
                        <input className={styles.inputField} value={apartment.city.country} disabled={true} />
                    </div>
                    <div className={styles.inputDiv}>
                        <label>City</label>
                        <input className={styles.inputField} value={apartment.city.city} disabled={true} />
                    </div>
                    <LeafletPlacePicker staticLatLong={apartment.location} />
                    <div className={styles.inputDiv}>
                        <label>Square Footage</label>
                        <input className={styles.inputField} value={apartment.squareFootage} disabled={true} />
                    </div>
                    <div className={styles.inputDiv}>
                        <label>Number Of Floors</label>
                        <input className={styles.inputField} value={apartment.numberOfFloors} disabled={true} />
                    </div>
                    <div className={styles.inputDiv}>
                        <label>Number Of Rooms</label>
                        <input className={styles.inputField} value={apartment.numberOfRooms} disabled={true} />
                    </div>
                    <div className={styles.inputDiv}>
                        <div className={styles.submitDiv}>
                            <input className={styles.submitBtn} type="submit" value="Approve"

                                onClick={
                                    () => {
                                        axios.post(`${baseUrl}/api/apartment/confirmation_needed/${apartment.id}/confirm/`)
                                            .then(response => {
                                                router.replace('/');
                                            })
                                            .catch(err => {
                                                console.error(err);
                                            });
                                    }
                                }
                            />
                        </div>
                    </div>
                    <div className={styles.inputDiv}>
                        <label>Or Provide Reject Reason</label>
                        <input className={styles.inputField} value={reason} onChange={
                            (event) => {
                                setReason(event.target.value);
                            }
                        } />
                    </div>
                    <div className={styles.inputDiv}>
                        <div className={styles.submitDiv}>
                            <input className={styles.rejectBtn} type="submit" value="Reject" disabled={
                                reason.length === 0
                            }
                                onClick={
                                    () => {
                                        axios.post(`${baseUrl}/api/apartment/confirmation_needed/${apartment.id}/reject/`, reason)
                                            .then(response => {
                                                router.replace('/');
                                            })
                                            .catch(err => {
                                                console.error(err);
                                            });
                                    }
                                }
                            />
                        </div>
                    </div>
                </form>
            </div>
            }
        </div>

    );
}