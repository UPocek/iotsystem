import styles from '@/styles/Energy.module.css'
import { baseUrl } from '@/pages/_app';
import axios from 'axios';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import { useEffect, useState } from 'react';
import AdminReport from '@/components/AdminReport';
import NavBar from '@/components/Navbar';
ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);


export default function Energy() {
    const [type, setType] = useState('apartment')
    const [cities, setCities] = useState([])
    const [apartments, setApartments] = useState([])

    useEffect(() => {
        axios.get(`${baseUrl}/api/apartment/energyReportData`)
            .then(response => {
                setApartments(response.data['apartments'])
                setCities(response.data['cities'])
            })
            .catch(_err => { });
    }, [])



    return <div>
        <NavBar />
        <div className={styles.marginTop}>
            <div className={styles.containerMid}>
                <h2>Energy reports for selected city or apartment</h2>
                <div>
                    <label htmlFor="type">Choose type of report: </label>
                    <select className={styles.inputField} name="type" id="type" value={type} onChange={(e) => { setType(e.target.value) }}>
                        <option value="apartment">Apartment</option>
                        <option value="city">City</option>
                    </select>
                </div>
                {
                    type && type == 'city' ? <AdminReport selections={cities} type={'city'} /> : <AdminReport selections={apartments} type={'apartment'} />
                }

            </div>
        </div>
    </div>
}