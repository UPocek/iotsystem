import styles from "@/styles/Login.module.css"
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import axios from "axios";
import { baseUrl } from "../_app";
import { getUserRole } from "@/helper/helper";
import NavBar from "@/components/Navbar";

export default function ChengePasswordPage() {
    const router = useRouter();
    const [credentialsNotValid, setCredentialsNotValid] = useState();

    useEffect(() => {
        getUserRole() == null && router.replace('/login');
    }, [router]);

    const handleSubmit = (event) => {
        event.preventDefault();
        const password = event.currentTarget.password.value;
        const repassword = event.currentTarget.repassword.value;
        if (repassword == '' || password == '' || password != repassword) {
            alert("You didn't fill out the form properly. Try again.");
        } else {
            resetPassword({ 'password': password }, router, setCredentialsNotValid)
        }
    }

    function resetPassword(credentials, router, setCredentialsNotValid) {
        axios.put(`${baseUrl}/api/user/change-password`, { 'password': credentials['password'] })
            .then(response => { alert("Password changed successfully"); localStorage.setItem('passwordChanged', true); router.replace('/'); })
            .catch(err => setCredentialsNotValid(true));
    }

    return (
        <div>
            <NavBar />
            <div className={styles.registrationDiv}>
                <form className={styles.registrationForm} onSubmit={handleSubmit}>
                    <div className={styles.titleDiv}>
                        <h1>Change password</h1>
                        <p>Welcome to IoT4Home. Change default password by providing informations below.</p>
                    </div>

                    <div className={styles.inputDiv}>
                        <input className={styles.inputField} type="password" id="password" name="password" placeholder="Password" />
                    </div>
                    <div className={styles.inputDiv}>
                        <input className={styles.inputField} type="password" id="repassword" name="repassword" placeholder="Repeat Password" />
                    </div>
                    <div className={styles.inputDiv}>
                        <div className={styles.submitDiv}>
                            <input className={styles.submitBtn} type="submit" value="Submit" />
                        </div>
                    </div>
                    {credentialsNotValid && <p className="err">New password not valid. Try again.</p>}
                </form>
            </div>
        </div>
    )

}