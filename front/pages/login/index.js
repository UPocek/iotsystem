import styles from "@/styles/Login.module.css"
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import axios from "axios";
import { baseUrl } from "../_app";
import { getUserRole } from "@/helper/helper";
import Link from "next/link";

export default function LoginPage() {
    const router = useRouter();
    const [credentialsNotValid, setCredentialsNotValid] = useState();

    useEffect(() => {
        getUserRole() != null && router.replace('/');
    }, [router]);

    const handleSubmit = (event) => {
        event.preventDefault();
        const username = event.currentTarget.username.value;
        const password = event.currentTarget.password.value;
        if (username == '' || password == '') {
            alert("You didn't fill out the form properly. Try again.");
        } else {
            login({ 'username': username, 'password': password }, router, setCredentialsNotValid)
        }
    }

    function login(credentials, router, setCredentialsNotValid) {
        axios.post(`${baseUrl}/api/user/login`, { 'username': credentials['username'], 'password': credentials['password'] }, { 'skip': true })
            .then(response => { localStorage.setItem('accessToken', response.data['accessToken']); localStorage.setItem('refreshToken', response.data['refreshToken']); router.replace('/'); })
            .catch(err => { console.log(err); setCredentialsNotValid(true) });
    }

    return <div className={styles.registrationDiv}>
        <form className={styles.registrationForm} onSubmit={handleSubmit}>
            <div className={styles.titleDiv}>
                <h1>Login to IoT4Home</h1>
                <p>Enter your profile information here.</p>
            </div>

            <div className={styles.inputDiv}>
                <input className={styles.inputField} type="text" id="username" name="username" placeholder="Email" />
            </div>
            <div className={styles.inputDiv}>
                <input className={styles.inputField} type="password" id="password" name="password" placeholder="Password" />
            </div>
            <div className={styles.inputDiv}>
                <div className={styles.submitDiv}>
                    <input className={styles.submitBtn} type="submit" value="Login" />
                </div>
            </div>
            <div>
                Do not have an account? Jump to <Link href='/registration'>registration</Link> instead.
            </div>
            {credentialsNotValid && <p className="err">Credentials not valid. Try again.</p>}
        </form>
    </div>
}