import Router from "next/router";

export function getUserAccessToken() {
    return localStorage.getItem('accessToken');
}
export function getUserRefreshToken() {
    return localStorage.getItem('refreshToken');
}
export function getUserRole() {
    return parseJwt(getUserAccessToken())['role'];
}
export function getUserUsername() {
    return parseJwt(getUserAccessToken())['sub'];
}
export function getUserId() {
    return parseJwt(getUserAccessToken())['id'];
}

export function getPasswordChangeConfirmed() {
    return localStorage.getItem("passwordChanged") ?? false;
}

function parseJwt(token) {
    if (token == null) {
        return {};
    }
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    const jsonPayload = decodeURIComponent(window.atob(base64).split('').map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
}

export function logOut() {
    localStorage.clear()
    Router.push('/login');
}

// Additional

export function getDeviceImageNameFromType(type) {
    switch (type) {
        case "AirConditioning":
            return "air_conditioner.png";
        case "AmbientConditions":
            return "ambient_conditions_sensor.png";
        case "WashingMachine":
            return "washing_machine.png";
        case "Battery":
            return "battery.png";
        case "Charger":
            return "charger.png";
        case "Lamp":
            return "lamp.png";
        case "SolarSystem":
            return "solar_system.png";
        case "SprinklerSystem":
            return "sprinkler.png";
        case "VehicleGate":
            return "electric_gate.png";
        default:
            return "iot4home.png";
    }
}  