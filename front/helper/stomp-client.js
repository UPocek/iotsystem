import { Client } from '@stomp/stompjs';

let stompClient = null;

const connect = (url, topic, onMessageCallback) => {
    stompClient = new Client({
        brokerURL: url,
        reconnectDelay: 5000,
        heartbeatIncoming: 4000,
        heartbeatOutgoing: 4000,
    });

    stompClient.onConnect = (frame) => {
        console.log('Stomp connected:', frame);
        stompClient.subscribe(topic, onMessageCallback);
    };

    stompClient.onStompError = (frame) => {
        console.error('Stomp error:', frame);
    };

    stompClient.activate();
};

const disconnect = () => {
    if (stompClient) {
        stompClient.deactivate();
        stompClient = null;
    }
};

export { connect, disconnect };
