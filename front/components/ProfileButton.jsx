/* eslint-disable @next/next/no-img-element */
import { getUserRole, getUserUsername } from '@/helper/helper';
import { ROLE_ADMIN, baseUrl } from '@/pages/_app';
import styles from '@/styles/ProfileButton.module.css'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react';

export default function ProfileButton() {
    const router = useRouter();
    const [userUsername, setUserUsername] = useState('');

    useEffect(() => {
        if (getUserUsername() == null) {
            return;
        }
        if (getUserRole() == ROLE_ADMIN) {
            setUserUsername("admin")
        } else {
            setUserUsername(getUserUsername());
        }
    }, [])

    return (
        <div onClick={() => router.push('/change-password')} className={styles.profileButton}>
            {
                userUsername &&
                    <img className={styles.profileImage} src={`${baseUrl}/profile/${userUsername}.jpg`}
                         width={34} height={34} alt={userUsername} />
            }
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                <g clipPath="url(#clip0_121_657)">
                    <path d="M3 9H21V11H3V9ZM3 16H21V18H3V16Z" fill="#151515" />
                </g>
                <defs>
                    <clipPath id="clip0_121_657">
                        <rect width="24" height="24" fill="white" />
                    </clipPath>
                </defs>
            </svg>
        </div>
    )
}