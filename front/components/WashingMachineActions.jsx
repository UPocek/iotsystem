import { useEffect, useState } from "react";
import ScheduleWashing from "./ScheduleWashing";
import styles from '@/styles/AirConditioningActions.module.css'
import axios from "axios";
import { baseUrl } from "@/pages/_app";

export default function WashingMachineActions({ deviceId, apartmentId }) {
    const [currentMode, setCurrentMode] = useState("");
    const [programEnd, setProgramEnd] = useState("");
    const [timeLeft, setTimeLeft] = useState("");
    const [showSchedulePopup, setShowSchedulePopup] = useState(false);
    const [schedulesList, setSchedulesList] = useState([]);
    const [expendSchedules, setExpendSchedules] = useState(false);

    useEffect(() => {
        Promise.all([
            axios.get(`${baseUrl}/api/device/washingMachine/${apartmentId}/${deviceId}`),
            axios.get(`${baseUrl}/api/device/schedule/${apartmentId}/${deviceId}`)
        ]).then(([WashingMachineSettings, AllSchedules]) => {
            setCurrentMode(WashingMachineSettings.data['currentProgram'].toLowerCase());
            setProgramEnd(WashingMachineSettings.data['programEndDateTime']);
            const timeLeft = getTimeDifferenceBetweenDateTimeAndNow(WashingMachineSettings.data['programEndDateTime']);
            setTimeLeft(timeLeft);
            setSchedulesList(AllSchedules.data);
        }).catch(error => { console.log(error) })
    }, [apartmentId, deviceId]);

    useEffect(() => {
        let interval = null;

        interval = setInterval(() => {
            const timeLeft = getTimeDifferenceBetweenDateTimeAndNow(programEnd);
            setTimeLeft(timeLeft);
        }, 60000);

        return () => clearInterval(interval);
    }, [programEnd]);


    function changeMode(newMode) {
        if (currentMode != "off" && newMode != "off") {
            alert("Machine is already washing. Turn it off to force finish the program!");
            return;
        }
        setCurrentMode(newMode);
        axios.post(`${baseUrl}/api/device/washingMachine/${apartmentId}/${deviceId}`, { 'command': `${newMode}` })
            .then(response => {
                setProgramEnd(response.data['programEndDateTime']);
                const timeLeft = getTimeDifferenceBetweenDateTimeAndNow(response.data['programEndDateTime']);
                setTimeLeft(timeLeft);
            })
            .catch(error => { console.log(error) })
    }

    function removeSchedule(scheduleId) {
        axios.delete(`${baseUrl}/api/device/schedule/${scheduleId}`)
            .then(_ => { setSchedulesList(prevState => prevState.filter((item) => item.id != scheduleId)) })
            .catch(err => console.log(err))
    }

    function getTimeDifferenceBetweenDateTimeAndNow(dateTime) {
        const now = new Date();
        const dateTimeInMs = new Date(dateTime).getTime();
        const timeDifferenceInMs = dateTimeInMs - now.getTime();
        const timeDifferenceInMinutes = Math.floor(timeDifferenceInMs / (1000 * 60));
        return timeDifferenceInMinutes;
    }

    return (<div>
        <div className={styles.actions}>
            <div className={styles.modes} >
                <div onClick={() => changeMode("black30")} className={`${styles.button} ${currentMode == "black30" ? styles.active : null}`}>
                    <p>BLACK30</p>
                </div>
                <div onClick={() => changeMode("black45")} className={`${styles.button} ${currentMode == "black45" ? styles.active : null}`}>
                    <p>BLACK45</p>
                </div>
                <div onClick={() => changeMode("white60")} className={`${styles.button} ${currentMode == "white60" ? styles.active : null}`}>
                    <p>WHITE60</p>
                </div>
                <div onClick={() => changeMode("white90")} className={`${styles.button} ${currentMode == "white90" ? styles.active : null}`}>
                    <p>WHITE90</p>
                </div>
                <div onClick={() => changeMode("color45")} className={`${styles.button} ${currentMode == "color45" ? styles.active : null}`}>
                    <p>COLOR45</p>
                </div>
                <div onClick={() => changeMode("off")} className={`${styles.button} ${currentMode == "off" ? styles.active : null}`}>
                    <p>OFF</p>
                </div>
            </div>
        </div>
        {currentMode != 'off' && <div className={styles.cen}>
            <p>{`Program will finish in ${timeLeft} minutes`}</p>
        </div>}
        <hr />
        <h3>Additional options:</h3>
        <div className={styles.schedules}>
            <button className={styles.button} onClick={() => setShowSchedulePopup(prevState => !prevState)}>
                Setup Schedule
            </button>
            <button className={styles.schedulesList} onClick={() => setExpendSchedules(prevState => !prevState)}><p>{expendSchedules ? "Hide Schedules" : "Show Schedules"}↓</p></button>
            <div>
                {expendSchedules && <table className={styles.table}>
                    <thead>
                        <tr>
                            <th>Program scheduled</th>
                            <th>Start time</th>
                            <th>End time</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {schedulesList.map(s =>
                            <tr key={s.id}>
                                <td>{s['command'].replace('_', ' ')}</td>
                                <td>{s['scheduledFrom'].replace('T', ' ')}</td>
                                <td>{s['scheduledTo'].replace('T', ' ')}</td>
                                <td onClick={() => removeSchedule(s.id)}>
                                    <svg fill="#000000" width="30px" height="30px" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                        <title>remove</title>
                                        <path d="M11.188 4.781c6.188 0 11.219 5.031 11.219 11.219s-5.031 11.188-11.219 11.188-11.188-5-11.188-11.188 5-11.219 11.188-11.219zM11.25 17.625l3.563 3.594c0.438 0.438 1.156 0.438 1.594 0 0.406-0.406 0.406-1.125 0-1.563l-3.563-3.594 3.563-3.594c0.406-0.438 0.406-1.156 0-1.563-0.438-0.438-1.156-0.438-1.594 0l-3.563 3.594-3.563-3.594c-0.438-0.438-1.156-0.438-1.594 0-0.406 0.406-0.406 1.125 0 1.563l3.563 3.594-3.563 3.594c-0.406 0.438-0.406 1.156 0 1.563 0.438 0.438 1.156 0.438 1.594 0z"></path>
                                    </svg>
                                </td>
                            </tr>)}
                    </tbody>
                </table>}
            </div>
            {showSchedulePopup && <ScheduleWashing deviceId={deviceId} apartmentId={apartmentId} setShowSchedulePopup={setShowSchedulePopup} setSchedulesList={setSchedulesList} />}
        </div>
    </div >)
}