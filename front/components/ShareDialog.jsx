import styles from '@/styles/PopUp.module.css'
import { baseUrl } from "@/pages/_app";
import axios from "axios";
import { useState } from "react";

export default function ShareDialog({ itemIdToShare, apartmentId, shareType, setShowShareDialog, setHaveAccessList }) {
    const [shareWithUsername, setShareWithUsername] = useState("");

    function share() {
        if (shareWithUsername == "") {
            alert("Input email of person you want to share this apartment with!x");
            return;
        }
        let url = "";
        if (shareType == "apartment") {
            url = `${baseUrl}/api/apartment/giveAccess/${itemIdToShare}/${shareWithUsername}`
        }
        else if (shareType == "device") {
            url = `${baseUrl}/api/device/giveAccess/${apartmentId}/${itemIdToShare}/${shareWithUsername}`
        }
        axios.post(url)
            .then(response => {
                setHaveAccessList(prevSchedules => {
                    return [...prevSchedules, response.data['username']]
                })
                setShowShareDialog(false);
                return;
            })
            .catch(error => { alert(`Error: ${error.response.data.message}`) })
    }

    return (
        <div className={styles.modal}>
            <div className={styles.modal__content}>
                <h2 className={styles.heading}>{`Share access with (enter user email):`}</h2>
                <button onClick={() => setShowShareDialog(false)} className={styles.modal__close}>
                    <span className={styles.modal__icon} />
                </button>
                <div className={styles.expend}>
                    <input className={styles.input} placeholder='email' type='text' id="to" name='to' value={shareWithUsername} onChange={(e) => setShareWithUsername(e.currentTarget.value)} />
                </div>
                <div>
                    <br />
                    <button className={styles.button} onClick={share}>Share</button>
                </div>
            </div>
        </div>
    );
}