/* eslint-disable @next/next/no-img-element */
import { baseUrl } from "@/pages/_app";
import styles from "@/styles/DeviceImage.module.css"
import { useRouter } from "next/router";

export default function DeviceImage({ deviceId }) {
    const router = useRouter();
    return (<div className={styles.container}>
        <button onClick={() => router.back()} className={styles.backbutton}>
            {`🔙`}
        </button>
        <div className={styles.image}>
            {
                deviceId &&
                    <img src={`${baseUrl}/device/${deviceId}.jpg`} alt={deviceId} />
            }
        </div>
    </div>)
}