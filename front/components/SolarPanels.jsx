import ActionsChart from "./ActionsChart";
import DeviceImage from "./DeviceImage";

export default function SolarPanels({ deviceId, apartmentId }) {
    return <>
        <DeviceImage deviceId={deviceId} />
        <ActionsChart deviceId={deviceId} apartmentId={apartmentId} title={'Solar panel actions taken'} />
    </>
}