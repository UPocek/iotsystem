import Link from 'next/link';
import style from '@/styles/Navbar.module.css'
import Image from 'next/image';
import { getUserRole, getUserUsername, logOut } from '@/helper/helper';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { ROLE_ADMIN } from '@/pages/_app';
import ProfileButton from './ProfileButton';

const LOGO_WIDTH = 789;
const LOGO_HEIGHT = 172;

export default function NavBar() {
    const [role, setRole] = useState('');
    const [username, setUsername] = useState('');

    const router = useRouter();

    useEffect(() => {
        setRole(getUserRole());
        setUsername(getUserUsername());
    }, [router])

    return (
        <nav className={style.navbarStyle}>
            <Link href='/'>
                <Image src="/images/logo.png" width={LOGO_WIDTH / 3} height={LOGO_HEIGHT / 3} alt='IoT4Home Logo' priority={true} />
            </Link>
            {
                role == ROLE_ADMIN ? <AdminOptions username={username} /> : <UserOptions />
            }
            <div className={style.navUl}>
                <p className={style.signOut} onClick={logOut} >Sign out</p>
                <ProfileButton />
            </div>
        </nav>
    );
}


function AdminOptions({ username }) {
    return (
        <>
            <ul className={style.navUl}>
                <li><Link className={style.navItem} href="/" >Control Panel</Link></li>
                <li><Link className={style.navItem} href="/admin/energy" >Energy Reports</Link></li>
                {username == 'admin' && <li><Link className={style.navItem} href="/registration/admin">Register new admin</Link></li>}
            </ul>
        </>
    );
}

function UserOptions() {
    return (
        <>
            <ul className={style.navUl}>
                <li><Link className={style.navItem} href="/" >Home</Link></li>
                <li><Link className={style.navItem} href="/apartments/add" >Add apartment</Link></li>
                <li><Link className={style.navItem} href="/devices/add" >Add device</Link></li>
            </ul>
        </>
    );
}