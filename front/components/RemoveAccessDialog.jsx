import styles from '@/styles/PopUp.module.css'
import { baseUrl } from "@/pages/_app";
import axios from "axios";
import { useState } from "react";

export default function RemoveAccessDialog({ itemId, itemName, type, personUsername, setShowDialog, setHaveAccessList, apartmentId = -1 }) {
    const [confirm, setConfirm] = useState(false);

    function removeAccess() {
        if (confirm == false) {
            setShowDialog(null);
            return;
        }
        let url = ''
        if (type == 'apartment') {
            url = `${baseUrl}/api/apartment/removeAccess/${itemId}/${personUsername}`
        } else if (type == 'device') {
            url = `${baseUrl}/api/device/removeAccess/${apartmentId}/${itemId}/${personUsername}`
        }
        axios.put(url)
            .then(response => {
                setHaveAccessList(prevSchedules => {
                    return prevSchedules.filter(userWithAccess => userWithAccess != personUsername);
                })
                setShowDialog(null);
                return;
            })
            .catch(error => { alert(`Error: ${error.response.data.message}`); setShowDialog(null); })
    }

    return (
        <div className={styles.modal}>
            <div className={styles.modal__content}>
                <h2 className={styles.heading}>{`Remove access on ${itemName} to ${personUsername}`} </h2>
                <button onClick={() => setShowDialog(null)} className={styles.modal__close}>
                    <span className={styles.modal__icon} />
                </button>
                <div className={styles.control}>
                    <label className={styles.radio}>
                        <input type="radio" name="confirm" checked={confirm} onChange={() => setConfirm(true)} />
                        Yes
                    </label>
                    <label className={styles.radio}>
                        <input type="radio" name="confirm" checked={!confirm} onChange={() => setConfirm(false)} />
                        No
                    </label>
                </div>
                <div>
                    <br />
                    <button className={styles.button} onClick={removeAccess}>Confirm</button>
                </div>
            </div>
        </div>
    );
}