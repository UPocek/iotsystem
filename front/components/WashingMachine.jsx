import DeviceImage from "./DeviceImage";
import WashingMachineActions from "./WashingMachineActions";
import WashingMachineChart from "./WashingMachineChart";

export default function WashingMachine({ deviceId, apartmentId }) {
    return (<div>
        <DeviceImage deviceId={deviceId} />
        <WashingMachineActions deviceId={deviceId} apartmentId={apartmentId} />
        <WashingMachineChart deviceId={deviceId} apartmentId={apartmentId} />
    </div>);
}