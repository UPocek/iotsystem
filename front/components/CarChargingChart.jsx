import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import { useState, useEffect } from 'react';
import { Bar } from 'react-chartjs-2';
ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
);
import styles from '@/styles/Charts.module.css'


export default function CarChargingChart({ carChargingData }) {
    const [labels, setLabels] = useState([]);
    useEffect(() => {
        if (carChargingData) {
            setLabels(carChargingData.map(a => a['date']))
            console.log(carChargingData)
        }

    }, [carChargingData])

    const options = {
        responsive: true,
        plugins: {
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: 'Car charging actions in system',
                font: {
                    size: 24
                }
            },
        },
    };

    const data = {
        labels,
        datasets: [
            {
                label: 'Energy consumed by charger per day',
                data: carChargingData ? carChargingData.map(a => a['value']) : [],
                backgroundColor: 'rgba(255, 99, 132, 0.5)',
            },
        ],
    };

    return <div className={styles.chartDiv}>{carChargingData && <Bar options={options} data={data} />}

    </div>
}
