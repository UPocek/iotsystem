import styles from '@/styles/Home.module.css'
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import axios from "axios";
import { baseUrl } from "../pages/_app";
import ApartmentCard from "./ApartmentCard.jsx";


export default function AdminApartmentOverview() {
    const router = useRouter();
    const [apartments, setApartments] = useState([]);

    useEffect(() => {
        axios.get(`${baseUrl}/api/apartment/confirmation_needed/`).then(response => {
            setApartments(response.data);
        }).catch(error => {
            console.error(error);
        });
    }, [router]);


    return (
        <div className={styles.container}>
            <h2>Confirmation needed:</h2>
            <div className={styles.apartmentSection}>
                {
                    apartments.map(
                        apartment => {
                            return <ApartmentCard key={apartment.id} apartment={apartment}
                                addDevice={false} url={`/admin/${apartment.id}`}
                            />;
                        }
                    )
                }
            </div>
        </div>
    );
}
