import styles from "@/styles/Lamp.module.css"
import axios from "axios";
import {baseUrl} from "../pages/_app.js";
import AvailabilityChart from "./AvailabilityChart.jsx";
import {useEffect, useMemo, useState} from "react";

export default function AvailabilityStatus({deviceId, apartmentId}) {
    const now = new Date().toISOString();
    const min1 = useMemo(() => new Date(Date.now() - 60000).toISOString(), []);
    const hr6 = useMemo(() => new Date(Date.now() - 3600000).toISOString(), []);
    const hr12 = useMemo(() => new Date(Date.now() - 7200000).toISOString(), []);
    const day = useMemo(() => new Date(Date.now() - 86400000).toISOString(), []);
    const week = useMemo(() => new Date(Date.now() - 604800000).toISOString(), []);
    const month = useMemo(() => new Date(Date.now() - 2592000000).toISOString(), []);
    const [type, setType] = useState(min1);

    return (<div>
        <label htmlFor="type">Choose chart type: </label>
        <select id="type" value={type} onChange={(e) => {
            console.log(e.target.value);
            setType(e.target.value);
        }}>
            <option value={min1}>Live</option>
            <option value={hr6}>6 hour</option>
            <option value={hr12}>12 hour</option>
            <option value={day}>1 day</option>
            <option value={week}>1 week</option>
            <option value={month}>1 month</option>
        </select>
        <AvailabilityChart deviceId={deviceId} apartmentId={apartmentId} fromTimestamp={type} toTimestamp={now}/>
    </div>)
}
