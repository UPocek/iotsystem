import styles from '@/styles/StepperDots.module.css'

export default function StepperDots({ currentStep, maxSteps }) {
    const dots = [];
    for (let i = 1; i <= maxSteps; i++) {
        if (i < currentStep) dots.push(<Dot key={i} type={'previous'} />);
        else if (i == currentStep) dots.push(<Dot key={i} type={'current'} />);
        else dots.push(<Dot key={i} type={'next'} />);
    }
    return <div className={styles.stepper}>
        <div className={styles.info}>{`Step ${currentStep} of ${maxSteps}`}</div>
        <div className={styles.dotContainer}>{dots}</div>
    </div>
}

function Dot({ type }) {
    return <div className={`${type == 'previous' ? styles.previousDot : type == 'current' ? styles.currentDot : styles.nextDot} ${styles.dot}`}></div>
}
