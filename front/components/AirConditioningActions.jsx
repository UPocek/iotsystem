import { baseUrl } from "@/pages/_app";
import axios from "axios";
import { useEffect, useState } from "react"

import styles from '@/styles/AirConditioningActions.module.css'
import ScheduleAC from "./ScheduleAC";

export default function AirConditioningActions({ deviceId, apartmentId }) {

    const [minTemperature, setMinTemperature] = useState(0);
    const [maxTemperature, setMaxTemperature] = useState(0);
    const [currentTemperature, setCurrentTemperature] = useState(0);
    const [currentMode, setCurrentMode] = useState("");
    const [showSchedulePopup, setShowSchedulePopup] = useState(false);
    const [schedulesList, setSchedulesList] = useState([]);
    const [expendSchedules, setExpendSchedules] = useState(false);

    useEffect(() => {
        Promise.all([
            axios.get(`${baseUrl}/api/device/airConditioning/${apartmentId}/${deviceId}`),
            axios.get(`${baseUrl}/api/device/schedule/${apartmentId}/${deviceId}`)
        ]).then(([ACsettings, AllSchedules]) => {
            setCurrentTemperature(ACsettings.data['currentTemp']);
            setMinTemperature(ACsettings.data['minTemp']);
            setMaxTemperature(ACsettings.data['maxTemp']);
            setCurrentMode(ACsettings.data['mode'].toLowerCase());
            setSchedulesList(AllSchedules.data);
        }).catch(error => { console.log(error) })
    }, [apartmentId, deviceId])

    function changeMode(newMode) {
        setCurrentMode(newMode);
        axios.post(`${baseUrl}/api/device/airConditioning/${apartmentId}/${deviceId}`, { 'command': `${newMode}_${currentTemperature}` })
            .then(response => { })
            .catch(error => { console.log(error) })
    }

    function changeTemperature(newTemp) {
        if (newTemp > maxTemperature) {
            alert("Your AC unit doesn't support that high temperature")
            return;
        }
        if (newTemp < minTemperature) {
            alert("Your AC unit doesn't support that low temperature")
            return;
        }
        setCurrentTemperature(newTemp);
        axios.post(`${baseUrl}/api/device/airConditioning/${apartmentId}/${deviceId}`, { 'command': `${currentMode}_${newTemp}` })
            .then(response => { })
            .catch(error => { console.log(error) })
    }

    function removeSchedule(scheduleId) {
        axios.delete(`${baseUrl}/api/device/schedule/${scheduleId}`)
            .then(_ => { setSchedulesList(prevState => prevState.filter((item) => item.id != scheduleId)) })
            .catch(err => console.log(err))
    }

    return (<div>
        <div className={styles.actions}>
            <div className={styles.modes} >
                <div onClick={() => changeMode("cool")} className={`${styles.button} ${currentMode == "cool" ? styles.active : null}`}>
                    <p>COOL</p>
                </div>
                <div onClick={() => changeMode("heat")} className={`${styles.button} ${currentMode == "heat" ? styles.active : null}`}>
                    <p>HEAT</p>
                </div>
                <div onClick={() => changeMode("auto")} className={`${styles.button} ${currentMode == "auto" ? styles.active : null}`}>
                    <p>AUTO</p>
                </div>
                <div onClick={() => changeMode("dry")} className={`${styles.button} ${currentMode == "dry" ? styles.active : null}`}>
                    <p>DRY</p>
                </div>
            </div>
            <div className={styles.current}>
                <p>{`${currentTemperature}°C`}</p>
            </div>

            <div className={styles.temp}>
                <div onClick={() => changeTemperature(currentTemperature + 1)} className={styles.topOption}><p>+</p></div>
                <div onClick={() => changeTemperature(currentTemperature - 1)} className={styles.bottomOption}><p>-</p></div>
            </div>
        </div>
        <hr />
        <h3>Additional options:</h3>
        <div className={styles.schedules}>
            <button className={styles.button} onClick={() => setShowSchedulePopup(prevState => !prevState)}>
                Setup Schedule
            </button>
            <button className={styles.schedulesList} onClick={() => setExpendSchedules(prevState => !prevState)}><p>{expendSchedules ? "Hide Schedules" : "Show Schedules"}↓</p></button>
            <div>
                {expendSchedules && <table className={styles.table}>
                    <thead>
                        <tr>
                            <th>Program scheduled</th>
                            <th>Start time</th>
                            <th>End time</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {schedulesList.map(s =>
                            <tr key={s.id}>
                                <td>{s['command'].replace('_', ' ')}</td>
                                <td>{s['scheduledFrom'].replace('T', ' ')}</td>
                                <td>{s['scheduledTo'].replace('T', ' ')}</td>
                                <td onClick={() => removeSchedule(s.id)}>
                                    <svg fill="#000000" width="30px" height="30px" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                        <title>remove</title>
                                        <path d="M11.188 4.781c6.188 0 11.219 5.031 11.219 11.219s-5.031 11.188-11.219 11.188-11.188-5-11.188-11.188 5-11.219 11.188-11.219zM11.25 17.625l3.563 3.594c0.438 0.438 1.156 0.438 1.594 0 0.406-0.406 0.406-1.125 0-1.563l-3.563-3.594 3.563-3.594c0.406-0.438 0.406-1.156 0-1.563-0.438-0.438-1.156-0.438-1.594 0l-3.563 3.594-3.563-3.594c-0.438-0.438-1.156-0.438-1.594 0-0.406 0.406-0.406 1.125 0 1.563l3.563 3.594-3.563 3.594c-0.406 0.438-0.406 1.156 0 1.563 0.438 0.438 1.156 0.438 1.594 0z"></path>
                                    </svg>
                                </td>
                            </tr>)}
                    </tbody>
                </table>}
            </div>
            {showSchedulePopup && <ScheduleAC deviceId={deviceId} apartmentId={apartmentId} minTemperature={minTemperature} maxTemperature={maxTemperature} setShowSchedulePopup={setShowSchedulePopup} setSchedulesList={setSchedulesList} />}
        </div>

    </div >)
}