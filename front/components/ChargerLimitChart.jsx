import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import { useEffect, useState } from 'react';
import { Line } from 'react-chartjs-2';
ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);
import styles from '@/styles/Charts.module.css'

export default function ChargerLimitChart({ chargingLimitData }) {
    const [labels, setLabels] = useState([]);
    useEffect(() => {
        if (chargingLimitData) {
            setLabels(chargingLimitData.map(a => a['date'].split("T")[0]))
        }

    }, [chargingLimitData])

    const options = {
        responsive: true,
        plugins: {
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: `Charger limit setting actions`,
                font: {
                    size: 24
                }
            },
        },
    };

    return (<div className={styles.chartDiv}>{
        chargingLimitData &&
        <Line
            datasetIdKey='1ß'
            data={{
                labels: labels,
                datasets: [
                    {
                        id: 1,
                        label: 'Limit in %',
                        data: chargingLimitData ? chargingLimitData.map(a => a['value'] * 100) : [],
                        borderColor: 'rgb(53, 162, 235)',
                        backgroundColor: 'rgba(53, 162, 235, 0.5)',
                    },
                ],
            }}
            options={options}
        />}
    </div>)
}