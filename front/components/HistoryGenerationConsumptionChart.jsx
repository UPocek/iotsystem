import { baseUrl } from '@/pages/_app';
import axios from 'axios';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import { useEffect, useState } from 'react';
import { Line } from 'react-chartjs-2';
ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);

export default function HistoryGenerationConsumptionChart({ deviceId, apartmentId, range }) {
    const [generation, setGeneration] = useState([]);
    const [consumption, setConsumption] = useState([]);
    const [chartLabels, setChartLabels] = useState([]);

    const options = {
        responsive: true,
        plugins: {
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: `Last ${range} Energy Generation and Consumption readings`,
                font: {
                    size: 24,
                }
            },
        },
    };
    useEffect(() => {
        const [from, to, step] = processRange(range);
        let timeStamps = getRequiredTimestamps(from, step);
        from.setHours(from.getHours() - 2)
        axios.get(`${baseUrl}/api/readings/batterySystem/history/${apartmentId}/${from.toISOString()}/${to.toISOString()}/${step}`)
            .then(response => {
                const g = [];
                const c = [];
                console.log(response.data)
                for (let ts of timeStamps) {
                    let generationReading = response.data['generation'].find(r => r['timestamp'].slice(0, -4) == ts);
                    g.push(generationReading ? generationReading['value'] : null);

                    let consumptionReading = response.data['consumption'].find(r => r['timestamp'].slice(0, -4) == ts);
                    c.push(consumptionReading ? consumptionReading['value'] : null);
                }

                timeStamps = timeStamps.map(ts => beautifyTime(ts, step < 30))

                setGeneration(g);
                setConsumption(c);
                setChartLabels(timeStamps);
            }
            )
            .catch(_err => { });
    }, [deviceId, apartmentId, range])

    return (<div>
        <Line
            datasetIdKey='1ß'
            data={{
                labels: chartLabels,
                datasets: [
                    {
                        id: 1,
                        label: 'Energy Generated',
                        data: generation,
                        borderColor: 'rgb(53, 162, 235)',
                        backgroundColor: 'rgba(53, 162, 235, 0.5)',
                    },
                    {
                        id: 2,
                        label: 'Energy Consumed',
                        data: consumption,
                        borderColor: 'rgb(255, 99, 132)',
                        backgroundColor: 'rgba(255, 99, 132, 0.5)',
                    },
                ],
            }}
            options={options}
        />
    </div>)
}

function processRange(range) {
    const now = new Date();
    const to = new Date(now);
    const from = new Date(now);
    from.setMinutes(Math.floor(from.getMinutes() / 15) * 15);
    switch (range) {
        case '6h':
            from.setHours(from.getHours() - 6);
            return [from, to, 15];
        case '12h':
            from.setHours(from.getHours() - 12);
            return [from, to, 15];
        case '1d':
            from.setHours(from.getHours() - 24);
            return [from, to, 15];
        case '1w':
            from.setDate(from.getDate() - 7);
            return [from, to, 60];
        case '1m':
            from.setMonth(from.getMonth() - 1);
            return [from, to, 60];
    }

}

function getRequiredTimestamps(from, step) {
    const timestamps = [];
    const now = new Date();
    const current = new Date(from);
    if (step > 30) {
        current.setMinutes(0);
    }
    while (is15MinutesDifference(now, current)) {
        timestamps.push(current.toISOString().split('.')[0].slice(0, -3));
        current.setMinutes(current.getMinutes() + step);
    }

    return timestamps;
}

function beautifyTime(current, date) {
    if (date) {
        return current.split("T")[1]
    }
    return current.replace("T", " ")

}
// Check wheather difference in date1 and date2 is more then 15 minutes
function is15MinutesDifference(date1, date2) {
    const diff = date1.getTime() - date2.getTime();
    return diff > 900000;
}