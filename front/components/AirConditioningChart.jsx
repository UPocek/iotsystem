import styles from '@/styles/ActionsChart.module.css'
import React from 'react';
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { Pie } from 'react-chartjs-2';
import { useState, useEffect } from 'react';
import axios from 'axios';
import { baseUrl } from '@/pages/_app';

ChartJS.register(ArcElement, Tooltip, Legend);

export default function AirConditioningChart({ deviceId, apartmentId }) {
    const [actions, setActions] = useState(null);
    const [possibleUsers, setPossibleUser] = useState([]);
    const [selectedUser, setSelectedUser] = useState('')
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');
    const [errorMessage, setErrorMessage] = useState('');

    useEffect(() => {
        axios.get(`${baseUrl}/api/device/actions/airConditioning/${apartmentId}/${deviceId}`)
            .then(response => { configureChart(response.data, true); })
            .catch(_err => console.log(_err));

    }, [apartmentId, deviceId])

    function nameToColor(name, alpha) {
        let r = Math.random() * 255;
        let g = Math.random() * 255;
        let b = Math.random() * 255;
        return `rgba(${r}, ${g}, ${b}, ${alpha})`;
    }

    function getFilteredData() {
        if (!validInputs()) return;
        let queries = {}
        if (selectedUser != '') queries['userId'] = selectedUser
        if (startDate != '') queries['startDate'] = startDate
        if (endDate != '') queries['endDate'] = endDate

        axios.get(`${baseUrl}/api/device/actions/airConditioning/${apartmentId}/${deviceId}`, { 'params': queries })
            .then(response => { configureChart(response.data, false); })
            .catch(_err => console.log(_err));
    }

    function validInputs() {
        if (new Date(startDate) >= new Date(endDate)) {
            setErrorMessage("Start date needs to come before end date");
            return false;
        }
        if (new Date(startDate) > new Date()) {
            setErrorMessage("Start date needs to come before today");
            return false;
        }
        setErrorMessage('');
        return true;
    }

    function configureChart(receivedData, firstCall) {
        const copyData = JSON.parse(JSON.stringify(receivedData))
        if (firstCall) {

            const addedNames = []
            const uniqueNames = []

            copyData.forEach(element => {
                if (!addedNames.includes(element.userFullName)) {
                    addedNames.push(element.userFullName);
                    uniqueNames.push({ name: element.userFullName.replace('_', ' '), id: element.userId })
                }
            })
            setPossibleUser(uniqueNames);

        }
        const uniqueCommands = []

        copyData.forEach(element => {
            if (!uniqueCommands.includes(element.action.split('_')[0])) {
                uniqueCommands.push(element.action.split('_')[0]);
            }
        })

        const backgroundColorsGenerated = uniqueCommands.map(c => nameToColor(c, 0.2))
        const borderColorsGenerated = uniqueCommands.map(c => nameToColor(c, 1))

        const actionCounts = []

        uniqueCommands.forEach(command => actionCounts.push(receivedData.map(a => a.action.split('_')[0] == command ? 1 : 0).reduce((partialSum, a) => partialSum + a, 0)));

        const dataToShow = {
            labels: uniqueCommands,
            datasets: [{
                label: '# of Commands',
                data: actionCounts,
                backgroundColor: backgroundColorsGenerated,
                borderColor: borderColorsGenerated,
                borderWidth: 1,
            }]
        }
        setActions(dataToShow)
    }

    return (<div>
        <div>
            <div className={styles.filterBar}>
                <div className={styles.inputDiv}>
                    <select className={styles.inputField} name="userFullName" id="userFullName" value={selectedUser} onChange={(e) => { setSelectedUser(e.target.value) }}>
                        <option value="">Select user</option>
                        {possibleUsers.map(u => <option key={u['name']} value={u['id']}>{u['name']}</option>)}
                    </select>
                </div>
                <div>
                    <input className={styles.inputField} min={0} type="text" id="startDate" name="startDate" placeholder="Start date" value={startDate} onFocus={(e) => (e.target.type = "date")} onBlur={(e) => (e.target.type = "text")} onChange={(e) => { setStartDate(e.target.value) }}></input>
                </div>
                <div>
                    <input className={styles.inputField} min={0} type="text" id="endDate" name="endDate" placeholder="End date" value={endDate} onFocus={(e) => (e.target.type = "date")} onBlur={(e) => (e.target.type = "text")} onChange={(e) => { setEndDate(e.target.value) }}></input>
                </div>
                <div className={`${styles.inputDiv}`}>
                    <div>
                        <button className={styles.btn} onClick={getFilteredData}>Filter</button>
                    </div>
                </div>
            </div>
            <div className={styles.inputDiv}>
                <p className={styles.err}>{errorMessage}</p>
            </div>
        </div>
        {actions &&
            <div className={styles.pieChart}>
                <Pie data={actions} />
            </div>}
    </div >);
}