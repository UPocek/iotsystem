import { baseUrl } from '@/pages/_app';
import axios from 'axios';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import { useEffect, useState } from 'react';
import { Line } from 'react-chartjs-2';
ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);

export default function HistoryTemperatureAndHumidityChart({ deviceId, apartmentId, range, custom_from, custom_to }) {

    const [temp, setTemp] = useState([]);
    const [humid, setHumid] = useState([]);
    const [chartLabels, setChartLabels] = useState([]);

    const options = {
        responsive: true,
        plugins: {
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: range != 'custom' ? `Last ${range} Temperature and Humidity readings` : `Custom range from ${custom_from ?? ''}, to ${custom_to ?? ''}`,
            },
        },
    };

    useEffect(() => {
        if (range == 'custom' && (custom_from == '' || custom_to == '')) {
            return;
        }
        const [from, to, step] = processRange(range, custom_from, custom_to);
        let timeStamps = getRequiredTimestamps(from, step);
        axios.get(`${baseUrl}/api/readings/temperatureAndHumidity/history/${from.toISOString()}/${to.toISOString()}/${step}?deviceId=${deviceId}&apartmentId=${apartmentId}`)
            .then(response => {

                const t = [];
                const h = [];
                console.log(response.data)
                for (let ts of timeStamps) {
                    let tempreading = response.data['temperature'].find(tempreading => tempreading['timestamp'].slice(0, -4) == ts);
                    t.push(tempreading ? tempreading['value'] : null);
                    let humidreading = response.data['humidity'].find(humidreading => humidreading['timestamp'].slice(0, -4) == ts);
                    h.push(humidreading ? humidreading['value'] : null);
                }

                timeStamps = timeStamps.map(ts => beautifyTime(ts, step < 30))


                setTemp(t);
                setHumid(h);
                setChartLabels(timeStamps);
            }
            )
            .catch(_err => { });
    }, [deviceId, apartmentId, range, custom_from, custom_to])

    return (<div>
        <Line
            datasetIdKey='1'
            data={{
                labels: chartLabels,
                datasets: [
                    {
                        id: 1,
                        label: 'Temperature',
                        data: temp,
                        borderColor: 'rgb(53, 162, 235)',
                        backgroundColor: 'rgba(53, 162, 235, 0.5)',
                    },
                    {
                        id: 2,
                        label: 'Humidity',
                        data: humid,
                        borderColor: 'rgb(255, 99, 132)',
                        backgroundColor: 'rgba(255, 99, 132, 0.5)',
                    },
                ],
            }}
            options={options}
        />
    </div>)
}

function processRange(range, custom_from, custom_to) {
    const now = new Date();
    const to = new Date(now);
    const from = new Date(now);
    from.setMinutes(Math.floor(from.getMinutes() / 15) * 15);
    switch (range) {
        case '6h':
            from.setHours(from.getHours() - 6);
            return [from, to, 15];
        case '12h':
            from.setHours(from.getHours() - 12);
            return [from, to, 15];
        case '1d':
            from.setHours(from.getHours() - 24);
            return [from, to, 15];
        case '1w':
            from.setDate(from.getDate() - 7);
            return [from, to, 60];
        case '1m':
            from.setMonth(from.getMonth() - 1);
            return [from, to, 60];
        case 'custom':
            return [new Date(custom_from), new Date(custom_to), 60];
    }

}

function getRequiredTimestamps(from, step) {
    const timestamps = [];
    const now = new Date();
    const current = new Date(from);
    if (step > 30) {
        current.setMinutes(0);
    }
    while (is15MinutesDifference(now, current)) {
        timestamps.push(current.toISOString().split('.')[0].slice(0, -3));
        current.setMinutes(current.getMinutes() + step);
    }

    return timestamps;
}

function beautifyTime(current, date) {
    if (date) {
        return current.split("T")[1]
    }
    return current.replace("T", " ")

}

// Check wheather difference in date1 and date2 is more then 15 minutes
function is15MinutesDifference(date1, date2) {
    const diff = date1.getTime() - date2.getTime();
    return diff > 900000;
}
