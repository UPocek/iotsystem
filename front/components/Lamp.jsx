import DeviceImage from "./DeviceImage";
import {useEffect, useMemo, useState} from "react";
import LiveIlluminationChart from "./LiveIlluminationChart.jsx";
import Image from "next/image.js";
import styles from "@/styles/Lamp.module.css"
import axios from "axios";
import {baseUrl} from "../pages/_app.js";

export default function Lamp({deviceId, apartmentId}) {
    const now = new Date().toISOString();
    const min1 = useMemo(() => new Date(Date.now() - 60000).toISOString(), []);
    const hr6 = useMemo(() => new Date(Date.now() - 3600000).toISOString(), []);
    const hr12 = useMemo(() => new Date(Date.now() - 7200000).toISOString(), []);
    const day = useMemo(() => new Date(Date.now() - 86400000).toISOString(), []);
    const week = useMemo(() => new Date(Date.now() - 604800000).toISOString(), []);
    const month = useMemo(() => new Date(Date.now() - 2592000000).toISOString(), []);
    const [type, setType] = useState(min1);

    const [autoStatus, setAutoStatus] = useState(false);
    const [lightStatus, setLightStatus] = useState(false);

    useEffect(() => {
        axios.get(`${baseUrl}/api/device/lamp/${apartmentId}/${deviceId}`).then(lampSettings => {
            setAutoStatus(lampSettings.data['automaticMode']);
            setLightStatus(lampSettings.data['lightOn']);
        }).catch(error => {
            console.log(error)
        })
    }, [apartmentId, deviceId]);

    function toggleAuto(e) {
        e.stopPropagation();
        axios.post(`${baseUrl}/api/device/lamp/${apartmentId}/${deviceId}`, {'command': autoStatus ? 'manual' : 'auto'})
            .then(response => {
                setAutoStatus(response.data['automaticMode']);
                setLightStatus(response.data['lightOn']);
            })
            .catch(error => {
                console.log(error);
            });
    }

    function toggleOnOff(e) {
        e.stopPropagation();
        axios.post(`${baseUrl}/api/device/lamp/${apartmentId}/${deviceId}`, {'command': lightStatus ? 'light-off' : 'light-on'})
            .then(response => {
                setAutoStatus(response.data['automaticMode']);
                setLightStatus(response.data['lightOn']);
            })
            .catch(error => {
                console.log(error);
            });
    }

    function onOffControl(autoStatus, lightStatus) {
        return !autoStatus ? <div className={styles.button}>
            <Image onClick={toggleOnOff} className={styles.button} src={
                lightStatus ? '/images/play.png' : '/images/pause.png'
            } width={30} height={30} alt={
                lightStatus ? 'LIGHT' : 'DARK'
            }/>
        </div> : <div> </div>
    }


    return (<div>
        <DeviceImage deviceId={deviceId}/>

        <div className={styles.settings}>Select light switch mode:
            <div className={styles.button} onClick={toggleAuto}>
                <Image src={
                    autoStatus ? '/images/auto_black.png' : '/images/manual_black.png'
                } width={30} height={30} alt={
                    autoStatus ? 'AUTO' : 'MANUAL'
                }/>
            </div>
            {
                onOffControl(autoStatus, lightStatus)
            }
        </div>


        <label htmlFor="type">Choose chart type: </label>
        <select id="type" value={type} onChange={(e) => {
            console.log(e.target.value);
            setType(e.target.value);
        }}>
            <option value={min1}>Live</option>
            <option value={hr6}>6 hour</option>
            <option value={hr12}>12 hour</option>
            <option value={day}>1 day</option>
            <option value={week}>1 week</option>
            <option value={month}>1 month</option>
        </select>
        <LiveIlluminationChart deviceId={deviceId} apartmentId={apartmentId} fromTimestamp={type} toTimestamp={now}/>
    </div>)
}
