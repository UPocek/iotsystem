import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import { Line } from 'react-chartjs-2';
ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);

export default function LiveGenerationConsumptionChart({ generation, consumption, chartLabels }) {


    const options = {
        responsive: true,
        animation: false,
        plugins: {
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: 'Live Energy Generation and Consumption readings',
                font: {
                    size: 24,
                }
            },
        },
    };

    return (<div>
        <Line
            datasetIdKey='1'
            data={{
                labels: chartLabels,
                datasets: [
                    {
                        id: 1,
                        label: 'Energy Generated',
                        data: generation,
                        borderColor: 'rgb(53, 162, 235)',
                        backgroundColor: 'rgba(53, 162, 235, 0.5)',
                    },
                    {
                        id: 2,
                        label: 'Energy Consumed',
                        data: consumption,
                        borderColor: 'rgb(255, 99, 132)',
                        backgroundColor: 'rgba(255, 99, 132, 0.5)',
                    },
                ],
            }}
            options={options}
        />

    </div>)
}
