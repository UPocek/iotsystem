import DeviceImage from "./DeviceImage";
import GenerationConsumptionChart from "./GenerationConsumptionChart";

export default function BatterySystem({ deviceId, apartmentId }) {
    return <>
        <DeviceImage deviceId={deviceId} />
        <GenerationConsumptionChart deviceId={deviceId} apartmentId={apartmentId} />
    </>
}