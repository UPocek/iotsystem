import styles from '@/styles/Energy.module.css'
import { baseUrl } from '@/pages/_app';
import axios from 'axios';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import { useEffect, useState } from 'react';
import { Line } from 'react-chartjs-2';
ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);

export default function AdminReport({ selections, type }) {
    const [choosenOption, setChoosenOption] = useState('')
    const [range, setRange] = useState('');
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');
    const [errorMessage, setErrorMessage] = useState('');

    const [generation, setGeneration] = useState([]);
    const [consumption, setConsumption] = useState([]);
    const [chartLabels, setChartLabels] = useState([]);

    const options = {
        responsive: true,
        plugins: {
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: `Last ${range} Energy Generation and Consumption readings`,
                font: {
                    size: 24,
                }
            },
        },
    };

    function selectNewRange(newRange) {
        setRange(newRange)

        if ((newRange != 'custom' && type && choosenOption != '' && newRange != '') || sendCustomDate()) {
            const [from, to, step] = processRange(newRange);
            let timeStamps = getRequiredTimestamps(from, step);
            from.setHours(from.getHours() - 2)
            axios.get(`${baseUrl}/api/readings/batterySystem/${type}/${choosenOption}/${from.toISOString()}/${to.toISOString()}/${step}`)
                .then(response => {
                    const g = [];
                    const c = [];
                    console.log(response.data)
                    for (let ts of timeStamps) {
                        let generationReading = response.data['generation'].find(r => r['timestamp'].slice(0, -4) == ts);
                        g.push(generationReading ? generationReading['value'] : null);

                        let consumptionReading = response.data['consumption'].find(r => r['timestamp'].slice(0, -4) == ts);
                        c.push(consumptionReading ? consumptionReading['value'] : null);
                    }

                    timeStamps = timeStamps.map(ts => beautifyTime(ts, step < 30))
                    setGeneration(g);
                    setConsumption(c);
                    setChartLabels(timeStamps);
                }
                )
                .catch(_err => { });
        }

    }

    function sendCustomDate() {
        return range == 'custom' && startDate != '' && endDate != '' && validInputs()
    }

    function validInputs() {
        if (range == 'custom') {
            if (new Date(startDate) >= new Date(endDate)) {
                setErrorMessage("Start date needs to come before end date");
                return false;
            }
            if (new Date(startDate) > new Date()) {
                setErrorMessage("Start date needs to come before today");
                return false;
            }

            if (getDaysBetweenDates(new Date(startDate), new Date(endDate)) > 32) {
                setErrorMessage("Start date needs to be at most 30 days in advance");
                return false;
            }
            setErrorMessage('');
            return true;
        }

    }

    function getDaysBetweenDates(dayFrom, dayTo) {
        const millisecondsPerDay = 1000 * 60 * 60 * 24;

        const start = dayFrom.getTime();
        const end = dayTo.getTime();

        const diff = end - start;

        return Math.ceil(diff / millisecondsPerDay);
    }

    function processRange(range) {
        const now = new Date();
        let to = new Date(now);
        let from = new Date(now);
        from.setMinutes(Math.floor(from.getMinutes() / 15) * 15);
        switch (range) {
            case '6h':
                from.setHours(from.getHours() - 6);
                return [from, to, 15];
            case '12h':
                from.setHours(from.getHours() - 12);
                return [from, to, 15];
            case '1d':
                from.setHours(from.getHours() - 24);
                return [from, to, 15];
            case '1w':
                from.setDate(from.getDate() - 7);
                return [from, to, 60];
            case '1m':
                from.setMonth(from.getMonth() - 1);
                return [from, to, 60];
            case 'custom':
                to = new Date(endDate);
                from = new Date(startDate);
                return [from, to, 60];
        }

    }

    function getRequiredTimestamps(from, step) {
        const timestamps = [];
        const now = new Date();
        now.setHours(now.getHours() - 1);
        const current = new Date(from);
        current.setHours(current.getHours() - 1);
        if (step > 30) {
            current.setMinutes(0);
        }
        while (is15MinutesDifference(now, current)) {
            timestamps.push(current.toISOString().split('.')[0].slice(0, -3));
            current.setMinutes(current.getMinutes() + step);
        }
        return timestamps;
    }

    function beautifyTime(current, date) {
        if (date) {
            return current.split("T")[1]
        }
        return current.replace("T", " ")

    }

    // Check wheather difference in date1 and date2 is more then 15 minutes
    function is15MinutesDifference(date1, date2) {
        const diff = date1.getTime() - date2.getTime();
        return diff > 900000;
    }

    return <div>
        <h3>{type.charAt(0).toUpperCase() + type.slice(1)} Report</h3>
        <div className={styles.filterBar}>
            <div className={styles.options}>
                <label htmlFor="possibleOptions">Choose {type}: </label>
                <select className={styles.inputField} name="possibleOptions" id="possibleOptions" value={choosenOption} onChange={(e) => { setChoosenOption(e.target.value) }}>
                    <option value="" disabled selected>Select option</option>
                    {selections.map(o => <option key={o['id']} value={o['id']}>{o['name']}</option>)}
                </select>
            </div>
            <div className={styles.options}>
                <label htmlFor="range">Choose time range: </label>
                <select id="range" value={range} onChange={(e) => { selectNewRange(e.target.value) }}>
                    <option value="" selected disabled>Set range</option>
                    <option value="6h">6 hour</option>
                    <option value="12h">12 hour</option>
                    <option value="1d">1 day</option>
                    <option value="1w">1 week</option>
                    <option value="1m">1 month</option>
                    <option value="custom">custom</option>
                </select>
            </div>

            {range == 'custom' &&
                <>
                    <div>
                        <input className={styles.inputField} min={0} type="text" id="startDate" name="startDate" placeholder="Start date" value={startDate} onFocus={(e) => (e.target.type = "date")} onBlur={(e) => (e.target.type = "text")} onChange={(e) => { setStartDate(e.target.value) }}></input>
                    </div>
                    <div>
                        <input className={styles.inputField} min={0} type="text" id="endDate" name="endDate" placeholder="End date" value={endDate} onFocus={(e) => (e.target.type = "date")} onBlur={(e) => (e.target.type = "text")} onChange={(e) => { setEndDate(e.target.value) }}></input>
                    </div>
                    <div className={`${styles.inputDiv}`}>
                        <div>
                            <button className={styles.btn} onClick={() => selectNewRange('custom')}>Filter</button>
                        </div>
                    </div>
                </>
            }

        </div>
        <div className={styles.inputDiv}>
            <p className={styles.err}>{errorMessage}</p>
        </div>
        <div>
            <Line
                datasetIdKey='1ß'
                data={{
                    labels: chartLabels,
                    datasets: [
                        {
                            id: 1,
                            label: 'Energy Generated',
                            data: generation,
                            borderColor: 'rgb(53, 162, 235)',
                            backgroundColor: 'rgba(53, 162, 235, 0.5)',
                        },
                        {
                            id: 2,
                            label: 'Energy Consumed',
                            data: consumption,
                            borderColor: 'rgb(255, 99, 132)',
                            backgroundColor: 'rgba(255, 99, 132, 0.5)',
                        },
                    ],
                }}
                options={options}
            />
        </div>
    </div>
}

