import DeviceImage from "./DeviceImage.jsx";

export default function VehicleGate({deviceId, apartmentId}) {
    return <div>
        <DeviceImage deviceId={deviceId}/>
    </div>
}