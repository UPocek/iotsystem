import { useEffect, useState } from "react";
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';

import { Line } from 'react-chartjs-2';
ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);

import styles from '@/styles/Charts.module.css'

export default function ChargerActions({ chargingOnOffData, colors }) {
    const [actions, setActions] = useState(null);

    const options = {
        responsive: true,
        plugins: {
            legend: {
                position: 'top',
                display: false,
            },
            title: {
                display: true,
                text: "Actions to turn-on/turn-off the charger",
                font: {
                    size: 24,
                }
            },
        },
        color: 'rgb(255,255,255)',
        tooltips: {
            callbacks: {
                label: function (tooltipItem) {
                    return tooltipItem.yLabel;
                }
            }
        },
        elements: {
            point: {
                radius: 4,

            },

        },
        scales: {
            y: {
                type: 'category',
                labels: ["TURNED ON", "TURNED OFF"],
                offset: true,
                position: 'left',
                stack: 'demo',
                stackWeight: 1,
            },
            x: {
                ticks: {
                    font: {
                        size: 12,
                    }
                },
            }
        }
    };

    useEffect(() => {
        if (chargingOnOffData) {
            configureChart(chargingOnOffData)
        }

    }, [chargingOnOffData])

    function configureChart(receivedData) {

        const dataToShow = {
            labels: receivedData.map(d => {
                let date = new Date(d['date']);
                return date.toLocaleString('en-GB', {
                    day: 'numeric',
                    month: 'long',
                    year: 'numeric',
                    hour: 'numeric',
                    minute: 'numeric'
                });
            }),

            datasets: [
                {
                    data: receivedData.map(d => d['action'] == "turn_on" ? "TURNED ON" : "TURNED OFF"),
                    borderColor: 'rgb(213, 213, 216)',
                    borderWidth: 0.75,
                    pointBackgroundColor: function (context) {
                        return colors ? colors[context.dataIndex] : 'rgb(0, 213, 0)';
                    },
                    stepped: true,
                    tooltip: {
                        callbacks: {
                            label: function (tooltipItem, data) {
                                return receivedData[tooltipItem.dataIndex]['userFullName'].replaceAll('"', '').replace('_', ' ');
                            }
                        }
                    },

                },]
        }
        setActions(dataToShow)
    }

    return (<div className={styles.chartDiv}>{actions && <Line options={options} data={actions}></Line>}</div>
    )
}