import styles from "@/styles/ChartSettings.module.css"
import { useMemo, useState } from "react";
import LiveTemperatureAndHumidityChart from "./LiveTemperatureAndHumidityChart";
import HistoryTemperatureAndHumidityChart from "./HistoryTemperatureAndHumidityChart";
import DeviceImage from "./DeviceImage";

export default function TemperatureAndHumidityChart({ deviceId, apartmentId }) {
    const [type, setType] = useState("live");
    const [fromDate, setFromDate] = useState('');
    const [toDate, setToDate] = useState('');
    const today = useMemo(() => new Date(), []);

    function changeFrom(newDateValue) {
        if (toDate != null && isMoreThen30DaysDifference(newDateValue, toDate)) {
            alert("'From' date can't be more then 30 days appart of 'To' date");
            return;
        }
        if (new Date(newDateValue) >= new Date(toDate)) {
            alert("'From' date can't be after 'To' date");
            return;
        }
        if (new Date(newDateValue) > new Date()) {
            alert("'From' date can't be in the future");
            return
        }
        setFromDate(newDateValue);
    }

    function changeTo(newDateValue) {
        if (fromDate != null && isMoreThen30DaysDifference(fromDate, newDateValue)) {
            alert("'To' date can't be more then 30 days appart of 'From' date");
            return;
        }
        if (new Date(newDateValue) <= new Date(fromDate)) {
            alert("'To' date can't be before 'From' date");
            return;
        }
        setToDate(newDateValue);
    }

    // function to chech is their is more then 30 days difference between two dates
    function isMoreThen30DaysDifference(date1, date2) {
        const diff = new Date(date2).getTime() - new Date(date1).getTime();
        return diff > 2592000000;
    }

    function calculateOneMonthBefore() {
        const oneMonthBefore = new Date(toDate);
        oneMonthBefore.setMonth(oneMonthBefore.getMonth() - 1);
        return oneMonthBefore;
    }

    return (<div>
        <DeviceImage deviceId={deviceId} />
        <div className={styles.options}>
            <div className={styles.chooseType}>
                <label htmlFor="type">Choose chart type: </label>
                <select id="type" value={type} onChange={(e) => { setType(e.target.value) }}>
                    <option value="live">Live</option>
                    <option value="6h">6 hour</option>
                    <option value="12h">12 hour</option>
                    <option value="1d">1 day</option>
                    <option value="1w">1 week</option>
                    <option value="1m">1 month</option>
                    <option value="custom">Custom range</option>
                </select>
            </div>
            {type == 'custom' &&
                <div className={styles.customRange}>
                    <label htmlFor="chart_from">From</label>
                    <input type="date" min={toDate != '' ? calculateOneMonthBefore().toISOString().split('T')[0] : ''} max={(toDate != '' && new Date(toDate) <= new Date()) ? new Date(toDate).toISOString().split('T')[0] : today.toISOString().split('T')[0]} id="chart_from" name="chart_from" value={fromDate} onChange={(e) => changeFrom(e.currentTarget.value)} />
                    <label htmlFor="chart_to">To</label>
                    <input type="date" id="chart_to" name="chart_to" value={toDate} onChange={(e) => changeTo(e.currentTarget.value)} />
                </div>}
        </div>
        <div>
            {type == 'live' ?
                <div className={styles.chart}><LiveTemperatureAndHumidityChart deviceId={deviceId} apartmentId={apartmentId} /></div>
                :
                <HistoryTemperatureAndHumidityChart deviceId={deviceId} apartmentId={apartmentId} range={type} custom_from={fromDate} custom_to={toDate} />}
        </div>
    </div>)
}