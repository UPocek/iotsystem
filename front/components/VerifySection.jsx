import { useState, useEffect } from 'react';
import styles from '@/styles/VerifySection.module.css'
import Image from 'next/image';
import axios from 'axios';
import { useRouter } from 'next/router';
import { baseUrl } from '@/pages/_app';

export default function VerifySection() {
    const router = useRouter();

    const [verified, setVerified] = useState(null);

    useEffect(() => {
        const code = router.query.code;
        if (code != null) {
            verifyUser(code);
        }
    }, [router.query.code]);

    function verifyUser(code) {
        axios.put(`${baseUrl}/api/user/verify/${code}`,)
            .then(res => setVerified(true))
            .catch(err => setVerified(false));
    }

    return (
        <>
            {verified == null ? <PrecessingVerification /> : <Verified verified={verified} />}
        </>
    );
}

function PrecessingVerification() {
    return (<div className={styles.precessingVerification}>
        <div className={styles.spinner}>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
        <p>Verification in process...</p>
    </div>
    );
}

function Verified({ verified }) {
    return (
        <div className={styles.verifiedContainer}>
            <div>
                {verified == true ? <Image src={'/images/memoji-share.png'} width={150} height={150} alt='Verification for IoT4Home is sucessfull' /> : <Image src={'/images/memoji-fail.png'} width={150} height={150} alt='Verification for IoT4Home is not sucessfull' />}
            </div>
            <div className={styles.result}>
                <div className={styles.memojiText}>
                    {verified == true ? <p>Verification for IoT4Home is successful. You can close this page.</p> : <p>Verification for IoT4Home is not sucessfull. Try again.</p>}
                </div>
                <div className={verified == true ? styles.verified : styles.notVerified}>
                    {verified == true ? <p>✓</p> : <p>!</p>}
                </div>
            </div>
        </div>
    )
}