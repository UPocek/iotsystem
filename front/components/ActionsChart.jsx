import { useEffect, useState } from "react";
import axios from "axios";
import { baseUrl } from "@/pages/_app";
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import styles from '@/styles/ActionsChart.module.css'
import { Line } from 'react-chartjs-2';
ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);

export default function ActionsChart({ deviceId, apartmentId, title }) {
    const [actions, setActions] = useState(null);
    const [possibleUsers, setPossibleUser] = useState([]);
    const [selectedUser, setSelectedUser] = useState('')
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');
    const [errorMessage, setErrorMessage] = useState('');
    const [colors, setColors] = useState([])


    const options = {
        responsive: true,
        plugins: {
            legend: {
                position: 'top',
                display: false,
            },
            title: {
                display: true,
                text: title,
                font: {
                    size: 24,
                }
            },
        },
        color: 'rgb(255,255,255)',
        tooltips: {
            callbacks: {
                label: function (tooltipItem) {
                    return tooltipItem.yLabel;
                }
            }
        },
        elements: {
            point: {
                radius: 6,
            }
        },
        scales: {
            y: {
                type: 'category',
                labels: ["TURNED ON", "TURNED OFF"],
                offset: true,
                position: 'left',
                stack: 'demo',
                stackWeight: 1,
            },
            x: {
                ticks: {
                    font: {
                        size: 12,
                    }
                },
            }
        }
    };

    useEffect(() => {
        axios.get(`${baseUrl}/api/device/actions/solar/${apartmentId}/${deviceId}`)
            .then(response => { configureChart(response.data, true); })
            .catch(_err => console.log(_err));

    }, [])

    function nameToColor(name) {
        let r = 0;
        let g = 255;
        let b = 0;

        for (let char of name) {
            r += char.charCodeAt(0);
            g -= char.charCodeAt(0);
            b += char.charCodeAt(0);
        }

        g = Math.max(g, 0);

        r = Math.floor(r / name.length);
        g = Math.floor(g / name.length);
        b = Math.floor(b / name.length);

        return `rgb(${r},${g},${b})`;
    }


    function configureChart(receivedData, firstCall) {

        if (firstCall) {
            const copyData = JSON.parse(JSON.stringify(receivedData))
            const addedNames = []
            const uniqueNames = []

            copyData.forEach(element => {
                if (!addedNames.includes(element.userFullName)) {
                    addedNames.push(element.userFullName);
                    uniqueNames.push({ name: element.userFullName.replace('_', ' '), id: element.userId })
                }
            })
            setPossibleUser(uniqueNames);

            setColors(uniqueNames.map(n => nameToColor(n['name'])))
        }


        const dataToShow = {
            labels: receivedData.map(d => {
                let date = new Date(d['date']);
                return date.toLocaleString('en-GB', {
                    day: 'numeric',
                    month: 'long',
                    year: 'numeric',
                    hour: 'numeric',
                    minute: 'numeric'
                });
            }),

            datasets: [
                {
                    data: receivedData.map(d => d['action'] == "turn_on" ? "TURNED ON" : "TURNED OFF"),
                    borderColor: 'rgb(213, 213, 216)',
                    pointBackgroundColor: function (context) {
                        return colors[context.dataIndex];
                    },
                    stepped: true,
                    tooltip: {
                        callbacks: {
                            label: function (tooltipItem, data) {
                                return receivedData[tooltipItem.dataIndex]['userFullName'].replaceAll('"', '').replace('_', ' ');
                            }
                        }
                    },

                },]
        }
        setActions(dataToShow)
    }

    function getFilteredData() {
        if (!validInputs()) return;
        let queries = {}
        if (selectedUser != '') queries['userId'] = selectedUser
        if (startDate != '') queries['startDate'] = startDate
        if (endDate != '') queries['endDate'] = endDate

        axios.get(`${baseUrl}/api/device/actions/solar/${apartmentId}/${deviceId}`, { params: queries })
            .then(response => { configureChart(response.data, false); })
            .catch(_err => console.log(_err));
    }

    function validInputs() {
        if (new Date(startDate) >= new Date(endDate)) {
            setErrorMessage("Start date needs to come before end date");
            return false;
        }
        if (new Date(startDate) > new Date()) {
            setErrorMessage("Start date needs to come before today");
            return false;
        }
        setErrorMessage('');
        return true;
    }

    return (<div>
        <div className={styles.filterBar}>
            <div className={styles.inputDiv}>
                <select className={styles.inputField} name="userFullName" id="userFullName" value={selectedUser} onChange={(e) => { setSelectedUser(e.target.value) }}>
                    <option value="" disabled>Select user</option>
                    {possibleUsers.map(u => <option key={u['name']} value={u['id']}>{u['name']}</option>)}
                </select>
            </div>
            <div>
                <input className={styles.inputField} min={0} type="text" id="startDate" name="startDate" placeholder="Start date" value={startDate} onFocus={(e) => (e.target.type = "date")} onBlur={(e) => (e.target.type = "text")} onChange={(e) => { setStartDate(e.target.value) }}></input>
            </div>
            <div>
                <input className={styles.inputField} min={0} type="text" id="endDate" name="endDate" placeholder="End date" value={endDate} onFocus={(e) => (e.target.type = "date")} onBlur={(e) => (e.target.type = "text")} onChange={(e) => { setEndDate(e.target.value) }}></input>
            </div>
            <div className={`${styles.inputDiv}`}>
                <div>
                    <button className={styles.btn} onClick={getFilteredData}>Filter</button>
                </div>
            </div>
        </div>
        <div className={styles.inputDiv}>
            <p className={styles.err}>{errorMessage}</p>
        </div>
        <div >{actions && <Line options={options} data={actions}></Line>}</div>
    </div>
    )
}