import {useEffect, useMemo, useState} from "react";
import axios from "axios";
import 'chartjs-adapter-moment';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
    TimeScale
} from 'chart.js';
import {Bar, Line} from "react-chartjs-2";
import {baseUrl, wsUrl} from "../pages/_app.js";
import {getUserUsername} from "../helper/helper.js";

ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
    TimeScale
);

export default function AvailabilityChart({deviceId, apartmentId, fromTimestamp, toTimestamp}) {
    const [data, setData] = useState({
        datasets: [{
            label: 'Illumination',
            data: [],
            backgroundColor: 'transparent',
            borderColor: 'red',
            borderWidth: 2,
            tension: 0.5
        }]
    });
    const [lock, setLock] = useState(true);
    const oneDay = useMemo(() => (new Date(toTimestamp) - new Date(fromTimestamp) < 86400000), [fromTimestamp, toTimestamp]);
    const format = useMemo(
        () => {
            if (oneDay) {
                return {
                    hour: 'numeric',
                    minute: 'numeric'
                }
            } else {
                return {
                    year: 'numeric',
                    month: 'short',
                    day: 'numeric',
                }
            }
        }, [oneDay]
    );

    const options = {
        scales: {
            x: {
                type: 'linear',
                ticks: {
                    suggestedMin: new Date(fromTimestamp).getTime() / 1000,
                    suggestedMax: new Date(toTimestamp).getTime() / 1000,
                    stepSize: 2,
                    callback: function (dataLabel, index) {
                        // dataLabel is seconds - need to get string representation
                        let date = new Date(dataLabel * 1000);
                        if (oneDay)
                            return date.toLocaleTimeString(undefined, format);
                        else
                            return date.toLocaleDateString(undefined, format);
                    }
                }
            },
        },
        responsive: true,
        animation: false,
    };

    useEffect(() => {
            axios.get(`${baseUrl}/api/readings/availability?deviceId=${deviceId}&apartmentId=${apartmentId}&fromTimestamp=${fromTimestamp}&toTimestamp=${toTimestamp}`)
                .then(response => {
                    console.log(response.data)

                    const availability = []
                    for (let i = 0; i < response.data['availability'].length; i++) {
                        availability.push({
                            x: new Date(response.data['availability'][i]['timestamp']).getTime() / 1000,
                            y: response.data['availability'][i]['value'],
                        })
                    }

                    const newData = {
                        datasets: [{
                            label: 'Availability in %',
                            data: availability,
                            backgroundColor: 'transparent',
                            borderColor: 'red',
                            tension: 0.5
                        }]
                    }
                    setData(newData)
                    console.log(newData);
                })
                .catch(_err => {
                });
        }
        ,
        [deviceId, apartmentId, fromTimestamp, toTimestamp]
    )


    return <div>
        <Line
            data={data}
            options={options}
            redraw={true}
        />
    </div>
}
