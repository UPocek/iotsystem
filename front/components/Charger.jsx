import styles from '@/styles/Charger.module.css'
import { useState } from 'react';
import axios from 'axios';
import { baseUrl } from '@/pages/_app';
import ChargerCharts from './ChargerCharts';
import DeviceImage from './DeviceImage';

export default function Charger({ apartmentId, deviceId }) {
    const [fullness, setFullnes] = useState(0);

    const [errorMessage, setErrorMessage] = useState('');

    function validateInput() {
        if (fullness < 1 || fullness > 100) return showError("You can set this charger to charge only between 1% and 100%.");
        setErrorMessage('');
        return true;
    }

    function showError(message) {
        setErrorMessage(message);
        return false;
    };

    function updateFullnes() {
        if (validateInput()) {
            axios.post(`${baseUrl}/api/device/fullness/${apartmentId}/${deviceId}`, { 'command': `percentage_battery_full/${fullness / 100}` })
                .then(response => {
                    console.log('d')
                    setFullnes(0)
                })
                .catch(error => {
                    console.log(error)
                });
        }

    }

    return <>
        <DeviceImage deviceId={deviceId} />
        <div>
            <h3>Set up your charger</h3>
            <div className={styles.fullness}>
                <div>
                    <input className={styles.inputField} min={0} max={100} type="number" id="fullness" name="fullness" placeholder="Charge till %" value={fullness === 0 ? "Charge till %" : fullness} onChange={(e) => { setFullnes(e.target.value) }}></input>
                </div>
                <div>
                    <button className={styles.btn} onClick={() => updateFullnes()}>Update</button>
                </div>
                <div className={styles.inputDiv}>
                    <p className={styles.err}>{errorMessage}</p>
                </div>
            </div>
            <ChargerCharts apartmentId={apartmentId} deviceId={deviceId} />

        </div>
    </>

}