import styles from '@/styles/PopUp.module.css'
import { baseUrl } from "@/pages/_app";
import axios from "axios";
import { useState } from "react";

export default function ScheduleWashing({ deviceId, apartmentId, setShowSchedulePopup, setSchedulesList }) {
    const [from, setFrom] = useState(() => {
        const today = new Date();
        today.setHours(today.getHours() + 1);
        return today.toISOString().split('T')[1].substring(0, 5)
    });

    const [selectedMode, setSelectedMode] = useState("black30");

    function createSchedule() {
        axios.post(`${baseUrl}/api/device/washingMachine/schedule/${apartmentId}/${deviceId}?program=${selectedMode}&from=${from + ':00'}`)
            .then(response => {
                setSchedulesList(prevSchedules => {
                    return [...prevSchedules, response.data]
                })
                setShowSchedulePopup(false);
                return;
            })
            .catch(error => { console.log(error) })
    }

    return (
        <div className={styles.modal}>
            <div className={styles.modal__content}>
                <h2 className={styles.heading}>Schedule Washing</h2>
                <button onClick={() => setShowSchedulePopup(false)} className={styles.modal__close}>
                    <span className={styles.modal__icon} />
                </button>
                <div>
                    <label htmlFor='from'>Start program at: </label>
                    <input className={styles.button} type='time' id="from" name='from' value={from} onChange={(e) => setFrom(e.currentTarget.value)} />
                </div>
                <br />
                <div className={styles.actions}>
                    <div className={styles.modes} >
                        <div onClick={() => setSelectedMode("black30")} className={`${styles.button} ${selectedMode == "black30" ? styles.active : null}`}>
                            <p>BLACK30</p>
                        </div>
                        <div onClick={() => setSelectedMode("black45")} className={`${styles.button} ${selectedMode == "black45" ? styles.active : null}`}>
                            <p>BLACK45</p>
                        </div>
                        <div onClick={() => setSelectedMode("white60")} className={`${styles.button} ${selectedMode == "white60" ? styles.active : null}`}>
                            <p>WHITE60</p>
                        </div>
                        <div onClick={() => setSelectedMode("white90")} className={`${styles.button} ${selectedMode == "white90" ? styles.active : null}`}>
                            <p>WHITE90</p>
                        </div>
                        <div onClick={() => setSelectedMode("color45")} className={`${styles.button} ${selectedMode == "color45" ? styles.active : null}`}>
                            <p>COLOR45</p>
                        </div>
                    </div>
                </div>
                <div>
                    <br />
                    <button className={styles.button} onClick={createSchedule}>Confirm Schedule</button>
                </div>
            </div>
        </div>
    );
}