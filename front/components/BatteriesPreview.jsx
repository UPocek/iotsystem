import styles from "@/styles/BatteriesPreview.module.css"

export default function BatteriesPreview({ batteries }) {
    return <div className={styles.container}>
        <h3>Batteries status</h3>
        <div className={styles.batteries}>
            {batteries && batteries.map(b => <div key={b['name']}><div className={styles.battery} style={{ backgroundColor: b['level'] <= 20 ? 'var(--Red)' : 'var(--Success)' }}>
                <div style={{
                    backgroundColor: 'rgba(51, 51, 51, .75)',
                    height: (100 - b['level']) + '%',
                    width: '100%'
                }}></div>
            </div>
                <p>Level: {Math.round(b['level'])}%</p>
                <p>Name: {b['name']}</p>
            </div>)}

        </div>
    </div>

}