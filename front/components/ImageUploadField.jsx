import { useState } from "react"
import styles from '@/styles/ImageUploadField.module.css'
import Image from "next/image";

export default function ImageUploadField({ image, setImage }) {
    const maxLogoSizeInMBs = 0.5;
    const [fileTooLarge, setFileToLarge] = useState(false);
    const [preview, setPreview] = useState(image && URL.createObjectURL(image));

    function previewLogo(e) {
        const file = e.currentTarget.files[0];
        if (file.size > 1048576 * maxLogoSizeInMBs) {
            setFileToLarge(true);
        }
        else {
            setFileToLarge(false);
            setImage(file);
            setPreview(URL.createObjectURL(file));
        }
    }

    return <div className={styles.uploadContainer}>
        <div>
            <p>Profile picture</p>
            {<label className={styles.fileUploadBasic} htmlFor="image">{image ? <Image className={styles.image} src={preview} alt="Logo" width={120} height={120} /> : <p>Choose image</p>}</label>}
            <input className={styles.hide} type="file" id="image" name="image" onChange={previewLogo} accept="image/*" />
        </div>
        <div>
            <p className={styles.smallNote}>{`*Recommended size 1080px*1080px (max ${maxLogoSizeInMBs}MB)`}</p>
            {fileTooLarge && <p className="err">{`Choeses image is too large. Please choose image smaller then ${maxLogoSizeInMBs}MB`}</p>}
        </div>
    </div>
}