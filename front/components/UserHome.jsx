import styles from '@/styles/Home.module.css'
import { useRouter } from "next/router.js";
import { useEffect, useState } from "react";
import { baseUrl } from "../pages/_app.js";
import axios from "axios";
import ApartmentCard from "./ApartmentCard.jsx";
import ItemCard from './ItemCard.jsx';
import { getDeviceImageNameFromType } from '@/helper/helper.js';

export default function UserHome() {
    const router = useRouter();
    const [apartments, setApartments] = useState([]);
    const [sharedApartments, setSharedApartments] = useState([]);
    const [devicesShared, setDevicesShared] = useState([]);

    useEffect(() => {
        Promise.all([
            axios.get(`${baseUrl}/api/apartment/`),
            axios.get(`${baseUrl}/api/apartment/shared`),
            axios.get(`${baseUrl}/api/device/shared`),
        ])
            .then(([ownerApartments, accessedApartments, justDevicesShared]) => {
                setApartments(ownerApartments.data);
                setSharedApartments(accessedApartments.data);
                setDevicesShared(justDevicesShared.data);
            }).catch(error => {
                console.error(error);
            });
    }, [router]);

    return (
        <div className={styles.container}>
            <h2>My Apartments:</h2>
            <div className={styles.apartmentSection}>
                {
                    apartments.map(
                        apartment => {
                            return <ApartmentCard key={apartment.id} apartment={apartment}
                                url={`/apartments/${apartment.id}`} />;
                        }
                    )
                }
            </div>
            {sharedApartments.length > 0 && <h2>Apartments shared with you:</h2>}
            {
                sharedApartments.length > 0 && <div className={styles.apartmentSection}>
                    {
                        sharedApartments.map(
                            apartment => {
                                return <ApartmentCard key={apartment.id} apartment={apartment}
                                    url={`/apartments/${apartment.id}`} shareable={false} />;
                            }
                        )
                    }
                </div>
            }
            {devicesShared.length > 0 &&
                (<>
                    <h2>Devices shared with you:</h2>
                    <div className={styles.devicesSection}>
                        {devicesShared.map(item => <ItemCard key={item['id']} deviceId={item['id']} haveAccess={item['usersWithAccess']} ownerUsername={item['ownerUsername']} deviceImage={getDeviceImageNameFromType(item['type'])} deviceName={item['name']} deviceStatus={item['online']} deviceType={item['type'].charAt(0).toLowerCase() + item['type'].slice(1)} setItems={setDevicesShared} apartmentId={item['apartmentId']} shareable={false} />)}
                    </div>
                </>)
            }
        </div>
    );
}