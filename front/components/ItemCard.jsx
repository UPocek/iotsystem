/* eslint-disable @next/next/no-img-element */
import { baseUrl } from "@/pages/_app";
import styles from "@/styles/ItemCard.module.css"
import axios from "axios";
import Image from "next/image"
import { useRouter } from "next/router";
import { useMemo, useState } from "react"
import ShareDialog from "./ShareDialog";
import RemoveAccessDialog from "./RemoveAccessDialog";

export default function ItemCard({ deviceId, ownerUsername, haveAccess, deviceName, deviceImage, deviceStatus, deviceType, apartmentId, setItems, hasOnOff = true, shareable = true, hasAutoMode = false, deviceAutoStatus = false, hasPrivatePublic = false, devicePrivateStatus = false }) {
    const router = useRouter();

    const activeStatus = useMemo(() => deviceStatus, [deviceStatus])
    const autoStatus = useMemo(() => deviceAutoStatus, [deviceAutoStatus])
    const privateStatus = useMemo(() => devicePrivateStatus, [devicePrivateStatus])
    const [showShareDialog, setShowShareDialog] = useState(false);
    const [removeAccessFor, setRemoveAccessFor] = useState(null);
    const [haveAccessList, setHaveAccessList] = useState(() => {
        let usersWithAccess = [ownerUsername];
        if (haveAccess != null && haveAccess.length > 0) {
            const temp = haveAccess.map(item => item['username']);
            usersWithAccess = usersWithAccess.concat(temp);
        }
        return usersWithAccess
    });

    function toggleActive(e) {
        e.stopPropagation();
        axios.post(`${baseUrl}/api/device/on-off/${apartmentId}/${deviceId}`, { 'command': activeStatus ? 'turn_off' : 'turn_on' })
            .then(response => {
                setItems(prevState => {
                    const newItems = [...prevState];
                    const itemIndex = newItems.findIndex(item => item['id'] == deviceId);
                    newItems[itemIndex]['online'] = response.data;
                    return newItems;
                })
            })
            .catch(error => {
                console.log(error);
            });
    }

    return (
        <>
            <div className={styles.cardOuterContainer}>
                <div onClick={() => router.push(`/devices/${deviceId}?apartment=${apartmentId}&device_type=${deviceType}`)} className={`${styles.card} ${activeStatus ? styles.on : styles.off}`}>
                    {shareable && <div onClick={(e) => { e.stopPropagation(); setShowShareDialog(true) }} className={styles.sharebtn}>
                        <p>+</p>
                    </div>}
                    <div><Image src={`/images/${deviceImage}`} width={50} height={50} alt={deviceName} /></div>
                    <div className={styles.text}>
                        <p className={styles.title}>{`${deviceName}`}</p>
                        <p className={styles.subTitle}>{activeStatus ? "Active" : "Paused"}</p>
                    </div>
                    {hasOnOff && <div onClick={toggleActive} className={styles.btn}>
                        <Image src={activeStatus ? '/images/pause_white.png' : '/images/play_white.png'} width={30} height={30} alt={deviceName} />
                    </div>}
                </div>
                {shareable && <div className={styles.accessBubles}>
                    {haveAccessList.map((userUsername, index) =>
                        <div onClick={(e) => { e.stopPropagation(); setRemoveAccessFor(userUsername); }} key={userUsername} style={{ zIndex: `${10 - index}` }} className={`${styles.bubble} ${styles.tooltip}`}>
                            <img src={`${baseUrl}/profile/${userUsername}.jpg`} alt={userUsername}></img>
                            <span className={styles.tooltiptext}>{userUsername}</span>
                            <div className={styles.overlay}>
                                <div className={styles.removeIcon}>X</div>
                            </div>
                        </div>)}
                </div>}

                {!shareable && <div className={styles.accessBubles}>
                    {haveAccessList.map((userUsername, index) =>
                        <div key={userUsername} style={{ zIndex: `${10 - index}` }} className={`${styles.bubble} ${styles.tooltip}`}>
                            <img src={`${baseUrl}/profile/${userUsername}.jpg`} alt={userUsername}></img>
                            <span className={styles.tooltiptext}>{userUsername}</span>
                        </div>)}
                </div>}
            </div>
            {removeAccessFor != null && <RemoveAccessDialog itemId={deviceId} itemName={deviceName} apartmentId={apartmentId} type={'device'} personUsername={removeAccessFor} setShowDialog={setRemoveAccessFor} setHaveAccessList={setHaveAccessList} />}
            {showShareDialog && <ShareDialog itemIdToShare={deviceId} apartmentId={apartmentId} shareType={"device"} setShowShareDialog={setShowShareDialog} setHaveAccessList={setHaveAccessList} />}
        </>
    )
}