import styles from '@/styles/PopUp.module.css'
import { baseUrl } from "@/pages/_app";
import axios from "axios";
import { useState } from "react";

export default function ScheduleAC({ deviceId, apartmentId, minTemperature, maxTemperature, setShowSchedulePopup, setSchedulesList }) {
    const [from, setFrom] = useState(() => {
        const today = new Date();
        today.setHours(today.getHours() + 1);
        return today.toISOString().split('T')[1].substring(0, 5)
    });
    const [to, setTo] = useState(() => {
        const today = new Date();
        today.setHours(today.getHours() + 2);
        return today.toISOString().split('T')[1].substring(0, 5)
    });
    const [selectedMode, setSelectedMode] = useState("cool");
    const [selectedTemperature, setSelectedTemperature] = useState(18);

    function createSchedule() {
        if (selectedTemperature > maxTemperature || selectedTemperature < minTemperature) {
            alert("Invalid temperature. Your AC doesn't suport that temperature.");
            return;
        }
        axios.post(`${baseUrl}/api/device/airConditioning/schedule/${apartmentId}/${deviceId}?temp=${selectedTemperature}&mode=${selectedMode}&from=${from + ':00'}&to=${to + ':00'}`)
            .then(response => {
                console.log(response.data);
                setSchedulesList(prevSchedules => {
                    return [...prevSchedules, response.data]
                })
                setShowSchedulePopup(false);
                return;
            })
            .catch(error => { console.log(error) })
    }

    return (
        <div className={styles.modal}>
            <div className={styles.modal__content}>
                <h2 className={styles.heading}>Schedule AC</h2>
                <button onClick={() => setShowSchedulePopup(false)} className={styles.modal__close}>
                    <span className={styles.modal__icon} />
                </button>
                <div>
                    <label htmlFor='from'>Start program at: </label>
                    <input className={styles.button} type='time' id="from" name='from' value={from} onChange={(e) => setFrom(e.currentTarget.value)} />
                </div>
                <br />
                <div>
                    <label htmlFor='to'>End program at: </label>
                    <input className={styles.button} type='time' id="to" name='to' value={to} onChange={(e) => setTo(e.currentTarget.value)} />
                </div>
                <br />
                <div className={styles.actions}>
                    <div className={styles.modes} >
                        <div onClick={() => setSelectedMode("cool")} className={`${styles.button} ${selectedMode == "cool" ? styles.active : null}`}>
                            <p>COOL</p>
                        </div>
                        <div onClick={() => setSelectedMode("heat")} className={`${styles.button} ${selectedMode == "heat" ? styles.active : null}`}>
                            <p>HEAT</p>
                        </div>
                        <div onClick={() => setSelectedMode("auto")} className={`${styles.button} ${selectedMode == "auto" ? styles.active : null}`}>
                            <p>AUTO</p>
                        </div>
                        <div onClick={() => setSelectedMode("dry")} className={`${styles.button} ${selectedMode == "dry" ? styles.active : null}`}>
                            <p>DRY</p>
                        </div>
                    </div>
                    <div className={styles.current}>
                        <p>{`${selectedTemperature}°C`}</p>
                    </div>

                    <div className={styles.temp}>
                        <div onClick={() => setSelectedTemperature(prevTemp => prevTemp + 1)} className={styles.topOption}><p>+</p></div>
                        <div onClick={() => setSelectedTemperature(prevTemp => prevTemp - 1)} className={styles.bottomOption}><p>-</p></div>
                    </div>
                </div>
                <div>
                    <br />
                    <button className={styles.button} onClick={createSchedule}>Confirm Schedule</button>
                </div>
            </div>
        </div>
    );
}