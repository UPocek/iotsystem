import { useEffect, useState } from "react";

export default function LeafletPlacePicker(staticLatLong = null) {
    const [mapExists, setMapExists] = useState(true);
    const mapId = 'map';
    const noviSadLatLng = [45.267136, 19.833549];
    const noviSadSizeDeg = 11;

    function onMapClick(map, marker, markerAdded, e) {
        if (staticLatLong !== null) {
            return;
        }

        localStorage.setItem("latlng", JSON.stringify({ latitude: e.latlng.lat, longitude: e.latlng.lng }));
        marker.setLatLng(e.latlng);
        if (!markerAdded) {
            markerAdded = true;
            marker.addTo(map);
        }
    }

    function initLeafletMap() {
        if (staticLatLong !== null && staticLatLong.staticLatLong !== undefined) {
            noviSadLatLng[0] = staticLatLong.staticLatLong.latitude;
            noviSadLatLng[1] = staticLatLong.staticLatLong.longitude;
        }

        const map = L.map(mapId).setView(noviSadLatLng, noviSadSizeDeg);

        const tiles = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="http://www.openstreetmap.org/">OpenStreetMap</a>'
        }).addTo(map);

        const marker = L.marker(noviSadLatLng);
        let markerAdded = false;

        if (staticLatLong !== null) {
            markerAdded = true;
            marker.addTo(map);
        }

        map.on('click', onMapClick.bind(this, map, marker, markerAdded));
    }

    useEffect(() => {
        if (typeof window === 'undefined' || !window.document) {
            return;
        }
        if (document.getElementById(mapId).childElementCount > 0) {
            return;
        }

        initLeafletMap();
    }, []);

    return <div>
        {
            mapExists &&
            <div>
                <div id={mapId} style={{ height: '180px' }} />
            </div>
        }
    </div>
}
