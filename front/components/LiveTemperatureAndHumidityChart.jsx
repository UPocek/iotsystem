import { getUserUsername } from '@/helper/helper';
import { baseUrl, wsUrl } from '@/pages/_app';
import axios from 'axios';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import { useEffect, useState } from 'react';
import { Line } from 'react-chartjs-2';
ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);

export default function LiveTemperatureAndHumidityChart({ deviceId, apartmentId }) {

    const [temp, setTemp] = useState([]);
    const [humid, setHumid] = useState([]);
    const [chartLabels, setChartLabels] = useState([]);

    const options = {
        responsive: true,
        animation: false,
        plugins: {
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: 'Live Temperature and Humidity readings',
            },
        },
    };

    useEffect(() => {
        let lastHourTimeStamps = getLastHourTimestamps();
        axios.get(`${baseUrl}/api/readings/temperatureAndHumidity/live?deviceId=${deviceId}&apartmentId=${apartmentId}`)
            .then(response => {

                const t = [];
                const h = [];

                for (let ts of lastHourTimeStamps) {
                    let tempreading = response.data['temperature'].find(tempreading => tempreading['timestamp'].split("T")[1].slice(0, -4) == ts);
                    t.push(tempreading ? tempreading['value'] : null);
                    let humidreading = response.data['humidity'].find(humidreading => humidreading['timestamp'].split("T")[1].slice(0, -4) == ts);
                    h.push(humidreading ? humidreading['value'] : null);
                }

                setTemp(t);
                setHumid(h);
                setChartLabels(lastHourTimeStamps);
            }
            )
            .catch(_err => { });
    }, [deviceId, apartmentId])

    useEffect(() => {
        const authToken = getUserUsername();
        document.cookie = 'X-Authorization=' + authToken + '; path=/';
        const ws = new WebSocket(`${wsUrl}/ws/readings`);

        ws.onmessage = function (newStatus) {
            const newState = JSON.parse(newStatus.data);

            if (!('temperature' in newState && 'humidity' in newState)) {
                return;
            }

            setChartLabels(getLastHourTimestamps());

            setTemp(prevState => [...prevState.slice(1), newState['temperature'].length > 0 ? newState['temperature'][0]['value'] : null])
            setHumid(prevState => [...prevState.slice(1), newState['humidity'].length > 0 ? newState['humidity'][0]['value'] : null])
        }

        return () => {
            if (ws.readyState === 1) {
                ws.close();
            }
        }
    }, [])


    return (<div>
        <Line
            datasetIdKey='1'
            data={{
                labels: chartLabels,
                datasets: [
                    {
                        id: 1,
                        label: 'Temperature',
                        data: temp,
                        borderColor: 'rgb(53, 162, 235)',
                        backgroundColor: 'rgba(53, 162, 235, 0.5)',
                    },
                    {
                        id: 2,
                        label: 'Humidity',
                        data: humid,
                        borderColor: 'rgb(255, 99, 132)',
                        backgroundColor: 'rgba(255, 99, 132, 0.5)',
                    },
                ],
            }}
            options={options}
        />
    </div>)
}

function getLastHourTimestamps() {
    const timestamps = [];
    const now = new Date();
    now.setHours(now.getHours() - 1);
    for (let i = 0; i < 60; i++) {
        now.setMinutes(now.getMinutes() + 1);
        const minutes = now.getMinutes();
        const hours = now.getHours();
        const time = `${String(hours).padStart(2, '0')}:${String(minutes).padStart(2, '0')}`;
        timestamps.push(time);
    }
    return timestamps;
}