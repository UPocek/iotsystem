import styles from "@/styles/ChartSettings.module.css"
import { useState, useEffect } from "react";
import HistoryGenerationConsumptionChart from "./HistoryGenerationConsumptionChart";
import LiveGenerationConsumptionChart from "./LiveGenerationConsumptionChart";
import BatteriesPreview from "./BatteriesPreview";
import axios from "axios";
import { baseUrl, wsUrl } from "@/pages/_app";
import { getUserUsername } from "@/helper/helper";


export default function GenerationConsumptionChart({ deviceId, apartmentId }) {
    const [type, setType] = useState("live");

    const [generation, setGeneration] = useState([]);
    const [consumption, setConsumption] = useState([]);
    const [batteryList, setBatteryList] = useState([]);
    const [chartLabels, setChartLabels] = useState([]);

    useEffect(() => {
        let lastHourTimeStamps = getLastHourTimestamps();
        axios.get(`${baseUrl}/api/readings/batterySystem/live?apartmentId=${apartmentId}&deviceId=${deviceId}`)
            .then(response => {
                const g = [];
                const c = [];
                console.log(response.data)

                for (let ts of lastHourTimeStamps) {
                    let generationReading = response.data['generation'].find(r => r['timestamp'].split("T")[1].slice(0, -4) == ts);
                    g.push(generationReading ? generationReading['value'] : null);
                    let consumptionReading = response.data['consumption'].find(r => r['timestamp'].split("T")[1].slice(0, -4) == ts);
                    c.push(consumptionReading ? consumptionReading['value'] : null);
                }

                setGeneration(g);
                setConsumption(c);
                setBatteryList(response.data['batteries']);
                setChartLabels(lastHourTimeStamps);
            }
            )
            .catch(err => console.log(err));
    }, [deviceId, apartmentId])

    useEffect(() => {
        const authToken = getUserUsername();
        document.cookie = 'X-Authorization=' + authToken + '; path=/';
        const ws = new WebSocket(`${wsUrl}/ws/readings`);

        ws.onmessage = function (newStatus) {
            const newState = JSON.parse(newStatus.data);

            if (!('generation' in newState && 'consumption' in newState)) {
                return;
            }

            setChartLabels(getLastHourTimestamps());
            setGeneration(prevState => [...prevState.slice(1), newState['generation'].length > 0 ? newState['generation'][newState['generation'].length - 1]['value'] : null])
            setConsumption(prevState => [...prevState.slice(1), newState['consumption'].length > 0 ? newState['consumption'][newState['consumption'].length - 1]['value'] : null])
            setBatteryList(newState['batteries'])
        }

        return () => {
            if (ws.readyState === 1) {
                ws.close();
            }
        }
    }, [])

    return (<div>
        <div className={styles.options}>
            <label htmlFor="type">Choose chart type: </label>
            <select id="type" value={type} onChange={(e) => { setType(e.target.value) }}>
                <option value="live">Live</option>
                <option value="6h">6 hour</option>
                <option value="12h">12 hour</option>
                <option value="1d">1 day</option>
                <option value="1w">1 week</option>
                <option value="1m">1 month</option>
            </select>
        </div>
        <div>
            {type == 'live' ?
                <div className={styles.chart}><LiveGenerationConsumptionChart generation={generation} consumption={consumption} chartLabels={chartLabels} /></div>
                : <HistoryGenerationConsumptionChart apartmentId={apartmentId} range={type} />}
        </div>
        <BatteriesPreview batteries={batteryList} />
    </div>)
}

function getLastHourTimestamps() {
    const timestamps = [];
    const now = new Date();
    now.setHours(now.getHours() - 1);
    for (let i = 0; i < 60; i++) {
        now.setMinutes(now.getMinutes() + 1);
        const minutes = now.getMinutes();
        const hours = now.getHours();
        const time = `${String(hours).padStart(2, '0')}:${String(minutes).padStart(2, '0')}`;
        timestamps.push(time);
    }
    return timestamps;
}