import AirConditioningActions from "./AirConditioningActions";
import AirConditioningChart from "./AirConditioningChart";
import DeviceImage from "./DeviceImage";

export default function AirConditioning({ deviceId, apartmentId }) {
    return (<div>
        <DeviceImage deviceId={deviceId} />
        <AirConditioningActions deviceId={deviceId} apartmentId={apartmentId} />
        <AirConditioningChart deviceId={deviceId} apartmentId={apartmentId} />
    </div>)
}