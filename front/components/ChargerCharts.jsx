import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import { useState, useEffect } from 'react';
ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
);
import axios from 'axios';
import { baseUrl } from '@/pages/_app';
import CarChargingChart from './CarChargingChart';
import ChargerActions from './ChargerActions';
import styles from '@/styles/ActionsChart.module.css'
import ChargerLimitChart from './ChargerLimitChart';


export default function ChargerCharts({ deviceId, apartmentId }) {
    const [carChargingData, setCarChargingData] = useState();
    const [chargingLimitData, setChargingLimitData] = useState();
    const [chargingOnOffData, setChargingOnOffData] = useState();
    const [possibleUsers, setPossibleUser] = useState([]);
    const [selectedUser, setSelectedUser] = useState('')
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');
    const [errorMessage, setErrorMessage] = useState('');
    const [colors, setColors] = useState();

    useEffect(() => {
        axios.get(`${baseUrl}/api/device/actions/charge/${apartmentId}/${deviceId}`)
            .then(response => {
                configureChart(response.data, true)
            })
            .catch(_err => console.log(_err));

    }, [])

    function configureChart(receivedData, initail) {
        if (initail) {
            const copyData = JSON.parse(JSON.stringify(receivedData))
            const addedNames = []
            const uniqueNames = []

            copyData['chargingLimitActions'].forEach(element => {
                if (!addedNames.includes(element.userFullName)) {
                    addedNames.push(element.userFullName);
                    uniqueNames.push({ name: element.userFullName.replace('_', ' '), id: element.userId })
                }
            })

            copyData['chargingTurnOnOffActions'].forEach(element => {
                if (!addedNames.includes(element.userFullName)) {
                    addedNames.push(element.userFullName);
                    uniqueNames.push({ name: element.userFullName.replace('_', ' '), id: element.userId })
                }
            })

            setPossibleUser(uniqueNames);

            setColors(uniqueNames.map(n => nameToColor(n['name'])))

            setCarChargingData(receivedData['carChargingActions'])

        }

        setChargingLimitData(receivedData['chargingLimitActions'])
        setChargingOnOffData(receivedData['chargingTurnOnOffActions'])
    }

    function nameToColor(name) {
        let r = 0;
        let g = 255;
        let b = 0;

        for (let char of name) {
            r += char.charCodeAt(0);
            g -= char.charCodeAt(0);
            b += char.charCodeAt(0);
        }

        g = Math.max(g, 0);

        r = Math.floor(r / name.length);
        g = Math.floor(g / name.length);
        b = Math.floor(b / name.length);

        return `rgb(${r},${g},${b})`;
    }

    function getFilteredData() {
        if (!validInputs()) return;
        let queries = {}
        if (selectedUser != '') queries['userId'] = selectedUser
        if (startDate != '') queries['startDate'] = startDate
        if (endDate != '') queries['endDate'] = endDate

        axios.get(`${baseUrl}/api/device/actions/charge/${apartmentId}/${deviceId}`, { params: queries })
            .then(response => { configureChart(response.data, selectedUser == ''); setSelectedUser('') })
            .catch(_err => console.log(_err));
    }

    function validInputs() {
        if (new Date(startDate) >= new Date(endDate)) {
            setErrorMessage("Start date needs to come before end date");
            return false;
        }
        if (new Date(startDate) > new Date()) {
            setErrorMessage("Start date needs to come before today");
            return false;
        }
        setErrorMessage('');
        return true;
    }

    return <div>
        <h3>Reports</h3>
        <div className={styles.filterBar}>
            <div className={styles.inputDiv}>
                <select className={styles.inputField} name="userFullName" id="userFullName" value={selectedUser} onChange={(e) => { setSelectedUser(e.target.value) }}>
                    <option value="" disabled>Select user</option>
                    {possibleUsers.map(u => <option key={u['name']} value={u['id']}>{u['name']}</option>)}
                </select>
            </div>
            <div>
                <input className={styles.inputField} min={0} type="text" id="startDate" name="startDate" placeholder="Start date" value={startDate} onFocus={(e) => (e.target.type = "date")} onBlur={(e) => (e.target.type = "text")} onChange={(e) => { setStartDate(e.target.value) }}></input>
            </div>
            <div>
                <input className={styles.inputField} min={0} type="text" id="endDate" name="endDate" placeholder="End date" value={endDate} onFocus={(e) => (e.target.type = "date")} onBlur={(e) => (e.target.type = "text")} onChange={(e) => { setEndDate(e.target.value) }}></input>
            </div>
            <div className={`${styles.inputDiv}`}>
                <div>
                    <button className={styles.btn} onClick={getFilteredData}>Filter</button>
                </div>
            </div>
        </div>
        <div className={styles.inputDiv}>
            <p className={styles.err}>{errorMessage}</p>
        </div>
        <ChargerActions chargingOnOffData={chargingOnOffData} colors={colors} />
        <ChargerLimitChart chargingLimitData={chargingLimitData} />
        <CarChargingChart carChargingData={carChargingData} />
    </div>

}