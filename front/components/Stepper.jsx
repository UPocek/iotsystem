import { useEffect, useState } from "react"
import StepperDots from "./StepperDots";
import styles from "../styles/Stepper.module.css"
import axios from "axios";
import Spinner from "./Spinner";
import { connect, disconnect } from "@/helper/stomp-client";
import { baseUrl, wsUrl } from "@/pages/_app";
import ImageUploadField from "./ImageUploadField";
import { useSearchParams } from "next/navigation.js";
import { useRouter } from "next/router";

const NUM_OF_STEPS = 4;

const predefinedDeviceTypes = {
    'SHA': 'Smart Home Appliances',
    'ESD': 'External smart devices',
    'LPD': 'Large power devices'
}

const predefinedPowerSupplys = {
    'EXTERNAL_POWER': 'External Battery',
    'INTERNAL_POWER': 'Home Battery/Grid',
}

const predefinedSMAs = {
    'AIRCONDITIONING': 'Air Conditioning',
    'AMBIENTSENSORS': 'Ambient conditions sensor',
    'WASHINGMACHINE': 'Washing machine'
}

const predefinedESDs = {
    'LAMP': 'Lamp',
    'GATE': 'Vehicle gate',
    'SPRINKLERS': 'Sprinkler system'
}

const predefinedLPDs = {
    'SOLAR_SYSTEM': 'Solar panel system',
    'BATTERY': 'Home battery',
    'CHARGER': 'Charger for electric vehicles'
}

const predefinedDevices = {
    'SHA': predefinedSMAs,
    'ESD': predefinedESDs,
    'LPD': predefinedLPDs
}

export default function Stepper() {
    const router = useRouter();

    const searchParams = useSearchParams()
    const [currentStep, setCurrentStep] = useState(1);
    const [deviceConnected, setDeviceConnected] = useState(-1);
    const [deviceCode, setDeviceCode] = useState('');
    const [deviceImg, setDeviceImg] = useState('');
    const [property, setProperty] = useState(searchParams.get('property'));
    const [apartments, setApartments] = useState([]);
    const [deviceType, setDeviceType] = useState('');
    const [deviceOption, setDeviceOption] = useState('');
    const [deviceName, setDeviceName] = useState('');
    const [powerSupply, setPowerSupply] = useState('');
    const [powerUsage, setPowerUsage] = useState(0);
    const [numberOfPanels, setNumberOfPanels] = useState(0);
    const [panelSize, setPanelSize] = useState(0);
    const [panelEfficiency, setPanelEfficiency] = useState(0);
    const [batteryCapacity, setBatteryCapacity] = useState(0);
    const [chargerPower, setChargerPower] = useState(0);
    const [numberOfConnections, setNumberOfConnections] = useState(0);
    const [minTemp, setMinTemp] = useState(0);
    const [maxTemp, setMaxTemp] = useState(0);

    const [errorMessage, setErrorMessage] = useState('');

    useEffect(() => {
        axios.get(`${baseUrl}/api/apartment/`).then(response => {
            setApartments(response.data);
        }).catch(error => {
            console.error(error);
        });
    }, [router]);

    function handleClickForward(step) {
        if (step < 0 || canContinue()) {
            if (currentStep + step > NUM_OF_STEPS - 1) {
                const reader = new FileReader();
                reader.addEventListener('load', (event) => {
                    const content = event.target.result;
                    createDevice(content);
                });
                reader.readAsDataURL(deviceImg);
            } else {
                setCurrentStep(Math.max(1, currentStep + step));
            }
        }
    }

    function canContinue() {
        if (currentStep === 1) {
            return validateStep1();
        } else if (currentStep === 2) {
            return validateStep2();
        } else {
            switch (deviceOption) {
                case 'solarPanelSystem':
                    return validateSolarPanelSystem();
                case 'homeBattery':
                    return validateHomeBattery();
                case 'chargerForElectricVehicles':
                    return validateChargerForElectricVehicles();
                default:
                    setErrorMessage('');
                    return true;
            }
        }
    }


    function validateStep1() {
        if (deviceType === '') return showError("You need to specify Device Type.");
        if (deviceImg === '') return showError("You need to set Device Image.");
        if (deviceName === '') return showError("You need to specify Device Name.");
        if (!property) return showError("You need to select Apartment where Device is installed.");
        setErrorMessage('');
        return true;
    };

    function validateStep2() {
        if (deviceOption === '') return showError("You need to specify Device Option.");
        if (!powerUsage && deviceOption != 'BATTERY' && deviceOption != 'SOLAR_SYSTEM' && deviceOption != 'CHARGER') return showError("You need to set Power Usage.");
        if (powerSupply === '' && deviceType != 'LPD') return showError("You need to specify Power Supply.");
        setErrorMessage('');
        return true;
    };

    function validateSolarPanelSystem() {
        if (!numberOfPanels) return showError("You need to set Number of Panels.");
        if (!panelSize) return showError("You need to set Panel Size.");
        if (!panelEfficiency) return showError("You need to set Panel Efficiency.");
        setErrorMessage('');
        return true;
    };

    function validateHomeBattery() {
        if (!batteryCapacity) return showError("You need to set Battery Capacity.");
        setErrorMessage('');
        return true;
    };

    function validateChargerForElectricVehicles() {
        if (!chargerPower) return showError("You need to set Charger Power.");
        if (!numberOfConnections) return showError("You need to set Number of Connections.");
        setErrorMessage('');
        return true;
    };

    function showError(message) {
        setErrorMessage(message);
        return false;
    };

    function createDevice(content) {
        axios.post(`${baseUrl}/api/device`, {
            'deviceCode': deviceCode,
            'deviceOption': deviceOption,
            'property': parseInt(property, 10),
            'image': content,
            'name': deviceName,
            'powerUsage': powerUsage,
            'powerSupply': powerSupply,
            'panelSize': panelSize,
            'panelEfficiency': panelEfficiency / 100,
            'batteryCapacity': batteryCapacity,
            'power': chargerPower,
            'numberOfConnections': numberOfConnections,
            'numberOfPanels': numberOfPanels,
            'minTemp': minTemp,
            'maxTemp': maxTemp
        })
            .then(response => {
                setCurrentStep(4);
            })
            .catch(_err => {
                console.log(_err);
            });

    }

    return (
        <div className={styles.container}>
            {
                deviceConnected < 1 ? <ConnectDevice deviceConnected={deviceConnected} setDeviceConnected={setDeviceConnected} deviceCode={deviceCode} setDeviceCode={setDeviceCode} /> : <>
                    <div className={styles.titleDiv}>
                        <h1>Register new device</h1>
                        <p>Succesfully connected. Enter your device information here.</p>
                    </div>
                    <StepperDots maxSteps={NUM_OF_STEPS} currentStep={currentStep} />
                    <div>
                        {currentStep == 1 && <StepOne
                            deviceImg={deviceImg}
                            setDeviceImg={setDeviceImg}
                            property={property}
                            setProperty={setProperty}
                            apartments={apartments}
                            deviceName={deviceName}
                            setDeviceName={setDeviceName}
                            deviceType={deviceType}
                            setDeviceType={setDeviceType} />}
                        {currentStep == 2 && <StepTwo
                            deviceType={deviceType}
                            deviceOption={deviceOption}
                            setDeviceOption={setDeviceOption}
                            powerSupply={powerSupply}
                            setPowerSupply={setPowerSupply}
                            powerUsage={powerUsage}
                            setPowerUsage={setPowerUsage}
                            minTemp={minTemp}
                            setMinTemp={setMinTemp}
                            maxTemp={maxTemp}
                            setMaxTemp={setMaxTemp} />}
                        {(currentStep == 3 && deviceType != 'LPD') && <BasicEndStep />}
                        {(currentStep == 3 && deviceOption == 'SOLAR_SYSTEM') && <SolarPanelsStep panelSize={panelSize} setPanelSize={setPanelSize} panelEfficiency={panelEfficiency} setPanelEfficiency={setPanelEfficiency} numberOfPanels={numberOfPanels} setNumberOfPanels={setNumberOfPanels} />}
                        {(currentStep == 3 && deviceOption == 'BATTERY') && <BatteryStep batteryCapacity={batteryCapacity} setBatteryCapacity={setBatteryCapacity} />}
                        {(currentStep == 3 && deviceOption == 'CHARGER') && <ChargerStep chargerPower={chargerPower} setChargerPower={setChargerPower} numberOfConnections={numberOfConnections} setNumberOfConnections={setNumberOfConnections} />}
                        {currentStep == 4 && <ForthStep router={router} />}
                    </div>
                    <div className={styles.inputDiv}>
                        <p className={styles.err}>{errorMessage}</p>
                    </div>
                    <div className={`${styles.inputDiv} ${styles.buttonDiv}`}>
                        <div>
                            {(currentStep > 1 && currentStep < 4) && <button className={styles.btn} onClick={() => handleClickForward(-1)}>Back</button>}
                            {currentStep < 4 && <button className={styles.btn} onClick={() => handleClickForward(1)}>{currentStep == NUM_OF_STEPS ? 'Create new device' : 'Next'}</button>}
                        </div>
                    </div>
                </>
            }
        </div>);
}

function ConnectDevice({ deviceConnected, setDeviceConnected, deviceCode, setDeviceCode }) {

    function onTryConnect() {
        setDeviceConnected(0);
        connect(`${wsUrl}/socket`, `/new-device/device-setup/${deviceCode}`, message => {
            setDeviceConnected(1);
            disconnect();
        });

        setTimeout(deviceNewConnection, 1000);

    }

    function deviceNewConnection() {
        axios.post(`${baseUrl}/api/device/connect-device`, { 'topic': deviceCode })
            .then(() => { })
            .catch(() => { console.error });
    }

    return (
        <>
            <div className={styles.titleDiv}>
                <h1>Register new device</h1>
                <p>Enter code form your device.</p>
            </div>
            {deviceConnected == -1 ?
                <>
                    <div className={`${styles.nameDiv} ${styles.inputDiv}`}>
                        <div>
                            <input className={styles.inputField} type="text" id="deviceCode" name="deviceCode" placeholder="Device code" value={deviceCode} onChange={(e) => setDeviceCode(e.currentTarget.value)} />
                        </div>
                    </div>
                    <div className={`${styles.inputDiv} ${styles.buttonDiv}`}>
                        <div>
                            <button className={styles.btn} onClick={() => onTryConnect()}>Connect</button>
                        </div>
                    </div>
                </> : <Spinner spinnerText={'Connecting with device...'} />}
        </>
    );
};

function StepOne({ deviceImg, setDeviceImg, deviceName, setProperty, property, setDeviceName, deviceType, setDeviceType, apartments }) {
    return (
        <div>
            <div className={styles.inputDiv}>
                <select className={styles.inputField} name="property" id="property" value={property ?? ""}
                    onChange={(e) => setProperty(e.currentTarget.value)}>
                    <option value="">Choose Apartment</option>
                    {
                        apartments.map(
                            apartment =>
                                <option key={apartment.id} value={apartment.id}>
                                    {apartment.name}, {apartment.address}
                                </option>
                        )
                    }
                </select>
            </div>
            <div className={styles.inputDiv}>
                <select className={styles.inputField} name="device_type" id="device_type" value={deviceType} onChange={(e) => setDeviceType(e.currentTarget.value)}>
                    <option value="">Device type</option>
                    {Object.keys(predefinedDeviceTypes).map(device => <option key={device} value={device}>{predefinedDeviceTypes[device]}</option>)}
                </select>
            </div>
            <div className={`${styles.nameDiv} ${styles.inputDiv}`}>
                <div>
                    <input className={styles.inputField} type="text" maxLength={20} id="name" name="name" placeholder="Name" value={deviceName} onChange={(e) => setDeviceName(e.currentTarget.value)}></input>
                </div>
            </div>
            <ImageUploadField image={deviceImg} setImage={setDeviceImg} />
        </div>
    );
}

function StepTwo({ deviceType, deviceOption, setDeviceOption, powerSupply, setPowerSupply, powerUsage, setPowerUsage, minTemp, maxTemp, setMinTemp, setMaxTemp }) {
    return (
        <div>
            <div className={styles.inputDiv}>
                <select className={styles.inputField} name="deviceOption" id="deviceOption" value={deviceOption} onChange={(e) => setDeviceOption(e.currentTarget.value)}>
                    <option value="">Device option</option>
                    {Object.keys(predefinedDevices[deviceType]).map(op => <option key={op} value={op}>{predefinedDevices[deviceType][op]}</option>)}
                </select>
            </div>
            {
                deviceType != "LPD" &&
                <div className={styles.inputDiv}>
                    <select className={styles.inputField} name="powerSupply" id="powerSupply" value={powerSupply} onChange={(e) => setPowerSupply(e.currentTarget.value)}>
                        <option value="">Power option</option>
                        {Object.keys(predefinedPowerSupplys).map(supply => <option key={supply} value={supply}>{predefinedPowerSupplys[supply]}</option>)}
                    </select>
                </div>
            }
            {
                (deviceOption != 'BATTERY' && deviceOption != 'SOLAR_SYSTEM' && deviceOption != 'CHARGER') &&
                <div className={styles.inputDiv}>
                    <div>
                        <input className={styles.inputField} min={0} type="number" max={4} step={1} id="powerUsage" name="powerUsage" placeholder="Power usage in kW" value={powerUsage == 0 ? "Power usage in kW" : powerUsage} onChange={(e) => { setPowerUsage(parseInt(Math.max(0, e.currentTarget.value))); }}></input>
                    </div>
                </div>
            }
            {
                deviceOption == 'AIRCONDITIONING' &&
                <div className={styles.inputDiv}>
                    <div>
                        <input className={styles.inputField} min={0} type="number" maxLength={2} step={1} id="minTemp" name="minTemp" placeholder="Min air condition temp" value={minTemp === 0 ? "Min air condition temp" : minTemp} onChange={(e) => setMinTemp(parseInt(Math.min(40, Math.max(0, e.currentTarget.value))))} />
                    </div>
                    <div>
                        <input className={styles.inputField} min={0} type="number" maxLength={2} step={1} id="maxTemp" name="maxTemp" placeholder="Max air condition temp" value={maxTemp === 0 ? "Max air condition temp" : maxTemp} onChange={(e) => setMaxTemp(parseInt(Math.min(40, Math.max(0, e.currentTarget.value))))} />
                    </div>
                </div>
            }
        </div>
    )
}

function BasicEndStep() {
    return (
        <div>
            <div className={styles.inputDiv}>
                <p className={styles.endText}>All set up!</p>
            </div>
        </div>
    );
}

function SolarPanelsStep({ panelSize, setPanelSize, panelEfficiency, setPanelEfficiency, numberOfPanels, setNumberOfPanels }) {
    return (
        <div>
            <div className={styles.inputDiv}>
                <div>
                    <input className={styles.inputField} min={0} type="number" maxLength={3} id="numberOfPanels" name="numberOfPanels" placeholder="Number of Panels" value={numberOfPanels == 0 ? "Number of Panels" : numberOfPanels} onChange={(e) => setNumberOfPanels(Math.max(0, e.currentTarget.value))}></input>
                </div>
            </div>
            <div className={styles.inputDiv}>
                <div>
                    <input className={styles.inputField} min={0} type="number" id="panelSize" maxLength={3} name="panelSize" placeholder="Panel Size in m²" value={panelSize == 0 ? "Panel Size in m²" : panelSize} onChange={(e) => setPanelSize(Math.max(0, e.currentTarget.value))}></input>
                </div>
            </div>
            <div className={styles.inputDiv}>
                <div>
                    <input className={styles.inputField} min={0} max={100} type="number" maxLength={3} id="panelEfficiency" name="panelEfficiency" placeholder="Panel Efficiency in %" value={panelEfficiency == 0 ? "Panel Efficiency in %" : panelEfficiency} onChange={(e) => setPanelEfficiency(Math.min(Math.max(0, e.currentTarget.value), 100))}></input>
                </div>
            </div>
        </div>
    );
}

function BatteryStep({ batteryCapacity, setBatteryCapacity }) {
    return (
        <div>
            <div className={styles.inputDiv}>
                <div>
                    <input className={styles.inputField} min={0} type="number" maxLength={5} id="capacity" name="capacity" placeholder="Capacity" value={batteryCapacity == 0 ? "Capacity" : batteryCapacity} onChange={(e) => setBatteryCapacity(Math.max(0, e.currentTarget.value))}></input>
                </div>
            </div>
        </div>
    );
}

function ChargerStep({ chargerPower, setChargerPower, numberOfConnections, setNumberOfConnections }) {
    return (
        <div>
            <div className={styles.inputDiv}>
                <div>
                    <input className={styles.inputField} min={0} type="number" maxLength={5} id="chargerPower" name="chargerPower" placeholder="Charger Power" value={chargerPower == 0 ? "Charger Power" : chargerPower} onChange={(e) => setChargerPower(Math.max(0, e.currentTarget.value))}></input>
                </div>
            </div>
            <div className={styles.inputDiv}>
                <div>
                    <input className={styles.inputField} min={0} type="number" maxLength={5} id="numberOfConnections" name="numberOfConnections" placeholder="Number Of Connections" value={numberOfConnections == 0 ? "Number Of Connections" : numberOfConnections} onChange={(e) => setNumberOfConnections(Math.max(0, e.currentTarget.value))}></input>
                </div>
            </div>
        </div>
    );
}

function ForthStep({ router }) {
    return <div>
        <div className={styles.inputDiv}>
            <p className={styles.endText}>Device created Succesfully!</p>
        </div>
        <div className={styles.cen}>
            <button className={styles.btn} onClick={() => router.replace('/')}>Jump to Home</button>
        </div>
    </div>
}
