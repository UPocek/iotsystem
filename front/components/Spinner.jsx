import styles from '../styles/Spinner.module.css'
export default function Spinner({ spinnerText }) {
    return (<div className={styles.precessing}>
        <div className={styles.spinner}>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
        <p>{spinnerText}</p>
    </div>
    );
}