/* eslint-disable @next/next/no-img-element */
import { baseUrl } from "@/pages/_app";
import styles from "@/styles/ApartmentCard.module.css"
import { useRouter } from "next/router";
import { useState } from "react";
import ShareDialog from "./ShareDialog";
import RemoveAccessDialog from "./RemoveAccessDialog";

export default function ApartmentCard({ apartment, url, addDevice = true, shareable = true }) {
    const router = useRouter();

    const [showShareDialog, setShowShareDialog] = useState(false);
    const [removeAccessFor, setRemoveAccessFor] = useState(null);
    const [haveAccessList, setHaveAccessList] = useState(() => {
        let usersWithAccess = [apartment['owner']['username']];
        if (apartment['usersWithAccess'] != null && apartment['usersWithAccess'].length > 0) {
            const temp = apartment['usersWithAccess'].map(item => item['username']);
            usersWithAccess = usersWithAccess.concat(temp);
        }
        return usersWithAccess
    });

    function convertScreamSnakeToPrettyWords(screamSnake) {
        // Replace underscores with spaces
        let words = screamSnake.replace(/_/g, ' ');

        // Convert to lowercase and then capitalize each word
        words = words.toLowerCase().replace(/\b\w/g, (char) => char.toUpperCase());

        return words;
    }

    function getStatusLabel() {
        return <div className={styles.label}>
            {convertScreamSnakeToPrettyWords(apartment['status'])}
        </div>;
    }

    return (
        <>
            <div className={styles.cardOuterContainer}>
                <div onClick={() => router.push(url)} className={styles.card}>
                    {shareable && <div onClick={(e) => { e.stopPropagation(); setShowShareDialog(true) }} className={styles.sharebtn}>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" id="share"><path d="m23.665 8.253-9-8A.998.998 0 0 0 13 1v3.207C9.996 5.013 0 8.765 0 23a1 1 0 0 0 1.928.371c2.965-7.413 8.745-8.96 11.071-9.283V17a1 1 0 0 0 1.666.747l9-8a1 1 0 0 0 0-1.494z"></path></svg>
                    </div>}
                    <div>
                        <img className={styles.cardImage} src={`${baseUrl}/apartment/${apartment.id}.jpg`}
                            alt={`Apartmant ${apartment.name}`} />
                    </div>
                    <div className={styles.text}>
                        <p className={styles.title}>{
                            apartment.name
                        }</p>
                        <p className={styles.subTitle}>{
                            `${apartment.address}, ${apartment.city.city}, ${apartment.city.country}`
                        }</p>
                    </div>
                    {
                        (addDevice && shareable) && <div onClick={
                            (e) => {
                                router.push(`/devices/add?property=${apartment.id}`);
                                e.stopPropagation();
                            }
                        } className={styles.btn}>
                            <p>Add device</p>
                        </div>
                    }
                    {
                        getStatusLabel()
                    }
                </div>
                {shareable && <div className={styles.accessBubles}>
                    {haveAccessList.map((userUsername, index) =>
                        <div onClick={(e) => { e.stopPropagation(); setRemoveAccessFor(userUsername); }} key={userUsername} style={{ zIndex: `${10 - index}` }} className={`${styles.bubble} ${styles.tooltip}`}>
                            <img src={`${baseUrl}/profile/${userUsername}.jpg`} alt={userUsername}></img>
                            <span className={styles.tooltiptext}>{userUsername}</span>
                            <div className={styles.overlay}>
                                <div className={styles.removeIcon}>X</div>
                            </div>
                        </div>)}
                </div>}

                {!shareable && <div className={styles.accessBubles}>
                    {haveAccessList.map((userUsername, index) =>
                        <div key={userUsername} style={{ zIndex: `${10 - index}` }} className={`${styles.bubble} ${styles.tooltip}`}>
                            <img src={`${baseUrl}/profile/${userUsername}.jpg`} alt={userUsername}></img>
                            <span className={styles.tooltiptext}>{userUsername}</span>
                        </div>)}
                </div>}
            </div>
            {removeAccessFor != null && <RemoveAccessDialog itemId={apartment.id} itemName={apartment.name} type={'apartment'} personUsername={removeAccessFor} setShowDialog={setRemoveAccessFor} setHaveAccessList={setHaveAccessList} />}
            {showShareDialog && <ShareDialog itemIdToShare={apartment.id} apartmentId={apartment.id} shareType={"apartment"} setShowShareDialog={setShowShareDialog} setHaveAccessList={setHaveAccessList} />}
        </>
    )

}