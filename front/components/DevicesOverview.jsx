import { useMemo, useState } from 'react';
import { getUserId, getUserUsername } from "@/helper/helper";
import { baseUrl, wsUrl } from "@/pages/_app";
import styles from "@/styles/DevicesOverview.module.css"
import axios from "axios";
import { useEffect } from 'react';
import ItemCard from './ItemCard';

export default function DevicesOverview({ apartmentId }) {
    const [airConditioningList, setAirConditioningList] = useState([]);
    const [ambientConditionsList, setAmbientConditionsList] = useState([]);
    const [batteryList, setBatteryList] = useState([]);
    const [chargerList, setChargerList] = useState([]);
    const [lampList, setLampList] = useState([]);
    const [solarSystemList, setSolarSystemList] = useState([]);
    const [sprinklerSystemList, setSprinklerSystemList] = useState([]);
    const [vehicleGateList, setVehicleGateList] = useState([]);
    const [washingMachineList, setWashingMachineList] = useState([]);

    const [owner, setOwner] = useState(false);
    const isOwner = useMemo(() => owner.id == getUserId(), [owner]);

    useEffect(() => {
        Promise.all([
            axios.post(`${baseUrl}/api/device/subscribeStatus/${apartmentId}`),
            axios.get(`${baseUrl}/api/apartment/getOwner/${apartmentId}`)
        ]).then(([apartmentDevices, ownershipCheck]) => {
            setAirConditioningList(apartmentDevices.data['airConditioningList']);
            setAmbientConditionsList(apartmentDevices.data['ambientConditionsList']);
            setBatteryList(apartmentDevices.data['batteryList']);
            setChargerList(apartmentDevices.data['chargerList']);
            setLampList(apartmentDevices.data['lampList']);
            setSolarSystemList(apartmentDevices.data['solarSystemList']);
            setSprinklerSystemList(apartmentDevices.data['sprinklerSystemList']);
            setVehicleGateList(apartmentDevices.data['vehicleGateList']);
            setWashingMachineList(apartmentDevices.data['washingMachineList']);
            setOwner(ownershipCheck.data)
        })
            .catch(err => { console.log(err) });

        const authToken = getUserUsername();
        document.cookie = 'X-Authorization=' + authToken + '; path=/';
        let ws = new WebSocket(`${wsUrl}/ws/status`);

        ws.onmessage = (newStatus) => {
            newStatus = JSON.parse(newStatus.data);
            setAirConditioningList(newStatus['airConditioningList']);
            setAmbientConditionsList(newStatus['ambientConditionsList']);
            setBatteryList(newStatus['batteryList']);
            setChargerList(newStatus['chargerList']);
            setLampList(newStatus['lampList']);
            setSolarSystemList(newStatus['solarSystemList']);
            setSprinklerSystemList(newStatus['sprinklerSystemList']);
            setVehicleGateList(newStatus['vehicleGateList']);
            setWashingMachineList(newStatus['washingMachineList']);
        }

        return () => {
            if (ws.readyState === 1) {
                ws.close();
            }
        }

    }, [apartmentId]);

    return (
        <div className={styles.container}>
            {airConditioningList.length > 0 &&
                (<>
                    <h2>Air Conditioning</h2>
                    <div className={styles.devicesSection}>
                        {airConditioningList.map(item => <ItemCard key={item['id']} deviceId={item['id']} haveAccess={item['usersWithAccess']} ownerUsername={owner['username']} deviceImage={"air_conditioner.png"} deviceName={item['name']} deviceStatus={item['online']} deviceType={"airConditioning"} setItems={setAirConditioningList} apartmentId={apartmentId} shareable={isOwner} />)}
                    </div>
                </>)

            }
            {ambientConditionsList.length > 0 &&
                (<>
                    <h2>Ambient Conditioning Sensors</h2>
                    <div className={styles.devicesSection}>
                        {ambientConditionsList.map(item => <ItemCard key={item['id']} deviceId={item['id']} haveAccess={item['usersWithAccess']} ownerUsername={owner['username']} deviceImage={"ambient_conditions_sensor.png"} deviceName={item['name']} deviceStatus={item['online']} deviceType={"ambientConditions"} setItems={setAmbientConditionsList} apartmentId={apartmentId} shareable={isOwner} />)}
                    </div>
                </>
                )
            }
            {owner &&
                (<>
                    <h2>Battery system</h2>
                    <div className={styles.devicesSection}>
                        <ItemCard key={'battery_system'} deviceImage={"battery.png"} deviceName={"Battery system"} deviceStatus={true} deviceId={batteryList.length > 0 ? batteryList[0]['id'] : -1} apartmentId={apartmentId} deviceType={"battery"} setItems={setBatteryList} hasOnOff={false} shareable={isOwner} ownerUsername={owner['username']} />
                    </div>
                </>)
            }

            {chargerList.length > 0 &&
                (<>
                    <h2>Home Chargers</h2>
                    <div className={styles.devicesSection}>
                        {chargerList.map(item => <ItemCard key={item['id']} deviceId={item['id']} haveAccess={item['usersWithAccess']} ownerUsername={owner['username']} deviceImage={"charger.png"} deviceName={item['name']} deviceStatus={item['online']} deviceType={"charger"} setItems={setChargerList} apartmentId={apartmentId} shareable={isOwner} />)}
                    </div>
                </>
                )
            }
            {lampList.length > 0 &&
                (<>
                    <h2>Lamps</h2>
                    <div className={styles.devicesSection}>
                        {lampList.map(item => <ItemCard key={item['id']} deviceId={item['id']} haveAccess={item['usersWithAccess']} ownerUsername={owner['username']} deviceImage={"lamp.png"} deviceName={item['name']} deviceStatus={item['online']} deviceType={"lamp"} setItems={setLampList} apartmentId={apartmentId} shareable={isOwner}
                                                        hasAutoMode={true}
                                                        deviceAutoStatus={item['autoMode']}/>)}
                    </div>
                </>
                )
            }
            {solarSystemList.length > 0 &&
                (<>
                    <h2>Solar Systems</h2>
                    <div className={styles.devicesSection}>
                        {solarSystemList.map(item => <ItemCard key={item['id']} deviceId={item['id']} haveAccess={item['usersWithAccess']} ownerUsername={owner['username']} deviceImage={"solar_system.png"} deviceName={item['name']} deviceStatus={item['online']} deviceType={"solarSystem"} setItems={setSolarSystemList} apartmentId={apartmentId} shareable={isOwner} />)}
                    </div>
                </>
                )
            }
            {sprinklerSystemList.length > 0 &&
                (<>
                    <h2>Sparkler Systems</h2>
                    <div className={styles.devicesSection}>
                        {sprinklerSystemList.map(item => <ItemCard key={item['id']} deviceId={item['id']} haveAccess={item['usersWithAccess']} ownerUsername={owner['username']} deviceImage={"sprinkler.png"} deviceName={item['name']} deviceStatus={item['online']} deviceType={"sprinklerSystem"} setItems={setSolarSystemList} apartmentId={apartmentId} shareable={isOwner} />)}
                    </div>
                </>
                )
            }
            {vehicleGateList.length > 0 &&
                (<>
                    <h2>Gates</h2>
                    <div className={styles.devicesSection}>
                        {vehicleGateList.map(item => <ItemCard key={item['id']} deviceId={item['id']} haveAccess={item['usersWithAccess']} ownerUsername={owner['username']} deviceImage={"electric_gate.png"} deviceName={item['name']} deviceStatus={item['online']} deviceType={"vehicleGate"} setItems={setVehicleGateList} apartmentId={apartmentId} shareable={isOwner} />)}
                    </div>
                </>
                )
            }
            {washingMachineList.length > 0 &&
                (<>
                    <h2>Washing Machines</h2>
                    <div className={styles.devicesSection}>
                        {washingMachineList.map(item => <ItemCard key={item['id']} deviceId={item['id']} haveAccess={item['usersWithAccess']} ownerUsername={owner['username']} deviceImage={"washing_machine.png"} deviceName={item['name']} deviceStatus={item['online']} deviceType={"washingMachine"} setItems={setWashingMachineList} apartmentId={apartmentId} shareable={isOwner} />)}
                    </div>
                </>
                )
            }
        </div>
    );
}